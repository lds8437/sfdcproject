({
     createAddEditList : function(component,event,helper) { 
        //console.log('CurrentQuoteAddEditHelper: createAddEditList');
        
        var experienceMetaData = component.get('v.experienceMetaData');        
        var products = component.get('v.products');  
        
       // console.log('**QuoteAddEditHelper:',  JSON.parse(JSON.stringify(experienceMetaData)));
       // console.log('**products:',  products);
        var editList = [];
        var addList = [];
        var productsList = [];
        var products2;
        productsList = products.split(';');
       // console.log('**productsList:',  productsList);
        productsList = productsList.forEach(function(p){
            if(p.startsWith('360') && p !== '360Academic'){
                p = 'X360';
            }
            else if(p === '360Academic'){
                p = 'A360Academic';
            }
            if(products2===undefined){
                products2 = p;
            }else{
                products2 += ';'+p;
            }
        });
       
        _.forEach(experienceMetaData, function(meta){
            var code = meta.Code_for_Edits__c;         
            if(code === 'RSRV'){
                code = 'RS1';
            }
            var indexOfProd = products2.search(code);
            if(indexOfProd > -1 ){                 
                editList.push({'label' : meta.MasterLabel,
                               'value' : meta.Code__c} );                  
            }else{                   
                addList.push({'label' : meta.MasterLabel,
                              'value' : meta.Code__c} );
            }            
        });
        component.set('v.editList',editList);
        component.set('v.addList',addList);       
    },
    
    closeModal: function(component, event, helper) {
        //console.log('CurrentQuoteAddEditHelper: closeModal');
        component.set("v.isOpen", false);
    }, 
    
    updateQuoteList : function(component,event,helper){
        //console.log('CurrentQuoteAddEditHelper: updateQuoteList');
		var appEvent = component.getEvent("updateQuoteList");
		appEvent.fire();
    }
})