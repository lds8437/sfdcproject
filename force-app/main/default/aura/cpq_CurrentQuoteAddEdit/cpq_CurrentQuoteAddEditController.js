({
  
    getLists: function(component,event,helper){
       // console.log('CCAE CTRL: getLists');        
         if(component.get('v.quote').discountApproved === true){
            component.set('v.discountApproved',true);
        }else{
            component.set('v.discountApproved',false);
        }
         helper.createAddEditList(component, event, helper);
    },
    
    openModal: function(component, event, helper) {   
        //console.log('CurrentQuoteAddEditController : openModal');
        component.set("v.isOpen", true);
    },
    
    closeModal: function(component, event, helper) {
        //console.log('CurrentQuoteAddEditController : closeModal');
        helper.closeModal(component, event, helper);
    },  
    
    openDiscountModal: function(component, event, helper) {   
        //console.log('CurrentQuoteAddEditController : openModal');
        component.set("v.discountIsOpen", true);
    },
    
    closeDiscountModal: function(component, event, helper) {
        //console.log('CurrentQuoteAddEditController : closeModal');
        component.set("v.discountIsOpen", false);
    },  
    
    
    onEdit : function(component, event, helper){
       // console.log('CurrentQuoteAddEditController: onEdit');
        var selectedMenuItemValue = event.getParam("value");
        var appEvent = component.getEvent("editExperience");
        appEvent.setParams({
            quoteTerm : component.get('v.quote').License_Term_in_months__c,
            quoteStartDate : component.get('v.quote').License_Start_Date__c,
            quoteEndDate : component.get('v.quote').License_End_Date__c,
            quoteId : component.get('v.quoteId'),
            experience : selectedMenuItemValue,
            selectedPriceList : component.get('v.selectedPriceList'),
            quoteName : component.get('v.quoteName'),
            xmProducts : component.get('v.xmProducts'),
            quoteType : component.get('v.quote').Quote_Type__c,
            discountApproved: component.get('v.quote').discountApproved,
            relatedClients : component.get('v.relatedClients')
        });
        appEvent.fire();
    },
    
    onAdd : function(component, event, helper){
      //  console.log('CurrentQuoteAddEditController: onAdd');
        var selectedMenuItemValue = event.getParam("value");
        
        var appEvent = component.getEvent("addExperience");
        appEvent.setParams({
            quoteTerm : component.get('v.quote').License_Term_in_months__c,
            quoteStartDate : component.get('v.quote').License_Start_Date__c,
            quoteEndDate : component.get('v.quote').License_End_Date__c,
            quoteId : component.get('v.quoteId'),
            experience : selectedMenuItemValue,
            selectedPriceList : component.get('v.selectedPriceList'),
            quoteName : component.get('v.quoteName'),
            xmProducts : component.get('v.xmProducts')
        });
        appEvent.fire();      
    },
    
    onDelete : function(component, event, helper){
        //console.log('CurrentQuoteAddEditController : onDelete');
        var Quote = component.get('v.quote');
        var newItem = {};
        newItem = JSON.parse(JSON.stringify(Quote));
        newItem.Id = component.get('v.quoteId');
        helper.callServer( 
            component,
            "c.deleteRecord",
            function(response) {               
                var recordId = response.Id;
                helper.closeModal(component, event, helper);   
                helper.updateQuoteList(component,event,helper);
                location.reload();
            },
            {
                record : newItem
            }
        );
    },
    
    showDetails : function(component, event, helper) {
      //  console.log('CurrentQuoteAddEditController : showDetails');
        component.set('v.showDetails', true);
        var quoteId = component.get('v.quoteId');
        helper.callServer(
            component,
            "c.getQuoteLines",
            function(response) {
                _.forEach(response, function(r) {
                    if(r.Service__c != undefined) {
                        if(r.Service__r.Qualtrics_Invoicing__c === false && r.Service__r.Custom__c === false && r.Service__r.Q_Partner__c != undefined) {
                            r.TotalPrice = r.Total_Partner_Amount__c;
                        }
                    }
                });
                component.set('v.quoteLineItems', response);
            },
            {
                quoteId : quoteId
            }
        );
    },
    
    closeButton : function(component, event, helper) {
        //console.log('CurrentQuoteAddEditController : closeButton');
        component.set('v.showDetails', false);
    },
    
    escEvent : function(component, event, helper) {
        //console.log('CurrentQuoteAddEditController : escEvent');
        if(event.key = 'Escape') {
            component.set('v.showDetails', false);
        }
    },
    
})