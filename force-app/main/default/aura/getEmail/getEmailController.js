({
	doInit : function(component, event, helper) {
        var getCall = component.get("c.getEmailFromEmailHunter");
        getCall.setParams({
            "id": component.get("v.recordId")
        });
        getCall.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set("v.emailHunterResponse", response.getReturnValue());
            } else if (state === "ERROR") {
                console.log("Error validating email address");
            } else {
                console.log("Unknown problem validating email address");
            }
            component.set("v.isProcessing", false);
        })
        $A.enqueueAction(getCall);
	},
    changeEmailAddress: function(component, event, helper) {
		component.set("v.isProcessing", true);
        var updateCall = component.get("c.updateEmailAddress");
        var emailHunterResponse = component.get("v.emailHunterResponse");
        updateCall.setParams({
            "id": component.get("v.recordId"),
            "email": emailHunterResponse.data.email
        });
        updateCall.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Record Updated",
                    "message": "The email address has been updated successfully."
                });
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            } else if (state === "ERROR") {
                console.log("Error validating email address");
            } else {
                console.log("Unknown problem validating email address");
            }
            component.set("v.isProcessing", false);
        })
        $A.enqueueAction(updateCall);
    }
})