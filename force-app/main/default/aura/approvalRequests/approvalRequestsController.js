({
    doInit : function(cmp, event, helper) 
    {
        console.log('doinit');
         helper.getTotal(cmp);
        cmp.set('v.columns', [
            {label: 'Submitted Date', fieldName: 'CreatedDate', type: 'date'},
            {label: 'Record Name', fieldName: 'ObjectId', type: 'url', typeAttributes: {label: { fieldName: 'ObjectName'}}},
            {label: 'Approval Process', fieldName: 'ApprovalProcess', type: 'text'},
            {label: 'Status', fieldName: 'Status', type: 'text'},
            {label: 'Completed Date', fieldName: 'CompletedDate', type: 'date'}
            ]);
        helper.getData(cmp);
     
            },
    
    loadMoreData: function (cmp, event, helper) {
        console.log('loadmoredata');
        event.getSource().set("v.isLoading", true);
        cmp.set('v.loadMoreStatus', 'Loading....');
        helper.getMoreApprovals(cmp, cmp.get('v.rowsToLoad')).then($A.getCallback(function (data) {
            console.log(cmp.get('v.data').length);
            console.log(cmp.get('v.totalNumberOfRows'));
            
            if (cmp.get('v.data').length == cmp.get('v.totalNumberOfRows')) {
                console.log('thisworks');
                cmp.set('v.enableInfiniteLoading', false);
                cmp.set('v.loadMoreStatus', 'No more data to load');
            } else {
                var currentData = cmp.get('v.data');
                var newData = currentData.concat(data);
                cmp.set('v.data', newData);
                cmp.set('v.loadMoreStatus', 'Please scroll down to load more data');
            }
            event.getSource().set("v.isLoading", false);
        }));
    }
 
})