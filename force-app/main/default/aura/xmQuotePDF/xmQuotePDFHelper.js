({
	redirectUser : function(component) {
        var redirectEvent = $A.get("e.force:navigateToURL");
        var recordId = component.get('v.recordId');
        redirectEvent.setParams({
            "url": "/apex/cdr_GenerateXMQuote?id=" + recordId
        });
        redirectEvent.fire();
        $A.get("e.force:closeQuickAction").fire();
	}
})