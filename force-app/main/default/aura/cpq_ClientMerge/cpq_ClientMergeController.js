({
    showClientMerge : function(component, event, helper) {
       // console.log('showing client merge dialog');
        var params = event.getParam('arguments');
        if (params) {
            var clients = params.clients;
            component.set("v.clientoptions", clients);
            component.set("v.showmodal", true);
            //component.find("client-merge-modal-content").getElement().focus();
        }
    },
    selectClient : function(component, event, helper) {
        var clientid = component.get("v.selectedclient");
        if(clientid != ''){
            var selectEvent = component.getEvent("clientMergeSelect");
            selectEvent.setParams({"clientid" : clientid });
            selectEvent.fire();
            component.set('v.showmodal', false);
        }
        else{
            //Need to add error message here
        }
    },
    newClient : function(component, event, helper) {
        //console.log('client merge recieved new client event');
        var interfacecontext = event.getParam("interfacecontext");
        if(interfacecontext == 'cpqmerge'){
            var clientid = event.getParam("clientid");
          //  console.log('cpq_ClientMerge received new client created with id ' + clientid);
            if(clientid != ''){
                var selectEvent = component.getEvent("clientMergeSelect");
                selectEvent.setParams({"clientid" : clientid });
                selectEvent.fire();
            }
        }
    },
    closeButton : function(component, event, helper) {
       // console.log('ClientMerge Controller: closeButton');
        component.set("v.showmodal",false);
        component.set("v.selectedclient", "");
        //helper.closeModal(component);      
    },
    escEvent : function(component, event, helper) {
       // console.log('ClientMerge Controller: escEvent');
      //  console.log('keypress: ' + event.which);
      //  console.log('key: ' + event.key);
      //  console.log('code: ' + event.code);
      //  console.log('locatoin: ' + event.location);
        if(event.key == 'Escape') {
            //helper.closeModal(component);
            component.set("v.showmodal",false);
            component.set("v.selectedclient", "");
        }
    }
})