({
init: function (component, event, helper) {
        component.set('v.columns', [
            {label: 'Related Retainer', fieldName: 'name', type: 'text'},
            {label: 'Cur', fieldName: 'CurrencyIsoCode', type: 'text', initialWidth: 65},
            {label: 'Amt Against Retainer', fieldName: 'Value_against_Retainer__c', type: 'currency' ,editable: true, initialWidth:200},
            {label: 'Amt Remaining', fieldName: 'remainingValue', type: 'currency', initialWidth:150}
            
        ]);
        helper.fetchData(component,event, helper);
    },
    handleSaveEdition: function (component, event, helper) {
        var draftValues = event.getParam('draftValues');
        var data = component.get('v.data');
        var error = false;
        data.forEach(function(d){
            draftValues.forEach(function(dv){
                if(d.Id === dv.Id && dv.Value_against_Retainer__c > d.Value_against_Retainer__c + d.remainingValue){
                  error = true
                    var header = "Error" 
                  var message = 'The value against retainer for '+ d.name +' exceeds the remaing retainer amount'
                  helper.processErrors(component,header,message);
                }
            })
           
        })
        if(error === false){
            var action = component.get("c.updateLinks");
        action.setParams({"rl" : draftValues});
        action.setCallback(this, function(response) {
            var state = response.getState();
           helper.reloadFab(component,event,helper);  
        });
        $A.enqueueAction(action);
        }
        
    },
    
    onErrorClose: function(component,event,helper){
       
        component.set('v.isError',false)
    }
    
     
    
})