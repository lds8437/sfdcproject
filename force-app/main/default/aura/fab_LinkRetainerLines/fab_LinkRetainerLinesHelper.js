({
    
    fetchData: function (component) {
        // simulates a call to the server, similar to an apex controller.
        
        
        var opportunityId = component.get('v.opportunity.Id');
        var action = component.get('c.getLinkedRetainers');
        action.setParams({
            "opportunityId": opportunityId
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                console.log(dataObj)
                dataObj.forEach(function(rl){
                    rl.remainingValue = rl.Related_Retainer__r.Value_Remaining__c;
                    rl.name = rl.Related_Retainer__r.Name;
                })
                component.set('v.data', dataObj);
                var totalValue = 0;
                dataObj.forEach(function(lr){
                    totalValue += lr.Value_against_Retainer__c;
                })
                component.set('v.totalValue',totalValue)
               
            }  
            else if (state === 'ERROR') {
                var header = 'User Id Retrieval Error';
                //  helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action);
        
         
    },
    
   processErrors : function(component,header,message){       
        component.set('v.errorHeader',header);
        component.set('v.errorBody',message);
        component.set('v.isError',true);        
    },
    
    reloadFab :function(component,event,helper){
        //  console.log('RSC: reloadFab')
        var cmpEvent = $A.get("e.c:fab_Reload");
        cmpEvent.fire();
    },
});