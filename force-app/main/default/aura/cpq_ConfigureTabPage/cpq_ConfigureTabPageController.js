({
    doInit : function(component, event, helper) {
        //console.log('CT CTRL: doInit');      
        var experienceMetaData = component.get('v.experienceMetaData');       
        var selectedExperience = component.get('v.selectedExperience');       
        var quotingPermissions = component.get('v.quotingPermissions');       
        helper.setAvailableBundles(component, event, helper, quotingPermissions);          
        var currentExperienceMeta = _.filter(experienceMetaData,{'Code__c' : selectedExperience})       
        component.set('v.coreEntryType',currentExperienceMeta[0].Core_Entry_Type__c);       
        component.set('v.coreMinimum',currentExperienceMeta[0].Core_Minimum__c);       
        component.set('v.coreMaximum',currentExperienceMeta[0].Core_Maximum__c);       
        if(component.get('v.quoteType') === 'Upgrade License' && currentExperienceMeta[0].Core_Entry_Type__c === 'Enter') {
            component.set('v.enteredTier', component.get('v.selectedTier'));
        }
    },
        
    onActiveSectionSet : function(component,event){
        //console.log('CT CTRL: onActiveSelectionSet');
        component.find("accordion").set('v.activeSectionName', event.getParam('activeSection'));
    },
    
    onBundleChange : function(component, event, helper) {
        //console.log('---------------------CHANGE BUNDLE---------------------');
        //console.log('CT CTRL: onBundleChange');       
        component.set('v.allServiceProducts',[]);
        component.set('v.bundleChanged',true);
        var bundleId = component.find('bundleSelect').get('v.value');
        //console.log('CT CTRL: onBundleChange - CALL HLPR setSelectedBundle'); 
        helper.setSelectedBundle(component,event,helper,bundleId)
    },
    
    onTierChange: function(component,event,helper){
        //console.log('---------------------CHANGE TIER---------------------');
        //console.log('CT CTRL: onTierChange');      
        component.set('v.tierChanged',true);
        var tier = component.find('coreTierSelect').get('v.value');     
        component.set('v.coreQuantity', tier);      
        var tierNumber = Number(tier);
         var tierInTiers =  _.findIndex(component.get('v.coreTiers'),{'Maximum__c':tierNumber});               
        if(tierInTiers === -1){
            component.set('v.matchingTier',false);
        }else{
            component.set('v.matchingTier',true);
        }
        helper.onTierChangeEvent(component,event,helper,tier);
    },
    
    onTierChangeUnlimited: function(component,event,helper){      
        //console.log('CT CTRL: onTierChangeUnlimited');       
        component.set('v.selectedTier', '1');
        component.set('v.coreQuantity', '1');      
    },
    
    onEnterTierChange: function(component,event,helper){ 
         //console.log('CT CTRL: onEnterTierChange');       
        component.set('v.coreQuantity', component.get('v.enteredTier'));
        //console.log('CT CTRL: onEnterTierChange - SET selectedTier');
        component.set('v.selectedTier', component.get('v.enteredTier'));     
        helper.onTierChangeEvent(component,event,helper,component.get('v.enteredTier'));
    },
    
    onDateOrNameChange : function(component,event,helper){
       //console.log('CT CTRL: onDateOrNameChange');
        component.set('v.selectedPriceList',event.getParam('selectedPriceList'));  
        //console.log('CT CTRL: onDateOrNameChange - CALL HLPR onDateOrNameChange');
        helper.getPricing(component,event,helper);
    },
     
    onFeatureSelect : function(component,event,helper){
      // console.log('---------------------SELECT FEATURE---------------------');
        //console.log('CT CTRL: onFeatureSelect');
       // console.log(component.get('v.selectedServiceBundles'))
        var appEvent = $A.get("e.c:cpq_FeatureSelection");
        var target=event.getSource();      
        var feature = target.get('v.value');  
      //  console.log('feature');
      //  console.log(feature);
        var featureComparison = target.get('v.name');
     //    console.log('featureComparison');
     //   console.log(featureComparison);
        var techQlis = component.get('v.techQlis');  
     //   console.log('techQlis');
     //   console.log(techQlis);
        
        var availableTechProducts = component.get('v.availableTechProducts');
       // console.log('availableTechProducts');
      //  console.log(availableTechProducts);
        
        var bundleProduct = _.filter(availableTechProducts, {'Id' : feature});
    // console.log('bundleProduct');
      //  console.log(bundleProduct);
        
        var featureInQlis = _.findIndex(techQlis,{'Comparison': featureComparison});  
      //  console.log('featureInQlis');
      //  console.log(featureInQlis);
        if(featureInQlis === -1 && bundleProduct[0].Add_Multiple__c != true ){//|| bundleProduct[0].priceEditable != true)
            var featureRemove = _.findIndex(availableTechProducts,{'Comparison': featureComparison});
            var matchingFeatures = component.get('v.matchingFeatures');
            var matchingFeatureRemove = _.findIndex(matchingFeatures, {'Comparison' : featureComparison});  
            _.pullAt(availableTechProducts,featureRemove);
            _.pullAt(matchingFeatures, matchingFeatureRemove);
            component.set('v.availableTechProducts',availableTechProducts);
            component.set('v.matchingFeatures', matchingFeatures);
            appEvent.setParams({
                pk: featureComparison          
            });
           
            // console.log(component.get('v.selectedServiceBundles'))
            //console.log('CT CTRL: onFeatureSelect - FIRE appEvent cpq_FeatureSelection');
            appEvent.fire();
        } else /*if(featureInQlis && bundleProduct[0].Add_Multiple__c)*/{
            appEvent.setParams({
                pk: featureComparison
            });
           
         //   console.log('featureComparison2')
          //  console.log(featureComparison)
           // console.log('CT CTRL: onFeatureSelect - FIRE appEvent cpq_FeatureSelection');
            appEvent.fire();
        }
        
    },
    
    onAvUpdate: function(component,event,helper){
      //console.log('CT CTRL: onAvUpdate');
        var availableTechProducts = component.get('v.availableTechProducts');
        var matchingFeatures = component.get('v.matchingFeatures');
        var searchKey = component.get('v.searchKey');
        var singleRec = event.getParam('singleRec');
        var featureToAdd = _.filter(component.get('v.allBundleProducts'),{'Comparison': singleRec[0].Comparison});
        var featureInAvailable = _.findIndex(availableTechProducts, {'Comparison' : singleRec[0].Comparison});
        var featureInMatching = _.findIndex(matchingFeatures, {'Comparison' : singleRec[0].Comparison});
        if(featureToAdd[0].Related_Product__r.Name.toUpperCase().includes(searchKey.toUpperCase()) && featureInMatching === -1) {
            matchingFeatures.push(featureToAdd[0]);
            component.set('v.matchingFeatures', _.sortBy(matchingFeatures, 'ProductName'));
        }
        if(featureInAvailable === -1){
            availableTechProducts.push(featureToAdd[0]);
            component.set('v.availableTechProducts', _.sortBy(availableTechProducts,'ProductName'));
        }
    },
    
    onPriceListChange:function(component,event,helper){
     //console.log('CT CTRL: onPriceListChange');
        component.set('v.selectedPriceList',event.getParam('selectedPriceList'));
        //console.log('CT CTRL: onPriceListChange CALL HLPR getPricing');
         helper.getPricing(component,event,helper);
    },
    
    rulesAvailability: function(component,event,helper){
        //console.log('CT CTRL: rulesAvailability');
        var availability = event.getParam('availability');
        //console.log('CT CTRL: rulesAvailability - CALL HLPR updateAvailabilityFromRules');
        helper.updateAvailabilityFromRules(component,event,helper,availability);
    },
    
    toggleLicenseVisiblity : function(component, event, helper){
       //console.log('CT CTRL: toggleLicenseVisiblity');
        if(component.get("v.licensesExpanded")){
            component.set("v.licensesExpanded", false);
        }
        else{
            component.set("v.licensesExpanded", true);
        }
        //console.log('license visibility toggled. new value = ' + component.get("v.licensesExpanded"));
    },
    
    onMergeChange : function(component, event, helper) {
        //console.log('CT CTRL: onMergeChange');
        if(component.get('v.mergeTermsChanged') == true) {
            //console.log('CT CTRL: onMergeChange - CALL HLPR calculateMergeCredits');
            helper.calculateMergeCredits(component, event, helper);
        }
        //console.log(component.get('v.mergeTermsChanged'));
    },
    
    changeState : function(component, event, helper) {
        //console.log('CT CTRL: changeState');
        var group = event.target.id;
        var groupedProducts = component.get('v.groupedProducts2');
        group = _.findIndex(groupedProducts, {'Name' : group});
        groupedProducts[group].isexpanded = !groupedProducts[group].isexpanded;
        component.set('v.groupedProducts2', groupedProducts);
    },

    onSearch : function(component, event, helper) {
        //console.log('ConfigureTabPage Controller: onSearch');
        var searchKey = event.getSource().get('v.value');
        var availableTechProducts = component.get('v.availableTechProducts');
        var matchingFeatures = _.filter(availableTechProducts, function(product) {
            return product.Related_Product__r.Name.toUpperCase().includes(searchKey.toUpperCase());
        });
        if(searchKey != '') {
            component.set('v.matchingFeatures', matchingFeatures);
            component.set('v.groupedProducts2', _.forEach(component.get('v.groupedProducts2'), function(group) {
                group.isexpanded = true;
            }));
        }
        else {
            component.set('v.groupedProducts2', _.forEach(component.get('v.groupedProducts2'), function(group) {
                group.isexpanded = false;
            }));
        }
    },

})