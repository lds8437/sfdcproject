({
    search : function(component, event, helper) {
        
        var action = event.getParam('arguments').serverAction;
        
        action.setParams({
            "searchTerm" : component.get('v.searchTerm'),
            "selectedIds" : helper.getSelectedIds(component),
            "accountId" : component.get('v.accountId')
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                const returnValue = response.getReturnValue();
                component.set('v.searchResults', returnValue);
                
            } 
            else if (state === 'ERROR') {
                helper.processErrors(component,response);
            }
        }));
        action.setStorable();
        $A.enqueueAction(action);
    },
    
    onInput : function(component, event, helper) {
        // Prevent action if selection is not allowed
        if (!helper.isSelectionAllowed(component)) {
            return;
        }
        const newSearchTerm = event.target.value;
        helper.updateSearchTerm(component, newSearchTerm);
    },
    
    onResultClick : function(component, event, helper) {
        const recordId = event.currentTarget.id;
        helper.selectResult(component, recordId);
    },
    
    onComboboxClick : function(component, event, helper) {
        // Hide combobox immediatly
        const blurTimeout = component.get('v.blurTimeout');
        if (blurTimeout) {
            clearTimeout(blurTimeout);
        }
        component.set('v.hasFocus', false);
    },
    
    onFocus : function(component, event, helper) {
        // Prevent action if selection is not allowed
        if (!helper.isSelectionAllowed(component)) {
            return;
        }
        component.set('v.hasFocus', true);
    },
    
    onRemoveSelectedItem : function(component, event, helper) {
        const itemId = event.getSource().get('v.name');
        helper.removeSelectedItem(component, itemId);
    },
    
    onClearSelection : function(component, event, helper) {
        helper.clearSelection(component);
    },
    
    onBlur : function(component, event, helper) {
        // Prevent action if selection is not allowed
        if (!helper.isSelectionAllowed(component)) {
            return;
        }        
        // Delay hiding combobox so that we can capture selected result
        const blurTimeout = window.setTimeout(
            $A.getCallback(function (response)  {
                component.set('v.hasFocus', false);
                component.set('v.blurTimeout', null);
            }),
                300
                );
                component.set('v.blurTimeout', blurTimeout);
            },
            })