({
	calculateCurrency : function(component) {
        // 1. Calculate monthly payment
        var usdamount = component.get("v.usdamount");
        var currency = component.get("v.currency");
		var convertamount = usdamount * currency;
        component.set("v.convertamount", convertamount);
        
        // 2. Fire event with new mortgage data
		var event = $A.get("e.c:CalculatorChange");
        event.setParams({"usdamount": usdamount, 
                         "currency": currency, 
                         });
        event.fire();
	}
})