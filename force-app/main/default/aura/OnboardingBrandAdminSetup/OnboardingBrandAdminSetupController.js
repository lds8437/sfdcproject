({
    debug: function(cmp,event,helper){
        //console.log('relatedcontacts' + JSON.stringify(cmp.get("v.RelatedContacts")));  
        //cmp.find("contactRecordLoader").reloadRecord();
        //console.log('opportunityid ' + cmp.get('v.OpportunityId'));
        //console.log('accountid ' + cmp.get('v.accountId'));
        //console.log('newbrand info' + JSON.stringify(cmp.get('v.newbrand')));
        console.log('adminids' + JSON.stringify(cmp.get('v.AdminIds')));
    },
    doInit: function (cmp, event, helper) {
        
        var result = decodeURIComponent((new RegExp('[?|&]' + "Id" + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
        cmp.set("v.OpportunityId", result);
        helper.showSpinner(cmp, false);
        //helper.getClientList (cmp);
        cmp.find("contactRecordLoader").reloadRecord();
        cmp.find("oppRecordLoader").reloadRecord();
        cmp.find("brandRecordLoader").reloadRecord();
        cmp.set('v.resetForm', true);
    },
    brandChange: function(cmp, event, helper) {
        cmp.find("brandRecordLoader").reloadRecord();
    },
    saveAdmins: function(cmp, event, helper) {
        let allValid = true;
        if (cmp.find("existing-admin-field")) {
            allValid = cmp.find("existing-admin-field").reduce(function(validSoFar, field) {
                field.reportValidity();
                return validSoFar && field.checkValidity();
            }, true);
        } else {
            console.log("Here");
            console.log("BRAND ERROR?", cmp.get("v.brandRecordError"));
            cmp.set("v.simpleBrandRecord.Existing_Brand_Admin_Username__c", null);
            cmp.set("v.simpleBrandRecord.Existing_Brand_Admin_Brand_ID__c", null);
        }
        if (allValid) {
            console.log("ALL VALID!");
            //Show spinner
            cmp.set('v.Spinner', true);
            //Concatenate admin ids and assign to brand object.
            var adminidstring = '';
            var adminids = cmp.get('v.AdminIds');
            if(adminids !== undefined){
                for(var i=0; i < adminids.length; i++){
                    if(!!adminids[i]){
                        adminidstring += adminids[i] + ',';
                    }
                }
                adminidstring = adminidstring.substring(0, adminidstring.length - 1);//Chop off the final comma
            }
            var brand = cmp.get('v.simpleBrandRecord');
            brand.Brand_Admin_Ids__c = adminidstring;
            //Concatenate admin info and assign to brand object.
            var admininfostring = '';
            var admincomponents = cmp.find('singleadmin');
            if(admincomponents !== undefined){
                if(admincomponents.length === undefined && !!admincomponents){
                    admininfostring = admincomponents.returnAdminInfo(); 
                }
                else{
                    for(var i = 0;i < admincomponents.length;i++){
                        admininfostring += admincomponents[i].returnAdminInfo();
                        
                    }
                }
                admininfostring = admininfostring.substring(0, admininfostring.length - 2);//Chop off the final semicolon and space
            }
            brand.Brand_Admins__c = admininfostring;
            cmp.set('v.simpleBrandRecord', brand);
            //Save brand record via Lightning Data Service.
            cmp.find("brandRecordLoader").saveRecord($A.getCallback(function(saveResult) {
                cmp.set('v.Spinner', false);//Hide spinner
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    console.log('brand save result ' + saveResult.state);
                    //Fire application event with ID of updated brand.
                    var appEvent = $A.get("e.c:OnboardingBrandUpdatedEvent");
                    appEvent.setParams({
                        "brandId" : cmp.get('v.selectedBrandId') });
                    appEvent.fire();
                    //Show success toast for user
                    if(cmp.get('v.isLightningOut') === false){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "type" : "success",
                            "message": "Brand Admins updated."
                        });
                        toastEvent.fire();
                    }
                    var cmpEventContainer = $A.get("e.c:OnboardingBrandContainerEvent");
                    cmpEventContainer.fire();
                    helper.triggerUnsavedChanges(cmp, false);
                    cmp.find("brandRecordLoader").reloadRecord();                
                } else if (saveResult.state === "INCOMPLETE") {
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    console.log('Problem saving record, error: ' + JSON.stringify(saveResult.error));
                } else {
                    console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                }
            }));
        }
    },
    handleNewContactLoad : function(cmp, event, helper){
        cmp.set("v.showFormSpinner", false);
    },
    handleNewContactSubmit : function(cmp, event, helper){
        event.preventDefault();
        //Handle field validation for submitted new contact form.
        var firstname = cmp.find('newcontactfirstname');
        var lastname = cmp.find('newcontactlastname');
        var email = cmp.find('newcontactemail');
        var username = cmp.find('newcontactusername');
        var country = cmp.find('newcontactcountry');
        var level = cmp.find('newcontactlevel');
        var valid = true;
        if(!firstname.get("v.value")){
            $A.util.addClass(firstname, 'slds-has-error'); 
            valid = false;
        }
        if(!lastname.get("v.value")){
            $A.util.addClass(lastname, 'slds-has-error'); 
            valid = false;
        }
        if(!email.get("v.value")){
            $A.util.addClass(email, 'slds-has-error'); 
            valid = false;
        }
        if(!username.get("v.value")){
            $A.util.addClass(username, 'slds-has-error'); 
            valid = false;
        }
        if(!country.get("v.value")){
            $A.util.addClass(country, 'slds-has-error'); 
            valid = false;
        }
        if(!level.get("v.value")){
            $A.util.addClass(level, 'slds-has-error'); 
            valid = false;
        }
        if(valid){
            cmp.find("newcontactform").submit();
            var spinner = cmp.find("newcontactspinner");
            cmp.set("v.showFormSpinner", true);
        }
    },
    handleNewContactError: function(cmp,event,helper){
        cmp.set("v.showFormSpinner", false);
    },
    handleInputFieldChange: function(cmp, event, helper){
        //Handle populating username with email
        if (cmp.get("v.resetForm") && cmp.find('newcontactemail') && cmp.find('newcontactusername')) {
            var email = cmp.find('newcontactemail').get('v.value');
            var username = cmp.find('newcontactusername').get('v.value');
            if(email != username){
                cmp.find('newcontactusername').set('v.value', email);
            }
            //Remove error messages
            $A.util.removeClass(cmp.find('newcontactfirstname'), 'slds-has-error');
            $A.util.removeClass(cmp.find('newcontactlastname'), 'slds-has-error');
            $A.util.removeClass(cmp.find('newcontactemail'), 'slds-has-error');
            $A.util.removeClass(cmp.find('newcontactusername'), 'slds-has-error');
            $A.util.removeClass(cmp.find('newcontactcountry'), 'slds-has-error');
            $A.util.removeClass(cmp.find('newcontactlevel'), 'slds-has-error');
        }
    },
    handleNewContactSuccess: function(cmp, event) {
        //cmp.set('v.resetForm', false); 
        //Get the new contact's id
        var payload = event.getParams().response;
        console.log('onsuccess: ' + payload.id);
        //Add contact to selected contacts array.
        var adminids = cmp.get("v.AdminIds");
        adminids.push(payload.id);
        cmp.set("v.AdminIds", adminids);
        //Fire success toast.
        if(cmp.get('v.isLightningOut') === false){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Success!",
                "type" : "success",
                "message": "New Admin Contact has been added successfully."
            });
            toastEvent.fire();
        }
        // Hide the recordEditForm, then fire the event to show it with all fields blank.
        cmp.set('v.resetForm', false); 
        cmp.getEvent("adminContactSaved").fire();
    },
    handleAdminContactSaved: function(cmp, event, helper) {
        // Setting timeout before re-showing form to avoid race condition errors with handleInputFieldChange function
        setTimeout($A.getCallback(function() {
            cmp.set("v.resetForm", true);
        }), 1000)
    },
    handleBrandComponentEvent : function (cmp, event, helper) {
        var brandObj = event.getParam("Brand") ;
        
        cmp.set ('v.newbrand' , brandObj);
        console.log('#########  BrandToMerge' );
        console.log ( event.getParam("Brand") ) ;
        console.log ( event.getParam("CurrentBrand") ) ;
        console.log(brandObj ) ;
        if ( cmp.get("v.newbrand.Brand_Admin__c" ) != undefined  ){
            cmp.set("v.isContactSelected", true);
            cmp.set("v.SelectedContact" , cmp.get("v.newbrand.Brand_Admin__c" ) ) ;
            
            //var cmpEventContainer = $A.get("e.c:OnboardingBrandContainerEvent");
            //cmpEventContainer.setParams({
            //    "BrandAdminComplete": true,
            //});
            //cmpEventContainer.fire();
            //var cmpEvent = $A.get("e.c:OnboardingSummaryEvent");
            //cmpEvent.setParams({"BillingAdminComplete":true , "FromBillingAdmin": true});
            //cmpEvent.fire();
        } else {
            var cmpEventContainer = $A.get("e.c:OnboardingBrandContainerEvent");
            cmpEventContainer.setParams({
                "BrandAdminComplete": false,
            });
            cmpEventContainer.fire();
        }
        cmp.find("contactRecordLoader").reloadRecord();
        cmp.find("brandRecordLoader").reloadRecord();
    },
    AddBrandAdminSetup : function (cmp, event, helper) {
        helper.showSpinner(cmp, true);
        helper.saveBrandRecord(cmp);
    } ,
    /*handleNewClientCheckbox: function (cmp, event, helper) {
        var isChecked = cmp.find("iCreateNewContact").get("v.checked");
        var obj = {FirstName: '' , LastName: '' , 'Email': '', 'Phone' : '' , 'ContactLevel' : '' , Status__c : '' , LeadSource: ''} ;
        cmp.set("v.ContactObj", obj);
        console.log(isChecked)
        if (isChecked) {
            var elem = document.getElementById("newbrandadmincontact");
            elem.classList.add('slds-show');
            elem.classList.remove('slds-hide');
            var elem1 = document.getElementById("existingbrandadmincontact");
            elem1.classList.add('slds-hide');
            elem1.classList.remove('slds-show');
            cmp.set("v.NewContact" , true);
        } else {
            var elem = document.getElementById("newbrandadmincontact");
            elem.classList.add('slds-hide');
            elem.classList.remove('slds-show');
            var elem1 = document.getElementById("existingbrandadmincontact");
            elem1.classList.add('slds-show');
            elem1.classList.remove('slds-hide');
            cmp.set("v.NewContact" , false);
        }
    },*/
    ContactSelected: function (cmp, event, helper) {
        cmp.find("contactRecordLoader").reloadRecord(false, $A.getCallback(function() {
            if (!cmp.get("v.simpleContactRecord.Requested_Username__c")) {
                cmp.set("v.simpleContactRecord.Requested_Username__c", cmp.get("v.simpleContactRecord.Email"));
            };
        }));
        /*
        var contactId = cmp.get('v.SelectedContact');
        if (contactId == null) {
            document.getElementById('ContactInfo').style = 'display:none';
        } else {
            var contacts = cmp.get('v.ContactList')
            for (var c = 0; contacts.length > c; c++) {
                if (contacts[c].Id == contactId) {
                    cmp.find('ContactEmail').set("v.value", contacts[c].Email);
                    cmp.find('ContactPhone').set("v.value", contacts[c].Phone);
                }
            }
            document.getElementById('ContactInfo').style = 'display:block';
        }
        */
    },
    handleSelectContact : function (cmp, event, helper) {
        const allValid = cmp.find("selected-contact-field").reduce(function(validSoFar, field) {
            field.reportValidity();
            return validSoFar && field.checkValidity();
        }, true);
        if (allValid) {
            //Update the selected contact's information using Lightning Data Service.
            cmp.find("contactRecordLoader").saveRecord($A.getCallback(function(saveResult) {
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    console.log('contact save result ' + saveResult.state);
                } else if (saveResult.state === "INCOMPLETE") {
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    console.log('Problem saving record, error: ' + JSON.stringify(saveResult.error));
                } else {
                    console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                }
            }));
            //Update list of admin ids.
            var adminids = cmp.get("v.AdminIds");
            adminids.push(cmp.get("v.SelectedContact"));
            cmp.set("v.AdminIds", adminids);
            //Set selected contact to null.
            
            //helper.showSpinner(cmp, true);
            //helper.saveBrandRecord (cmp);
            //helper.getSelectedContact(cmp);
        }
    },
    handleContactSaved : function(component, event, helper) {
        const response = event.getParam("saveResponse");
        const state = response.state;
        const savedContactId = event.getParam("savedContactId");
        console.log("SAVED CONTACT STATE: ", state, savedContactId);
        if (state === "SUCCESS" || state === "DRAFT") {
            var adminids = component.get("v.AdminIds");
            adminids.push(savedContactId);
            component.set("v.AdminIds", adminids);
            helper.triggerUnsavedChanges(component, true);
        } else if (state === "INCOMPLETE") {
            console.log("INCOMPLETE");
            if(component.get('v.isLightningOut') === false){
                component.find('brandAdminSetupNotifLib').showToast({
                    "title": "There was a problem saving the Brand Admin",
                    "message": "User is offline, device doesn't support drafts.",
                    "variant": "error"
                });
            }
        } else if (state === "ERROR") {
            console.log("ERROR");
            const errors = response.error;
            console.log(errors);
            let errorMessage;
            if (errors) {
                if (errors[0] && errors[0].message) {
                    console.log(errors[0]);
                    errorMessage = errors[0].message;
                }
            } else {
                errorMessage = "Unknown error."
            }
            if(component.get('v.isLightningOut') === false){
                component.find('brandAdminSetupNotifLib').showToast({
                    "title": "There was a problem saving the Brand Admin",
                    "message": "Error: " + errorMessage,
                    "variant": "error"
                });
            }
        } else {
            console.log("UNKNOWN");
            if(component.get('v.isLightningOut') === false){
                component.find('brandAdminSetupNotifLib').showToast({
                    "title": "There was a problem saving the Brand Admin",
                    "message": "Error: Unknown error",
                    "variant": "error"
                });
            }
        }
    },
    handleContactRecordUpdated: function(cmp,event,helper){
        //Handles loading data and assigning default requested username for a selected contact via LDS.
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            var contact = cmp.get("v.simpleContactRecord");
            if(!contact.Requested_Username__c){
                contact.Requested_Username__c = contact.Email;
                cmp.set("v.simpleContactRecord", contact);
            }
            
        } else if(eventParams.changeType === "CHANGED") {
            // record is changed
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        }  
    },
    handleOppRecordUpdated: function(cmp,event,helper){
        //Handles loading data and assigning account ID from Opportunity info via LDS.
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            var opp = cmp.get("v.simpleOppRecord");
            cmp.set('v.accountId', opp.AccountId);
            
        } else if(eventParams.changeType === "CHANGED") {
            // record is changed
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        }  
    },
    handleBrandRecordUpdated: function(cmp,event,helper){
        //Handles loading data and assigning account ID from Opportunity info via LDS.
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            var brand = cmp.get("v.simpleBrandRecord");
            //Validate if Brand Admins tab has been completed
            if(!!brand.Brand_Admins__c || (brand.Brand_Admin_Exists_As_User__c && !!brand.Existing_Brand_Admin_Username__c && !!brand.Existing_Brand_Admin_Brand_ID__c)){
                cmp.set('v.ShowBrandAdminIcon', true);
            }
            else{
                cmp.set('v.ShowBrandAdminIcon', false);
            }
            //Parse Brand Admin Ids string into an array.
            var contactids = brand.Brand_Admin_Ids__c;
            var idarray = [];
            if(!!contactids){
                idarray = contactids.split(',');
            }
            cmp.set('v.AdminIds', idarray);
        } else if(eventParams.changeType === "CHANGED") {
            // record is changed
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        }  
    },
    handleRemoveAdmin: function(cmp,event,helper){
        var idToRemove = event.getParam("idToRemove");
        var adminids = cmp.get("v.AdminIds");
        var idIndex = adminids.indexOf(idToRemove);
        if(idIndex >= 0){
            adminids.splice(idIndex,1);
            cmp.set("v.AdminIds", adminids);
        }        
    },
    /*
    SearchClients : function (cmp, event, helper) {
        helper.getClientList(cmp, event, helper);
    } ,
    */
    saveBrandAdmin : function (cmp, event, helper) {
        helper.saveBrandRecord (cmp);
    },
    creareAdminContact : function (cmp, event, helper) {
        helper.showSpinner(cmp, true);
        helper.SaveContactAdminRecord (cmp);
    } ,
    removeSelectedContact : function (cmp, event, helper) {
        helper.showSpinner(cmp, true);
        helper.removeAdmin (cmp)
    } ,
    handleAdminExistsCheckbox :  function (cmp, event, helper) {
        var isChecked = cmp.find("exisitngUserName").get("v.checked");
        if (isChecked) {
            cmp.set("v.AdminExists" , true)
        } else {
            cmp.set("v.AdminExists" , false);
        }
    },
    handleBrandAdminExists : function (component, event, helper) {
        helper.triggerUnsavedChanges(component, true);
    }
})