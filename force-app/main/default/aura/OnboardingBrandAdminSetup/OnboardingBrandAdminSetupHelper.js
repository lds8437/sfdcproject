({
    showSpinner: function (cmp, statusFlag) {
        cmp.set('v.Spinner', statusFlag);
    },
    AddBrandAdminSetup: function (cmp) {

        var action = cmp.get("c.SaveBrandRecord");
        var dataObj = cmp.get("v.newbrand");
        dataObj["Brand_Admin__c"] = cmp.get("v.SelectedContact");
        action.setParams({
            "dataString": JSON.stringify(cmp.get("v.newbrand")),
            "OppId": cmp.get("v.OpportunityId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                //cmp.set("v.NewContactObj", response.getReturnValue());
                //cmp.set("v.isContactSelected", true);
                window.scrollTo(0, 0);
                var brandData = response.getReturnValue()
                var cmpEvent = $A.get("e.c:OnboardingBrandEvent");
                cmpEvent.setParams({"Brand": brandData, "CurrentBrand": brandData.Id});
                cmpEvent.fire();
                helper.showSpinner(cmp, false);
                if(cmp.get('v.isLightningOut') === false){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type": "success",
                        "message": "Brand Record updated.",
                        duration: ' 2000',
                        type: 'success',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                }


                /*
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success Message',
                    message: 'Brand Record Created',
                    messageTemplate: 'Brand Record Created',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
                */
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    /*
    getContactLabels: function (cmp) {

        // Prepare the action to load account record
        var action = cmp.get("c.getContactLabels");


        // Configure response handler
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                cmp.set("v.ContactMap", response.getReturnValue());
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    */
    getContactRole: function (cmp, event, helper) {

        // Prepare the action to load account record
        var action = cmp.get("c.getContactRole");

        // Configure response handler
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                cmp.set("v.ContactRole", JSON.parse(response.getReturnValue()));
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    /*
    getContactStatus: function (cmp, event, helper) {

        // Prepare the action to load account record
        var action = cmp.get("c.getContactStatus");

        // Configure response handler
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                cmp.set("v.ContactStatus", JSON.parse(response.getReturnValue()));
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    
    getContactLeadSource: function (cmp, event, helper) {

        // Prepare the action to load account record
        var action = cmp.get("c.getContactLeadSource");

        // Configure response handler
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                cmp.set("v.ContactLeadSource", JSON.parse(response.getReturnValue()));
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    */
    getClientList: function (cmp, event, helper) {

        // Prepare the action to load account record
        var action = cmp.get("c.getContacts");

        action.setParams({"opportunityId": cmp.get('v.OpportunityId'), "searchTerm": cmp.get('v.SearchValue')});

        // Configure response handler
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var contacts = response.getReturnValue();

                cmp.set("v.ContactList", contacts);
                var radioContacts = '';
                for (var c = 0; contacts.length > c; c++) {
                    radioContacts += '{"label": "' + contacts[c].Name + '", "value" : "' + contacts[c].Id + '"},';
                }

                console.log('[' + radioContacts.substring(0, radioContacts.length - 1) + ']');
                var json = JSON.parse('[' + radioContacts.substring(0, radioContacts.length - 1) + ']');
                var arry = new Array();
                for (var i = 0; i < json.length; i++) {
                    if (i >= 10) {
                        cmp.set('v.showSearch', true);
                        break;
                    } else {
                        arry.push(json[i]);
                    }
                }

                cmp.set("v.RelatedContacts", arry);
                //console.log("Brand Admin Related Contacts");
                
                //console.log(cmp.get("v.RelatedContacts"));
                window.setTimeout(
                    $A.getCallback(function() {
                        cmp.find("contactRecordLoader").reloadRecord();
                    }), 1000
                );

            } else {
                console.log('Problem getting contact, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    /*
    getSelectedContact: function (cmp) {
        var action = cmp.get("c.getSelectedContact");
        var data = new Object;
        data["Phone"] = cmp.find("ContactPhone").get("v.value");
        data["Email"] = cmp.find("ContactEmail").get("v.value");
        console.log(data);
        action.setParams({
            "contactId": cmp.get("v.SelectedContact"),
            "dataString": JSON.stringify(data)
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                cmp.set('v.SelectedContactName', result.ContactName)
                cmp.set("v.ContactObj", response.getReturnValue());
                this.showSpinner(cmp, false);
                cmp.set("v.isContactSelected", true);
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    */
    saveBrandRecord: function (cmp) {

        var action = cmp.get("c.SaveBrandRecord");
        console.log('Starting savingg brands to merge');
        console.log('***** OpportunityId ******' + cmp.get("v.OpportunityId"));
        console.log('***** newbrand ******' + cmp.get("v.newbrand"));

        var brand = cmp.get("v.newbrand");
        brand["Brand_Admin__c"] = cmp.get("v.SelectedContact");
        action.setParams({
            "dataString": JSON.stringify(cmp.get("v.newbrand")),
            "OppId": cmp.get("v.OpportunityId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                cmp.set("v.isContactSelected", true);
                var obj = response.getReturnValue() ;
                cmp.set("v.newbrand" , obj ) ;
                var cmpEvent = $A.get("e.c:OnboardingBrandEvent");
                cmpEvent.setParams({"Brand": obj , "CurrentBrand" : obj.Id });
                cmpEvent.fire();


                //cmpContainerEvent.fire();

                this.showSpinner(cmp, false);
                if(cmp.get('v.isLightningOut') === false){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type": "success",
                        "message": "Brand Record updated.",
                        duration: ' 2000',
                        type: 'success',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
                }


            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    SaveContactAdminRecord : function (cmp) {
        console.log('Saving contact record');
        var action = cmp.get("c.SaveContactAdminRecord");

        var dataString = JSON.stringify(cmp.get('v.ContactObj') );
        console.log('********** dataString ' + dataString );
        console.log(dataString);
        action.setParams({
            "dataString": dataString,
            "OppId": cmp.get('v.OpportunityId')
        });
        // Configure response handler
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                //this.showSpinner (cmp , false);
                cmp.set("v.isContactSelected", true);
                cmp.set("v.SelectedContact" , response.getReturnValue() ) ;
                var obj = cmp.get("v.newbrand");
                var ct =   cmp.get('v.ContactObj') ;
                obj["Requested_Username__c"] = ct.Email;

                Requested_Username__c
                this.saveBrandRecord (cmp);

            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    removeAdmin  : function (cmp) {

        var action = cmp.get("c.RemoveBrandAdmin ");

        var dataString = JSON.stringify(cmp.get('v.ContactObj') );
        console.log('********** dataString ' + dataString );
        console.log(dataString);
        action.setParams({
            "brandId": cmp.get('v.newbrand.Id')
        });
        // Configure response handler
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                this.showSpinner (cmp , false);
                cmp.set("v.isContactSelected", false);
                cmp.set("v.SelectedContact" , "" ) ;
                var cmpEvent = $A.get("e.c:OnboardingBrandEvent");
                cmpEvent.setParams({"Brand": obj , "CurrentBrand" : obj.Id });
                cmpEvent.fire();
                if(cmp.get('v.isLightningOut') === false){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type": "success",
                        "message": "Brand Admin has been removed.",
                        duration: ' 2000',
                        type: 'success',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
                }

            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    triggerUnsavedChanges : function(component, showUnsavedIndicator) {
        const unsavedChangesEvent = component.getEvent("unsavedChangesEvent");
        unsavedChangesEvent.setParams({
            "tabName": "Admin Setup",
            "showUnsavedIndicator": showUnsavedIndicator
        });
        unsavedChangesEvent.fire();
    }
})