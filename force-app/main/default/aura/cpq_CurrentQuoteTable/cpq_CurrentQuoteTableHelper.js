({
    getQuoteList : function(component,event,helper) {
        //console.log('CurrentQuoteTable Helper: getQuoteList');
        helper.callServer(
            component,
            "c.getQuoteList",
            function(response){           
                var numOfQuotes = 1;
                _.forEach(response, function(quote) {
                   // console.log(response);
                    
                    numOfQuotes++;
                    if(quote.Freeze_Pricing__c===false && quote.Locked__c===false && quote.Opportunity.IsClosed===false                        
                      ){
                        quote.quoteEditable = true;
                    }else{
                        quote.quoteEditable = false;
                    }
                    if(quote.Discount_Approval_Status__c === 'Not Submitted' || quote.Discount_Approval_Status__c === 'Rejected' || quote.Discount_Approval_Status__c === 'Recalled'){
                        quote.discountApproved = false;
                    }else{
                        quote.discountApproved = true;
                    }
                    if(quote.CPQ_Completed__c == false){
                        quote.Quote_Type__c = 'QUOTE CREATION FAILED'
                        quote.quoteEditable = false;
                    }
                });
                component.set('v.quoteList',response);
                component.set('v.numOfQuotes', numOfQuotes);                
            },
            {
                opportunityId : component.get('v.opportunityId')
            }
        );
    }
})