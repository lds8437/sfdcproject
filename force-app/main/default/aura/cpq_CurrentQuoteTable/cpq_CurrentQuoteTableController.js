({
	doInit : function(component, event, helper) {
        //console.log('CurrentQuoteTable Controller: doInit');
        var x = component.get('v.experienceMetaData')
       // console.log('**currentQuoteTable:',  JSON.parse(JSON.stringify(x)));
        helper.getQuoteList(component,event,helper); 
        var quoteList = component.get('v.quoteList');
       // console.log('**quoteList:', quoteList);
	},
    
      afterScriptsLoaded : function (component,event,helper){
       helper.getQuoteList(component,event,helper); 
    },
    
    onQuoteListChange : function(component, event, helper) {
        //console.log('CurrentQuoteTable Controller: QuoteListChange');
        helper.getQuoteList(component,event,helper); 
	},
    
    quoteSaved: function(component,event,helper){
        //console.log('CurrentQuoteTable Controller: QuoteSaved');
         helper.getQuoteList(component,event,helper); 
    },

     onSync : function(component, event, helper) {
      //console.log('CurrentQuoteTable: onSync');
      var quoteList = component.get('v.quoteList');
         component.set('v.quoteList', quoteList);
      var quote = event.getSource().get('v.name');
      var sync = event.getSource().get('v.value');
      var opportunityId = component.get('v.opportunityId');
     
        var total = 0;
         
      _.forEach(quoteList, function(q) {   
        if(q.Id != quote.Id) {
          q.IsSyncing = false;
        }else{
            total = q.TotalPrice;
        }
      });     
       
      if(sync && !quote.XM_Products__c.includes('RS1')) {
        helper.callServer(
        component,
        "c.fullQuoteSync",
        function(response) {
        },
        {
          quote : quote,
          opportunityId : opportunityId
        });
      } else if(sync && quote.XM_Products__c.includes('RS1')) {
        helper.callServer(
          component,
          "c.syncQuote",
          function(response) { 
          },
          {
            quoteId : quote.Id,
            opportunityId : opportunityId,
            total : total
          });
      } else if(!sync) {
        helper.callServer(
          component,
          "c.unsyncQuote",
          function(response) {
          },
          {
            opportunityId : opportunityId
          });
      }
    }
})