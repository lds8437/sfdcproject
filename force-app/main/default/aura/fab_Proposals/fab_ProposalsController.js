({
    doInit : function(component,event,helper){
       // console.log('PC: doInit')   
        helper.getSessionId(component,event,helper);
        helper.getCongaQueries(component,event,helper);
        helper.getFabVariables(component,event,helper);
        helper.getTemplateIds(component,event,helper);
        helper.setProposalOptions(component,event,helper)
    },
    
    send : function(component,event,helper){
      //  console.log('PC: send')
        var target = event.getSource();
        var buttonName = target.get("v.value");
        helper.getCongaMetaData(component,event,helper,buttonName);
    },
    
    handleChange: function (component, event, helper) {
      //  console.log('PC: handlechange')
        // This will contain an array of the "value" attribute of the selected options
        var selectedOptionValue = event.getParam("value");
      //  console.log(selectedOptionValue)
        helper.createContractDocumentTemplates(component,event,helper,selectedOptionValue)
        //alert("Option selected with value: '" + selectedOptionValue.toString() + "'");
    }
    
})