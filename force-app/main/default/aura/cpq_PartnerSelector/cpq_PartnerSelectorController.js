({
    popModal : function(component, event, helper) {
         console.log('Partner Selector: popModal');
        component.set("v.showmodal",true);//Pop up the modal for the Partner Selection tool
        component.set("v.showspinner", true);//Show spinner until data loaded
        var servicebundleids = [];//Set for Service Bundle Ids to send to Apex
        var servicebundles = component.get("v.selectedservicebundles");
        // console.log('servicebundles: ' + JSON.stringify(servicebundles));
        // console.log('iterate through service bundles');
        var i = 0;
        //console.log('i=' + i);
        if(!(servicebundles === undefined)){
            for (i = 0; i < servicebundles.length; i++) {
                //  console.log('i=' + i);
                //  console.log('current servicebundle: ' + JSON.stringify(servicebundles[i]));
                servicebundleids.push(servicebundles[i].Id);
            }
        }
        //  console.log('servicebundleids sent to Apex: ' + JSON.stringify(servicebundleids));
        //Set up the call to Apex to retrieve the Partner Availability records.	
        var action = component.get("c.getPartnerAvailability");
        action.setParams({ bundleids : servicebundleids,
                          region : component.get("v.opportunityRegion"),
                          arr : component.get("v.arr")
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var partners = response.getReturnValue();
                component.set("v.partners", partners);
                  console.log('--------------partners-----------------')
                  console.log(partners)
                var totalpartnercount = 0;
                //Put all the partners into array with each service bundle they belong to.
                if(!(servicebundles === undefined)){
                    for (i = 0; i < servicebundles.length; i++) {
                        var n = 0;
                        var partnersubarray = [];                        
                        for (n = 0; n < partners.length; n++) {
                            if(partners[n].Highlighted_Partner__c) {
                                partners[n].bgcolor = "#faffbd";
                            }
                            //console.log('current partner id: ' + partners[n].Id + ' current partner service bundle: ' + partners[n].Service_Bundle__c + 'current service bundle id: ' + servicebundles[i].Id);
                            if(partners[n].Service_Bundle__c == servicebundles[i].Id){
                                partnersubarray.push(partners[n]);
                                totalpartnercount++;
                            }
                            var indexOfPartner = _.findIndex(partners, {'Id' : servicebundles[i].Partner_Availability__c});
                            if(indexOfPartner > -1) {
                                if(servicebundles[i].Qualtrics_Invoicing__c === true) {
                                    partners[indexOfPartner].qualtricsSelected = true;
                                    //component.set('v.qualtricsinvoice', true);
                                    //component.set('v.selectedpartner', partners[indexOfPartner]);
                                }
                                else {
                                    partners[indexOfPartner].partnerSelected = true;
                                    //component.set('v.qualtricsinvoice', false);
                                    //component.set('v.selectedpartner', partners[indexOfPartner]);
                                }
                            }
                        }
                        servicebundles[i].PartnerCount = partnersubarray.length;
                        servicebundles[i].Partners = partnersubarray;
                        if(servicebundles[i].PartnerCount > 0){
                            var techQlis = component.get("v.techQlis");
                            for (n = 0; n < techQlis.length; n++) {
                                if(techQlis[n].Id == servicebundles[i].Bundle_Product_Prerequisite__c){
                                    if(servicebundles[i].Q_Partner__c === undefined) {
                                        servicebundles[i].ServiceOriginalPrice = techQlis[n].PriceTotal;
                                    }
                                    else {
                                        servicebundles[i].ServiceOriginalPrice = techQlis[n].PriceTotal / servicebundles[i].PartnerDiscount;
                                    }
                                    servicebundles[i].PriceTotal = techQlis[n].PriceTotal;
                                    // console.log('pricetotal assigned: ' + techQlis[n].PriceTotal);
                                }
                            }
                        }
                    }
                }
                //  console.log('----servbundle----')
                //  console.log(servicebundles)
                component.set("v.totalpartnercount",totalpartnercount);
                component.set("v.selectedservicebundles", servicebundles);
                component.set("v.showspinner", false);
            }
            else if (state === "INCOMPLETE") {
                //   console.log('partner availability retrieval incomplete');
                component.set("v.showspinner", false);
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                            component.set("v.showspinner", false);
                        }
                    } else {
                        console.log("Unknown error while retrieving partner availability records");
                        component.set("v.showspinner", false);
                    }
                    
                }
            
        });
        // console.log('action enqueue to retrieve partner records');
        $A.enqueueAction(action);
    },
    
    save: function(component, event, helper) {
        component.set("v.showspinner", true);
        // console.log('running save');
        var selectedpartners = [];//Array for partner selections
        var bundletable = component.find("partnertable"); //Array of child components
        if(bundletable.length === undefined && !!bundletable){
            selectedpartners.push(bundletable.getSelectedPartner());
        }
        else{
            for(var i = 0;i < bundletable.length;i++){
                selectedpartners.push(bundletable[i].getSelectedPartner());            
            }
        }
        // console.log('selectedpartners');
        // console.log(selectedpartners);
        //console.log('selected partners' + JSON.stringify(selectedpartners));
        var saveEvent = component.getEvent("savepartnerselection");
        saveEvent.setParams({"selectedpartners" : selectedpartners });
        saveEvent.fire();
        component.set("v.showspinner", false);
        component.set("v.showmodal",false);
    },
    
    closeButton : function(component, event, helper) {
        // console.log('Partner Selector: closeButton');
        var cancelEvent = component.getEvent('savepartnerselection');
        cancelEvent.setParams({'selectedpartners' : []});
        cancelEvent.fire();
        component.set("v.showmodal",false);
        component.set('v.partnerSelected', false);
    },
    
    escEvent : function(component, event, helper) {
        if(event.key == 'Escape') {
            component.set("v.showmodal",false);
        }
    },
    
    debugOutput : function(component, event, helper) {
        //console.log(JSON.stringify(component.get("v.techQlis")));
    },
    
    closeModal : function(component, event, helper) {
        //  console.log('Partner Selector: closeModal');
        component.set('v.showmodal', false);
    }
})