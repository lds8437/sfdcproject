({
    buttonPress : function(component, event, helper, buttontype) {
        //console.log('ClientLicenses Helper: buttonPress');
        //   console.log('button press helper running with button type ' + buttontype);
        //Execute Client Method to return clients
        var licensetable = component.find("licensetable");
        var clients = licensetable.returnClients();
        var filteredClients = _.uniqBy(clients, 'Id');
        if(clients.length == 0){//If size of client array is 0, return error message.
            //console.log('There are no clients selected');
            component.set("v.errormessage", "Error: no license selected!");
        }
        else if(clients.length == 1 || filteredClients.length == 1){//If size of client array is 1, execute SendToConfig
            var clientid = clients[0].Id;
            this.validateLicenses(component, event, helper, clients);
            if(component.get('v.messageCount') === 0) {
                this.sendToConfig(component, buttontype, clientid, false, clients);
            } else if(!component.get('v.caseCreated')) {
                this.createARCase(component, event, helper, clients);
            }
        }
            else if(filteredClients.length > 1) {//If size of client array is >1, send client array to merge choice and show merge modal.
                //console.log('There are more than one clients selected');
                component.set("v.upgradechoice", buttontype);
                var clientmerge = component.find("clientmerge");
                clientmerge.showClientMerge(clients);
            }
        
    },
    sendToConfig : function(component, buttontype, clientid, merge, clients){
        var relatedClients = [];
        var sendEvent = $A.get("e.c:cpq_licensesToConfig");
        //Execute license method to get selected licenses from table.
        var licensetable = component.find("licensetable");
        var licenses = licensetable.returnAllLicenses();
        sendEvent.setParams({            
            "licenses" : licenses,
            "upgradechoice" : buttontype,
            "clientid" : clientid,
            "merge" : merge,
            "clients" : clients
            //"start" : component.get('v.start'),
            //"end" : component.get('v.end'),
            //"licenseCurrencyCode" : component.get('v.currencycode'),
        });
        //console.log('sending license group to config event params: ' + JSON.stringify(sendEvent.getParams()));
        sendEvent.fire();
    },
    
    validateLicenses : function(component, event, helper, clients) {
        component.set('v.licenseErrors', '');
        component.set('v.messageCount', 0);
        var licenseTable = component.find("licensetable");
        var licenses = licenseTable.returnAllLicenses();
        _.forEach(licenses, function(license) {
            helper.validateField(component, helper, license, license.Bundle_Product__c, 'bundle product');
            helper.validateField(component, helper, license, license.Product__c, 'product');
            if(license.Bundle_Product__c) {
            	helper.validateField(component, helper, license, license.Bundle_Product__r.Related_Product__c, 'bundle product', license.Product__c, 'product');    
            }
        });
    },
    
    validateField : function(component, helper, license, referenceField, referenceLabel, comparisonField, comparisonLabel) {
        if(arguments.length === 5) {
            referenceField ? true : helper.getErrorMessage(component, license, referenceLabel);
        } else {
            referenceField === comparisonField ? true : helper.getErrorMessage(component, license, referenceLabel, comparisonLabel);
        }
    },
    
    getErrorMessage : function(component, license, referenceLabel, comparisonLabel) {
        let licenseErrors = 'There is an issue with the client licenses. A case has automatically been filed for you.';
        let caseMessage = component.get('v.caseMessage');
        if(arguments.length === 3) {
            caseMessage += 'This client license is missing a ' + referenceLabel + ': ' + license.Id + '.';
        } else {
            caseMessage += 'The ' + referenceLabel + ' does not match the ' + comparisonLabel + ' on this license: ' + license.Id + '.';
        }
        component.set('v.licenseErrors', licenseErrors);
        component.set('v.messageCount', component.get('v.messageCount') + 1);
        component.set('v.caseMessage', caseMessage);
    },
    
    createARCase : function(component, event, helper, clients) {
        let accountId = component.get('v.accountId');
        let action = component.get('c.createARCase');
        let opportunityId = component.get('v.opportunityId');
        action.setParams({
            accountId : accountId,
            opportunityId : opportunityId,
            clientId : clients[0].Id,
            message : component.get('v.caseMessage')
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if(state === 'SUCCESS') {
                component.set('v.caseCreated', true);
            }
        });
        $A.enqueueAction(action);
    }
})