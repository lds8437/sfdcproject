({
    handlePopEvent : function(component, event, helper) {
        //  console.log('ClientLicense Controller: handlePopEvent');
        //console.log('top level component handling pop event: ' + JSON.stringify(event.getParams()));
        var clientname = event.getParam("clientnameparam");
        var bundlename = event.getParam("bundlenameparam");
        var licenses = event.getParam("licensesparam");
        //console.log('client name sent to method' + clientname);
        var modalComponent = component.find('licenselinesmodal');
        modalComponent.popModal(clientname, bundlename, licenses);
    },
    escEvent : function(component, event, helper) {
        //  console.log('ClientLicense Controller: escEvent');
        //console.log('top level keypress: ' + event.which);
        //console.log('top level key: ' + event.key);
        //console.log('top level code: ' + event.code);
        //console.log('top level locatoin: ' + event.location);
        if(event.key == "Escape"){
            component.set("v.showmodal", false);
        }
    },
    handleSelectEvent : function(component, event, helper) {
        console.log('Client Licenses Controller: handleSelectEvent');
        var selected = event.getParam('selected');
        var selectedExperience = event.getParam('selectedExperience');
        var selectedBundle = event.getParam('selectedBundle');
        var selectedUpgradeBundles = event.getParam('selectedUpgradeBundles');
        if(selected) {
            component.set('v.selectedCount', component.get('v.selectedCount') + 1);
        }
        else {
            component.set('v.selectedCount', component.get('v.selectedCount') - 1);
        }
        if(component.get('v.selectedCount') == 0) {
            component.set('v.selectedExperience', undefined);
            component.set('v.selectedUpgradeBundles', undefined);
            component.set('v.selectedBundle', undefined);
            component.set('v.expired', false);
        }
        else {
            if(selectedExperience != undefined) {
                component.set('v.selectedExperience', selectedExperience);
                component.set('v.selectedBundle', selectedBundle);
                component.set('v.selectedUpgradeBundles', selectedUpgradeBundles);
                component.set('v.expired', event.getParam('expired'));
            }
        }
        //console.log(component.get('v.selectedCount'));
        //Clear error message for no licenses selected.
        component.set("v.errormessage",'');
        component.set('v.messageCount', 0);
        component.set('v.licenseErrors', '');
        component.set('v.caseMessage', '');
    },
    keepDatesButton : function(component, event, helper) {
        //   console.log('ClientLicenseGroup Controller: keepDatesButton');
        console.log('keep dates button press');
        var buttontype = 'keep-dates';
        helper.buttonPress(component, event, helper, buttontype);
    },
    changeDatesButton : function(component, event, helper) {
        //   console.log('ClientLicenseGroup Controller: changeDatesButton');
        console.log('change dates button press');
        var buttontype = 'change-dates';
        helper.buttonPress(component, event, helper, buttontype);
    },
    onRenewalButton : function(component, event, helper) {
        //    console.log('ClientLicenseGroup Controller: onRenewalButton');
        console.log('on renewal button press');
        var buttontype = 'on-renewal';
        helper.buttonPress(component, event, helper, buttontype);
    },
    onMergeButton : function(component, event, helper) {
        console.log('Client Licenses Controller: onMergeButton');
        var buttontype = 'merge';
        helper.buttonPress(component, event, helper, buttontype);
    },
    onMergeClientSelected : function(component, event, helper) {
        //console.log('Client Licenses Controller: onMergeClientSelected');
        var clientid = event.getParam("clientid");
        var merge = true;
        var buttontype = component.get("v.upgradechoice");
        var licensetable = component.find("licensetable");
        var clients = licensetable.returnClients();
        helper.validateLicenses(component, event, helper, clients);
        if(component.get('v.messageCount') === 0) {
        	helper.sendToConfig(component, buttontype, clientid, merge, clients);    
        } else if(!component.get('v.caseCreated')){
            helper.createARCase(component, event, helper, clients);
        }
    },
    
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }
})