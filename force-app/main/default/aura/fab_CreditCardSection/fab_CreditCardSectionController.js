({
    
    doInit : function(component, event, helper) {
      //  console.log('Init')
        var country = component.get('v.opportunity.Client__r.ShippingCountry2__c');
        component.set('v.parentId',component.get('v.opportunity.SyncedQuoteId'))
       // console.log(component.get('v.opportunity'))
        if(country === 'United States' || country === 'Canada' || country === '' || country === undefined){
            component.set('v.taxableCountry',true);
        }
        helper.getProcessInstance(component,event,helper);
    },
    
    requestTaxExempt : function(component,event,helper){
       component.set('v.saving',true);       
       helper.save(component,event,helper);
    },
        
    uploadFile : function(component,event,helper){
        console.log('CCSC: uploadFile')
        component.set('v.noFile',false);
        
    },
    
    
})