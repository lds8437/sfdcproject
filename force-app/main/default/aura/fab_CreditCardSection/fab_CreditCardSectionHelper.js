({
    save : function(component,event,helper) {
       console.log('CCSH: save')
       
        
        component.set('v.showSpinner', true);
        
        var fileInput = component.find("file").getElement();
        var file = fileInput.files[0];   
        
        if (file.size > 4500000) {
            alert('File size cannot exceed ' + 4500000 + ' bytes.\n' +
    		  'Selected file size: ' + file.size);
    	    return;
        }
        
        if(file === undefined){
            component.set('v.noFile',true);
            component.set('v.saving',false);
            component.set('v.showSpinner',false);
        }else{
            console.time("fileupload");
            var fr = new FileReader();
            fr.onload = function() {
                var target = event.getSource();        
                var description = target.get("v.name");
                var fileContents = fr.result;
                var base64Mark = 'base64,';
                var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
                
                fileContents = fileContents.substring(dataStart);
                
                helper.upload(component, file, fileContents,description,helper);
            };
            fr.readAsDataURL(file); 
        }
    },
    
    upload: function(component, file, fileContents, description,helper) {
        console.log('CCSH: upload')
       // console.log('helper: upload');
       // console.log('**file: ', file);
       // console.log('**fileContents: ', fileContents);
       // var action = component.get('c.saveTheFile');
        
       // console.log('**parentId:', component.get("v.parentId"));
       // console.log('**fileName:', file.name);
       // console.log('**base64Data:', encodeURIComponent(fileContents));
       // console.log('**contentType:', file.type);
       // console.log('**oppId:', component.get('v.opportunity').Id);
        
        var fromPos = 0;
        var toPos = Math.min(fileContents.length, fromPos + 950000);
        
        // start with the initial chunk
        this.uploadChunk(component, file, fileContents, description, helper, fromPos, toPos, '');   
        
        
      /*  action.setParams({
            "parentId": component.get("v.parentId"),
            "fileName": file.name,
            "base64Data": encodeURIComponent(fileContents), 
            "contentType": file.type,
            "oppId" : component.get('v.opportunity').Id
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
       //     console.log(state);
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
              //  console.log(dataObj);
                component.set('v.saving',false);
                component.set('v.exemptionStatus','Requested');
            }  
            else if (state === 'ERROR') {
              //  console.log('**Error log');
                helper.processErrors(component,response);
            }
        }));
        //   console.log('call action');
        $A.enqueueAction(action); 
        //   console.log('action called');*/
    },
    
    uploadChunk: function(component, file, fileContents, description,helper, fromPos, toPos, attachId) {
        console.log('CCSH: uploadChunk')
        //  console.log('RSH: upload');
       // console.log(' uploading chunk fromPos ' + fromPos + ' toPos ' + toPos);
        var oppId = component.get('v.opportunity').Id
        var action = component.get('c.saveTheChunk2'); 
         // console.log(file.type)
         // console.log(encodeURIComponent(fileContents))
        
        //Get the current chunk from the file contents.
        var chunk = fileContents.substring(fromPos, toPos);
        
        action.setParams({          
            "fileName": file.name,
            "base64Data": encodeURIComponent(chunk), 
            "contentType": file.type,
            "oppId" : oppId,
            "description" : description,
            "fileId" : attachId,
            "parentId": component.get("v.parentId")
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();   
          //  console.log(state)
            if (state === "SUCCESS") {
             //   console.log('successful load of chunk fromPos ' + fromPos + ' toPos ' + toPos);
                //Get the uploaded file id
                var fileId = response.getReturnValue();  
                //Set up the next chunk file position
                fromPos = toPos;
                toPos = Math.min(fileContents.length, fromPos + 950000);
             //   console.log('next chunk fromPos ' + fromPos + ' toPos ' + toPos);
                //If there is still another chunk left, upload it
                if (fromPos < toPos) {
                    this.uploadChunk(component, file, fileContents, description, helper, fromPos, toPos, fileId);  
                }
                else{
                    var dataObj= response.getReturnValue();
                    component.set('v.saving',false);                    
                    component.set('v.showSpinner', false);
                    component.set('v.exemptionStatus','Requested');
                  //  console.log('should be final hide spinner');
                    console.timeEnd("fileupload");
                    helper.getFiles(component,helper,oppId,description);
                    helper.reloadFab(component,event,helper);
                }
                
            }  
            else if (state === 'ERROR') {
                console.timeEnd("fileupload");
                helper.handleErrors(component,response);
                component.set('v.showSpinner', false);
            }
        }));
        $A.enqueueAction(action); 
        
    },
    
    getProcessInstance : function(component,event,helper){
      //  console.log('getProcessInstance');
        var action = component.get('c.getProcessInstance');
     //   console.log('**quote: ', component.get('v.opportunity.SyncedQuoteId'))
        action.setParams({
            "quoteId": component.get('v.opportunity.SyncedQuoteId')           
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
           // console.log('state');
          //  console.log(state);
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
              //  console.log('process instance');
               // console.log(dataObj);
                component.set('v.exemptionStatus',dataObj);
            }  
            else if (state === 'ERROR') {
                helper.processErrors(component,response);
            }
        }));
        
        $A.enqueueAction(action); 
    },
    
    processErrors : function(component,response){
        console.log('MCH: processErrors');
        const errors = response.getError();
      //  console.log('**error: ', errors)
      //  console.log('**error detail: ', JSON.parse(JSON.stringify(errors)))
        let message = 'Unknown error';
        if (errors && Array.isArray(errors) && errors.length > 0) {
            const error = errors[0];
           // console.log(error)
            if (typeof error.message != 'undefined') {
                message = error.message;
            } else if (typeof error.pageErrors != 'undefined' && Array.isArray(error.pageErrors) && error.pageErrors.length > 0) {
                const pageError = error.pageErrors[0];
                if (typeof pageError.message != 'undefined') {
                    message = pageError.message;
                }
            }
           // console.log(message)
        }
    },
    
    handleErrors : function(component,response){
        // Retrieve the error message sent by the server
        const errors = response.getError();
        let message = 'Unknown error'; // Default error message
        if (errors && Array.isArray(errors) && errors.length > 0) {
            const error = errors[0];
            if (typeof error.message != 'undefined') {
                message = error.message;
            } else if (typeof error.pageErrors != 'undefined' && Array.isArray(error.pageErrors) && error.pageErrors.length > 0) {
                const pageError = error.pageErrors[0];
                if (typeof pageError.message != 'undefined') {
                    message = pageError.message;
                }
            }
        }
        // Display error in console
        console.error('Error: '+ message);
        console.error(JSON.stringify(errors));
        
        
    },
    
})