({
    doInit : function (cmp, event, helper) {
        var result = decodeURIComponent((new RegExp('[?|&]' + "Id" + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
        cmp.set("v.OpportunityId", result);
        helper.getOpportunityWithClient(cmp);
        helper.getBrands(cmp);
    } ,
    recalculateBrandStats : function(cmp, event, helper){
        var brandcomponents = cmp.find('linkedbrand');
        var brandstats = {};
        var brandcount = 0;
        var admincount = 0;
        var mergecount = 0;
        var usercount = 0;
        
        if(brandcomponents !== undefined){
            if(brandcomponents.length === undefined && !!brandcomponents){//Handling for single linked brand component
                brandcount = 1;
                var thisbrandstats = brandcomponents.sendStats();
                if(thisbrandstats != null){
                    admincount = thisbrandstats.admincount;
                    mergecount = thisbrandstats.mergecount;
                    usercount = thisbrandstats.usercount;
                }
            }
            else{
                for(var i = 0;i < brandcomponents.length;i++){//Handling for multiple linked brand components.
                    brandcount++;
                    var thisbrandstats = brandcomponents[i].sendStats();
                    if(thisbrandstats != null){
                        console.log('thisbrandstats' + JSON.stringify(thisbrandstats));
                        admincount += thisbrandstats.admincount;
                        mergecount += thisbrandstats.mergecount;
                        usercount += thisbrandstats.usercount;
                    }
                }
            }
            brandstats.brandcount = brandcount;
            brandstats.admincount = admincount;
            brandstats.mergecount = mergecount;
            brandstats.usercount = usercount;
        }
     //   console.log(JSON.stringify(brandstats));
        cmp.set('v.BrandCount', brandcount);
        cmp.set('v.AdminCount', admincount);
        cmp.set('v.BrandsToMergeCount', mergecount);
        cmp.set('v.UsersToMoveCount', usercount);
    },
    deleteBrand : function (component, event, helper) {
        //if(!confirm('Are you sure?')) return false;
        var r = confirm("You are about to delete this record. Are you sure?");
        if (r){
            //helper.showSpinner(cmp, true);
            //helper.deleteBrands(cmp ,   event.target.getAttribute('aria-label') ) ;
            //helper.deleteBrands(cmp, event.getSource().get("v.name"));
          //  console.log("fire the delete event!");
            const brandId = event.getSource().get("v.name");
            const deleteEvent = component.getEvent("deleteBrandEvent");
            deleteEvent.setParams({
                "brandId": brandId
            });
            deleteEvent.fire();
        }
    },
    handleBrandComponentEvent : function (cmp, event, helper) {
        
        helper.getBrands(cmp);
        
    },
    selectBrand :  function (cmp, event, helper) {
        var brandId = event.getParam("brandId");
        //var brandId = event.getSource().get("v.name");
        cmp.set('v.selectedBrandId', brandId);
        //helper.selectBrand(cmp ,   event.target.getAttribute('aria-label') ) ;
    },
    handleBrandsChange : function(component, event, helper) {
        helper.checkIfComplete(component);
    }
})