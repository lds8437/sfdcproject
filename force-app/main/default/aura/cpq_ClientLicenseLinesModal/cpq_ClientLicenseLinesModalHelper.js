({
	closeModal : function(component) {
      //  console.log('ClientLicenseLineModal Helper: closeModal');
        var modalcontent = component.find("modal-content");
        var modalbackdrop = component.find("modal-backdrop");        
        $A.util.addClass(modalcontent, 'slds-hide');
        $A.util.addClass(modalbackdrop, 'slds-hide');        
    }
})