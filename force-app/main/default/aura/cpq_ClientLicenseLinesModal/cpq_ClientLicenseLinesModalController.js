({
	executePopModal : function(component, event, helper) {
       // console.log('ClientLicenseLineModal Controller: executePopModal');
        //console.log('modal received method event: ' + event);
		var params = event.getParam('arguments');
        //console.log('modal received method parameters: ' + JSON.stringify(params));
        //console.log('Param client: '+ params.clientnameparam);
        //console.log('Param bundle: '+ params.bundlenameparam); 
        //console.log('Param licenses: '+ params.licensesparam);
        if (params) {
            var clientnameparam = params.clientnameparam;
            var bundlenameparam = params.bundlenameparam;
            var licensesparam = params.licensesparam;
            component.set("v.clientname",clientnameparam);
            component.set("v.bundlename",bundlenameparam);
            component.set("v.licenses",licensesparam);
            component.set("v.showmodal",true);
            //var modalcontent = component.find("modal-content");
            //var modalbackdrop = component.find("modal-backdrop");
            //$A.util.removeClass(modalbackdrop, 'slds-hide');
            //$A.util.removeClass(modalcontent, 'slds-hide');
        }        
	},
    closeButton : function(component, event, helper) {
       // console.log('ClientLicenseLineModal Controller: closeButton');
        component.set("v.showmodal",false);
        //helper.closeModal(component);      
    },
    escEvent : function(component, event, helper) {
       // console.log('ClientLicenseLineModal Controller: escEvent');
        //console.log('keypress: ' + event.which);
        //console.log('key: ' + event.key);
        //console.log('code: ' + event.code);
        //console.log('locatoin: ' + event.location);
        if(event.key == 'Escape') {
            //helper.closeModal(component);
            component.set("v.showmodal",false);
        }
    }
})