({
    doInit : function(component, event, helper) {
        console.log('**Hi');
        console.log('**Check multiyear', component.get("v.multiYear"));
    },
    
    handleSuccess : function(component, event, helper) {
      //  console.log('QLC: handleSuccess');      
        var quoteId = event.getParam("id")
        //  var quoteList = component.get('v.quoteList');
        var y = component.get('v.quoteYear');
       
        var d = component.get('v.myStartDate')
        helper.updateQuote(component,event,helper,quoteId,d,y)
    },
    
    handleSubmit : function(component, event, helper) {
        console.log('**QLC: handleSuccess');      
        var fields = event.getParam("fields")
        var y = fields['Quote_Year_Multi_Year__c']
      //  console.log(y)
        component.set('v.quoteYear',y)
    },
    
    goToCPQ : function (component,event,helper){        
      //  console.log('goToCPQ');
        var context = component.get("v.UserContext");
        var opportunityId = component.get('v.opportunity').Id
        var url = '/apex/cpq_VFPage?Id='+component.get('v.opportunity').Id
        if(context != undefined) {
            if(context == 'Theme4t' || context == 'Theme4d') {
                //   console.log('VF in S1 or LEX');
                sforce.one.navigateToURL(url);
            } else {
                //  console.log('VF in Classic'); 
                window.location.assign(url);
            }
        } else {
            //   console.log('standalone Lightning Component');
            var event = $A.get("e.force:navigateToURL");
            event.setParams({"url": url});
            event.fire();
        }
    },
    
    zeroToggle : function(component, event, helper) {
        console.log('zeroToggle')
        component.set('v.showSpinner',true)
        var toggle = component.find('zeroToggle').get('v.value');
        // console.log(toggle)
        component.set('v.zeroLineItems',toggle);     
        helper.updateCd(component,event,helper,'Zero_Dollar_Line_Items__c',toggle);
        helper.updateQuoteToggle(component,event,helper,toggle)
    },
    
    itemizedToggle : function(component, event, helper) {
         console.log('itemizedToggle')
        var toggle = component.find('itemizedToggle').get('v.value');
        component.set('v.itemized',toggle);    
        helper.updateCd(component,event,helper,'Itemized__c',toggle);
    },
    
    discountVisibleToggle : function(component, event, helper) {
          console.log('discountVisibleToggle')
        var toggle = component.find('discountVisibleToggle').get('v.value');
        component.set('v.discountVisible',toggle);    
        helper.updateCd(component,event,helper,'Discount_Visible__c',toggle);
    },
    
    servicesOnlyToggle : function(component, event, helper) {
          console.log('servicesOnlyToggle')
        var toggle = component.find('servicesOnlyToggle').get('v.value');
        component.set('v.servicesOnly',toggle); 
        helper.updateCd(component,event,helper,'Services_Only__c',toggle);
    },
    
    multiYearToggle : function(component, event, helper) {
        console.log('**ContractExample multiYearToggle')
        var toggle = component.find('multiYearToggle').get('v.value');
        component.set('v.multiYear',toggle); 
        helper.updateCd(component,event,helper,'MultiYear_Quote__c',toggle);
    },
    
})