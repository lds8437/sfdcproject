({
    updateQuote : function(component,event,helper,quoteId,d,y){  
        console.log('--------------QLH: updateQuote--------------')
        var Quote = component.get('v.quote');  
        var action = component.get('c.updateQuote');        
        var newItem = {};
        newItem = JSON.parse(JSON.stringify(Quote));
        newItem.Id = quoteId  
        if(y>=1){
            newItem.Multi_Year_Quote__c = true
        }else{
            newItem.Multi_Year_Quote__c = false
        }        
        action.setParams({
            "record": newItem,
            "d" : d,
            "y" : y
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();               
                var opportunityId = component.get('v.opportunity').Id
                helper.reloadFab(component,event,helper)             
            }  
            else if (state === 'ERROR') {
                helper.processErrors(component,response);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    updateQuoteToggle : function(component,event,helper,toggle){  
         console.log('--------------CEH: updateQuotetoggle--------------')
        var quoteId = component.get('v.opportunity').SyncedQuoteId;
        var Quote = component.get('v.quote');         
        var action = component.get('c.updateQuoteRecord');        
        var newItem = {};
        newItem = JSON.parse(JSON.stringify(Quote));
        newItem.Id = quoteId;   
        newItem.Exclude_0_Items_From_PDF__c = toggle    
        
        action.setParams({
            "record": newItem
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                // console.log(dataObj)
                helper.reloadFab(component,event,helper)
                component.set('v.showSpinner',false)
            }  
            else if (state === 'ERROR') {
                var header = 'Quote Update Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    getQuoteList : function(component,event,helper,opportunityId) {
           console.log('MCH: getQuoteList')		
        var action = component.get('c.getQuoteList');
        //console.log(opportunityId)
        action.setParams({
            "opportunityId": opportunityId
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            //console.log(state)
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue(); 
                //  console.log(dataObj)
                
                var total = 0;
                var startDate;
                dataObj.forEach(function(ql){
                    if(ql.Quote_Year_Multi_Year__c != undefined){                      
                        total += ql.TotalPrice;
                        if(ql.Quote_Year_Multi_Year__c === 1 || ql.IsSyncing === true){
                            startDate = ql.License_Start_Date__c;
                        }
                    }  
                })               
                component.set('v.myTotal',total);
                component.set('v.myStartDate',startDate)
                //  helper.updateCd(component,event,helper);
                
                component.set('v.quoteList',dataObj)
                helper.reloadFab(component,event,helper)
            }  
            else if (state === 'ERROR') {
                helper.processErrors(component,response);
            }
        }));
        $A.enqueueAction(action); 
        
    },
    
    updateCd: function(component,event,helper,field,value){
           console.log('CEH: saveCdChanges');
        var recordType = component.get('v.opportunity').RecordType.Name;
        
        var ContractDocument = component.get('v.cdObjInfo');
        
        var action = component.get('c.updateRecord');
        
        var cd = component.get('v.contractDocument');  
        
        var fieldName = field;
        
        var myTotal = 0;
        var newItem = {};
        console.log('1');
        newItem = JSON.parse(JSON.stringify(ContractDocument));
        console.log('2');
        newItem.Id = cd.Id;  
        console.log('3');
        if(field === 'Zero_Dollar_Line_Items__c'){
            newItem.Zero_Dollar_Line_Items__c = value;
        } else if(field === 'Itemized__c'){
            newItem.Itemized__c = value;
            
        } else if(field === 'Discount_Visible__c'){
            newItem.Discount_Visible__c = value;
        } else if(field === 'MultiYear_Quote__c'){
            newItem.MultiYear_Quote__c = value;
        }   
        console.log('4');
        newItem.MultiYear_Start_Date__c = component.get('v.myStartDate');
        console.log('5');
        newItem.MultiYear_End_Date__c = component.get('v.myEndDate');
         console.log('6');
        newItem.MultiYear_Term_in_months__c = component.get('v.myTerm');
         console.log('7');
        //  console.log(component.get('v.myTotal'))
        newItem.MultiYear_Total__c = component.get('v.myTotal');
         console.log('8');
        action.setParams({
            "record": newItem
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            console.log(state)
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                console.log(dataObj)
                // helper.updateQlis(component,event,helper);
            }  
            else if (state === 'ERROR') {
                var header = 'Contract Document Update Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    updateQlis : function(component,event,helper){  
           console.log('--------------CEH: updateQLis--------------')
        var itemized = component.get('v.itemized');
        var discountVisible = component.get('v.discountVisible');
        var quoteLines = component.get('v.quoteLines');
        var discountAmount = component.get('v.opportunity').SyncedQuote.XM_Discount_Amount__c;
        var QuoteLineItem = component.get('v.quoteLineItem');  
        var newItems = [];
        var action = component.get('c.saveQlis');
        
        quoteLines.forEach(function(qli){
            var newItem = {};
            newItem = JSON.parse(JSON.stringify(QuoteLineItem));
            newItem.Id = qli.Id;          
            if(discountAmount >= 0){
                newItem.Contract_PDF_Amount__c = Item_Original_Price__c
            }else{
                if(discountVisible === true){
                    newItem.Contract_PDF_Amount__c = TotalPrice
                }else{
                    newItem.Contract_PDF_Amount__c = Item_Original_Price__c
                }
            }
            newItems.push(newItem);
        });
        action.setParams({
            "records": newItems
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
            }  
            else if (state === 'ERROR') {
                var header = 'Quote Line Item Update Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
     reloadFab :function(component,event,helper){
          console.log('RSC: reloadFab')
        var cmpEvent = $A.get("e.c:fab_Reload");
        cmpEvent.fire();
    },
    
    processErrors : function(component,response,header){
          console.log('MCH: processErrors');
        const errors = response.getError();
        let message = 'Unknown error'; 
        if (errors && Array.isArray(errors) && errors.length > 0) {
            const error = errors[0];
            if (typeof error.message != 'undefined') {
                message = error.message;
            } else if (typeof error.pageErrors != 'undefined' && Array.isArray(error.pageErrors) && error.pageErrors.length > 0) {
                const pageError = error.pageErrors[0];
                if (typeof pageError.message != 'undefined') {
                    message = pageError.message;
                }
            }
        }
        component.set('v.errorHeader',header);
        component.set('v.errorBody',message);
        component.set('v.isError',true);        
        component.set('v.showSpinner',false)
    },
})