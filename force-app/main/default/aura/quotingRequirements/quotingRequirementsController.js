({
   
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
           // record is loaded (render other component which needs record data value)
            console.log("Record is loaded successfully.");
            var quotingRequirements = component.get('v.simpleRecord').Quoting_Requirements__c;
            if(quotingRequirements === null || quotingRequirements === undefined){
                component.set('v.hasQuotingRequirements',false);
            }else{
                var quotingReq = quotingRequirements.split('<br>');
            quotingReq.pop();
            component.set('v.quotingRequirements',quotingReq);
            }
           
        } else if(eventParams.changeType === "CHANGED") {
            // record is changed
             console.log("Record is changed.");
            var quotingRequirements = component.get('v.simpleRecord').Quoting_Requirements__c;
            if(quotingRequirements === null || quotingRequirements === undefined){
                component.set('v.hasQuotingRequirements',false);
            }else{
                var quotingReq = quotingRequirements.split('<br>');
            quotingReq.pop();
            component.set('v.quotingRequirements',quotingReq);
            }
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        }
    }
})