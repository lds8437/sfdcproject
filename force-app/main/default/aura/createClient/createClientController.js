({
    handleClientSubmitted : function(component, event, helper) {
        var spinner = component.find("spinner");
        $A.util.removeClass(spinner, "slds-hide");
        //console.log('client submitted debug input: ' + JSON.stringify(event));
        var client = event.getParam("fields");
        component.set("v.client", client);
        //console.log('client attribute after submit' + JSON.stringify(component.get("v.client")));
        //Do some validation here
        var isValid = true;
        var validationerrors = [];
        //Verify account name filled out
        if(!client.Name || client.Name.value === null){
            isValid = false;
            validationerrors.push('Client Name is required.');            
        }
        //Verify bill to name filled out
        if(!client.BillToName__c || client.BillToName__c.value === null){
            isValid = false;
            validationerrors.push('Bill To Name is required.');            
        }
        //If different billing address checked, verify billing address fields filled out
        if(component.get("v.newbillingaddr")){
            if(!client.Billing_Street_Address__c || client.Billing_Street_Address__c.value === null){
                isValid = false;
                validationerrors.push('Billing Street Address is required.');            
            }
            if(!client.Billing_City__c || client.Billing_City__c.value === null){
                isValid = false;
                validationerrors.push('Billing City is required.');            
            }
            if(!client.Billing_State_Province__c || client.Billing_State_Province__c.value === null){
                isValid = false;
                validationerrors.push('Billing State/Province is required.');            
            }
            if(!client.Billing_Zip_Postal_Code__c || client.Billing_Zip_Postal_Code__c.value === null){
                isValid = false;
                validationerrors.push('Billing Zip/Postal Code is required.');            
            }
        }        
        //Verify billing country filled out
        if(!client.BillingCountry2__c || client.BillingCountry2__c.value === null){
            isValid = false;
            validationerrors.push('Billing Country is required.');            
        }
        //If different end user address checked, verify end user address fields filled out
        if(component.get("v.newendclientaddr")){
            if(!client.Shipping_Street__c || client.Shipping_Street__c.value === null){
                isValid = false;
                validationerrors.push('End User/Shipping Street is required.');            
            }
            if(!client.Shipping_City__c || client.Shipping_City__c.value === null){
                isValid = false;
                validationerrors.push('End User/Shipping City is required.');            
            }
            if(!client.Shipping_State__c || client.Shipping_State__c.value === null){
                isValid = false;
                validationerrors.push('End User/Shipping State is required.');            
            }
            if(!client.Shipping_Zip_Postal_Code__c || client.Shipping_Zip_Postal_Code__c.value === null){
                isValid = false;
                validationerrors.push('End User/Shipping Zip/Postal Code is required.');            
            }
            if(!client.ShippingCountry2__c || client.ShippingCountry2__c.value === null){
                isValid = false;
                validationerrors.push('End User/Shipping Country is required.');            
            }
        }
        if(!isValid){
            component.set("v.validationmessages", validationerrors);
            $A.util.addClass(spinner, "slds-hide");
            var errorbanner = component.find("errormessages");
            $A.util.removeClass(errorbanner, "slds-hide");
        }
        if(isValid){
            //Set up variables for Apex call
            //component.set("v.client", {'Account__c': component.get("v.accountId")});
            var action = component.get("c.newClient");
            action.setParams({ 
                accountid : component.get("v.accountId"),
                client : component.get("v.client"),
                newbillingadd : component.get("v.newbillingaddr"),
                newendclientadd : component.get("v.newendclientaddr")
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var newclientid = response.getReturnValue();
                    console.log(newclientid);
                    var interfacecontext = component.get("v.interfacecontext");
                    console.log('interface context ' + interfacecontext);
                    if(interfacecontext != ''){
                        console.log('setting up client created application event');
                        var newClientEvent = $A.get("e.c:clientCreated");
                        console.log('got client created application event');
                        newClientEvent.setParams({
                            "clientid" : newclientid ,
                            "interfacecontext" : component.get("v.interfacecontext")
                        });
                        console.log('set up params');
                        newClientEvent.fire();
                        console.log('create client app event fired');
                    }
                    $A.util.addClass(spinner, "slds-hide");
                }
                else if (state === "INCOMPLETE") {
                    console.log('add new client operation incomplete');
                    $A.util.addClass(spinner, "slds-hide");
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                        $A.util.addClass(spinner, "slds-hide");
                    }
            });
            $A.enqueueAction(action);
            // console.log('create client action enqueued');
        }
    },
})