({
	doInit : function(component, event, helper) {
        var updateCall = component.get("c.revertOppHandover");
        updateCall.setParams({
            "id": component.get("v.recordId")
        });
        updateCall.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Record Updated",
                    "message": "The opportunity has been updated successfully."
                });
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            } else if (state === "ERROR") {
                console.log("Error reverting opp handover");
            } else {
                console.log("Unknown problem reverting opp handover");
            }
        })
        $A.enqueueAction(updateCall);
	}
})