({
    saveCdChanges: function(component, event, helper) { 
        //console.log('CSH: saveCdChanges');
        var cd = component.get('v.contractDocument');
        cd.Contract_Method_Type__c = component.get('v.contractMethodType');
        component.set('v.contractDocument',cd);
        var cmpEvent = $A.get("e.c:fab_saveCdChanges");
        cmpEvent.setParams({
            "contractDocument": cd
        });
        cmpEvent.fire();
    },
    
    urlDirection : function(component,event,helper,url){
      //  console.log('RBH: urlDirection')
        
        //Detect if in Visualforce domain
        if(window.location.hostname.includes('visual.force.com')){//If in Visualforce domain
            //Navigation in the salesforce1/lightning app
           // console.log('typeof sforce' + typeof sforce);
            if( (typeof sforce != 'undefined') && sforce && (!!sforce.one) ) {
              //  console.log('using sforce one navigation');
                
                window.open(url)
                //sforce.one.navigateToURL(url);
                //sforce.one.back()
            }
            else {
                //Navigation in classic.
              //  console.log('using window.location navigation');
                window.location.assign(url);
            }
        }
        else{//If not in Visualforce domain
          //  console.log('using lightning event navigation');
            var event = $A.get("e.force:navigateToURL");
            event.setParams({"url": url});
            event.fire();
        }
        component.set('v.showSpinner',false)
    },
    
    processErrors : function(component,response){
      //  console.log('process errors')
        // Retrieve the error message sent by the server
        const errors = response.getError();
        let message = 'Unknown error'; // Default error message
        if (errors && Array.isArray(errors) && errors.length > 0) {
            const error = errors[0];
            if (typeof error.message != 'undefined') {
                message = error.message;
            } else if (typeof error.pageErrors != 'undefined' && Array.isArray(error.pageErrors) && error.pageErrors.length > 0) {
                const pageError = error.pageErrors[0];
                if (typeof pageError.message != 'undefined') {
                    message = pageError.message;
                }
            }
            console.error('Error: '+ message);
            console.error(JSON.stringify(errors));
        } 
        
    },
    
})