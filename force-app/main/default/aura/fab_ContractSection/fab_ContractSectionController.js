({
    doInit : function(component,event,helper){
     //   console.log('CSC: doinit');     
     //   console.log(component.get('v.client'))
    },
    
    handleMenuSelect : function(component, event, helper) {
     //   console.log('handleMenuSelect');
        var selectedMenuItemValue = event.getParam("value");
        component.set('v.contractMethodType',selectedMenuItemValue);
       
        helper.saveCdChanges(component,event,helper);
    },
 
    openQSOModel : function(component,event,helper){
        component.set('v.qsoIsOpen',true);
    },
    
    onQSOClose : function(component,event,helper){
        component.set('v.qsoIsOpen',false);
    }, 
    
    openModel: function(component, event, helper) {   
     //   console.log('Open Modal');
        var recordId = event.getSource().get("v.value").trim();
        var docusignStatusRecords = component.get('v.docusignStatusRecords');
        _.forEach(docusignStatusRecords,function(s){
            if(s.Id === recordId){
                // console.log(s);
                component.set('v.docusignStatusId',recordId);  
                s.isOpen = true;
            }
        });     
        component.set('v.docusignStatusRecords',docusignStatusRecords);
    },
    
    onClose: function(component, event, helper) {
      //  console.log('onClose');
        var docusignStatusRecords = component.get('v.docusignStatusRecords');
        _.forEach(docusignStatusRecords,function(s){
            s.isOpen = false;
        });  
        component.set('v.docusignStatusRecords',docusignStatusRecords);
    },
    
    recordUpdated: function(component, event, helper) {
        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") { 
            
        }
        else if (changeType === "LOADED") {  
        }
            else if (changeType === "REMOVED") { 
            }
                else if (changeType === "CHANGED") { 
                    console.log('recordhaschanged');
                    
                }
    },
    
    openStatusRecords: function(component,event,helper){
         var context = component.get("v.UserContext");
        var docusignStatusId = event.getSource().get("v.value").trim();
        var url = '/apex/fab_StatusRecords?id='+docusignStatusId; 
        + '?retURL=/apex/OnboardingLightning?Id='+component.get('v.opportunity.Id')
      //  console.log(url)
        helper.urlDirection(component,event,helper,url)
       
        
    }
  
    
 /* previewQSO : function(component,event,helper){
        var target = event.getSource();
        var buttonName = target.get("v.value");
        helper.getCongaMetaData(component,event,helper,buttonName);       
    },*/
    
})