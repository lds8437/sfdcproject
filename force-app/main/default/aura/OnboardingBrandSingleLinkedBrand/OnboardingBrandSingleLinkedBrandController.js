({
    handleBrandRecordUpdated: function(cmp,event,helper){
        //Handles loading data
        var eventParams = event.getParams();
        console.log("Error updating brand", cmp.get("v.brandRecordError"));
        if(eventParams.changeType === "LOADED") {
            //record is loaded
            //console.log('brand reloaded' + cmp.get('v.brandId'));
            //Fire event to recalculate the brand summary.
            const brandName = cmp.get("v.simpleBrandRecord.Name");
            const existingBrandId = cmp.get("v.simpleBrandRecord.Existing_Brand_Id__c");
            const requestedBrandId = cmp.get("v.simpleBrandRecord.Requested_Brand_Id__c");
            cmp.set("v.brandCardTitle", existingBrandId ? `${brandName} - ${existingBrandId}` : `${brandName} - ${requestedBrandId}`);
            var compEvent = cmp.getEvent("recalculateBrands");
            compEvent.fire();
        } else if(eventParams.changeType === "CHANGED") {
            // record is changed
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        }  
    },
    deleteBrand: function(cmp,event,helper){
        var r = confirm("You are about to delete this record. Are you sure?");
        if (r){
            console.log("fire the delete event!");
            var brandId = cmp.get('v.brandId');
            var deleteEvent = cmp.getEvent("deleteBrandEvent");
            deleteEvent.setParams({
                "brandId": brandId
            });
            deleteEvent.fire();
        }
    },
    handleBrandUpdated : function(cmp,event,helper){
        
        var brandId = event.getParam("brandId");
        var thisBrandId = cmp.get("v.brandId");
        //console.log('heard brand updated event with brandid ' + brandId + ' this brand id is ' + thisBrandId);
        if(brandId == thisBrandId){
            cmp.find("brandRecordLoader").reloadRecord();
        }
    },
    selectBrand: function(cmp,event,helper){
        var compEvent = cmp.getEvent("selectBrandEvent");
        compEvent.setParams({"brandId" : cmp.get('v.brandId') });
        compEvent.fire();
        cmp.getEvent("clearUnsavedChanges").fire();
    },
    sendBrandStats: function(cmp,event,helper){
        //cmp.find("brandRecordLoader").reloadRecord();
        var brand = cmp.get('v.simpleBrandRecord');
        if(brand != null){
        //var admins = brand.Brand_Admin_Ids__c;
        //var merge = brand.Brands_To_Merge__c;
        //var users = brand.Users_To_Move__c;
        var admincount = 0;
        var mergecount = 0;
        var usercount = 0;
        console.log('brand info' + JSON.stringify(brand));
        console.log('brand admin ids' + brand.Brand_Admin_Ids__c);
        if(brand.Brand_Admin_Ids__c != null){
            var adminarray = brand.Brand_Admin_Ids__c.split(',');
            admincount = adminarray.length;
        }
        if(brand.Brands_To_Merge__c != null){
            var mergearray = brand.Brands_To_Merge__c.split(';');
            mergecount = mergearray.length;
        }
        if(brand.Users_To_Move__c != null){
            var userarray = brand.Users_To_Move__c.split(';');
            usercount = userarray.length;
        }
        var returnobject = {};
        returnobject.admincount = admincount;
        returnobject.mergecount = mergecount;
        returnobject.usercount = usercount;
        return returnobject;
        }
        else{
            return null;
        }
    },
})