<aura:component controller="OnboardingLightningController" implements="force:appHostable,flexipage:availableForAllPageTypes,flexipage:availableForRecordHome,force:hasRecordId" access="global" >
    <aura:attribute name="infoUnknown" type="Boolean" default="false" />
    <aura:attribute name="newBrand" type="Object"/>
    <aura:attribute name="simpleNewBrand" type="Object"/>
    <aura:attribute name="newBrandError" type="String"/>
    <aura:attribute name="newBrandFields" type="List" default="Name,Opportunity__c,Brand_Type__c,Client_Has_Existing_Brand__c,Existing_Brand_Id__c,Requested_Brand_Id__c,SingleUserorEntryLicense__c,Data_Center__c,EMEAOnlyAccess__c,Enable_Self_Enrollment__c,Valid_Email_Domain_s__c,Brand_Admin_Exists_As_User__c" />
    <aura:attribute name="opp" type="Object"/>
    <aura:attribute name="tempOpp" type="Object"/>
    <aura:attribute name="oppError" type="String"/>
    <aura:attribute name="oppFields" type="List" default="Name,AccountId,Brand_Info_Unknown_at_Time_of_Invoice__c" />
    <aura:attribute name="clientName" type="String" />
    <aura:attribute name="opportunityId" type="String" />
    <aura:attribute name="brandTypes" type="Object[]" />
    <aura:attribute name="dataCenters" type="Object[]" />
    <aura:attribute name="hasRC1Product" type="Boolean" default="false" />
    <aura:attribute name="showSpinner" type="Boolean" default="false" />
    <aura:attribute name="isLightningOut" type="Boolean" default="false" />
    <aura:registerEvent name="newBrandEvent" type="c:OnboardingNewBrandEvent" />
    <aura:registerEvent name="deleteBrandsEvent" type="c:OnboardingDeleteBrandsEvent" />
    <aura:handler name="init" value="{!this}" action="{!c.doInit}" />
    <aura:handler name="change" value="{!v.clientName}" action="{!c.handleClientNameChange}" />
    <aura:handler name="change" value="{!v.opportunityId}" action="{!c.handleOppIdChange}" />

    <force:recordData aura:id="brandCreator" 
                      fields="{!v.newBrandFields}"
                      targetRecord="{!v.newBrand}"
                      targetFields="{!v.simpleNewBrand}"
                      targetError="{!v.newBrandError}" />

    <force:recordData aura:id="oppRecordLoader" 
                      recordId="{!v.opportunityId}"
                      fields="{!v.oppFields}"
                      mode="EDIT"
                      targetRecord="{!v.opp}"
                      targetFields="{!v.tempOpp}"
                      targetError="{!v.oppError}" />

    <lightning:notificationsLibrary aura:id="brandSetupNotifLib" />

    <aura:if isTrue="{!v.showSpinner}">
        <lightning:spinner alternativeText="Saving..." variant="brand" />
    </aura:if>
    <div class="slds-form slds-form_stacked">
        <lightning:input aura:id="brandinfo-unknown" name="brandinfo-unknown" type="checkbox" label="Brand Info Unknown at Time of Invoice" checked="{!v.tempOpp.Brand_Info_Unknown_at_Time_of_Invoice__c}" onchange="{! c.handleUnknown}" />
        <aura:if isTrue="{! !v.tempOpp.Brand_Info_Unknown_at_Time_of_Invoice__c}">
            <lightning:input aura:id="brandinfo-field" name="brandinfo-name" type="text" label="Organization Name" value="{!v.simpleNewBrand.Name}" required="true" />
            <lightning:combobox aura:id="brandinfo-field" name="brandinfo-type" label="Brand Type" value="{!v.simpleNewBrand.Brand_Type__c}" placeholder="Select Brand Type" options="{! v.brandTypes }" required="true" />
            <lightning:input aura:id="brandinfo-existing" name="brandinfo-existing" type="checkbox" label="Deal should be linked to an existing brand" checked="{!v.simpleNewBrand.Client_Has_Existing_Brand__c}" />
            <aura:if isTrue="{!v.simpleNewBrand.Client_Has_Existing_Brand__c}">
                <lightning:input aura:id="brandinfo-field" name="brandinfo-existingId" type="text" label="Existing Brand Id" value="{!v.simpleNewBrand.Existing_Brand_Id__c}" required="true" pattern="[a-z0-9]{3,20}" messageWhenPatternMismatch="Existing Brand Id must be between 3 and 20 characters long, lowercase letters and numbers only (no special characters, punctuations, or spaces)" />
            </aura:if>
            <!--
            <aura:if isTrue="{!and(v.hasRC1Product, !v.simpleNewBrand.Client_Has_Existing_Brand__c)}">
                <lightning:input aura:id="brandinfo-single-or-entry" name="brandinfo-single-or-entry" type="checkbox" label="Single User or Entry License" checked="{!v.simpleNewBrand.SingleUserorEntryLicense__c}" />
            </aura:if>
            -->
            <aura:if isTrue="{!and(!v.hasRC1Product, !v.simpleNewBrand.Client_Has_Existing_Brand__c)}">
                <aura:if isTrue="{! !v.isLightningOut}">
                    <span style="float:left; margin-right:0.25rem">
                    <lightning:helptext
                        content="This ID will appear in the Base URL of the organization and on survey URLs sent to the client (e.g. https://brandid.qualtrics.com). Brand IDs cannot be changed once the brand is created, so be sure to check with the client on their desired Brand ID."
                    />
                </span>
                    </aura:if>
                <lightning:input aura:id="brandinfo-field" name="brandinfo-requestedId" type="text" label="Requested Brand Id" value="{!v.simpleNewBrand.Requested_Brand_Id__c}" required="true" pattern="[a-z0-9]{3,20}" messageWhenPatternMismatch="Request Brand Id must be between 3 and 20 characters long, lowercase letters and numbers only (no special characters, punctuations, or spaces)" />
            <aura:if isTrue="{!v.isLightningOut}">
                <div class="slds-form-element__help">This ID will appear in the Base URL of the organization and on survey URLs sent to the client (e.g. https://brandid.qualtrics.com). Brand IDs cannot be changed once the brand is created, so be sure to check with the client on their desired Brand ID.</div>
                </aura:if>
            </aura:if>    
            <lightning:combobox aura:id="brandinfo-field" name="brandinfo-data-center" label="Data Center" value="{!v.simpleNewBrand.Data_Center__c}" placeholder="Select Data Center" options="{! v.dataCenters }" required="true" />
            <lightning:input aura:id="brandinfo-emea" name="brandinfo-emea" type="checkbox" label="Client is in EMEA and their data should only be accessed by EMEA based Qualtrics employees" checked="{!v.simpleNewBrand.EMEAOnlyAccess__c}" />
            <lightning:input aura:id="brandinfo-self-enrollment" name="brandinfo-self-enrollment" type="checkbox" label="Enable self-enrollment for this brand" checked="{!v.simpleNewBrand.Enable_Self_Enrollment__c}" />
            <aura:if isTrue="{!v.simpleNewBrand.Enable_Self_Enrollment__c}">
                <lightning:input aura:id="brandinfo-field" name="brandinfo-email-domains" type="text" label="Valid Email Domains for User Enrollment" value="{!v.simpleNewBrand.Valid_Email_Domain_s__c}" required="true" />
            </aura:if>
            <lightning:input aura:id="brandinfo-admin" name="brandinfo-admin" type="checkbox" label="Brand administrator for this brand already exists as a Qualtrics user" checked="{!v.simpleNewBrand.Brand_Admin_Exists_As_User__c}" />
            <aura:if isTrue="{!v.simpleNewBrand.Brand_Admin_Exists_As_User__c}">
                <lightning:input aura:id="brandinfo-field" name="brandinfo-existing-admin" type="email" label="Brand Admin Username" value="{!v.simpleNewBrand.Existing_Brand_Admin_Username__c}" required="true" />
            </aura:if>
            <lightning:button variant="brand" label="Add Brand to Create" title="Add Brand to Create" onclick="{!c.handleCreateBrand}" />
        </aura:if>
    </div>
</aura:component>