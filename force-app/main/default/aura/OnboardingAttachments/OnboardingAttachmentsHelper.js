({
    MAX_FILE_SIZE: 4500000, //Max file size 4.5 MB
    CHUNK_SIZE: 750000,      //Chunk Max size 750Kb

    getAttachmentList: function (cmp) {
        cmp.set("v.showLoadingSpinner", true);
        // Prepare the action to load account record
        var action = cmp.get("c.getAttachmentList");
        action.setParams({
            "OppId": cmp.get("v.OpportunityId")
        });

        // Configure response handler
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                cmp.set("v.AttachmentList", response.getReturnValue());
                cmp.set("v.showLoadingSpinner", false);
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    uploadFile  : function (cmp , event) {
        //AddAttachment(String OppId , String fileName ,  Blob fileBody
        cmp.set("v.showLoadingSpinner", true);
        var fileInput = event.getSource().get("v.files");;
        // get the first file using array index[0]
        var file = fileInput[0];
        var self = this;


        var objFileReader = new FileReader();
        var self = this;
        objFileReader.onload = $A.getCallback(function() {
            var fileContents = objFileReader.result;
            var base64 = 'base64,';
            var dataStart = fileContents.indexOf(base64) + base64.length;

            fileContents = fileContents.substring(dataStart);
            // call the uploadProcess method
            self.uploadProcess(cmp, file, fileContents);
        });
        objFileReader.readAsDataURL(file);

    },
    uploadProcess: function(cmp, file, fileContents) {
        // set a default size or startpostiton as 0
        console.log ('uploadProcess ') ;
        var startPosition = 0;
        // calculate the end size or endPostion using Math.min() function which is return the min. value
        var endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);
        var getchunk = fileContents.substring(startPosition, endPosition );
        // start with the initial chunk, and set the attachId(last parameter)is null in begin
        var action = cmp.get("c.AddAttachment");
        action.setParams({
            "OppId": cmp.get("v.OpportunityId"),
            "fileName": file.name,
            "base64Data": encodeURIComponent(getchunk),
            "contentType": file.type
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                alert('your File is uploaded successfully');
                cmp.set("v.showLoadingSpinner", false);
                this.getAttachmentList (cmp) ;
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },

})