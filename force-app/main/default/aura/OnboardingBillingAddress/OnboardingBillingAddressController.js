({
    doInit : function(component, event, helper) {
        helper.getPicklistOptions(component);
    },
    // If the client ID has changed, reload the Billing Address information with values pre-populated from the client.
    //   Set initialized variable to true after first load to ensure opportunity billing address values are set to default and not overwritten.
    handleClientChange : function(component, event, helper) {
        const currentClientId = component.get("v.simpleClientRecord.Id");
        const newClientId = component.get("v.clientId");
        //console.log("CLIENTIDS:", currentClientId, newClientId);
        if (currentClientId !== newClientId) {
            // console.log("CLIENT HAS CHANGED");
            const overwrite = component.get("v.initialized");
            component.find("clientRecordLoader").reloadRecord(false, $A.getCallback(function(reloadResult) {
                // console.log("CLIENT RECORD RELOAD");
                // console.log("Error?", component.get("v.clientError"));
                //  console.log("simpleClientRecord", component.get("v.simpleClientRecord"));
                if (component.get("v.simpleClientRecord")) {
                    //  console.log("CLIENT RECORD RELOAD COMPLETE");
                    const prepopulatedOpp = helper.prepopulateAddress(component.get("v.tempOpp"), component.get("v.simpleClientRecord"), overwrite);
                    component.set("v.tempOpp", prepopulatedOpp);
                    if (overwrite) {
                        component.find("oppRecordLoader").saveRecord($A.getCallback(function(saveResult) {
                            if (saveResult.state === "INCOMPLETE") {
                                console.log("User is offline, device doesn't support drafts.");
                            } else if (saveResult.state === "ERROR") {
                                const errors = saveResult.error;
                                let errorMessage;
                                if (errors && errors[0] && errors[0].message) {
                                    errorMessage = errors[0].message;
                                } else {
                                    errorMessage = "Unknown error.";
                                }
                                console.log("ERROR:", errorMessage);
                            } else if (saveResult.state !== "SUCCESS" && saveResult.state !== "DRAFT") {
                                console.log("Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error));
                            }
                        }));
                    }
                    component.set("v.initialized", true);
                    component.set("v.showSpinner", false);
                }
            }));
        }
    },
    handleOppChange : function(component, event, helper) {
        component.find("oppRecordLoader").reloadRecord(false, $A.getCallback(function(reloadResult) {
            if (component.get("v.simpleClientRecord")) {
                const prepopulatedOpp = helper.prepopulateAddress(component.get("v.tempOpp"), component.get("v.simpleClientRecord"), false);
                component.set("v.tempOpp", prepopulatedOpp);
                component.set("v.showSpinner", false);
            }
        }));
    },
    handleSameAsBilling : function(component, event, helper) {
        const isChecked = component.find("same-as-billing").get("v.checked");
        const shipToName = isChecked ? component.get("v.tempOpp.Sales_Order_Bill_To_Name__c") : "";
        const street = isChecked ? component.get("v.tempOpp.Sales_Order_Billing_Street_Address__c") : "";
        const additional = isChecked ? component.get("v.tempOpp.Sales_Order_Additional_Billing_Street__c") : "";
        const city = isChecked ? component.get("v.tempOpp.Sales_Order_Billing_City__c") : "";
        const stateProvince = isChecked ? component.get("v.tempOpp.Sales_Order_Billing_State_Province__c") : "";
        const zipPostal = isChecked ? component.get("v.tempOpp.Sales_Order_Billing_ZIP_Postal_Code__c") : "";
        const country = isChecked ? component.get("v.tempOpp.Sales_Order_Billing_Country__c") : "";
        
        component.set("v.tempOpp.Sales_Order_Ship_To_Name__c", shipToName);
        component.set("v.tempOpp.Sales_Order_Shipping_Street_Address__c", street);
        component.set("v.tempOpp.Sales_Order_Additional_Shipping_Street__c", additional);
        component.set("v.tempOpp.Sales_Order_Shipping_City__c", city);
        component.set("v.tempOpp.Sales_Order_Shipping_State_Province__c", stateProvince);
        component.set("v.tempOpp.Sales_Order_Shipping_ZIP_Postal_Code__c", zipPostal);
        component.set("v.tempOpp.Sales_Order_Shipping_Country__c", country);
    },
    handleClick : function(component, event, helper) {
        const allValid = component.find("address-field").reduce(function(validSoFar, field) {
            field.reportValidity();
            return validSoFar && field.checkValidity();
        }, true);
        var billingStateValidity = component.find("billing-state").checkValidity();
        var shippingStateValidity = component.find("shipping-state").checkValidity();
        // console.log('initial billing state validity ' + billingStateValidity);
        //Check validity for 2 letter abbreviation if Country is United States for Billing Address and display invalid message if applicable.
        var billingcountry = component.get('v.tempOpp.Sales_Order_Billing_Country__c');
        if (billingcountry == 'US' || billingcountry == 'USA' || billingcountry == 'United States' || billingcountry == 'United States of America'){
            var billingStateInput = component.find("billing-state");
            var billingStateValue = billingStateInput.get("v.value");
            billingStateValidity = helper.validateStateAbbreviation(billingStateValue);
            if (billingStateValidity === true) {
                billingStateInput.setCustomValidity("");
            } else {
                billingStateInput.setCustomValidity("Enter a valid capitalized two-letter State Abbreviation for this United States address");
            }
            billingStateInput.reportValidity(); 
        }
        //  console.log('final billing state validity ' + billingStateValidity);
        //Check validity for 2 letter abbreviation if Country is United States for Shipping Address and display invalid message if applicable.
        var shippingcountry = component.get('v.tempOpp.Sales_Order_Shipping_Country__c');
        if (shippingcountry == 'US' || shippingcountry == 'USA' || shippingcountry == 'United States' || shippingcountry == 'United States of America'){
            var shippingStateInput = component.find("shipping-state");
            var shippingStateValue = shippingStateInput.get("v.value");
            shippingStateValidity = helper.validateStateAbbreviation(shippingStateValue);
            if (shippingStateValidity === true) {
                shippingStateInput.setCustomValidity("");
            } else {
                shippingStateInput.setCustomValidity("Enter a valid capitalized two-letter State Abbreviation for this United States address");
            }
            shippingStateInput.reportValidity(); 
        }
        if (allValid && billingStateValidity && shippingStateValidity) {
            /*
            const tempOpp = component.get("v.tempOpp");
            const saveOppEvent = component.getEvent("saveOpportunity");
            saveOppEvent.setParam("oppObject", tempOpp);
            saveOppEvent.fire();
            */
            component.set("v.showSpinner", true);
            component.find("oppRecordLoader").saveRecord($A.getCallback(function(saveResult) {
                const fabReloadEvent = $A.get("e.c:fab_Reload");
                if(fabReloadEvent !== undefined){
                    fabReloadEvent.fire();
                }
                if (saveResult.state === "INCOMPLETE") {
                    if(component.get('v.isLightningOut') === false){
                        component.find('addressNotifLib').showToast({
                            "title": "There was a problem saving the Opportunity",
                            "message": "User is offline, device doesn't support drafts.",
                            "variant": "error"
                        });
                    }
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    const errors = saveResult.error;
                    let errorMessage;
                    if (errors && errors[0] && errors[0].message) {
                        errorMessage = errors[0].message;
                    } else {
                        errorMessage = "Unknown error.";
                    }
                    if(component.get('v.isLightningOut') === false){
                        component.find('addressNotifLib').showToast({
                            "title": "There was a problem saving the Opportunity",
                            "message": "Error: " + errorMessage,
                            "variant": "error"
                        });
                    }
                } else if (saveResult.state !== "SUCCESS" && saveResult.state !== "DRAFT") {
                    if(component.get('v.isLightningOut') === false){
                        component.find('addressNotifLib').showToast({
                            "title": "There was a problem saving the Opportunity",
                            "message": "Error: " + "Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error),
                            "variant": "error"
                        });
                    }
                    console.log("Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error));
                }
                component.set("v.showSpinner", false);
            }));
        }
    }
})