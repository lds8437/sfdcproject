({
    doInit : function(component,event,helper){
      //  console.log('RSC: doInit')
        var arecordId = component.get('v.opportunity').Id; 
        var cdId = component.get('v.contractDocument').Id;
        //    console.log(component.get('v.contractDocument'))
        //  helper.getLegalContact(component,event,helper);
        helper.getFiles(component,helper,arecordId);
        helper.getAttachments(component,event,helper,cdId)
        //   helper.getSessionId(component,event,helper);
        
    },
    
    uploadFile : function(component,event,helper){
      //  console.log('RSC: uploadfile')
        component.set('v.noFile',false);    
        component.set('v.showSpinner', true);
        
        var fileInput = component.find("file").getElement();
        var file = fileInput.files[0];   
        
        if (file.size > 4500000) {
            alert('File size cannot exceed ' + 4500000 + ' bytes.\n' +
    		  'Selected file size: ' + file.size);
    	    return;
        }
        
        if(file === undefined){
            component.set('v.noFile',true);
            component.set('v.saving',false);
            component.set('v.showSpinner',false);
        }else{
            console.time("fileupload");
            var fr = new FileReader();
            fr.onload = function() {
                var target = event.getSource();        
                var description = target.get("v.name");
                var fileContents = fr.result;
                var base64Mark = 'base64,';
                var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
                
                fileContents = fileContents.substring(dataStart);
                
                helper.upload(component, file, fileContents,description,helper);
            };
            fr.readAsDataURL(file); 
        }
    },
    
    deleteFile : function(component,event,helper){
      //  console.log('RSC: deleteFile')
        component.set('v.showSpinner', true);
        var target = event.getSource();        
        var fileId = target.get("v.value");
        var description = target.get("v.name");
        //  console.log(fileId);
        helper.deletePOAttachment(component,event,helper,fileId,component.get('v.opportunity').Id,description);
    },
    
    reloadFab :function(component,event,helper){
      //  console.log('RSC: reloadFab')
        helper.reloadFab(component,event,helper);
    },
    
    onVerify : function (component,event,helper){   
       // console.log('RSC: onVerify')
        component.set('v.verified',true);
    },
    
    handleQRUpdated: function(component, event, helper) {
      //  console.log('handleQRUpdated')
        var eventParams = event.getParams();
        var hasQuotingRequirements = true;
        if(eventParams.changeType === "LOADED") {
            // record is loaded (render other component which needs record data value)
           // console.log("Record is loaded successfully.");
            var quotingRequirements = component.get('v.qr').Quoting_Requirements__c;
            if(quotingRequirements === null || quotingRequirements === undefined){
                hasQuotingRequirements = false
                
            }else{
                var quotingReq = quotingRequirements.split('<br>');
                quotingReq.pop();
                component.set('v.quotingRequirements',quotingReq);
            }
            component.set('v.hasQuotingRequirements',hasQuotingRequirements);
           // console.log(component.get('v.hasQuotingRequirements'))            
        } else if(eventParams.changeType === "CHANGED") {
            // record is changed
           //console.log("Record is changed.");
            var quotingRequirements = component.get('v.qr').Quoting_Requirements__c;
            if(quotingRequirements === null || quotingRequirements === undefined){
                hasQuotingRequirements = false
                
            }else{
                var quotingReq = quotingRequirements.split('<br>');
                quotingReq.pop();
                component.set('v.quotingRequirements',quotingReq);
            }
            component.set('v.hasQuotingRequirements',hasQuotingRequirements);
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        }
    },
    
    handleBRUpdated: function(component, event, helper) {
     //   console.log('handleBRUpdated')
        var eventParams = event.getParams();        
        var hasInvoiceRequirements = true;
        if(eventParams.changeType === "LOADED") {
            // record is loaded (render other component which needs record data value)
            //console.log("Record is loaded successfully.");
            var invoiceRequirements
            if(component.get('v.br').Onboarding_Requirements__c === null || component.get('v.br').Onboarding_Requirements__c === undefined ){
                invoiceRequirements = component.get('v.br').Ready_to_Invoice__c;
            }else{
                invoiceRequirements = component.get('v.br').Onboarding_Requirements__c + "<br>" + component.get('v.br').Ready_to_Invoice__c;
            }
         //  console.log('invoiceRequirements')
        //   console.log(invoiceRequirements)
            if(invoiceRequirements === null || invoiceRequirements === undefined || invoiceRequirements === 'Click Finish and Bill<br>' || invoiceRequirements === 'Submitted for Invoice'){
                hasInvoiceRequirements = false;
            }else{
                var invoiceReq = invoiceRequirements.split('<br>');
              //  console.log(invoiceReq)
                invoiceReq.pop();
               // console.log(invoiceReq)
                component.set('v.invoiceRequirements',invoiceReq);
            }
         //   console.log('hasInvoiceRequirements')
          //  console.log(hasInvoiceRequirements)
            component.set('v.hasInvoiceRequirements',hasInvoiceRequirements);
            
        } else if(eventParams.changeType === "CHANGED") {
            // record is changed
            //console.log("Record is changed.");
            var invoiceRequirements = component.get('v.br').Onboarding_Requirements__c;
            
            
            if(invoiceRequirements === null || invoiceRequirements === undefined){
                hasInvoiceRequirements = false;
                
            }else{
                var invoiceReq = invoiceRequirements.split('<br>');
                invoiceReq.pop();
                component.set('v.invoiceRequirements',invoiceReq);
            }
            component.set('v.hasInvoiceRequirements',hasInvoiceRequirements);
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        }
    },
    
    openStatusRecords: function(component,event,helper){
         var context = component.get("v.UserContext");
        var docusignStatusId = event.getSource().get("v.value").trim();
        var url = '/apex/fab_StatusRecords?id='+docusignStatusId; 
        + '?retURL=/apex/OnboardingLightning?Id='+component.get('v.opportunity.Id')
      //  console.log(url)
        helper.urlDirection(component,event,helper,url)
       
        
    }
    
})