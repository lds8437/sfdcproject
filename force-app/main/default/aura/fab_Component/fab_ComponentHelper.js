({
    
    getOpportunity : function(component,event,helper) {
        console.log('CH: getOpportunity');
        var opportunityId = component.get('v.opportunityId');
        var action = component.get('c.getOpportunity');
        
        action.setParams({
            "opportunityId": opportunityId
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                component.set('v.opportunity',dataObj);
            } 
            else if (state === 'ERROR') {
                helper.processErrors(component,response);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    processErrors : function(component,response){
        // Retrieve the error message sent by the server
        const errors = response.getError();
        let message = 'Unknown error'; // Default error message
        if (errors && Array.isArray(errors) && errors.length > 0) {
            const error = errors[0];
            if (typeof error.message != 'undefined') {
                message = error.message;
            } else if (typeof error.pageErrors != 'undefined' && Array.isArray(error.pageErrors) && error.pageErrors.length > 0) {
                const pageError = error.pageErrors[0];
                if (typeof pageError.message != 'undefined') {
                    message = pageError.message;
                }
            }
        }
        // Display error in console
        console.error('Error: '+ message);
        console.error(JSON.stringify(errors));
        
        component.find('notifLib').showToast({
            "title": "Error",
            "message": message,
            "variant":"error"
        });
    },
    
    
    
})