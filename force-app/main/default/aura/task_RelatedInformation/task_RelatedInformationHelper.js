({
	getContact : function(component,event,helper){
        console.log('getTask');
        var whatId = component.get('v.recordId');
        var action = component.get('c.getContact');
       
        action.setParams({
            "whatId": whatId
        });
        console.log(whatId)
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                console.log(dataObj)
                if(dataObj != null && dataObj != undefined){
                    component.set('v.hasContact',true)
                     component.set('v.contact',dataObj)
                if(dataObj.Direct_Line__c !== undefined){
                    component.set('v.hasDirectLine',true);
                }
                }
                
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    
})