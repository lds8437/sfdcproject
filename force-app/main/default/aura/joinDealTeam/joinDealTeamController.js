({
	doInit : function(component, event, helper) {
        console.log("doing init");
        var insertCall = component.get("c.joinOpportunityTeam");
        console.log("opportunityId: " + component.get("v.recordId"));
        console.log("opportunityId: " + $A.get("$SObjectType.CurrentUser.Id"));
        insertCall.setParams({
            "opportunityId": component.get("v.recordId"),
            "userId": $A.get("$SObjectType.CurrentUser.Id"),
            "accessLevel": "Read"
        });
        insertCall.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Success!",
                    "message": "You are a member of the opportunity team."
                });
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            } else if (state === "ERROR") {
                console.log("Error reverting opp handover");
            } else {
                console.log("Unknown problem reverting opp handover");
            }
        })
        $A.enqueueAction(insertCall);
	}
})