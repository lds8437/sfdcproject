({
    doInit : function(component, event, helper) {
        //Get info needed from Apex in 3 synchronous calls when component is initialized
        //console.log('cpq rules engine init running');        
        //Action 1: Get Service Bundle Products List and store in serviceBundleProducts attribute
        var action = component.get("c.getServiceBundleProducts");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.serviceBundleProducts", response.getReturnValue());
            }
            else if (state === "INCOMPLETE") {
                //console.log('retrieval of service bundle products for Rules Engine incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            //console.log("Error message: " + 
                            //errors[0].message);
                        }
                    } else {
                        //console.log("Unknown error during retrieval of service bundle products for Rules Engine");
                    }
                }
        });
        $A.enqueueAction(action);
        
        //Action 2: Get Bundle Products List and store in bundleProducts attribute
        var action2 = component.get("c.getBundleProducts");
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.bundleProducts", response.getReturnValue());
            }
            else if (state === "INCOMPLETE") {
                //console.log('retrieval of bundle products for Rules Engine incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            //console.log("Error message: " + 
                            //errors[0].message);
                        }
                    } else {
                        //console.log("Unknown error during retrieval of bundle products for Rules Engine");
                    }
                }
        });
        $A.enqueueAction(action2);
        
        //Action 3: Get Bundle ID Name Map and store in bundleMap attribute
        var action3 = component.get("c.getBundleNamesById");
        action3.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.bundleMap", response.getReturnValue());
            }
            else if (state === "INCOMPLETE") {
                //console.log('retrieval of bundle Name Id Map for Rules Engine incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            //console.log("Error message: " + 
                            //errors[0].message);
                        }
                    } else {
                        //console.log("Unknown error during retrieval of bundle Name Id Map for Rules Engine");
                    }
                }
        });
        $A.enqueueAction(action3);
    },
    
    runRulesEngine : function(component, event, helper) {//This function runs when the component receives a cpq_SendToRulesEngine event.
        //console.log('rules engine running');
        //Get variables out of event                
        var techQlis = event.getParam("techQlis");//Get Abbreviated Tech Line Items
        //console.log(techQlis);
        //console.log('rules engine received techQlis' + JSON.stringify(techQlis));
        var serviceQlis = event.getParam("serviceQlis");//Get Abbreviated Service Line Items
        //console.log(serviceQlis);
        //console.log('rules engine received serviceQlis' + JSON.stringify(serviceQlis));
        var allBundleProducts = event.getParam("allBundleProducts");
        //  console.log('rules engine received allBundleProducts' + JSON.stringify(allBundleProducts));
        var allServiceProducts = event.getParam("allServiceProducts");
        //  console.log('rules engine received allServiceProducts' + JSON.stringify(allServiceProducts));
        if(techQlis.length > 0 || serviceQlis.length > 0){//Continue to run the rules engine only if there is data in the techQlis or serviceQlis.
            //Continue getting variables out of event
            var currency = event.getParam('currency');
            //console.log(currency);
            var arr = (event.getParam("arr")/currency).toFixed(2);
            //console.log('arr: ' + arr);
            var netTotal = (event.getParam("netTotal")/currency).toFixed(2);
            //console.log('netTOtal: ' + netTotal);
            var surveyCount = event.getParam("surveyCount");
            //console.log('surveyCount: ' + surveyCount);
            var dashboardUsers = event.getParam("dashboardUsers");
            //console.log('dashboardUsers: ' + dashboardUsers);
            var employeeCount = event.getParam("employeeCount");
            //console.log('employeeCount: ' + employeeCount);
            var digitalFeedbackDomainQuantity = event.getParam("digitalFeedbackDomainQuantity");
            //console.log('digitalFeedbackDomainQuantity: ' + digitalFeedbackDomainQuantity);
            var digitalFeedbackInterceptQuantity = event.getParam("digitalFeedbackInterceptQuantity");
            //console.log('digitalFeedbackInterceptQuantity: ' + digitalFeedbackInterceptQuantity);
            var featureSelected = event.getParam("featureSelected");
            //console.log('featureSelected: ' + featureSelected);
            var featureAddOrRemove = event.getParam("featureAddOrRemove");
            //console.log('featureAddOrRemove: ' + featureAddOrRemove);
            var selectedServiceBundles = event.getParam("selectedServiceBundles");
            //console.log('selectedServiceBundles'+ JSON.stringify(selectedServiceBundles));
            var partnerAvailability = event.getParam("partnerAvailability");
            //console.log(partnerAvailability);
            var selectedPartner = event.getParam("selectedPartner");
            //console.log('seletedPartner' + JSON.stringify(selectedPartners));
            var quoteType = event.getParam('quoteType');
            //console.log(quoteType);
            var licenseExp = event.getParam('licenseExp');
            //console.log(licenseExp);
            var licenses = event.getParam('licenses');
            //console.log(licenses);
            var coreQuantity = event.getParam('coreQuantity');
            //console.log('coreQuantity');
            //console.log(coreQuantity);
            //console.log(coreQuantity > 375000);
            //Get maps/lists previously downloaded from Apex
            var bundleMap = component.get("v.bundleMap");
            var bundleproducts = component.get("v.bundleProducts");
            var servicebundleproducts = component.get("v.serviceBundleProducts");
            //console.log('SETTING VARIABLES');
            //Get name of current bundle
            var bundleid = techQlis[0].TechBundle;
            var bundlename = bundleMap[bundleid];
            // console.log('bundlename: ' + bundlename);
            
            //Get list of service bundle IDs of selected service bundles
            var servicebundles = [];
            for (var i = 0; i < serviceQlis.length; i++){
                if (!servicebundles.includes(serviceQlis[i].ServiceBundle)){
                    servicebundles.push(serviceQlis[i].ServiceBundle)
                }
            }
            //Set number of selected providers 
            var providerQuantity = 0;
            for(var i = 0; i < selectedServiceBundles.length; i++){
                var name = selectedServiceBundles[i].Name;
                //console.log(name.includes('Standard'));
                //console.log(name);
                if(selectedServiceBundles[i].ProviderSelected != true && name.includes('Standard') || selectedServiceBundles[i].ProviderSelected != true && name.includes('Premium')  ){
                    providerQuantity++;
                }
            }
            //Find duplicate services 
            //console.log('quotingPermissions');
            //console.log(event.getParam('quotingPermissions'));
            //console.log(event.getParam('quotingPermissions')[0].Price_Editable__c)
            var uniqueServices = [];
            var duplicateServices = 0;
            for(var i = 0; i < selectedServiceBundles.length; i++){
                if(!uniqueServices.includes(selectedServiceBundles[i].BundleType) && selectedServiceBundles[i].BundleType.includes('Implementation')){
                    uniqueServices.push(selectedServiceBundles[i].BundleType);
                }
                else if(selectedServiceBundles[i].BundleType.includes('Implementation')){
                    duplicateServices++;
                }
            }
            //console.log(duplicateServices);
            //Find count of non implementation services
            var nonImplementationServicesCount = 0;
            for(var i = 0; i < selectedServiceBundles.length; i++){
                //  console.log(selectedServiceBundles[i].Name);
                var serviceName = selectedServiceBundles[i].Name;
                if(!serviceName.includes('Implementation')){
                    
                    nonImplementationServicesCount++;
                    //console.log(nonImplementationServicesCount)
                }
            }
            var xmSolutionsBundleProductIncluded = false;
            for(var i=0; i < techQlis.length; i++){
                if(xmSolutionsBundleProductIncluded == true){break;}
                var bundleprodname = techQlis[i].TechName;
                if(bundleprodname.includes('XM Solutions')){
                    xmSolutionsBundleProductIncluded = true;
                }
            }
            // console.log(nonImplementationServicesCount);
            //
            //VALIDATION RULES
            //
            //console.log('CHECKING VALIDATION RULES');
            var messages = [];//This will hold the array of validation messages to be displayed to the user.
            var validationpass = true;//Default the isValid/validation to true until a validation failure condition.
            //New License Validation rules
            if(quoteType.includes('New License') || quoteType.includes('Upgrade')){
                if(bundlename.includes('RC')){
                    var bundleproductRC = helper.checkIfBundleProductIncluded(techQlis,'RC Core');
                    var bundleproductWebFeedback = helper.checkIfBundleProductIncluded(techQlis,'SI Core');
                    var bundleproductSSO = helper.checkIfBundleProductIncluded(techQlis, 'SSO');
                    //var bundleproductimplementation = helper.checkIfBundleProductIncluded(techQlis,'RC Self Service Implementation');
                    // Validation Rules for Avalibility 
                    if(bundleproductRC && ((arr >= 100000) || (bundleproductWebFeedback  && arr >= 20000 ) || (bundleproductSSO)) 
                       && helper.checkIfBundleProductIncluded(techQlis,'RC Self Service Implementation')){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Self Service Implementation is not available for the current quote configuration. Please select from the available implementation options.';
                        msgUpdate.Link = 'https://qualtrics--c.na3.visual.force.com/resource/1527344432000/RCImpChart';
                        msgUpdate.Label = 'Click here for details';
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    if(bundleproductRC && ((!bundleproductSSO && !bundleproductWebFeedback && arr >= 50000) || (bundleproductSSO && arr >= 100000) || bundleproductWebFeedback) && helper.checkIfBundleProductIncluded(techQlis,'RC Standard Implementation Package')){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Standard Implementation is not available for the current quote configuration. Please select from the available implementation options.';
                        msgUpdate.Link = 'https://qualtrics--c.na3.visual.force.com/resource/1527344432000/RCImpChart';
                        msgUpdate.Label = 'Click here for details';
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    if(bundleproductRC && (arr >= 100000) && helper.checkIfBundleProductIncluded(techQlis,'RC Standard Plus Implementation Package')){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Standard Plus Implementation is not available for the current quote configuration. Please select from the available implementation options.';
                        msgUpdate.Link = 'https://qualtrics--c.na3.visual.force.com/resource/1527344432000/RCImpChart';
                        msgUpdate.Label = 'Click here for details';
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    if(bundleproductRC && arr < 20000 && helper.checkIfBundleProductIncluded(techQlis,'RC Custom Implementation')){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Custom Implementation is not available for the current quote configuration. Please select from the available implementation options.';
                        msgUpdate.Link = 'https://qualtrics--c.na3.visual.force.com/resource/1527344432000/RCImpChart';
                        msgUpdate.Label = 'Click here for details';
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    if((selectedServiceBundles.length == 0 || selectedServiceBundles.length == nonImplementationServicesCount)  && quoteType == 'New License Quote' && bundlename != 'RC1'){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Implementation is required for this quote. Please select from the available implementation options.';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    if(!helper.checkIfBundleProductIncluded(techQlis,'SI Core') && !helper.checkIfLicenseProductIncluded(licenses,'SI Core') && helper.checkIfBundleProductIncluded(techQlis,'SI In-App')){                   
                        var msgUpdate = {};
                        msgUpdate.Message = 'You must have SI Core selected on the current quote or the client must have an existing SI Core license in order to add SI In-App';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    if(bundleproductRC && !helper.checkIfBundleProductIncluded(techQlis,'Developer Tools') && (helper.checkIfServiceBundleProductIncluded(serviceQlis,'RC Developer Tools Advanced Support'))){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Remove the Developer Tools Advanced Support add-on from the service configuration or add back in Developer Tools.';
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    if(bundleproductRC && !helper.checkIfBundleProductIncluded(techQlis,'RC iQ Directory - State of the Art') && (helper.checkIfServiceBundleProductIncluded(serviceQlis,'RC iQ Directory Automations Support - per automation'))){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Remove the iQ Directory Automations Support add-on from the service configuration or add back in iQ Directory - State of the Art.';
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    if(bundlename == 'RC5d'){
                        if(digitalFeedbackDomainQuantity == 0 && digitalFeedbackInterceptQuantity > 0){
                            var msgUpdate = {};
                            msgUpdate.Message = 'At least one Domain Implementation is required when one or more Intercept Implementation is included.';
                            msgUpdate.Link = null;
                            msgUpdate.Label = null;
                            messages.push(msgUpdate);
                            validationpass = false;
                        }
                        if(digitalFeedbackDomainQuantity > digitalFeedbackInterceptQuantity){
                            var msgUpdate = {};
                            msgUpdate.Message = 'Quantity of Domain Implementations cannot exceed quantity of Intercept Implementations';
                            msgUpdate.Link = null;
                            msgUpdate.Label = null;
                            messages.push(msgUpdate);
                            validationpass = false;
                        }
                        if(bundleproductRC && (arr >= 250000) && helper.checkIfBundleProductIncluded(techQlis,'RC Premium Implementation')){
                            var msgUpdate = {};
                            msgUpdate.Message = 'Premium Implementation is not available for quotes where ARR > 250K. Please select Custom Implementation.';
                            msgUpdate.Link = 'https://qualtrics--c.na3.visual.force.com/resource/1527344432000/RCImpChart';
                            msgUpdate.Label = 'Click here for details';
                            messages.push(msgUpdate);
                            validationpass = false;
                        }
                        if(bundleproductRC && !bundleproductWebFeedback && (helper.checkIfServiceBundleProductIncluded(serviceQlis,'RC Add\'l Site Intercept Support - one domain or app, one intercept') || helper.checkIfServiceBundleProductIncluded(serviceQlis,'RC Add\'l Site Intercept Support - per add\'l intercept'))){
                            var msgUpdate = {};
                            msgUpdate.Message = 'Remove the Add\'l Site Intercept Support add-on(s) from the service configuration or add back in SI Core.';
                            messages.push(msgUpdate);
                            validationpass = false;
                        }
                    }
                }
                //console.log('ABOUT TO START THE CX VALIDATION RULES');
                if(bundlename.includes('CX')){                    
                    //console.log(helper.checkIfServiceBundleProductIncluded(serviceQlis,'CX Developer Tools Advanced Support'));
                    if(arr > 250000 && helper.checkIfBundleProductIncluded(techQlis, 'CX Standard Implementation')){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Standard Implementation is not available on quotes where ARR > 250,000. Please select custom implementation.';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    if(!helper.checkIfBundleProductIncluded(techQlis,'Developer Tools') && helper.checkIfServiceBundleProductIncluded(serviceQlis,'CX Developer Tools Advanced Support')){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Developer Tools Advanced Support can only be added to quotes with Developer Tools. Please add Developer Tools or remove CX Developer Tools Advanced Support';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;
                    }                    
                    if(digitalFeedbackDomainQuantity == 0 && digitalFeedbackInterceptQuantity > 0){
                        var msgUpdate = {};
                        msgUpdate.Message = 'At least one Domain Support is required when one or more Intercept Support is included.';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    if(digitalFeedbackDomainQuantity > digitalFeedbackInterceptQuantity){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Quantity of Domain Support cannot exceed quantity of Intercept Support';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    if((selectedServiceBundles.length == 0 || selectedServiceBundles.length == nonImplementationServicesCount)  && quoteType == 'New License Quote'){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Implementation is required for this quote. Please select from the available implementation options.';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    if(coreQuantity > 375000 && helper.checkIfBundleProductIncluded(techQlis, 'CX Standard Implementation')) {
                        var msgUpdate = {};
                        msgUpdate.Message = 'Standard Implementation is not available for the current quote configuration. Please select from the available implementation options.';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    //console.log('ABOUT TO CHECK LICENSES');
                    if(!helper.checkIfBundleProductIncluded(techQlis,'Website Feedback') && !helper.checkIfLicenseProductIncluded(licenses,'Website Feedback') && helper.checkIfBundleProductIncluded(techQlis,'Website Feedback (In-app)')){  
                        var msgUpdate = {};
                        msgUpdate.Message = 'You must have Website Feedback selected on the current quote or the client must have an existing Website Feedback license in order to add Website Feedback (In-app)';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    if(helper.checkIfBundleProductIncluded(techQlis,'CX Standard Implementation') && xmSolutionsBundleProductIncluded){  
                        var msgUpdate = {};
                        msgUpdate.Message = 'Standard Implementation is not available for quotes including XM Solutions. Please select XM Solution Implementation';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    if(helper.checkIfBundleProductIncluded(techQlis,'CX Custom Implementation') && xmSolutionsBundleProductIncluded){  
                        var msgUpdate = {};
                        msgUpdate.Message = 'Standard Implementation is not available for quotes including XM Solutions. Please select XM Solution Implementation';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    if(helper.checkIfBundleProductIncluded(techQlis,'CX Custom XM Solution Implementation') && !xmSolutionsBundleProductIncluded){  
                        var msgUpdate = {};
                        msgUpdate.Message = 'XM Solutions Implementation is not available for quotes without XM Solutions products.';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    if(!helper.checkIfBundleProductIncluded(techQlis,'Website Feedback') && (helper.checkIfServiceBundleProductIncluded(serviceQlis,'CX Add\'l Website Feedback Intercept Support - per add\'l intercept') || helper.checkIfServiceBundleProductIncluded(serviceQlis,'CX Add\'l Website Feedback Support - one domain or app, one intercept'))){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Remove the Add\'l Website Feedback Support add-on(s) from the service configuration or add the Website Feedback feature.';
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    if(!helper.checkIfBundleProductIncluded(techQlis,'CX iQ Directory - State of the Art') && helper.checkIfServiceBundleProductIncluded(serviceQlis,'CX iQ Directory Automations Support - per automation')){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Remove the iQ Directory Automations Support add-on from the service configuration or add the iQ Directory - State of the Art feature.';
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    var xmSolutionCount = 0;
                    for(var i=0; i < techQlis.length; i++){
                        if(techQlis[i].TechName.includes('XM Solutions')){
                            if(((bundlename.includes('CX1') || bundlename.includes('CX0')) && coreQuantity < 10000) || (bundlename.includes('CX3') && coreQuantity < 5000)) {
                                xmSolutionCount++;
                            }
                        }
                        if(xmSolutionCount > 0){
                            var msgUpdate = {};
                            msgUpdate.Message = 'XM Solutions are not available for this response tier. Please change the response tier or remove all XM Solutions products before saving.';
                            msgUpdate.Link = null;
                            msgUpdate.Label = null;
                            messages.push(msgUpdate);
                            validationpass = false;                            
                        }                        
                    }
                    //console.log('LICENSES HAVE BEEN CHECKED');
                }
                if(bundlename.includes('EX')){
                    for(var i=0; i < serviceQlis.length; i++){
                        if(serviceQlis[i].ServiceName == "EX Add'l Survey Build" && serviceQlis[i].Quantity > surveyCount){
                            var msgUpdate = {};
                            msgUpdate.Message = 'Quantity of Survey Builds cannot exceed quantity of surveys purchased. ';
                            msgUpdate.Link = null;
                            msgUpdate.Label = null;
                            messages.push(msgUpdate);
                            validationpass = false;
                        }
                    }
                    for(var i=0; i < serviceQlis.length; i++){
                        if(serviceQlis[i].ServiceName == "EX Add'l Dashboard Build" && serviceQlis[i].Quantity > surveyCount){
                            var msgUpdate = {};
                            msgUpdate.Message = 'Quantity of Dashboard Builds cannot exceed quantity of surveys purchased. ';
                            msgUpdate.Link = null;
                            msgUpdate.Label = null;
                            messages.push(msgUpdate);
                            validationpass = false;
                        }
                    }
                    for(var i=0; i < serviceQlis.length; i++){
                        //console.log(surveyCount);
                        //console.log(serviceQlis[i].ServiceName);
                        if(serviceQlis[i].ServiceName == "EX Full Technology Management" && serviceQlis[i].Quantity > surveyCount){
                            var msgUpdate = {};
                            msgUpdate.Message = 'Quantity of Full Technology Management Services cannot exceed quantity of surveys purchased.';
                            msgUpdate.Link = null;
                            msgUpdate.Label = null;
                            messages.push(msgUpdate);
                            validationpass = false;
                        }
                    }
                    if((selectedServiceBundles.length == 0 || selectedServiceBundles.length == nonImplementationServicesCount)  && arr > 10000 && quoteType == 'New License Quote'){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Implementation is required for this quote. Please select from the available implementation options.';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;
                    }
                    if(employeeCount > 20000 && helper.checkIfBundleProductIncluded(techQlis,'EX Standard Implementation')){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Standard Implementation is not available on quotes where Number of Employees > 20,000. Please select from the available implementation options.';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;                
                    }
                    if(arr > 250000 && helper.checkIfBundleProductIncluded(techQlis,'EX Standard Implementation')){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Standard Implementation is not available on quotes where ARR > 250,000. Please select custom implementation.';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;                
                    }
                    if(arr > 10000 && helper.checkIfBundleProductIncluded(techQlis,'EX Self Service Implementation')){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Self Service Implementation is not available on quotes where ARR > $10,000 USD. Please select from the available implementation options.';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;                
                    }                    
                    
                    for(var i=0; i < serviceQlis.length; i++){
                        if(serviceQlis[i].ServiceName == "EX Survey Item Design" && serviceQlis[i].Quantity > surveyCount){
                            var msgUpdate = {};
                            msgUpdate.Message = 'Quantity of Survey Item Design cannot exceed quantity of surveys purchased. ';
                            msgUpdate.Link = null;
                            msgUpdate.Label = null;
                            messages.push(msgUpdate);
                            validationpass = false;
                        }
                    }                    
                }
                if(bundlename.includes('360')){
                    if(employeeCount > 10000 && helper.checkIfBundleProductIncluded(techQlis,'360 Standard Implementation')){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Standard Implementation is not available on quotes where Number of Subjects > 10,000. Please select from the available implementation options.';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;                
                    }
                    if(arr > 250000 && helper.checkIfBundleProductIncluded(techQlis,'360 Standard Implementation')){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Standard Implementation is not available on quotes where ARR > 250,000. Please select custom implementation.';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;                
                    }
                    if(arr > 10000 && helper.checkIfBundleProductIncluded(techQlis,'360 Self Service Implementation')){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Self Service Implementation is not available on quotes where ARR > $10,000 USD. Please select from the available implementation options.';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;                
                    }
                    for(var i=0; i < serviceQlis.length; i++){
                        if(serviceQlis[i].ServiceName == "360 Survey Item Design" && serviceQlis[i].Quantity > surveyCount){
                            var msgUpdate = {};
                            msgUpdate.Message = 'Quantity of Survey Item Design cannot exceed quantity of surveys purchased. ';
                            msgUpdate.Link = null;
                            msgUpdate.Label = null;
                            messages.push(msgUpdate);
                            validationpass = false;
                        }
                    }
                }
                
                if(bundlename.includes('360Academic')){
                    if(arr > 250000 && helper.checkIfBundleProductIncluded(techQlis,'360 Academic Standard Implementation')){
                        var msgUpdate = {};
                        msgUpdate.Message = 'Standard Implementation is not available on quotes where ARR > 250,000. Please select Custom Implementation.';
                        msgUpdate.Link = null;
                        msgUpdate.Label = null;
                        messages.push(msgUpdate);
                        validationpass = false;                
                    }
                }
                
                if(bundlename.includes('VOC')){
                    if(bundlename.includes('VOCK12')){
                        var bundleproductDistrictAccess = helper.checkIfBundleProductIncluded(techQlis,'District Level Access');
                        var bundleproductPrincipalAccess = helper.checkIfBundleProductIncluded(techQlis,'Principal Level Access');
                        var bundleproductTeacherAccess = helper.checkIfBundleProductIncluded(techQlis,'Teacher Level Access');
                        if(!bundleproductDistrictAccess && !bundleproductPrincipalAccess && !bundleproductTeacherAccess){
                            var msgUpdate = {};
                            msgUpdate.Message = 'At least one access level is required. Please add one or more from the list of available add-ons.';
                            msgUpdate.Link = null;
                            msgUpdate.Label = null;
                            messages.push(msgUpdate);
                            validationpass = false;
                        }                        
                        if(!helper.checkIfBundleProductIncluded(techQlis,'Developer Tools') && helper.checkIfServiceBundleProductIncluded(serviceQlis,'Vocalize K12 Developer Tools Advanced Support')){
                            var msgUpdate = {};
                            msgUpdate.Message = 'Advanced Developer Tools Implementation can only be added to quotes with Developer Tools. Please add Developer Tools or remove Developer Tools Advanced Support';
                            msgUpdate.Link = null;
                            msgUpdate.Label = null;
                            messages.push(msgUpdate);
                            validationpass = false;
                        }                        
                        if((selectedServiceBundles.length == 0 || selectedServiceBundles.length == nonImplementationServicesCount)  && quoteType == 'New License Quote'){
                            var msgUpdate = {};
                            msgUpdate.Message = 'Implementation is required for this quote. Please select from the available implementation options.';
                            msgUpdate.Link = null;
                            msgUpdate.Label = null;
                            messages.push(msgUpdate);
                            validationpass = false;
                        }
                        if(digitalFeedbackDomainQuantity == 0 && digitalFeedbackInterceptQuantity > 0){
                            var msgUpdate = {};
                            msgUpdate.Message = 'At least one Domain Support is required when one or more Intercept Support is included.';
                            msgUpdate.Link = null;
                            msgUpdate.Label = null;
                            messages.push(msgUpdate);
                            validationpass = false;
                        }
                        if(digitalFeedbackDomainQuantity > digitalFeedbackInterceptQuantity){
                            var msgUpdate = {};
                            msgUpdate.Message = 'Quantity of Domain Support cannot exceed quantity of Intercept Support';
                            msgUpdate.Link = null;
                            msgUpdate.Label = null;
                            messages.push(msgUpdate);
                            validationpass = false;
                        }
                        if((selectedServiceBundles.length == 0 || selectedServiceBundles.length == nonImplementationServicesCount)  && quoteType == 'New License Quote'){
                            var msgUpdate = {};
                            msgUpdate.Message = 'Implementation is required for this quote. Please select from the available implementation options.';
                            msgUpdate.Link = null;
                            msgUpdate.Label = null;
                            messages.push(msgUpdate);
                            validationpass = false;
                        }
                        if(arr > 250000 && helper.checkIfBundleProductIncluded(techQlis, 'Vocalize K12 Standard Implementation')) {
                            var msgUpdate = {};
                            msgUpdate.Message = 'Standard Implementation is not available for the current quote with ARR > 250,000. Please select Custom Implementation.';
                            msgUpdate.Link = null;
                            msgUpdate.Label = null;
                            messages.push(msgUpdate);
                            validationpass = false;
                        }
                        if(!helper.checkIfBundleProductIncluded(techQlis,'Website Targeting') && (helper.checkIfServiceBundleProductIncluded(serviceQlis,'VocK12 Add\'l Website Targeting Intercept Support - per add\'l intercept') || helper.checkIfServiceBundleProductIncluded(serviceQlis,'VocK12 Add\'l Website Targeting Support - per add\'l domain + intercept'))){
                            var msgUpdate = {};
                            msgUpdate.Message = 'Remove the Add\'l Website Targeting Support add-on(s) from the service configuration or add the Website Targeting feature.';
                            messages.push(msgUpdate);
                            validationpass = false;
                        }
                        if(!helper.checkIfBundleProductIncluded(techQlis,'K12 VoC iQ Directory - State of the Art') && helper.checkIfServiceBundleProductIncluded(serviceQlis,'VocK12 iQ Directory Automations Support - per automation')){
                            var msgUpdate = {};
                            msgUpdate.Message = 'Remove the iQ Directory Automations Support add-on from the service configuration or add the iQ Directory - State of the Art feature.';
                            messages.push(msgUpdate);
                            validationpass = false;
                        }
                        //console.log('ABOUT TO CHECK LICENSES');
                        if(!helper.checkIfBundleProductIncluded(techQlis,'Website Feedback') && !helper.checkIfLicenseProductIncluded(licenses,'Website Feedback') && helper.checkIfBundleProductIncluded(techQlis,'Website Feedback (In-app)')){  
                            var msgUpdate = {};
                            msgUpdate.Message = 'You must have Website Feedback selected on the current quote or the client must have an existing Website Feedback license in order to add Website Feedback (In-app)';
                            msgUpdate.Link = null;
                            msgUpdate.Label = null;
                            messages.push(msgUpdate);
                            validationpass = false;
                        }
                    }
                }
                //console.log('ABOUT TO CHECK DUPLICATE SERVICES AND PARTNERS');
                if(duplicateServices > 0 && !event.getParam('quotingPermissions')[0].Price_Editable__c.includes('Implementation')){
                    var msgUpdate = {};
                    msgUpdate.Message = 'Only one implementation package can be included on each quote. Please remove one of the packages on this quote.';
                    msgUpdate.Link = null;
                    msgUpdate.Label = null;
                    messages.push(msgUpdate);
                    validationpass = false;
                }
                if(providerQuantity >= 1){
                    var msgUpdate = {};
                    msgUpdate.Message = "A provider must be selected for each standard service. Please click 'Select Providers' above to view a list of available providers.";
                    msgUpdate.Link = null;
                    msgUpdate.Label = null;
                    messages.push(msgUpdate);
                    validationpass=false;
                }
                if(partnerAvailability != undefined){
                    for(var i = 0; i < partnerAvailability.length; i++){
                        if(arr > partnerAvailability[i].maxDealSize || arr < partnerAvailability[i].minDealSize){
                            var msgUpdate = {};
                            msgUpdate.Message = 'The selected provider is no longer available for the current quote configuration.';
                            msgUpdate.Link = null;
                            msgUpdate.Label = null;
                            messages.push(msgUpdate);
                            validationpass=false;
                        }
                    }
                }
            }
            //console.log('CHECKING UPGRADE VALIDATION RULES');
            //Upgrade Validation Rules 
            if((quoteType.includes('Upgrade') || quoteType.includes('Merge')) && licenseExp != undefined){
                //console.log(licenseExp);
                if(bundlename.includes('CX') && (selectedServiceBundles.length == 0 || selectedServiceBundles.length == nonImplementationServicesCount) && licenseExp.includes('RC')){
                    var msgUpdate = {};
                    msgUpdate.Message = 'Implementation is required for this quote. Please select from the available implementation options.';
                    msgUpdate.Link = null;
                    msgUpdate.Label = null;
                    messages.push(msgUpdate);
                    validationpass = false;
                }
                if(bundlename.includes('VOC') && (selectedServiceBundles.length == 0 || selectedServiceBundles.length == nonImplementationServicesCount) && licenseExp.includes('RS')){
                    var msgUpdate = {};
                    msgUpdate.Message = 'Implementation is required for this quote. Please select from the available implementation options.';
                    msgUpdate.Link = null;
                    msgUpdate.Label = null;
                    messages.push(msgUpdate);
                    validationpass = false;
                }               
            }
            //console.log('DONE CHECKING UPGRADE VALIDATION RULES');
            //Set messages and message display attribute in component.
            component.set("v.messages", messages);
            component.set("v.messageCount", messages.length);
            //Always send the validation event whether validation passes or fails.
            var validationEvent = $A.get("e.c:cpq_RulesValidation");
            //console.log('validation event setup');
            //console.log('validationpass: ' + validationpass);
            validationEvent.setParams(
                { "isValid" : validationpass,
                 "messageCount": component.get('v.messageCount')
                }
            );
            validationEvent.fire();
            //console.log('validation event fired');
            //END VALIDATION RULES
            //
            //AVAILABILITY RULES
            //
            var availability = [];//To hold array of availability updates to be sent out in the availability event if needed.
            
            if(bundlename.includes('RC')){
                //   console.log('thisworks');
                var bundleproductRC = helper.checkIfBundleProductIncluded(techQlis,'RC Core');
                var bundleproductWebFeedback = helper.checkIfBundleProductIncluded(techQlis,'SI Core');
                var bundleproductSSO = helper.checkIfBundleProductIncluded(techQlis,'SSO');
                var bundleproductiQ = helper.checkIfBundleProductIncluded(techQlis,'RC iQ Directory - State of the Art');
                //   console.log(bundleid);
                //   console.log(allBundleProducts);
                if(bundleproductRC && ((arr >= 100000) || (bundleproductWebFeedback && arr >= 20000 ) || (bundleproductSSO)) && !helper.checkIfBundleProductIncluded(techQlis, 'Dashboards')){
                    // availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, servicebundleproducts, 'RC Self Service Implementation', 'true');     
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'RC Self Service Implementation', 'false');
                }
                else{
                    //availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, servicebundleproducts, 'RC Self Service Implementation', 'false');
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'RC Self Service Implementation', 'true');
                }
                
                if(bundleproductRC && ((!bundleproductSSO && !bundleproductWebFeedback && arr >= 50000) || (bundleproductSSO && arr >= 100000) || (bundleproductWebFeedback)) && !helper.checkIfBundleProductIncluded(techQlis, 'Dashboards')){
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'RC Standard Implementation Package', 'false');
                }
                else{
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'RC Standard Implementation Package', 'true');
                }
                
                if(arr >= 100000 && bundleproductRC && !helper.checkIfBundleProductIncluded(techQlis, 'Dashboards')){
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'RC Standard Plus Implementation Package', 'false');
                }
                else{
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'RC Standard Plus Implementation Package', 'true');
                }
                if(arr < 20000 && bundleproductRC){
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'RC Custom Implementation', 'false');
                }
                else{
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'RC Custom Implementation', 'true');
                }
                if(bundlename == 'RC5d'){
                    
                    if(helper.checkIfBundleProductIncluded(techQlis, 'SI Core')){
                        availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'RC Add\'l Site Intercept Support - one domain or app, one intercept', 'true');
                    }
                    else{
                        availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'RC Add\'l Site Intercept Support - one domain or app, one intercept', 'false');
                    }
                    if(helper.checkIfBundleProductIncluded(techQlis, 'SI Core')){
                        availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'RC Add\'l Site Intercept Support - per add\'l intercept', 'true');
                    }
                    else{
                        availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'RC Add\'l Site Intercept Support - per add\'l intercept', 'false');
                    }
                    if(bundlename == 'RC5d'){
                        if(bundleproductiQ){
                            availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'RC iQ Directory Automations Support - per automation', 'true');
                        }
                        else{
                            availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'RC iQ Directory Automations Support - per automation', 'false');
                        }
                        if(helper.checkIfBundleProductIncluded(techQlis, 'Developer Tools')){
                            availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'RC Developer Tools Advanced Support', 'true');
                        }
                        else{
                            availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'RC Developer Tools Advanced Support', 'false');
                        }
                    }
                }
            }
            if(bundlename.includes('CX')){
                //      console.log(digitalFeedbackDomainQuantity);
                if(helper.checkIfBundleProductIncluded(techQlis,'Developer Tools')){
                    availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'CX Developer Tools Advanced Support', 'true');
                } else {
                    availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'CX Developer Tools Advanced Support', 'false');
                }
                
                if(helper.checkIfBundleProductIncluded(techQlis,'Website Feedback')){
                    availability = helper.updateAvailabilityForServiceBundleProduct(availability, bundleid, allServiceProducts, 'CX Add\'l Website Feedback Intercept Support - per add\'l intercept', 'true');
                } else {
                    availability = helper.updateAvailabilityForServiceBundleProduct(availability, bundleid, allServiceProducts, 'CX Add\'l Website Feedback Intercept Support - per add\'l intercept', 'false');
                }
                if(helper.checkIfBundleProductIncluded(techQlis,'Website Feedback')){
                    availability = helper.updateAvailabilityForServiceBundleProduct(availability, bundleid, allServiceProducts, 'CX Add\'l Website Feedback Support - one domain or app, one intercept', 'true');
                } else {
                    availability = helper.updateAvailabilityForServiceBundleProduct(availability, bundleid, allServiceProducts, 'CX Add\'l Website Feedback Support - one domain or app, one intercept', 'false');
                }
                if(helper.checkIfBundleProductIncluded(techQlis,'CX iQ Directory - State of the Art') || helper.checkIfBundleProductIncluded(techQlis,'iQ Directory - CX Advanced')){
                    availability = helper.updateAvailabilityForServiceBundleProduct(availability, bundleid, allServiceProducts, 'CX iQ Directory Automations Support - per automation', 'true');
                } else {
                    availability = helper.updateAvailabilityForServiceBundleProduct(availability, bundleid, allServiceProducts, 'CX iQ Directory Automations Support - per automation', 'false');
                }              
                if(!xmSolutionsBundleProductIncluded && bundlename.includes('CX1') && (!helper.checkIfBundleProductIncluded(techQlis,'Text IQ') && !helper.checkIfBundleProductIncluded(techQlis,'CLFU (Case Management)'))){
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'CX Standard Implementation', 'true');
                }
                else if(bundlename.includes('CX1')){
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'CX Standard Implementation', 'false');
                }
                if(!xmSolutionsBundleProductIncluded && bundlename.includes('CX3') && !helper.checkIfBundleProductIncluded(techQlis,'Text IQ')){
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'CX Standard Implementation', 'true');
                }
                else if(bundlename.includes('CX3')){
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'CX Standard Implementation', 'false');
                }
                if(!xmSolutionsBundleProductIncluded && coreQuantity <= 375000) {
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'CX Standard Implementation', 'true');
                } else {
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'CX Standard Implementation', 'false');
                }
                if(!xmSolutionsBundleProductIncluded){
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'CX Custom Implementation', 'true');
                } else {
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'CX Custom Implementation', 'false');
                }
                if(xmSolutionsBundleProductIncluded){
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'CX Custom XM Solution Implementation', 'true');
                } else {
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'CX Custom XM Solution Implementation', 'false');
                }
                for(var i=0; i < allBundleProducts.length; i++){
                    //console.log(allBundleProducts);
                    var bundleprodname = allBundleProducts[i].Related_Product__r.Name;
                    //console.log(bundleprodname);
                    if(bundleprodname.includes('XM Solutions')){
                        if(((bundlename.includes('CX1') || bundlename.includes('CX0')) && coreQuantity >= 10000) || (bundlename.includes('CX3') && coreQuantity >= 5000)||bundlename.includes('CX5')) {
                            availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, bundleprodname, 'true');
                        } else {
                            availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, bundleprodname, 'false');
                        } 
                    }
                }
                
            }
            
            if(bundlename.includes('EX')){
                if(employeeCount < 15001){
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'EX Standard Implementation', 'true');
                }
                else{
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'EX Standard Implementation', 'false');
                }
                if(arr < 10001){
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'EX Self Service Implementation', 'true');
                }
                else{
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'EX Self Service Implementation', 'false');
                }   
            }
            if(bundlename.includes('360')){
                if(employeeCount < 10001){
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, '360 Standard Implementation', 'true');
                }
                else{
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, '360 Standard Implementation', 'false');
                }
                if(arr < 10001){
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, '360 Self Service Implementation', 'true');
                }
                else{
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, '360 Self Service Implementation', 'false');
                }
            }
            if(bundlename.includes('VOCK12')){
                if(helper.checkIfBundleProductIncluded(techQlis,'VOC Features K12 - Developer Tools (C)')){
                    availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'VocK12 Developer Tools Advanced Support', 'true');
                } else {
                    availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'VocK12 Developer Tools Advanced Support', 'false');
                }
                if(helper.checkIfBundleProductIncluded(techQlis,'K12 VoC iQ Directory - State of the Art')){
                    availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'VocK12 iQ Directory Automations Support - per automation', 'true');
                } else {
                    availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'VocK12 iQ Directory Automations Support - per automation', 'false');
                }
                if(helper.checkIfBundleProductIncluded(techQlis,'Website Targeting')){
                    availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'VocK12 Add\'l Website Targeting Intercept Support - per add\'l intercept', 'true');
                } else {
                    availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'VocK12 Add\'l Website Targeting Intercept Support - per add\'l intercept', 'false');
                }
                if(helper.checkIfBundleProductIncluded(techQlis,'Website Targeting')){
                    availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'VocK12 Add\'l Website Targeting Support - per add\'l domain + intercept', 'true');
                } else {
                    availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'VocK12 Add\'l Website Targeting Support - per add\'l domain + intercept', 'false');
                }
                if(digitalFeedbackDomainQuantity > 1){
                    availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'Vocalize K12 Website Feedback Intercept', 'true');
                } else{
                    availability = helper.updateAvailabilityForServiceBundleProduct(availability, servicebundles, allServiceProducts, 'Vocalize K12 Website Feedback Intercept', 'false');
                }
                if(!xmSolutionsBundleProductIncluded && bundlename.includes('VOCK12') && (!helper.checkIfBundleProductIncluded(techQlis,'Intelligent Text Analytics') && !helper.checkIfBundleProductIncluded(techQlis,'CLFU (Case Management)'))){
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'Vocalize K12 Standard Implementation', 'true');
                }
                else if(bundlename.includes('VOCK12')){
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'Vocalize K12 Standard Implementation', 'false');
                }
                if(!xmSolutionsBundleProductIncluded && bundlename.includes('VOCK12') && !helper.checkIfBundleProductIncluded(techQlis,'Intelligent Text Analytics')){
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'Vocalize K12 Standard Implementation', 'true');
                }
                else if(bundlename.includes('VOCK12')){
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'Vocalize K12 Standard Implementation', 'false');
                }
                if(!xmSolutionsBundleProductIncluded && arr <= 80000) {
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'Vocalize K12 Standard Implementation', 'true');
                } else {
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'Vocalize K12 Standard Implementation', 'false');
                }
                if(!xmSolutionsBundleProductIncluded){
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'Vocalize K12 Custom Implementation', 'true');
                } else {
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'Vocalize K12 Custom Implementation', 'false');
                }
                if(xmSolutionsBundleProductIncluded){
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'Vocalize K12 Custom XM Solution Implementation', 'true');
                } else {
                    availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, 'Vocalize K12 Custom XM Solution Implementation', 'false');
                }
                
                for(var i=0; i < allBundleProducts.length; i++){
                    //console.log(allBundleProducts);
                    var bundleprodname = allBundleProducts[i].Related_Product__r.Name;
                    //console.log(bundleprodname);
                    if(bundleprodname.includes('XM Solutions')){
                        if(bundlename.includes('VOCK12')) {
                            availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, bundleprodname, 'true');
                        } else {
                            availability = helper.updateAvailabilityForBundleProduct(availability, bundleid, allBundleProducts, bundleprodname, 'false');
                        } 
                    }
                }
                
            }
            if(availability.length > 0){//Send the availability event only if there are availability updates to send
                
                var availabilityEvent = $A.get("e.c:cpq_RulesAvailability");
                availabilityEvent.setParams({ "availability" : availability });
                availabilityEvent.fire();
                //console.log('availability event fired with data: ' + JSON.stringify(availability));
            }
            //console.log('DONE CHECKING AVAILABILITY RULES');
            //
            //End availability rules
            //
            //Price/Quantity rules
            var lineItemUpdates = [];//To hold array of price/quantity updates
            if(bundlename.includes('CX')){
                lineItemUpdates = helper.serviceLineItemUpdate(serviceQlis, lineItemUpdates, 'CX Batch User Creation (per 1000 users)', null, Math.ceil(dashboardUsers / 1000), null);      
            }   
            if(bundlename.includes('RC')){
                lineItemUpdates = helper.serviceLineItemUpdate(serviceQlis, lineItemUpdates, 'RC Batch User Creation (per 1000 users)', null, Math.ceil(dashboardUsers / 1000), null);      
            } 
            if(bundlename.includes('EX')){
                let bundlePrice = 0;
                _.forEach(techQlis, function(qli) {
                    if(qli.Type__c === 'Tech' && qli.Revenue_Type__c === 'Recurring') {
                        bundlePrice += qli.PriceTotal - qli.BundleDiscount;
                    }
                });
                if(bundlename.includes('EX1') && .15 * bundlePrice > 2000){
                    lineItemUpdates = helper.serviceLineItemUpdate(serviceQlis, lineItemUpdates, 'EX Implementation Engagement Base Package', .15 * bundlePrice, null, null);
                }
                if(bundlename.includes('EX1') && .15 * bundlePrice <= 2000){
                    lineItemUpdates = helper.serviceLineItemUpdate(serviceQlis, lineItemUpdates, 'EX Implementation Engagement Base Package', 2000, null, null);
                }
                if(bundlename.includes('EX3') && .1 * bundlePrice > 2000){
                    lineItemUpdates = helper.serviceLineItemUpdate(serviceQlis, lineItemUpdates, 'EX Implementation Engagement Base Package', .1 * bundlePrice, null, null);
                }
                if(bundlename.includes('EX3') && .1 * bundlePrice <= 2000){
                    lineItemUpdates = helper.serviceLineItemUpdate(serviceQlis, lineItemUpdates, 'EX Implementation Engagement Base Package', 2000, null, null);
                }
                if(bundlename.includes('EX5') && .05 * bundlePrice > 2000){
                    lineItemUpdates = helper.serviceLineItemUpdate(serviceQlis, lineItemUpdates, 'EX Implementation Engagement Base Package', .05 * bundlePrice, null, null);
                }
                if(bundlename.includes('EX5') && .05 * bundlePrice <= 2000){
                    lineItemUpdates = helper.serviceLineItemUpdate(serviceQlis, lineItemUpdates, 'EX Implementation Engagement Base Package', 2000, null, null);
                }
                //console.log(lineItemUpdates);
            }
            if(bundlename.includes('360') && .05 * arr > 2000){
                lineItemUpdates = helper.serviceLineItemUpdate(serviceQlis, lineItemUpdates, '360 Implementation Base Package', .05 * arr, null, null);
            }
            if(bundlename.includes('360') && .05 * arr <= 2000){
                lineItemUpdates = helper.serviceLineItemUpdate(serviceQlis, lineItemUpdates, '360 Implementation Base Package', 2000, null, null);
            }
            //This is a special Price/Quantity rule that is more complex than the helper is useful for.
            if(bundlename.includes('RSK12NonDistrict') || bundlename.includes('RSK12District')){
                for(var i=0; i < techQlis.length; i++){
                    //console.log('currently reviewing techname ' + techQlis[i].TechName);
                    if(techQlis[i].TechName == 'Stats iQ'){
                        var corequantity;
                        for(var n=0; n < techQlis.length; n++){
                            if(techQlis[n].BundleKey.includes('Core')){
                                corequantity = techQlis[n].Quantity;
                            }
                        }
                        if(corequantity > 20000) {
                            var liUpdate = {};
                            liUpdate.product = techQlis[i].TechId;
                            liUpdate.totalPrice = .25 * corequantity;
                            liUpdate.quantity = null;
                            liUpdate.maxQuantity = null;
                            lineItemUpdates.push(liUpdate)
                        }
                        if(corequantity <= 20000) {
                            var liUpdate = {};
                            liUpdate.product = techQlis[i].TechId;
                            liUpdate.totalPrice = 5000;
                            liUpdate.quantity = null;
                            liUpdate.maxQuantity = null;
                            lineItemUpdates.push(liUpdate)
                        }
                        
                    }
                }
            }
            if(bundlename.includes('VOCK12')){
                for(var i=0; i < techQlis.length; i++){
                    //console.log('currently reviewing techname ' + techQlis[i].TechName);
                    if(techQlis[i].TechName == 'Stats iQ'){
                        var corequantity;
                        for(var n=0; n < techQlis.length; n++){
                            if(techQlis[n].BundleKey.includes('Core')){
                                corequantity = techQlis[n].Quantity;
                            }
                        }
                        if(corequantity > 20000) {
                            var liUpdate = {};
                            liUpdate.product = techQlis[i].TechId;
                            liUpdate.totalPrice = .25 * corequantity;
                            liUpdate.quantity = null;
                            liUpdate.maxQuantity = null;
                            lineItemUpdates.push(liUpdate)
                        }
                        if(corequantity <= 20000) {
                            var liUpdate = {};
                            liUpdate.product = techQlis[i].TechId;
                            liUpdate.totalPrice = 5000;
                            liUpdate.quantity = null;
                            liUpdate.maxQuantity = null;
                            lineItemUpdates.push(liUpdate)
                        }
                        
                    }
                }
            }
            if(bundlename.includes('VOCK12')){
                lineItemUpdates = helper.techLineItemUpdate(techQlis, lineItemUpdates, 'Vocalize Core', null, 15000, null, null);
            }             
            //SI In-App pricing rules
            for(var i = 0; i < techQlis.length; i++){
                //console.log(techQlis[i].TechName);
                if(techQlis[i].TechName == 'SI In-App' && quoteType.includes('Upgrade') && helper.checkIfLicenseProductIncluded(licenses,'SI Core')){
                    for(var k = 0; k < licenses.length; k++){
                        if(licenses[k].product == 'SI Core'){
                            var liUpdate = {};
                            liUpdate.product = techQlis[i].TechId;
                            liUpdate.totalPrice = Math.max(licenses[k].price*0.20,5000*currency);
                            liUpdate.quantity = null;
                            liUpdate.maxQuantity = null;
                            lineItemUpdates.push(liUpdate)
                        }
                    }                    
                }
                else if(techQlis[i].TechName == 'SI In-App'){
                    for(var k = 0; k < techQlis.length; k++){
                        if(techQlis[k].TechName == 'SI Core'){
                            var liUpdate = {};
                            liUpdate.product = techQlis[i].TechId;
                            liUpdate.totalPrice = Math.max(techQlis[k].PriceTotal*0.20,5000*currency);
                            liUpdate.quantity = null;
                            liUpdate.maxQuantity = null;
                            lineItemUpdates.push(liUpdate)
                        }
                    }
                }    
            }
            for(var i = 0; i < techQlis.length; i++){
                if(techQlis[i].TechName == 'Website Feedback (In-app)' && quoteType.includes('Upgrade') && helper.checkIfLicenseProductIncluded(licenses,'Website Feedback') && !bundlename.includes('CX5')){
                    for(var k = 0; k < licenses.length; k++){
                        if(licenses[k].product == 'Website Feedback'){
                            var liUpdate = {};
                            liUpdate.product = techQlis[i].TechId;
                            liUpdate.totalPrice = Math.max(licenses[k].price*0.20,5000*currency);
                            liUpdate.quantity = null;
                            liUpdate.maxQuantity = null;
                            lineItemUpdates.push(liUpdate)
                        }
                    }                    
                }
                else if(techQlis[i].TechName == 'Website Feedback (In-app)' && !bundlename.includes('CX5')){
                    for(var k = 0; k < techQlis.length; k++){
                        if(techQlis[k].TechName == 'Website Feedback'){
                            var liUpdate = {};
                            liUpdate.product = techQlis[i].TechId;
                            liUpdate.totalPrice = Math.max(techQlis[k].PriceTotal*0.20,5000*currency);
                            console.log('techQlis PriceTotal: '+techQlis[k].PriceTotal+ ' discounted to: '+techQlis[k].PriceTotal*0.20);
                            console.log('currency: '+ currency);
                            console.log(techQlis[k].PriceTotal*0.20+' vs '+5000*currency);
                            liUpdate.quantity = null;
                            liUpdate.maxQuantity = null;
                            lineItemUpdates.push(liUpdate)
                        }
                    }
                }                 
            }
            //console.log('DONE CHECKING PRICING RULES');
            //Update currencies 
            var convertedLineItemUpdates = [];
            //console.log(lineItemUpdates);
            for(var i = 0; i < lineItemUpdates.length; i++){
                var convertedLineItem = {};
                if(lineItemUpdates[i].price != null) {
                    convertedLineItem.price = lineItemUpdates[i].price*currency;
                }
                if(lineItemUpdates[i].totalPrice != null){
                    convertedLineItem.totalPrice  = lineItemUpdates[i].totalPrice; 
                }
                convertedLineItem.product = lineItemUpdates[i].product;
                convertedLineItem.quantity = lineItemUpdates[i].quantity;
                convertedLineItem.maxQuantity = lineItemUpdates[i].maxQuantity;
                convertedLineItem.tech = lineItemUpdates[i].tech;
                convertedLineItem.multiplicityKey = lineItemUpdates[i].multiplicityKey;
                convertedLineItemUpdates.push(convertedLineItem);
            }
            //console.log(convertedLineItemUpdates);
            if(convertedLineItemUpdates.length > 0){//Send Line Item Updates event only if there are line item updates to send.
                var pricingEvent = $A.get("e.c:cpq_RulesPricing");
                //console.log('pricing event setup');
                pricingEvent.setParams({ "lineItemUpdates" : convertedLineItemUpdates });
                pricingEvent.fire();
                //console.log('pricing event fired with line item updates: ' + JSON.stringify(convertedLineItemUpdates));
            }
            //End Price/Quantity rules
            //
            //Add/Remove rules
            var addremoves = [];
            //console.log('starting on add/remove rules');
            if(bundlename.includes('CX')){
                addremoves = helper.processAddRemoveRuleTechBP(addremoves, techQlis, bundleproducts, bundlename, 'CX Engineering Services Maintenance', 'CX Custom Engineering Services', selectedServiceBundles, 'Custom Engineering Services' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Engineering Services Maintenance', 'Custom Engineering Services' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX SSO Configuration', 'SSO');
                if(bundlename.includes('CX1')){
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX iQ Directory Advanced Support', 'CX iQ Directory - State of the Art');
                    //addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX TA Partic Mgmt Implementation', 'Participant Management');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Stats IQ Pivot/Regression Support', 'Stats IQ (Pivot and Regression)');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Vanity URL Setup', 'Vanity URL');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Closed Loop Follow-up Configuration', 'Closed-Loop Follow-up');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Advanced Text iQ Configuration', 'Text IQ');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Action Planning Configuration', 'Action Planning');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Developer Tools Introductory Support', 'Developer Tools');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Salesforce Integration Support', 'SFDC Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Marketo Integration Support', 'Marketo Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Adobe Analytics Integration Support', 'Adobe Analytics Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Slack Integration Support', 'Slack Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Data Isolation Setup', 'Data Isolation');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Tableau Integration Support', 'Tableau Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Predict iQ Support', 'Predict iQ');
                    //addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Website Feedback Support - one domain or app, one intercept', 'Website Feedback (In-app)');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Website Feedback Support - one domain or app, one intercept', 'Website Feedback');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Journey Optimizer Support', 'CX Journey Optimizer');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX CRM Integration Support', 'CRM Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Marketing Integration Support', 'Marketing Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Customer Support / Help Desk Integration Support', 'Customer Support / Help Desk Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Communication and Chat Integration Support', 'Communication and Chat Integration');
                    //console.log('======================================Begin======================================')
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX BI/Visualization Integration Support', 'BI/Visualization Integration');
                    //console.log('======================================End======================================')
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Digital Insights Integration Support', 'Digital Insights Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Incentives Integration Support', 'Incentives Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Integration Services Support', 'Integration Services');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Closed Loop Follow-Up Configuration', 'CLFU (Case Management)');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Online Reputation Management Setup', 'Online Reputation Management'); 
                }
                if(bundlename.includes('CX3')){
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX iQ Directory - State of the Art Implementation', 'CX iQ Directory - State of the Art');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Stats IQ Pivot/Regression Support', 'Stats IQ (Pivot and Regression)');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Vanity URL Setup', 'Vanity URL');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Advanced Text iQ Configuration', 'Text IQ');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Developer Tools Introductory Support', 'Developer Tools');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Salesforce Integration Support', 'SFDC Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Marketo Integration Support', 'Marketo Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Adobe Analytics Integration Support', 'Adobe Analytics Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Slack Integration Support', 'Slack Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Data Isolation Setup', 'Data Isolation');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Tableau Integration Support', 'Tableau Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Predict iQ Support', 'Predict iQ');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Website Feedback Support - one domain or app, one intercept', 'Website Feedback');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Journey Optimizer Support', 'CX Journey Optimizer');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Action Planning Configuration', 'Action Planning');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX CRM Integration Support', 'CRM Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Marketing Integration Support', 'Marketing Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Customer Support / Help Desk Integration Support', 'Customer Support / Help Desk Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Communication and Chat Integration Support', 'Communication and Chat Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX BI/Visualization Integration Support', 'BI/Visualization Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Digital Insights Integration Support', 'Digital Insights Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Incentives Integration Support', 'Incentives Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Integration Services Support', 'Integration Services');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Online Reputation Management Setup', 'Online Reputation Management');
                }
                if(bundlename.includes('CX5')){
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Developer Tools Introductory Support', 'Developer Tools');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Salesforce Integration Support', 'SFDC Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Marketo Integration Support', 'Marketo Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Adobe Analytics Integration Support', 'Adobe Analytics Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Slack Integration Support', 'Slack Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Data Isolation Setup', 'Data Isolation');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Tableau Integration Support', 'Tableau Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Action Planning Configuration', 'Action Planning');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Website Feedback Domain Support', 'Website Feedback');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX CRM Integration Support', 'CRM Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Marketing Integration Support', 'Marketing Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Customer Support / Help Desk Integration Support', 'Customer Support / Help Desk Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Communication and Chat Integration Support', 'Communication and Chat Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX BI/Visualization Integration Support', 'BI/Visualization Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Digital Insights Integration Support', 'Digital Insights Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Incentives Integration Support', 'Incentives Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Integration Services Support', 'Integration Services');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Online Reputation Management Setup', 'Online Reputation Management');   
                }
            }
            if(bundlename.includes('EX')){
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts,'EX SSO Configuration', 'SSO');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'EX Developer Tools Introductory Support', 'Developer Tools');
                addremoves = helper.processAddRemoveRuleTechBP(addremoves, techQlis, bundleproducts, bundlename, 'EX Engineering Services Maintenance', 'EX Custom Engineering Services', selectedServiceBundles, 'Custom Engineering Services' );
                //addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts,'EX Benchmark Support', 'WorldNorms (Additional Cuts)');            
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'EX Advanced Security Management Implementation ', 'Advanced Security Management' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'EX Data Isolation Implementation', 'Data Isolation' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'EX Branded URL Setup', 'Branded URL' );
                
                if(bundlename.includes('EX1a') || bundlename.includes('EX0')){
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'EX Contact Import & Distribution Automation Support', 'EX Contact Import and Distribution Automation');
                }
            }
            if(bundlename.includes('360') && !bundlename.includes('Academic')){
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts,'360 SSO Configuration', 'SSO');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, '360 Developer Tools Introductory Support', 'Developer Tools');
                addremoves = helper.processAddRemoveRuleTechBP(addremoves, techQlis, bundleproducts, bundlename, '360 Engineering Services Maintenance', '360 Custom Engineering Services', selectedServiceBundles, 'Custom Engineering Services' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, '360 Engineering Services Maintenance', 'Custom Engineering Services' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, '360 Directory Import Automation Support', 'Employee Directory Import Automation' );
            }
            console.log('Bundle Name: '+bundlename)
            console.log('Select Service Bundles:' + JSON.stringify(selectedServiceBundles))
            if(bundlename.includes('RC')){
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts,'RC SSO Configuration', 'SSO');
                addremoves = helper.processAddRemoveRuleTechBP(addremoves, techQlis, bundleproducts, bundlename, 'RC Engineering Services Maintenance', 'RC Custom Engineering Services', selectedServiceBundles, 'Custom Engineering Services' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Engineering Services Maintenance', 'Custom Engineering Services' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Conjoint Implementation', 'Conjoint' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Digital Insights Integration Support', 'Digital Insights Integration' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC CRM Integration Support', 'CRM Integration' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Marketing Integration Support', 'Marketing Integration');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Customer Support / Help Desk Integration Support', 'Customer Support / Help Desk Integration');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Communication and Chat Integration Support', 'Communication and Chat Integration');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC BI/Visualization Integration Support', 'BI/Visualization Integration');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Digital Insights Integration Support', 'Digital Insights Integration');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Incentives Integration Support', 'Incentives Integration');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Integration Services Support', 'Integration Services');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Developer Tools Introductory Support', 'Developer Tools');
                
                //addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Advanced Security Management Implementation ', 'Advanced Security Management' );
                //addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Data Isolation Implementation ', 'Data Isolation' );
                if(bundlename == 'RC5'){
                    addremoves = helper.processAddRemoveRuleTechBP(addremoves, techQlis, bundleproducts, bundlename, 'Advanced Security Management', 'RC Core', selectedServiceBundles, 'Advanced Security Management' , 25000 ); 
                }
                if(bundlename == 'RC5d'){
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC iQ Directory Advanced Support', 'RC iQ Directory - State of the Art');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC TA Customer Directory Imp', 'TA Respondent Management');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC TA Partic Mgmt Implementation', 'Participant Management');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Stats IQ Pivot/Reg Implementation', 'Stats IQ (Pivot and Regression)');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Vanity URL Setup', 'Vanity URL');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Closed Loop Follow-up Implementation', 'Closed Loop Follow-up');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Text iQ Configuration', 'Text IQ');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC SFDC Integration Implementation', 'SFDC Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Marketo Integration Implementation', 'Marketo Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Adobe Analytics Implementation', 'Adobe Analytics Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Slack Integration Implementation', 'Slack Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Data Isolation Support', 'Data Isolation');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Tableau Integration Implementation', 'Tableau Integration');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Predict iQ Implementation', 'Predict iQ');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Website Feedback Intercept', 'SI Core');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Site Intercept Support - one domain or app, one intercept', 'SI Core');
                    addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Conjoint Support', 'Conjoint');
                }
            }
            if(bundlename.includes('Services Only')){
                addremoves = helper.processAddRemoveRuleTechBP(addremoves, techQlis, bundleproducts, bundlename, 'Engineering Services Maintenance', 'Custom Engineering Services', selectedServiceBundles, 'Custom Engineering Services' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'Engineering Services Maintenance', 'Custom Engineering Services' );
            }
            if(bundlename.includes('RSAcademic')){
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts,'RS Academic SSO Configuration', 'SSO');
                addremoves = helper.processAddRemoveRuleTechBP(addremoves, techQlis, bundleproducts, bundlename, 'RC Engineering Services Maintenance', 'RS Academic Custom Engineering Services', selectedServiceBundles, 'Custom Engineering Services' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Engineering Services Maintenance', 'Custom Engineering Services' );
                //addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RS Academic Advanced Security Management Implementation ', 'Advanced Security Management' );
                //addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RS Academic Isolation Implementation ', 'Data Isolation' );
            }
            if(bundlename.includes('RSK12')){
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts,'RSK12 SSO Configuration', 'SSO');
                addremoves = helper.processAddRemoveRuleTechBP(addremoves, techQlis, bundleproducts, bundlename, 'RC Engineering Services Maintenance', 'RS K12 Custom Engineering Services', selectedServiceBundles, 'Custom Engineering Services' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RC Engineering Services Maintenance', 'Custom Engineering Services' );
                //addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RS K12 Advanced Security Management Implementation ', 'Advanced Security Management' );
                //addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'RS K12 Data Isolation Implementation ', 'Data Isolation' );
            }
            if(bundlename.includes('360Academic')){
                addremoves = helper.processAddRemoveRuleTechBP(addremoves, techQlis, bundleproducts, bundlename, '360 Engineering Services Maintenance', '360 Academic Custom Engineering Services', selectedServiceBundles, 'Custom Engineering Services' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, '360 Engineering Services Maintenance', 'Custom Engineering Services' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, '360 Academic Directory Import Automation Support', 'Employee Directory Import Automation' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, '360 Academic Developer Tools Introductory Support', '360 Feature - Developer Tools' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, '360 Academic SSO Configuration', '360 Feature - SSO' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, '360 Academic Vanity URL Setup', '360 Feature - Vanity URL');
            }
            if(bundlename.includes('VOC')){
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts,'VOC Academic SSO Configuration', 'SSO');
                addremoves = helper.processAddRemoveRuleTechBP(addremoves, techQlis, bundleproducts, bundlename, 'CX Engineering Services Maintenance', 'Vocalize Academic Custom Engineering Services', selectedServiceBundles, 'Custom Engineering Services' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'CX Engineering Services Maintenance', 'Custom Engineering Services' );
                //addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'Vocalize Academic Advanced Security Management Implementation ', 'Advanced Security Management' );
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'Vocalize Academic Data Isolation Implementation ', 'Data Isolation' );
            }
            
            if(bundlename.includes('VOCK12')){
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 iQ Directory Advanced Support', 'K12 VoC iQ Directory - State of the Art');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 Stats iQ Pivot/Regression Support', 'Stats iQ');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 Stats iQ Describe/Relate Support', 'Stats iQ');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 Vanity URL Setup', 'Vanity URL');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 Closed Loop Configuration', 'Closed-Loop Follow Up');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 Intelligent Text Analytics Configuration', 'Intelligent Text Analytics');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 Extended Implementation w/ Text Analytics', 'Intelligent Text Analytics');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 SSO Configuration', 'VOC Features K12 - SSO (C)');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 Developer Tools Introductory Support', 'VOC Features K12 - Developer Tools (C)');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 Salesforce Integration Support', 'SFDC/CRM Integration');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 Marketo Integration Support', 'Marketo Integration');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 Adobe Analytics Integration Support', 'Adobe Analytics Integration');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 Slack Integration Support', 'Slack Integration');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 Data Isolation Setup', 'Data Isolation');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 Tableau Integration Support', 'Tableau Integration');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'Vocalize K12 Predict iQ Support', 'Predict iQ');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 Website Targeting Support - one domain or app, one intercept', 'Website Targeting');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 Conjoint Support', 'Conjoint');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 Action Planning Configuration', 'Action Planning');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 Zendesk Integration Support', 'Zendesk Integration');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 Freshdesk Integration Support', 'Freshdesk Integration');
                addremoves = helper.processAddRemoveRuleServiceBP(addremoves, techQlis, serviceQlis, selectedServiceBundles, servicebundleproducts, 'VocK12 ServiceNow Integration Support', 'ServiceNow Integration');
                
            }
            
            //console.log('will set up add removes event if any add/remove update have been made by rules engine');
            if(addremoves.length > 0){
                //console.log('addremoves: ' + JSON.stringify(addremoves));
                var additions = [];
                var removals = [];
                for(var i=0; i < addremoves.length; i++){
                    if(addremoves[i].type == "Add"){
                        delete addremoves[i].type;
                        additions.push(addremoves[i]);
                    }
                    if(addremoves[i].type == "Remove"){
                        delete addremoves[i].type;
                        removals.push(addremoves[i]);
                    }
                }
                var addremoveEvent = $A.get("e.c:cpq_RulesAddRemove");
                //console.log('add remove event setup');
                addremoveEvent.setParams({ "additions" : additions,
                                          "removals" : removals});
                addremoveEvent.fire();
                //console.log('add remove event fired with data: adds' + JSON.stringify(additions) + 'and removes' + JSON.stringify(removals));
            }
            //End Add/Remove rules
            //
        }
        //console.log('END OF THE RULES ENGINE');
    },
})