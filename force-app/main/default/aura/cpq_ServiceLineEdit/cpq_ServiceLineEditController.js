({
    inlineEditQuantity : function(component,event,helper){  
       // console.log('ServiceLIneEdit Controller: inlineEditQuantity');
        component.set("v.quantityEditMode", true); 
        setTimeout(function(){ 
            component.find("Quantity").focus();
        }, 100);
    },
    
    inlineEditPrice : function(component,event,helper){   
       // console.log('ServiceLIneEdit Controller: inlineEditPrice');
        component.set("v.priceEditMode", true); 
        setTimeout(function(){ 
            component.find("PriceEach").focus();
        }, 100);
    },
    
    onQuantityChange : function(component,event,helper){ 
       // console.log('ServiceLIneEdit Controller: QuantityChange');
        var quantity = event.getSource().get("v.value").trim();
        var record = component.get('v.singleRec');
        var max = component.get('v.singleRec').Max_Cap__c;
        var min = component.get('v.singleRec').Min_Cap__c;
        if(quantity > max){
            alert('The quantity cannot be higher than '+max);
            quantity = max;
        }
        if(quantity < min){
            alert('The quantity cannot be lower than '+min);
            quantity = min;
        }
        component.find('Quantity').set('v.value',quantity);
        var p = record.PriceEach * quantity;
        component.set('v.singleRec.PriceTotal',p);
        helper.updateServiceQlis(component, event, helper);
    },
    
    onPriceChange : function(component,event,helper){
      //  console.log('ServiceLIneEdit Controller: PriceChange');
        var price = event.getSource().get("v.value").trim();
        var record = component.get('v.singleRec');
        var p = price * record.Quantity;
        component.set('v.singleRec.PriceTotal',p);
        helper.updateServiceQlis(component, event, helper);
    },  
    
    closeQuantityBox : function (component, event, helper) {
      //  console.log('ServiceLIneEdit Controller: closeQuantityBox');
        component.set("v.quantityEditMode", false); 
        if(event.getSource().get("v.value").trim() == ''){
            component.set("v.showErrorClass",true);            
        }else{
            component.set("v.showErrorClass",false);
        }
    }, 
    
    closePriceBox : function (component, event, helper) {
      //  console.log('ServiceLIneEdit Controller: closePriceBox');
        component.set("v.priceEditMode", false); 
        if(event.getSource().get("v.value").trim() == ''){
            component.set("v.showErrorClass",true);
        }else{
            component.set("v.showErrorClass",false);
        }
    }, 
    
    deleteFeature: function(component,event,helper){
       // console.log('ServiceLIneEdit Controller: deleteFeature');
        component.set('v.deleteFeature',true);
        helper.updateServiceQlis(component, event, helper);
    }
    
})