({
	updateServiceQlis : function(component, event, helper) {
        //console.log('ServiceLineEditHelper: updateServiceQlis');
        var singleRec = component.get('v.singleRec');
        var records = [];
        records.push(singleRec);
        var appEvent = component.getEvent("serviceLineEdit");
        appEvent.setParams({
            singleRecord : records,
            deleteFeature: component.get('v.deleteFeature')
        });
        appEvent.fire();
    }
})