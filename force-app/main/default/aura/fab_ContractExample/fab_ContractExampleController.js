({
    zeroToggle : function(component, event, helper) {
        //console.log('zeroToggle')
        var toggle = component.find('zeroToggle').get('v.value');
        // console.log(toggle)
        component.set('v.zeroLineItems',toggle);     
        helper.updateCd(component,event,helper,'Zero_Dollar_Line_Items__c',toggle);
        helper.updateQuote(component,event,helper,toggle)
    },
    
    itemizedToggle : function(component, event, helper) {
        // console.log('itemizedToggle')
        var toggle = component.find('itemizedToggle').get('v.value');
        component.set('v.itemized',toggle);    
        helper.updateCd(component,event,helper,'Itemized__c',toggle);
    },
    
    discountVisibleToggle : function(component, event, helper) {
        //  console.log('discountVisibleToggle')
        var toggle = component.find('discountVisibleToggle').get('v.value');
        component.set('v.discountVisible',toggle);    
        helper.updateCd(component,event,helper,'Discount_Visible__c',toggle);
    },
    
    servicesOnlyToggle : function(component, event, helper) {
        //  console.log('servicesOnlyToggle')
        var toggle = component.find('servicesOnlyToggle').get('v.value');
        component.set('v.servicesOnly',toggle); 
        helper.updateCd(component,event,helper,'Services_Only__c',toggle);
    },
    
    multiYearToggle : function(component, event, helper) {
        // console.log('multiYearToggle')
        var toggle = component.find('multiYearToggle').get('v.value');
        component.set('v.multiYear',toggle); 
        helper.updateCd(component,event,helper,'MultiYear_Quote__c',toggle);
    },
    
    goToCPQ : function (component,event,helper){        
        // console.log('goToCPQ');
        var context = component.get("v.UserContext");
        var opportunityId = component.get('v.opportunity').Id
        var url = '/apex/cpq_VFPage?Id='+component.get('v.opportunity').Id
        //Detect if in Visualforce domain
        if(window.location.hostname.includes('visual.force.com')){//If in Visualforce domain
            //Navigation in the salesforce1/lightning app
            //  console.log('typeof sforce' + typeof sforce);
            if( (typeof sforce != 'undefined') && sforce && (!!sforce.one) ) {
                //   console.log('using sforce one navigation');
                sforce.one.navigateToURL(url);
                //sforce.one.back()
            }
            else {
                //Navigation in classic.
                //   console.log('using window.location navigation');
                window.location.assign(url);
            }
        }
        else{//If not in Visualforce domain
            // console.log('using lightning event navigation');
            var event = $A.get("e.force:navigateToURL");
            event.setParams({"url": url});
            event.fire();
        }
    },
    
    goToDiscounts : function (component,event,helper){        
        // console.log('goToCPQ');
        var context = component.get("v.UserContext");
        var opportunityId = component.get('v.opportunity').Id
        var url = '/apex/XM_Discounts?Id='+component.get('v.opportunity').SyncedQuoteId
        //Detect if in Visualforce domain
        if(window.location.hostname.includes('visual.force.com')){//If in Visualforce domain
            //Navigation in the salesforce1/lightning app
            //  console.log('typeof sforce' + typeof sforce);
            if( (typeof sforce != 'undefined') && sforce && (!!sforce.one) ) {
                //  console.log('using sforce one navigation');
                sforce.one.navigateToURL(url);
                //sforce.one.back()
            }
            else {
                //Navigation in classic.
                //  console.log('using window.location navigation');
                window.location.assign(url);
            }
        }
        else{//If not in Visualforce domain
            //  console.log('using lightning event navigation');
            var event = $A.get("e.force:navigateToURL");
            event.setParams({"url": url});
            event.fire();
        }
    },
    
    goToQuote : function (component,event,helper){        
        //console.log('goToCPQ');
        var context = component.get("v.UserContext");
        var opportunityId = component.get('v.opportunity').Id
        var url = '/apex/QuoteLayout?Id='+component.get('v.opportunity').SyncedQuoteId
        //Detect if in Visualforce domain
        if(window.location.hostname.includes('visual.force.com')){//If in Visualforce domain
            //Navigation in the salesforce1/lightning app
            // console.log('typeof sforce' + typeof sforce);
            if( (typeof sforce != 'undefined') && sforce && (!!sforce.one) ) {
                //  console.log('using sforce one navigation');
                sforce.one.navigateToURL(url);
                //sforce.one.back()
            }
            else {
                //Navigation in classic.
                //  console.log('using window.location navigation');
                window.location.assign(url);
            }
        }
        else{//If not in Visualforce domain
            //  console.log('using lightning event navigation');
            var event = $A.get("e.force:navigateToURL");
            event.setParams({"url": url});
            event.fire();
        }
    },
    
    handleSubmit : function(component, event, helper) {
        // console.log('CEC: handleSuccess');
        var eventFields = event.getParam("fields");
        var quoteList = component.get('v.quoteList');
        var numberOfYears = eventFields["MultiYear_Number_of_Years__c"]
        var start;
        var end;
        var term = 0;
        var total = 0;
        quoteList.forEach(function(ql){
            if(ql.Quote_Year_Multi_Year__c != undefined){
                term += ql.License_Term_in_months__c;
                total += ql.TotalPrice;
            }
            
            if(ql.Quote_Year_Multi_Year__c == 1){
                start = ql.License_Start_Date__c;
            }
            if(ql.Quote_Year_Multi_Year__c == numberOfYears){                
                end = ql.License_End_Date__c
            }
        })
        component.set('v.myStartDate',start);
        component.set('v.myEndDate',end);
        component.set('v.myTerm',term);
        component.set('v.myTotal',total);
        helper.updateCd(component,event,helper);
    },
    
})