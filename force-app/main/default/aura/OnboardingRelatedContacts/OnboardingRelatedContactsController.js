({
    doInit : function(component, event, helper) {
        helper.getRelatedContacts(component, component.get("v.accountId"));
    },
    handleAccountIdChange : function(component, event, helper) {
        if (component.get("v.accountId") && event.getParam("oldValue") != event.getParam("value")) {
            helper.getRelatedContacts(component, component.get("v.accountId"));
        }
    },
    handleSelectOption : function(component, event, helper) {
        component.find("contactLoader").reloadRecord(false);
        component.set("v.showContactEdit", true);
    },
    handleSelectContact : function(component, event, helper) {
        const allValid = component.find("contact-edit-field").reduce(function(validSoFar, field) {
        	field.reportValidity();
        	return validSoFar && field.checkValidity();
        }, true);
        if (allValid) {
            if (component.get("v.showRequestedUsername") && !component.get("v.pendingContactFields.Requested_Username__c")) {
                component.set("v.pendingContactFields.Requested_Username__c", component.get("v.pendingContactFields.Email"));
            }
            helper.saveContact(component);
        }
    },
    filterContacts : function(component, event, helper) {
        const searchValue = component.get("v.searchValue").toLowerCase();
        if (searchValue) {
            const filteredContacts = component.get("v.relatedContactOptions").filter(function(contact) {
                return contact.label.toLowerCase().indexOf(searchValue) !== -1;
            }).slice(0, 10);
            component.set("v.shownContactOptions", filteredContacts);
            component.set("v.showNoContactsFound", filteredContacts.length === 0);
        } else {
            component.set("v.shownContactOptions", component.get("v.relatedContactOptions").slice(0,10));
        }
    }
})