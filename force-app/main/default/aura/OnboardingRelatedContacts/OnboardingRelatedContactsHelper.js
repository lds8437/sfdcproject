({
    getRelatedContacts : function(component, accountId) {
        component.set("v.showSpinner", true);
        const relatedContactsAction = component.get("c.getContactsFromAccount");
        relatedContactsAction.setParams({"accountId": accountId});
        relatedContactsAction.setCallback(this, function(response) {
            const state = response.getState();
            if (state === "SUCCESS") {
                const relatedContacts = response.getReturnValue();
                const contactRadioOptions = [];
                const contactList = [];
                relatedContacts.forEach(function(contact) {
                    contactList.push(contact);
                    contactRadioOptions.push({ "label": contact.Name, "value": contact.Id});
                });
                component.set("v.relatedContacts", contactList);
                component.set("v.relatedContactOptions", contactRadioOptions);
                component.set("v.shownContactOptions", contactRadioOptions.slice(0,10));
                if (contactRadioOptions.length > 10) {
                    component.set("v.showSearch", true);
                    component.set("v.showNoContactsFound", false);
                } else if (contactRadioOptions.length > 0) {
                    component.set("v.showSearch", false);
                    component.set("v.showNoContactsFound", false);
                } else {
                    component.set("v.showSearch", false);
                    component.set("v.showNoContactsFound", true);
                }
            }
            else if (state === "INCOMPLETE") {
                console.log("Could not retrieve related contacts: request incomplete.");
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Could not retrieve related contacts: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Could not retrieve related contacts: Unknown error");
                }
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(relatedContactsAction);
    },
    saveContact : function(component) {
        component.set("v.showSpinner", true);
        component.find("contactLoader").saveRecord($A.getCallback(function(saveResult) {
            const saveEvent = component.getEvent("contactSaved");
            saveEvent.setParams({
                "saveResponse": saveResult,
                "savedContactId": component.get("v.pendingContactId")
            });
            saveEvent.fire();
            const state = saveResult.state;
            if (state === "SUCCESS" || state === "DRAFT") {
                component.set("v.showContactEdit", false);
                component.set("v.pendingContactId", null);
            }
            component.set("v.showSpinner", false);
        }));
    }
})