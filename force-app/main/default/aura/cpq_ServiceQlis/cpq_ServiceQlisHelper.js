({
    getServiceBundles : function(component,event,helper){
        //console.log('Service Helper: getServiceBundles');
        var availableServiceBundles = _.filter(component.get('v.selectedServiceBundles'),
                                               {'Bundle_Product_Prerequisite__c':component.get('v.singleRec').Bundle_Product__c})
        //console.log(component.get('v.availableServiceBundles'));
        //console.log(component.get('v.singleRec'));
        if(availableServiceBundles.length === 1){                    
            component.set('v.selectedServiceBundle', availableServiceBundles[0]); 
        }else{
            component.set('v.singleBundle', true);
        }
        var currentServiceQlis = [];
        var total=0;
        _.forEach(component.get('v.serviceQlis'),function(qli){
            if(qli.Related_Service_Bundle__c === availableServiceBundles[0].Id){
                currentServiceQlis.push(qli);
                total += qli.PriceTotal;
            }
        })
        component.set('v.currentServiceQlis',currentServiceQlis);
        component.set('v.serviceQliTotal',total);
        helper.getAvailableServiceComponents(component,event,helper);  
        if(component.get('v.actionType')!=='Edit' && component.get('v.currentServiceQlis').length===0){
            helper.getIncludedServiceComponents(component,event,helper);  
        }
    },
    
    getIncludedServiceComponents : function(component,event,helper) {     
        // console.log('Service Helper: getServiceComponents'); 
        var bundleProducts  = _.filter(component.get('v.allServiceProducts'),{'Related_Service_Bundle__c':component.get('v.selectedServiceBundle')[0].Id});
        var includedProducts  = _.filter(bundleProducts,{'Availability__c':'Included'});
        var serviceQlis = [];
        _.forEach(includedProducts,function(sli){
            sli.PriceEach = 0;
            sli.PriceTotal = 0;
            sli.BundleDiscount = 0;
            sli.TierQty = 1;
            sli.PriceUpdate = false;
            sli.PartnerDiscount = 1;
            serviceQlis.push(sli);
        });  
        component.set('v.currentServiceQlis',serviceQlis);
        helper.updatePrice(component, event, helper);
    },
    
    getAvailableServiceComponents : function(component,event,helper) {     
        //console.log('Service Helper: getAvailableServiceComponents');  
        var selectedServiceBundleProducts = [];
        var total=0;
        _.forEach(component.get('v.allServiceProducts'),function(product){
            if(product.Related_Service_Bundle__c === component.get('v.selectedServiceBundle')[0].Id){
                if(product.Availability__c === 'Available'){
                    product.PartnerDiscount = component.get('v.selectedServiceBundle')[0].PartnerDiscount;
                    selectedServiceBundleProducts.push(product);
                } /*else if(product.Availability__c === 'Restricted' && component.get('v.restrictedStatus')==='Approved'){
                    product.PartnerDiscount = component.get('v.selectedServiceBundle')[0].PartnerDiscount;
                    selectedServiceBundleProducts.push(product);
                }*/
            }
        });
        var av = _.differenceBy(selectedServiceBundleProducts,component.get('v.serviceQlis'),'Service_Bundle_Product__c');
        component.set('v.selectedServiceBundleProducts',av);
        helper.updatePrice(component, event, helper);
    },
    
    updatePrice : function(component, event, helper) { 
        //console.log('Service Helper: updatePrice');
        var priceList = component.get('v.selectedPriceList');
        var currentServiceQlis = component.get('v.currentServiceQlis');
        var techQlis = component.get('v.techQlis');
        var price = component.get('v.allPricing');
        var group = _.groupBy(currentServiceQlis,'Price_Based_On__c'); 
        var priceGroup = [];
        _.forEach(group, function(value,key){
            _.filter(currentServiceQlis,function(o){
                if(o.PricingType__c === key){
                    priceGroup[key] = o.Quantity__c;
                }
            }); 
        });
        currentServiceQlis = _.forEach(currentServiceQlis,function(o){
            _.forIn(priceGroup,function(value,key){
                if(o.Price_Based_On__c === key){
                    o.TierQty = value;
                }
            });
        });
        var total=0;
        var currentServiceQlis = _.forEach(currentServiceQlis,function(o){    
            _.forEach(price,function(p){                
                if(o.PricingType__c === p.Pricing_Type__c && o.TierQty >= p.Minimum__c && o.TierQty <= p.Maximum__c){
                    if(o.PriceUpdate != true){
                        if(priceList == undefined || priceList == 'Corporate') {
                            o.ServiceOriginalPrice = p.List_Price__c;
                            o.PriceEach = p.List_Price__c * o.PartnerDiscount;
                        }
                        else if (priceList == 'GSA') {
                            o.ServiceOriginalPrice = p.GSA__c;
                            o.PriceEach = p.GSA__c * o.PartnerDiscount;
                        }
                            else if (priceList == 'GSACarahsoft') {
                                o.ServiceOriginalPrice = p.GSACarahsoft__c;
                                o.PriceEach = p.GSACarahsoft__c * o.PartnerDiscount;
                            }
                                else if (priceList == 'FedRamp') {
                                    o.ServiceOriginalPrice = p.FedRamp__c;
                                    o.PriceEach = p.FedRamp__c * o.PartnerDiscount;
                                }
                                    else if (priceList == 'FedRampCarahsoft') {
                                        o.ServiceOriginalPrice = p.FedRampCarahsoft__c;
                                        o.PriceEach = p.FedRampCarahsoft__c * o.PartnerDiscount;
                                    }
                        o.PriceTotal = o.Quantity__c * o.PriceEach;
                    }
                    total += o.PriceTotal;
                }
            });
        });
        //var markupLine = _.findIndex(techQlis, {'Type__c' : 'Markup'}); This had to be updated 9/25 because new markup bundle products were added
        var markupLine = _.findIndex(techQlis, function(qli) {
            return qli.Related_Product__r.ProductCode.includes('ImpMarkup');
        });
        if(markupLine > -1) {
            techQlis[markupLine].PriceEach = total * techQlis[markupLine].MarkupPercentage;
            techQlis[markupLine].PriceTotal = total * techQlis[markupLine].MarkupPercentage;
            techQlis[markupLine].PriceUpdate = "true";
        }
        component.set('v.currentServiceQlis', currentServiceQlis); 
        component.set('v.serviceQliTotal',total);
        component.set('v.serviceQlis', currentServiceQlis);
    },
    
    updateCombinedQlis: function(component,event,helper){
        //console.log('Service Helper: updateCombinedQlis');
        //console.log(component.get('v.techQlis'));
        var techQlis = [];
        var serviceQlis = [];
        var combinedQlis = [];
        var selectedServiceBundles = [];
        var partnerAvailability = [];
        _.forEach(component.get('v.techQlis'),function(t){  
            var techQli = {};
            techQli.TechBundle = t.Related_Bundle__c;
            techQli.TechId = t.Id;
            techQli.TechName = t.ProductName;     
            techQli.Quantity = t.Quantity__c;
            techQli.BundleKey = t.Bundle_Key__c;
            techQli.Revenue_Type__c = t.Revenue_Type__c;
            techQli.Type__c = t.Type__c;
            techQli.BundleDiscount = t.BundleDiscount;
            techQlis.push(techQli);
        });
        _.forEach(component.get('v.currentServiceQlis'),function(s){
            var serviceQli = {};
            serviceQli.TechId = s.Bundle_Product_Prerequisite__c;
            serviceQli.ServiceBundle = s.Related_Service_Bundle__c;
            serviceQli.ServiceId = s.Id;
            serviceQli.ServiceName = s.ProductName;
            serviceQli.Quantity = s.Quantity__c;
            serviceQli.PartnerDiscount = s.PartnerDiscount;
            serviceQlis.push(serviceQli);
        });
        _.forEach(component.get('v.selectedServiceBundles'),function(sb){
            var selectedServiceBundle = {};
            selectedServiceBundle.Id = sb.Id;
            selectedServiceBundle.BundleType = sb.Bundle_Type__c;
            selectedServiceBundle.ProviderSelected = sb.Provider_Selected__c;
            selectedServiceBundle.Name = sb.Name;
            selectedServiceBundle.PartnerDiscount = sb.PartnerDiscount;
            selectedServiceBundle.Bundle_Product_Prerequisite__c = sb.Bundle_Product_Prerequisite__c;
            selectedServiceBundle.Custom__c = sb.Custom__c;
            selectedServiceBundles.push(selectedServiceBundle);
        });
        _.forEach(component.get('v.selectedPartners'),function(partner){
            var selectedPartnerAvailability = {};
            selectedPartnerAvailability.maxDealSize = partner.Maximum_Deal_Size__c;
            selectedPartnerAvailability.minDealSize = partner.Minimum_Deal_Size__c;
            partnerAvailability.push(selectedPartnerAvailability);
        });
        
        var appEvent = $A.get("e.c:cpq_SendToRulesEngine");  
      //  console.log(component.get('v.surveyCount'));
        appEvent.setParams({
            arr: component.get('v.arr'),
            netTotal: component.get('v.netTotal'),
            surveyCount: component.get('v.surveyCount'),
            dashboardUsers: component.get('v.dashboardUsers'),
            employeeCount: component.get('v.employeeCount'),
            digitalFeedbackDomainQuantity: component.get('v.digitalFeedbackDomainQuantity'),
            digitalFeedbackInterceptQuantity: component.get('v.digitalFeedbackInterceptQuantity'),
            featureSelected: component.get('v.featureSelected'),
            featureAddOrRemove: component.get('v.featureAddOrRemove'),
            techQlis: techQlis,
            serviceQlis: serviceQlis,
            selectedServiceBundles: selectedServiceBundles,
            partnerAvailability: partnerAvailability,
            allBundleProducts: component.get('v.allBundleProducts'),
            allServiceProducts: component.get('v.allServiceProducts'),
            quoteType: component.get('v.quoteType'),
            currency: component.get('v.currency')
        });
        //console.log('fireEvent');
        appEvent.fire(); 
        
    },
    
    updateQuantityFromRules: function(component,event,helper,productId,quantity){
        //console.log('serviceQlisHelper: updateQuantityFromRules')
        //console.log(productId);
        //console.log(quantity);
        //console.log(component.get('v.currentServiceQlis'));
        
        var currentServiceQlis = component.get('v.currentServiceQlis');
        _.forEach(currentServiceQlis,function(s){
            if(s.Id === productId){
                s.Quantity__c = quantity;
                s.PriceTotal = s.PriceEach * quantity;
            }
        });
        
        component.set('v.currentServiceQlis', currentServiceQlis); 
        helper.updatePrice(component,event,helper);
    },
})