({
	handleContactRecordUpdated: function(cmp,event,helper){
        //Handles loading data and assigning default requested username for a selected contact via LDS.
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            console.log('success loading contact');
            
            
        } else if(eventParams.changeType === "CHANGED") {
            // record is changed
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
            console.log('error loading contact');
        }  
    },
    handleRemove : function(cmp,event,helper){
        var removeEvent = cmp.getEvent("adminRemoveEvent");
        removeEvent.setParams({"idToRemove" : cmp.get('v.contactId') });
        removeEvent.fire();
        const unsavedChangesEvent = cmp.getEvent("unsavedChangesEvent");
        unsavedChangesEvent.setParams({
            "tabName": "Admin Setup",
            "showUnsavedIndicator": true
        });
        unsavedChangesEvent.fire();
    },
    returnAdminInfo : function(cmp,event,helper){
      var contact = cmp.get('v.simpleContactRecord');
        var contactinfostring = contact.Email + ', ' + contact.Requested_Username__c + ', ' + contact.FirstName + ', ' + contact.LastName + '; ';
        return contactinfostring;
    },
})