({
	handleClick : function(component, event, helper) {
        component.set("v.showFlow", true);
        console.log("OPPID:", component.get("v.opportunityId"));
        console.log("ACCTID:", component.get("v.accountId"));
		const clientFlow = component.find("clientFlow");
        const oppId = component.get("v.opportunityId");
        const acctId = component.get("v.accountId");
        const inputs = [
            {
                name: "varOppID",
                type: "String",
                value: oppId
            },
            {
                name: "varAcctID",
                type: "String",
                value: acctId
            }
        ];
        clientFlow.startFlow("Q2C_Create_NetSuite_Client", inputs);
	},
    handleStatusChange : function (component, event, helper) {
        console.log('OCC: handleStatusChange')
        if (event.getParam('status') === "FINISHED") {
          const reloadEvent = component.getEvent("reloadOpportunity");
          reloadEvent.fire();
            const fabReloadEvent = $A.get("e.c:fab_Reload");
          if(fabReloadEvent !== undefined){
                        fabReloadEvent.fire();
                    }
       
          component.set("v.showFlow", false);
        }
    },
    handleClientChange : function(component, event, helper) {
        console.log('OCC: handleClientChange')
        
        const buttonLabel = component.get("v.selectedClientId") ? 'Remove Client' : 'Create or Select Client';
        component.set("v.buttonLabel", buttonLabel);
        
    }
})