({
    
    setAvailableExperiences : function(component, event, helper, response) { 
      //  console.log('CMP HLPR: setAvailableExperiences');
        var avExperiences=[];                
        _.mapKeys(response, function(value,key){
            if(value === true){
                avExperiences.push(_.replace(key,'__c',''));
            }}
                 );
        //console.log('CMP HLPR: setAvailableExperiences - callServer getExperiences');
        helper.callServer(
            component,
            "c.getExperiences",
            function(response) {
              //  console.log(response);
                component.set('v.experienceMetaData', response);
                var options=[];
                for(var i = 0; i < response.length; i++){ 
                    options.push({'label' : response[i].MasterLabel,
                                  'value' : response[i].Code__c}) 
                }
                component.set('v.options',options);
                //console.log('CMP HLPR: setAvailableExperiences - CALL HLPR setPriceListOptions');
                helper.setPriceListOptions(component, event, helper);
            },
            {
                quotingPermissions : avExperiences
            }
        );
    },
    
    getQuoteLineItems: function(component, event, helper){
     //  console.log('CMP HLPR: GetQuoteLineItems');
        //console.log('CMP HLPR: GetQuoteLineItems - callServer getQuoteLineItems');
        helper.callServer(
            component,
            "c.getQuoteLineItems",
            function(response){
                //console.log(response);
                _.forEach(response,function(r){
                    r.ProductName = r.Product2.Name;
                    r.Quantity__c = r.Quantity;
                    r.PriceEach = r.UnitPrice;
                    r.PriceTotal = r.TotalPrice;
                    r.BundleDiscount = r.Max_Bundle_Discount__c;
                     //r.PriceUpdate = true; This cannot be marked as true for all line items or it can't get updated if the tier changes.
                    if(r.Unlimited_Users__c){
                        r.Expected_Display_Name__c = r.Bundle_Product__r.Display_Label__c + ' (Unlimited)';
                    } else if (r.Quantity__c === 1 && r.Bundle_Product__r.User_Input_Required__c == false){
                        r.Expected_Display_Name__c = r.Bundle_Product__r.Display_Label__c;
                    } else {
                        r.Expected_Display_Name__c = r.Bundle_Product__r.Display_Label__c + ' ' + r.Bundle_Product__r.Display_Variable__c + ' ' + r.Quantity__c;
                    }
                    if(r.External_Display_Name__c != r.Expected_Display_Name__c) {
                        r.DisplayEdited = true;
                    }
                    if(r.Exclude_from_Quote_Display__c) {
                        r.Exclude_from_PDF__c = true;
                    }
                    if(r.List_Price__c > 0 && r.Product2.Eligible_for_Discount__c && (r.Quote.Discretionary_Discount__c == 0 || r.Quote.Discretionary_Discount__c == undefined || r.Quote.Discretionary_Discount__c == null) && r.Bundle_Product__r.PricingType__c == 'Core') {
                        component.set('v.coreDiscountPercentage', (r.UnitPrice / r.List_Price__c));
                        component.set('v.nonCoreDiscountPercentage', 1);
                    }
                    else if(r.List_Price__c > 0 && r.Product2.Eligible_for_Discount__c && r.Quote.Discretionary_Discount__c > 0) {
                        component.set('v.coreDiscountPercentage', (r.UnitPrice / r.List_Price__c));
                        component.set('v.nonCoreDiscountPercentage', r.UnitPrice / (r.List_Price__c - r.Max_Bundle_Discount__c));
                    }
                   if(r.Bundle_Product__r.PricingType__c == 'Cred') {
                            r.PriceUpdate = true;
                        }
                    if(r.Bundle_Lookup__r.Experience__c == 'RS' && r.Bundle_Product__r.Availability__c == 'Included') {
                        r.PriceUpdate = true;
                    }
                    if(r.Service__c != undefined) {
                        r.PriceUpdate = true;
                        if(r.Service__r.Qualtrics_Invoicing__c === false && r.Service__r.Custom__c == false && r.Service__r.Q_Partner__c != undefined) {
                            r.PriceEach = r.Total_Partner_Amount__c;
                            r.PriceTotal = r.Total_Partner_Amount__c;
                            r.PriceUpdate = true;
                        }
                        if(r.Service__r.Custom__c == true) {
                            r.DoNotRecreate = true;
                            r.PriceUpdate = true;
                            r.Availability__c = 'Included';
                        }
                        else if(r.Service__r.Markup_Amount__c > 0) {
                            r.MarkupPercentage = r.Service__r.Partner_Availability__r.Markup_Percentage__c / 100;
                            r.HasMarkupLine = true;
                            r.PriceUpdate = true;
                        }
                    }else if(r.Product2.ProductCode.toUpperCase().includes('MAINT')) {
                        r.PriceUpdate = true;
                        r.License = 'true';
                    }
                });
                if(component.get('v.coreDiscountPercentage')===undefined) {
                    component.set('v.coreDiscountPercentage', 1);
                }
                if(component.get('v.nonCoreDiscountPercentage')===undefined) {
                    component.set('v.nonCoreDiscountPercentage', 1);
                }
                component.set('v.trial', response[0].Quote.Pilot__c);
                component.set('v.quoteLines', response);
                component.set('v.bundleId', response[0].Bundle_Lookup__c);
                if(component.get('v.quoteType') === 'Upgrade License') {
                    helper.getClientLicenses(component, event, helper);
                }
            },
            {
                quoteId : component.get('v.quoteId'),
                exp : component.get('v.exp')
            }
        );
    },

    getClientLicenses : function(component, event, helper) {
       // console.log('Component Helper: getRelatedClientLicenses');
        var currencyConversionRates = component.get('v.currencyConversionRates');
        var startDate = component.get('v.quoteStartDate');
        var relatedClients = component.get('v.relatedClients');
        //console.log(relatedClients);
        var endDates = [];
        var bundles = [];
        var clients = [];
        _.forEach(relatedClients, function(client) {
            endDates.push(client.End_Date__c);
            bundles.push(client.Bundle__c);
            clients.push(client.Client__c);
        });
        endDates = _.uniq(endDates);
        bundles = _.uniq(bundles);
        clients = _.uniq(clients);
        helper.callServer(
            component,
            "c.getRelatedClientLicenses",
            function(response) {
                //console.log('getRelatedClientLicenses response');
                //console.log(response);
                _.forEach(response, function(license) {
                    _.filter(currencyConversionRates, function(value, key) {
                        if(key === license.CurrencyIsoCode) {
                            license.licenseCurrency = value;
                        }
                    });
                    license.MergeTerm = moment(license.License_End_Date__c).add(1, 'days').month() - moment(startDate).month();
                    license.TermLength = moment(license.License_End_Date__c).add(1, 'days').month() - moment(license.License_Start_Date__c).month();
                    if(license.MergeTerm <= 0) {
                        if(moment(license.License_End_Date__c).add(1, 'days').month() != moment(startDate).month() || moment(license.License_End_Date__c).add(1, 'days').year() != moment(startDate).year()) {
                            license.MergeTerm = license.MergeTerm + 12;
                        } else {
                            license.MergeTerm = 0;
                        }
                    }
                    if(license.TermLength <= 0) {
                        if(moment(license.License_End_Date__c).month() != moment(license.License_Start_Date__c).month() || moment(license.License_End_Date__c).add(1, 'days').year() != moment(license.License_Start_Date__c).year()) {
                            license.TermLength = license.TermLength + 12;
                        }
                    }
                    license.ProductName = license.Bundle_Product__r.Display_Label__c;
                    license.Quantity__c = license.Quantitiy__c;
                    license.Quantity = license.Quantitiy__c;
                    license.PriceEach = license.Invoice_Amount__c / license.Quantitiy__c;
                    license.PriceTotal = license.Invoice_Amount__c;
                    license.Comparison = license.Product__r.Upgrade_Comparison__c+license.Bundle_Product__r.User_Input_Required__c;

                })
                component.set('v.licenses', response);
            },
            {
                endDates : endDates,
                bundles : bundles,
                clients : clients
            })
    },
    
    setPriceListOptions : function(component, event, helper) {
      //  console.log('CMP HLPR: setPriceListOptions');
        var priceTables = component.get('v.quotingPermissions')[0].Price_Tables__c;
        var priceList = [];
        priceList.push(_.split(priceTables,';'));
        priceList = _.flatten(priceList);        
        var priceListOptions=[];
        for(var i = 0; i < priceList.length; i++){ 
            priceListOptions.push({'label' : priceList[i],
                                   'value' : priceList[i]}); 
        }
        component.set('v.priceListOptions',priceListOptions);
    },
    
})