({
	getCurrency : function(component,event,helper) {
		var action = component.get('c.getCurrencyCodes');
        action.setStorable();   
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
               helper.setCurrencyPicklist(component,dataObj);
            }  
            else if (state === 'ERROR') {
                var header = 'User Id Retrieval Error';
                //  helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
	},
    
    setCurrencyPicklist : function(component,currencyList){
       // console.log(currencyList)
        var oppCurrency = component.get('v.opportunity.CurrencyIsoCode')
        component.set('v.selCurrency',oppCurrency) 
      //  console.log(oppCurrency)
        
        var options=[];
        currencyList.forEach(function(cl){
            var selected = false;
            if(cl.IsoCode === oppCurrency){
                selected = true;
            }
            var option={
                id : cl.IsoCode,
                label : cl.IsoCode,
                selected : selected
            }
            options.push(option);
        })
        
        component.set('v.options',options);
    },
    
    getLinkedRetainers :function(component,event,helper){
        var opportunityId = component.get('v.opportunity.Id');
        var action = component.get('c.getLinkedRetainers');
        action.setParams({
            "opportunityId": opportunityId
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                console.log(dataObj)
                component.set('v.linkedRetainers',dataObj)
                var totalValue = 0;
                dataObj.forEach(function(lr){
                    totalValue += lr.Value_against_Retainer__c;
                })
                component.set('v.totalValue',totalValue)
                if(dataObj.length > 0){
                    console.log('thisworks')
                    component.set('v.linkLoaded',true)
                } 
                
                console.log(component.get('v.linkedRetainers'))
                component.set('v.showSpinner',false)
            }  
            else if (state === 'ERROR') {
                var header = 'User Id Retrieval Error';
                //  helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action); 
    },
    
    reloadFab :function(component,event,helper){
        //  console.log('RSC: reloadFab')
        var cmpEvent = $A.get("e.c:fab_Reload");
        cmpEvent.fire();
    },
})