({
    doInit: function(component, event, helper) {
        // Set default values
        var fieldValue = component.get("v.record.Guided_Sales_Path__c");
        helper.getCompletionCriteriaRecords(component, fieldValue);
        helper.getGlobalDynamicActions(component, fieldValue);
        helper.getStageProgressRecords(component);
        
        // If a modal is open from a previous action, close it.
        component.set("v.displayModal", false);
    },
    handleCompletionCriteriaSelected: function(component, event, helper) {
        var selectedCompletionCriteria = event.getParam("selectedCompletionCriteria");
		helper.setFieldValue(component, event, helper);
    },
    handlePathSelection: function(component, event, helper) {
        var fieldValue = event.getParam("detail").value;
        helper.getCompletionCriteriaRecords(component, fieldValue);
        helper.getGlobalDynamicActions(component, fieldValue);
    },
    handleDisplayModal: function(component, event, helper) {
		helper.displayModal(component, event);
    },
    closeModal: function(component, event, helper) {
    	component.set("v.displayModal", false);
	}
})