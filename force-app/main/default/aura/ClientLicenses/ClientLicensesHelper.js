({
    setUpGroupings: function (component, event, helper) {
        console.log('Client Licenses: setUpGroupings');
        var groupings = component.get('v.groupings');
        var groupObjects = [];
        var clientLicenses = component.get('v.licenses');
        console.log(clientLicenses);
        for (var i = 0; i < groupings.length; i++) {
            console.log('group iteration');
            var newgroupobject = {
                group: "",
                end: "",
                status: "",
                licenses: "",
                clientname: "",
                bundlename: "",
                start: "",
                currencycode: "",
                value: "",
            }; //New Object for group info.
            var groupParams = groupings[i].split("|");
            var clientid = groupParams[0];
            var bundleid = groupParams[1];
            var enddate = groupParams[2];
            var status = groupParams[3];
            newgroupobject.clientid = clientid;
            newgroupobject.bundleid = bundleid;
            newgroupobject.group = groupings[i];
            newgroupobject.end = enddate;
            newgroupobject.status = status;

            //Get group licenses
            var licenses = [];
            for (var n = 0; n < clientLicenses.length; n++) {
                var license = clientLicenses[n];
                if (!(license === undefined)) {
                    console.log('license under review');
                    console.log('current license under review ' + license);
                    console.log('matching paramters: ' + license.Client__c + clientid + license.Bundle__c + bundleid + license.License_End_Date__c + enddate + license.Status__c + status);
                    if (license.Client__c == clientid && license.Bundle__c == bundleid && license.License_End_Date__c == enddate && license.Status__c == status) {
                        licenses.push(license);
                        console.log('license pushed');
                    }
                }
            }
            newgroupobject.licenses = licenses;
            console.log('newgroupobject');
            console.log(newgroupobject);

            if (licenses.length > 0) {
                //Get the bundle name
                if (bundleid != null) {
                    var bundle = licenses[0].Bundle__r;
                    var bundlename = bundle.Name;
                    var experience = bundle.Experience__c;
                    newgroupobject.experience = experience;
                    newgroupobject.bundlename = bundlename;
                }

                //Get the earliest start date
                var starts = [];
                for (n = 0; n < licenses.length; n++) {
                    starts.push(licenses[n].License_Start_Date__c);
                }
                starts.sort();
                var firststart = starts[0];
                newgroupobject.start = firststart;

                //Get the currency code
                var currencycode = licenses[0].CurrencyIsoCode;
                newgroupobject.currencycode = currencycode;

                //Get summary of license value
                var totalvaluesum = 0;
                var listvaluesum = 0;
                for (n = 0; n < licenses.length; n++) {
                    totalvaluesum += licenses[n].Invoice_Amount__c;
                    listvaluesum += licenses[n].Original_Amount__c;
                }
                var locale = licenses[0].Culture_Code__c;
                var totalvaluesumformatted = totalvaluesum.toLocaleString(locale, {
                    style: 'currency',
                    currency: currencycode
                });
                var listvaluesumformatted = listvaluesum.toLocaleString(locale, {
                    style: 'currency',
                    currency: currencycode
                });
                newgroupobject.totalvalue = totalvaluesumformatted;
                newgroupobject.totalvaluesum = totalvaluesum;
                newgroupobject.listvalue = listvaluesumformatted;
                newgroupobject.listvaluesum = listvaluesum;
                if(listvaluesum > 0) {
                    newgroupobject.discount = (1 - (totalvaluesum / listvaluesum));
                } else {
                    newgroupobject.discount = 0;
                }
                newgroupobject.isexpanded = false;
                groupObjects.push(newgroupobject);
                component.set("v.groupObjects", groupObjects);
                console.log('groupObjects');
                console.log(groupObjects.length);
                component.set('v.countOfLicenses',groupObjects.length);
            }
        }
    },

    columnSort: function (component, column) {
        // console.log('ClientLicenseTable Helper: columnSort');
        //Get the current sort state of the column
        var sortstate = 'v.' + column + 'sort';
        // console.log('sortstate: ' + sortstate);
        var currentstate = component.get(sortstate);
        // console.log('currentstate: ' + currentstate);
        //Reset all the sort states to inactive
        if (column != 'bundle') {
            component.set("v.bundlesort", "inactive");
        }
        if (column != 'start') {
            component.set("v.startsort", "inactive");
        }
        if (column != 'end') {
            component.set("v.endsort", "inactive");
        }
        if (column != 'status') {
            component.set("v.statussort", "inactive");
        }
        if (column != 'listvalue') {
            component.set("v.listvaluesort", "inactive");
        }
        if (column != 'totalvalue') {
            component.set("v.totalvaluesort", "inactive");
        }
        if (column != 'discount') {
            component.set("v.discountsort", "inactive");
        }
        var groupings = component.get("v.groupObjects");
        //  console.log('groupings before sort' + groupings);
        if (currentstate == 'asc') {
            component.set(sortstate, "desc");
            var sorted = groupings.sort(function (a, b) {
                var x;
                var y;
                if (column == 'bundle') {
                    x = a.bundlename;
                    y = b.bundlename;
                }
                if (column == 'start') {
                    x = a.start;
                    y = b.start;
                }
                if (column == 'end') {
                    x = a.end;
                    y = b.end;
                }
                if (column == 'status') {
                    x = a.status;
                    y = b.status;
                }
                if (column == 'listvalue') {
                    x = a.listvaluesum;
                    y = b.listvaluesum;
                }
                if (column == 'totalvalue') {
                    x = a.totalvaluesum;
                    y = b.totalvaluesum;
                }
                if (column == 'discount') {
                    x = a.discount;
                    y = b.discount;
                }
                if (x < y) {
                    return 1;
                }
                if (x > y) {
                    return -1;
                }
                return 0;
            });
            component.set("v.groupObjects", sorted);
            //console.log('desc sort attempted');
            //    console.log('groupings after desc sort' + sorted);
        } else {
            component.set(sortstate, "asc");
            var sorted = groupings.sort(function (a, b) {
                var x;
                var y;
                if (column == 'bundle') {
                    x = a.bundlename;
                    y = b.bundlename;
                }
                if (column == 'start') {
                    x = a.start;
                    y = b.start;
                }
                if (column == 'end') {
                    x = a.end;
                    y = b.end;
                }
                if (column == 'status') {
                    x = a.status;
                    y = b.status;
                }
                if (column == 'listvalue') {
                    x = a.listvaluesum;
                    y = b.listvaluesum;
                }
                if (column == 'totalvalue') {
                    x = a.totalvaluesum;
                    y = b.totalvaluesum;
                }
                if (column == 'discount') {
                    x = a.discount;
                    y = b.discount;
                }
                if (x < y) {
                    return -1;
                }
                if (x > y) {
                    return 1;
                }
                return 0;
            });
            component.set("v.groupObjects", sorted);
            //console.log('asc sort attempted');
            // console.log('groupings after asc sort' + sorted);
        }
    }
})