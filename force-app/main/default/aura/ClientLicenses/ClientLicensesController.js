({
    doInit : function(component, event, helper) {
        console.log('Client Licenses: doInit');
        console.log(component.get('v.recordId'));
        var actionacomplete = false;
        var actionbcomplete = false;
        var groupingsinitialized = false;
        //return the license groupings
        var action = component.get("c.getLicenseGroupings");
        action.setParams({
            clientid : component.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set('v.groupings', response.getReturnValue());
                actionacomplete = true;
                if(actionbcomplete) {
                    if(!groupingsinitialized){
                        groupingsinitialized = true;
                        component.set('v.spinner', false);
                        helper.setUpGroupings(component, event, helper);
                    }
                }
            }
        });
        component.set('v.spinner', true);
        $A.enqueueAction(action);
        //return the relevant client licenses
        var baction = component.get("c.getClientLicenses");
        baction.setParams({
            clientid : component.get('v.recordId')
        });
        baction.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            if(state === 'SUCCESS') {
                //console.log(response.getReturnValue());
                //component.set('v.licenses', response.getReturnValue());
                var licenses = response.getReturnValue();
                for(var i=0;i<licenses.length;i++) {
                    if(licenses[i].Bundle_Product__c === undefined) {
                        licenses[i].sortOrder = 6;
                    }
                    else if(licenses[i].Bundle_Product__r.PricingType__c === 'Core' && licenses[i].Bundle_Product__r.PricingType__c != undefined) {
                        licenses[i].sortOrder = 1;
                    } else if(licenses[i].Bundle_Product__r.PricingType__c === 'UserD' && licenses[i].Bundle_Product__r.PricingType__c != undefined) {
                        licenses[i].sortOrder = 2;
                    } else if(licenses[i].Bundle_Product__r.User_Input_Required__c && licenses[i].Bundle_Product__r.User_Input_Required__c != undefined) {
                        licenses[i].sortOrder = 3;
                    } else if(licenses[i].Bundle_Product__r.Availability__c != undefined && (licenses[i].Bundle_Product__r.Availability__c === 'Available' || licenses[i].Bundle_Product__r.Availability__c === 'Restricted' || licenses[i].Bundle_Product__r.Availability__c === 'Historic')) {
                        licenses[i].sortOrder = 4;
                    } else if(licenses[i].Bundle_Product__r.PricingType__c === 'Cred' && licenses[i].Bundle_Product__r.PricingType__c != undefined) {
                        licenses[i].sortOrder = 5;
                    } else {
                        licenses[i].sortOrder = 6;
                    }
                }
                console.log(licenses);
                licenses.sort(function(a,b) {
                    return a.sortOrder - b.sortOrder;
                });
                //console.log(licenses);
                component.set('v.licenses', licenses);
                actionbcomplete = true
                if(actionacomplete) {
                    if(!groupingsinitialized){
                        groupingsinitialized = true;
                        component.set('v.spinner', false);
                        helper.setUpGroupings(component, event, helper);
                    }
                }
            } else if(state === 'INCOMPLETE') {

            } else if (state === "ERROR") {
                var errors = response.getError();
                console.log(errors);
                }
            });
            component.set('v.spinner', true);
            $A.enqueueAction(baction);

            //Get user permissions for client licenses
            var caction = component.get("c.getAccess");
            caction.setCallback(this, function(response) {
                var state = response.getState();
                if(state === 'SUCCESS') {
                    console.log('user permissions');
                    console.log(response.getReturnValue());
                    component.set('v.readAccess', response.getReturnValue()[0]);
                    component.set('v.createAccess', response.getReturnValue()[1]);
                    component.set('v.editAccess', response.getReturnValue()[2]);
                    component.set('v.deleteAccess', response.getReturnValue()[3]);
                } else if(state === 'INCOMPLETE') {

                } else if(state === 'ERROR') {
                    var errors = response.getError();
                    console.log(errors);
                }
            });
            $A.enqueueAction(caction);
    },

    handleRecordUpdated : function(component, event, helper) {
        console.log('Client Licenses: handleRecordUpdated');
        //console.log(event.getParams());
        var record = component.get('v.simpleRecord');
        if(record != null) {
            var clientId = record.Related_Client__c;
            var actionacomplete = false;
            var actionbcomplete = false;
            var groupingsinitialized = false;
            //return the license groupings
            var action = component.get("c.getLicenseGroupings");
            action.setParams({
                clientid : clientId
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log(state);
                if (state === "SUCCESS") {
                    console.log(response.getReturnValue());
                    component.set('v.groupings', response.getReturnValue());
                    actionacomplete = true;
                    if(actionbcomplete) {
                        if(!groupingsinitialized){
                            groupingsinitialized = true;
                            component.set('v.spinner', false);
                            helper.setUpGroupings(component, event, helper);
                        }
                    }
                }
            });
            component.set('v.spinner', true);
            $A.enqueueAction(action);
            //return the relevant client licenses
            var baction = component.get("c.getClientLicenses");
            baction.setParams({
                clientid : clientId
            });
            baction.setCallback(this, function(response) {
                var state = response.getState();
                console.log(state);
                if(state === 'SUCCESS') {
                    //console.log(response.getReturnValue());
                    //component.set('v.licenses', response.getReturnValue());
                    var licenses = response.getReturnValue();
                    for(var i=0;i<licenses.length;i++) {
                        if(licenses[i].Bundle_Product__c === undefined) {
                            licenses[i].sortOrder = 6;
                        }
                        else if(licenses[i].Bundle_Product__r.PricingType__c === 'Core' && licenses[i].Bundle_Product__r.PricingType__c != undefined) {
                            licenses[i].sortOrder = 1;
                        } else if(licenses[i].Bundle_Product__r.PricingType__c === 'UserD' && licenses[i].Bundle_Product__r.PricingType__c != undefined) {
                            licenses[i].sortOrder = 2;
                        } else if(licenses[i].Bundle_Product__r.User_Input_Required__c && licenses[i].Bundle_Product__r.User_Input_Required__c != undefined) {
                            licenses[i].sortOrder = 3;
                        } else if(licenses[i].Bundle_Product__r.Availability__c != undefined && (licenses[i].Bundle_Product__r.Availability__c === 'Available' || licenses[i].Bundle_Product__r.Availability__c === 'Restricted' || licenses[i].Bundle_Product__r.Availability__c === 'Historic')) {
                            licenses[i].sortOrder = 4;
                        } else if(licenses[i].Bundle_Product__r.PricingType__c === 'Cred' && licenses[i].Bundle_Product__r.PricingType__c != undefined) {
                            licenses[i].sortOrder = 5;
                        } else {
                            licenses[i].sortOrder = 6;
                        }
                    }
                    console.log(licenses);
                    licenses.sort(function(a,b) {
                        return a.sortOrder - b.sortOrder;
                    });
                    //console.log(licenses);
                    component.set('v.licenses', licenses);
                    actionbcomplete = true
                    if(actionacomplete) {
                        if(!groupingsinitialized){
                            groupingsinitialized = true;
                            component.set('v.spinner', false);
                            helper.setUpGroupings(component, event, helper);
                        }
                    }
                } else if(state === 'INCOMPLETE') {
                    
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    console.log(errors);
                }
            });
            component.set('v.spinner', true);
            $A.enqueueAction(baction);
            
            //Get user permissions for client licenses
            var caction = component.get("c.getAccess");
            caction.setCallback(this, function(response) {
                var state = response.getState();
                if(state === 'SUCCESS') {
                    console.log('user permissions');
                    console.log(response.getReturnValue());
                    component.set('v.readAccess', response.getReturnValue()[0]);
                    component.set('v.createAccess', response.getReturnValue()[1]);
                    component.set('v.editAccess', response.getReturnValue()[2]);
                    component.set('v.deleteAccess', response.getReturnValue()[3]);
                } else if(state === 'INCOMPLETE') {
                    
                } else if(state === 'ERROR') {
                    var errors = response.getError();
                    console.log(errors);
                }
            });
            $A.enqueueAction(caction);
        }
    },

    changeState : function(component, event, helper) {
        console.log('Client Licenses: changeState');
        var group = event.target.id;
        console.log(group);
        var groupObjects = component.get('v.groupObjects');
        console.log(groupObjects);
        var groupIndex = groupObjects.findIndex(function(groupObject) {
            return groupObject.group == group;
        });
        console.log(groupIndex);
        groupObjects[groupIndex].isexpanded = !groupObjects[groupIndex].isexpanded;
        component.set('v.groupObjects', groupObjects);
    },

    expandAll : function(component, event, helper) {
        var expandAll = component.get('v.expandAll');
        var groupObjects = component.get('v.groupObjects');
        for(var i=0;i<groupObjects.length;i++) {
            groupObjects[i].isexpanded = !expandAll;
        }
        component.set('v.groupObjects', groupObjects);
        component.set('v.expandAll', !expandAll);
    },

    bundleSort : function(component, event, helper) {
        //  console.log('ClientLicenseTable Controller: bundleSort');
          var column = 'bundle';
          helper.columnSort(component, column);
      },

      startSort : function(component, event, helper) {
          //  console.log('ClientLicenseTable Controller: startSort');
          var column = 'start';
          helper.columnSort(component, column);
      },

      endSort : function(component, event, helper) {
        //  console.log('ClientLicenseTable Controller: endSort');
          var column = 'end';
          helper.columnSort(component, column);
      },

      statusSort : function(component, event, helper) {
         // console.log('ClientLicenseTable Controller: statusSort');
          var column = 'status';
          helper.columnSort(component, column);
      },

      listvalueSort : function(component, event, helper) {
          // console.log('ClientLicenseTable Controller: valueSort');
          var column = 'listvalue';
          helper.columnSort(component, column);
      },

      totalvalueSort : function(component, event, helper) {
          var column = 'totalvalue';
          helper.columnSort(component, column);
      },

      discountSort : function(component, event, helper) {
          var column = 'discount';
          helper.columnSort(component, column);
      },

      edit : function(component, event, helper) {
          var editRecordEvent = $A.get("e.force:editRecord");
          var recordId = event.getSource().get('v.name');
          editRecordEvent.setParams({
              recordId : recordId
          });
          editRecordEvent.fire();
      },

      create : function(component, event, helper) {
          var createRecordEvent = $A.get("e.force:createRecord");
          createRecordEvent.setParams({
              entityApiName : 'Client_License__c',
              defaultFieldValues:{
                  'Client__c' : component.get('v.recordId')
              }
          });
          createRecordEvent.fire();
      },

      delete : function(component, event, helper) {
          var confirm = window.confirm('Are you sure you want to delete this record?');
          if(confirm) {
              var deleteEvent = component.get("c.deleteRecord");
              var recordId = event.getSource().get('v.name');
              deleteEvent.setParams({
                  recordId : recordId
              });
              deleteEvent.setCallback(this, function(response) {
                  console.log('record deleted');
                //   var licenses = component.get('v.licenses');
                //   var newLicenses = [];
                //   for(var i=0;i<licenses.length;i++) {
                //       if(licenses[i].Id != recordId) {
                //           newLicenses.push(licenses[i]);
                //       }
                //   }
                //   component.set('v.licenses', newLicenses);
                  //component.set('v.spinner', false);
                  location.reload();
              });
              component.set('v.spinner', true);
              $A.enqueueAction(deleteEvent);
          }
      },

      showSpinner : function(component, event, helper) {
          //component.set('v.spinner', true);
      },

      hideSpinner : function(component, event, helper) {
          //component.set('v.spinner', false);
      }
})