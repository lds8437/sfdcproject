({
    doInit : function(component, event, helper) {
        helper.getPicklistOptions(component);
    },
    handleOppChange : function(component, event, helper) {
        console.log("handling opp change in billing contact");
        /*
        const selectedContactId = component.get("v.opportunity.Billing_Contact__c");
        component.set("v.selectedContact", selectedContactId);
        component.set("v.showSelectedContact", !!selectedContactId);
        component.set("v.newContact.Status__c", "Active Customer");
        component.set("v.newContact.LeadSource", "Outbound - Sales");
        */
        component.find("oppLoader").reloadRecord();
    },
    toggleCreateNewContact : function(component, event, helper) {
        const currentValue = component.get("v.createNewContact");
        component.set("v.createNewContact", !currentValue);
    },
    handleCreateContact : function(component, event, helper) {
        const allValid = component.find("new-contact-field").reduce(function(validSoFar, field) {
        	field.reportValidity();
        	return validSoFar && field.checkValidity();
        }, true);
        if (allValid) {
            const newContact = component.get("v.newContact");
            newContact.AccountId = component.get("v.opportunity.AccountId");
            newContact.Client__c = component.get("v.opportunity.Client__c");
            if (!newContact.Status__c) {
                newContact.Status__c = "Active Customer";
            } 
            if (!newContact.LeadSource) {
                newContact.LeadSource = "Outbound - Sales";
            }
            helper.saveContact(component, newContact);
        }
    },
    handleContactSaved : function(component, event, helper) {
        const response = event.getParam("saveResponse");
        const state = response.state;
        const savedContactId = event.getParam("savedContactId");
        console.log("SAVED CONTACT STATE: ", state, savedContactId);
        if (state === "SUCCESS" || state === "DRAFT") {
            const oppObject = component.get("v.opportunity");
            const saveEvent = component.getEvent("saveOpportunity");
            const clearedContact = {
                "sobjectType": "Contact",
                "Status__c": "Active Customer",
                "LeadSource": "Outbound - Sales"
            }
            component.set("v.createNewContact", false);
            helper.saveOpportunityBillingContact(component, savedContactId);
            
            if(component.get('v.isLightningOut') === false){
                component.find('billingContactNotifLib').showToast({
                    "title": "Billing Contact Updated!",
                    "message": "The Billing Contact has been updated successfully.",
                    "variant": "success"
                });
            }
        } else if (state === "INCOMPLETE") {
            console.log("INCOMPLETE");
            if(component.get('v.isLightningOut') === false){
                component.find('billingContactNotifLib').showToast({
                    "title": "There was a problem saving the Billing Contact",
                    "message": "User is offline, device doesn't support drafts.",
                    "variant": "error"
                });
            }
        } else if (state === "ERROR") {
            console.log("ERROR");
            const errors = response.error;
            console.log(errors);
            let errorMessage;
            if (errors) {
                if (errors[0] && errors[0].message) {
                    console.log(errors[0]);
                    errorMessage = errors[0].message;
                }
            } else {
                errorMessage = "Unknown error."
            }
            if(component.get('v.isLightningOut') === false){
                component.find('billingContactNotifLib').showToast({
                    "title": "There was a problem saving the Billing Contact",
                    "message": "Error: " + errorMessage,
                    "variant": "error"
                });
            }
        } else {
            console.log("UNKNOWN");
            if(component.get('v.isLightningOut') === false){
                component.find('billingContactNotifLib').showToast({
                    "title": "There was a problem saving the Billing Contact",
                    "message": "Error: Unknown error",
                    "variant": "error"
                });
            }
        }
    },
    handleRemove : function(component, event, helper) {
        helper.saveOpportunityBillingContact(component, null);
        component.set("v.showSelectedContact", false);
        component.set("v.selectedContact", null);
    }
})