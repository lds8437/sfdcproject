({
    getRelatedContacts : function(component, accountId) {
        component.set("v.showSpinner", true);
        const relatedContactsAction = component.get("c.getContactsFromAccount");
        relatedContactsAction.setParams({"accountId": accountId});
        relatedContactsAction.setCallback(this, function(response) {
            const state = response.getState();
            if (state === "SUCCESS") {
                const relatedContacts = response.getReturnValue();
                const contactRadioOptions = [];
                const contactList = [];
                relatedContacts.forEach(function(contact) {
                    contactList.push(contact);
                    contactRadioOptions.push({ "label": contact.Name, "value": contact.Id});
                });
                component.set("v.relatedContacts", contactList);
                component.set("v.relatedContactOptions", contactRadioOptions);
                component.set("v.shownContactOptions", contactRadioOptions.slice(0,10));
                if (contactRadioOptions.length > 10) {
                    component.set("v.showSearch", true);
                } else if (contactRadioOptions.length > 0) {
                    component.set("v.showSearch", false);
                } else {
                    component.set("v.showSearch", false);
                    component.set("v.showNoContactsFound", true);
                }
                component.set("v.reloadContacts", false);
            }
            else if (state === "INCOMPLETE") {
                console.log("Could not retrieve related contacts: request incomplete.");
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Could not retrieve related contacts: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Could not retrieve related contacts: Unknown error");
                }
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(relatedContactsAction);
    },
    getPicklistOptions : function(component) {
        //const picklistOptionAction = component.get("c.getNewContactFieldOptions");
        const picklistOptionAction = component.get("c.generatePicklistJSON");
        picklistOptionAction.setParams({"sObjectType": "Contact", "fieldApiNames": ["Contact_Role__c", "Status__c", "LeadSource"]})
        picklistOptionAction.setCallback(this, function(response) {
            const state = response.getState();
            if (state === "SUCCESS") {
                const picklistJSON = JSON.parse(response.getReturnValue());
                if (picklistJSON.hasOwnProperty("Contact_Role__c")) {
                    component.set("v.contactLevels", picklistJSON["Contact_Role__c"]);
                }
                if (picklistJSON.hasOwnProperty("Status__c")) {
                    component.set("v.contactStatuses", picklistJSON["Status__c"]);
                }
                if (picklistJSON.hasOwnProperty("LeadSource")) {
                    component.set("v.contactSources", picklistJSON["LeadSource"]);
                }
            } else {
                console.log("There was a problem retrieving Contact picklist values: " + state + ", " + JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(picklistOptionAction);
    },
    saveContact : function(component, contactObj) {
        const saveContactAction = component.get("c.saveBillingContact");
        component.set("v.showSpinner", true);
        saveContactAction.setParams({"contactJSON": JSON.stringify(contactObj)});
        saveContactAction.setCallback(this, function(response) {
            const state = response.getState();
            if (state === "SUCCESS" || state === "DRAFT") {
                const savedContact = response.getReturnValue();
                const oppObject = component.get("v.opportunity");
                const saveEvent = component.getEvent("saveOpportunity");
                const clearedContact = {
                    "sobjectType": "Contact",
                    "Status__c": "Active Customer",
                    "LeadSource": "Outbound - Sales"
                }
                component.set("v.createNewContact", false);
                component.set("v.newContact", clearedContact);
                oppObject.Billing_Contact__c = savedContact.Id;
                saveEvent.setParams({
                    "oppObject": oppObject
                });
                saveEvent.fire();
                if(component.get('v.isLightningOut') === false){
                    component.find('billingContactNotifLib').showToast({
                        "title": "Billing Contact Updated!",
                        "message": "The Billing Contact has been updated successfully.",
                        "variant": "success"
                    });
                }
            } else if (state === "INCOMPLETE") {
                console.log("INCOMPLETE");
                if(component.get('v.isLightningOut') === false){
                    component.find('billingContactNotifLib').showToast({
                        "title": "There was a problem saving the Billing Contact",
                        "message": "User is offline, device doesn't support drafts.",
                        "variant": "error"
                    });
                }
            } else if (state === "ERROR") {
                console.log("ERROR");
                const errors = response.getError();
                console.log(errors);
                let errorMessage;
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(errors[0]);
                        errorMessage = errors[0].message;
                    }
                } else {
                    errorMessage = "Unknown error."
                }
                if(component.get('v.isLightningOut') === false){
                    component.find('billingContactNotifLib').showToast({
                        "title": "There was a problem saving the Billing Contact",
                        "message": "Error: " + errorMessage,
                        "variant": "error"
                    });
                }
            } else {
                console.log("UNKNOWN");
                if(component.get('v.isLightningOut') === false){
                    component.find('billingContactNotifLib').showToast({
                        "title": "There was a problem saving the Billing Contact",
                        "message": "Error: Unknown error",
                        "variant": "error"
                    });
                }
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(saveContactAction);
    },
    saveOpportunityBillingContact : function(component, billingContactId) {
        component.set("v.showSpinner", true);
        component.set("v.oppFields.Billing_Contact__c", billingContactId);
        component.find("oppLoader").saveRecord($A.getCallback(function(saveResult) {
            if(saveResult.state === "SUCCESS"){
                const fabReloadEvent = $A.get("e.c:fab_Reload");
          if(fabReloadEvent !== undefined){
                        fabReloadEvent.fire();
                    }
            }
            if (saveResult.state === "INCOMPLETE") {
                if(component.get('v.isLightningOut') === false){
                    component.find('billingContactNotifLib').showToast({
                        "title": "There was a problem saving the Opportunity",
                        "message": "User is offline, device doesn't support drafts.",
                        "variant": "error"
                    });
                }
                console.log("User is offline, device doesn't support drafts.");
                //component.find("opportunityData").reloadRecord(false); 
            } else if (saveResult.state === "ERROR") {
                const errors = saveResult.error;
                let errorMessage;
                if (errors && errors[0] && errors[0].message) {
                    errorMessage = errors[0].message;
                } else {
                    errorMessage = "Unknown error.";
                }
                if(component.get('v.isLightningOut') === false){
                    component.find('billingContactNotifLib').showToast({
                        "title": "There was a problem saving the Opportunity",
                        "message": "Error: " + errorMessage,
                        "variant": "error"
                    });
                }
                console.log("Problem saving record, error: " + JSON.stringify(saveResult));
                console.log(component.get("v.oppError"));
                //component.find("opportunityData").reloadRecord(false); 
            } else if (saveResult.state !== "SUCCESS" && saveResult.state !== "DRAFT") {
                if(component.get('v.isLightningOut') === false){
                    component.find('billingContactNotifLib').showToast({
                        "title": "There was a problem saving the Opportunity",
                        "message": "Error: " + "Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error),
                        "variant": "error"
                    });
                }
                //component.find("opportunityData").reloadRecord(false); 
                console.log("Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error));
            }
            component.set("v.showSpinner", false);
        }));
    }
})