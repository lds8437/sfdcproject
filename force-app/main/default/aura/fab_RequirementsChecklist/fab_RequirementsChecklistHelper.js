({
    setRequirementsList : function(component,event,helper,requirementType) {
     //   console.log('RCH: setRequirementsList')
        //console.log(requirementType)
        if(requirementType === 'Purchase Order'){
            var requirements = this.getPORequirements(component,event,helper);
            component.set('v.requirements',requirements)
        }
        else if(requirementType === 'Credit Card'){
            var requirements = this.getCCRequirements(component,event,helper);
            component.set('v.requirements',requirements)
        }
            else if(requirementType === 'Standard Service Order'){
                var requirements = this.getStdEqsoRequirements(component,event,helper);
                component.set('v.requirements',requirements)
            }
                else if(requirementType === 'Custom Service Order'){
                    var requirements = this.getCustEqsoRequirements(component,event,helper);
                    component.set('v.requirements',requirements)
                }
                    else if(requirementType === 'Paper Service Order'){
                        var requirements = this.getPaperEqsoRequirements(component,event,helper);
                        component.set('v.requirements',requirements)
                    }else if(requirementType === 'Retainer'){
                        var requirements = this.getRetainerRequirements(component,event,helper);
                        //  console.log(requirements)
                        component.set('v.requirements',requirements)
                    }
                        else if(requirementType === 'No Contract Method'){
                            var requirements = this.getNoContractRequirements(component,event,helper);
                            //  console.log(requirements)
                            component.set('v.requirements',requirements)
                        }
        
        
    },
    
    getPORequirements : function(component,event,helper){
     //   console.log('RCH: getPORequirements')
        var requirements=[];
        var opportunity = component.get('v.opportunity');
        var poNumberPresent;
        var submitForInvoice = false;
        var totalServices = component.get('v.totalServices');
        var servicesCompleted = component.get('v.servicesCompleted');
        var serviceComplete = false;   
        var billAndQuoteReqComplete = false;
        
        if(totalServices === servicesCompleted){
            serviceComplete = true;
        }
        if(opportunity.Purchase_Order_Number__c === undefined){
            poNumberPresent = false;
        }
        else {
            poNumberPresent = true;
        }
        if(opportunity.StageName === 'Invoice' || opportunity.StageName === 'Won'){
            submitForInvoice = true;
        } 
        //console.log(component.get('v.hasInvoiceRequirements'))
        //console.log(component.get('v.hasQuotingRequirements'))
        if(component.get('v.hasInvoiceRequirements') === false && component.get('v.hasQuotingRequirements') === false ){
            billAndQuoteReqComplete = true;
        }
        
        var requirement1 = {
            "Requirement":"Enter PO Number",
            "Order" : 1,
            "Completed" : poNumberPresent
        }
        requirements.push(requirement1);
        
        var requirement2 = {
            "Requirement":"Upload Purchase Order",
            "Order" : 2,
            "Completed" : opportunity.PO_Attached__c
        }
        requirements.push(requirement2);
        
        var requirement3 = {
            "Requirement":"Complete Onboarding Wizard",
            "Order" : 3,
            "Completed" : opportunity.OnboardingWizardUsed__c 
        }
        requirements.push(requirement3);
        
        var requirement4 = {
            "Requirement":"Complete Billing/Quoting Requirements",
            "Order" : 4,
            "Completed" : billAndQuoteReqComplete 
        }
        requirements.push(requirement4);
        
        if(totalServices > 0){            
            var requirement5 = {
                "Requirement":"Complete Service Requirements",
                "Order" : 5,
                "Completed" : serviceComplete                
            }
            requirements.push(requirement5);
            var requirement6 = {
                "Requirement":"Submit for Invoice",
                "Order" : 6,
                "Completed" : submitForInvoice
            }
            requirements.push(requirement6);
        }else{
            var requirement5 = {
                "Requirement":"Submit for Invoice",
                "Order" : 5,
                "Completed" : submitForInvoice
            }
            requirements.push(requirement5);
        }
        
        helper.setReadyToSubmit(component,event,helper,'Purchase Order');
        return requirements;
    },
    
    getCCRequirements : function(component,event,helper){
      //  console.log('RCH: getCCRequirements')
        var requirements=[];
        var opportunity = component.get('v.opportunity'); 
        var country = '';
        if(opportunity.Client__c !== undefined){
            country = opportunity.Client__r.ShippingCountry2__c;
        }    
        var client = false;
        if(opportunity.Client__c !== undefined){
            client = true;
        }
        var taxableCountry = component.get('v.taxableCountry');
        var billingContact = false;
        var billingEmail = false;
        var sentForPayment = false;
        var totalServices = component.get('v.totalServices');
        var servicesCompleted = component.get('v.servicesCompleted');
        var serviceComplete = false;
        var billAndQuoteReqComplete = false;
        //console.log(component.get('v.hasInvoiceRequirements'))
        //console.log(component.get('v.hasQuotingRequirements'))
        if(component.get('v.hasInvoiceRequirements') === false && component.get('v.hasQuotingRequirements') === false ){
            billAndQuoteReqComplete = true;
        }
        if(totalServices === servicesCompleted){
            serviceComplete = true;
        }
        if(opportunity.Billing_Contact__c !== undefined){
            billingContact = true;
            if(opportunity.Billing_Contact__r.Email !== undefined){
                billingEmail = true;
            }
        }
        if(country === 'United States' || country === 'Canada' || country === ''){
            taxableCountry = true;
            component.set('v.taxableCountry',true);
        }          
        if(opportunity.StageName==='Won' || opportunity.Sales_Process_Stage__c==='Awaiting Client Payment'){
            sentForPayment = true;   
        }
        
        var requirement1 = {
            "Requirement":"Create/Link Client",
            "Order" : 1,
            "Completed" : client
        }
        requirements.push(requirement1);
        if(opportunity.SyncedQuoteId !== undefined){
            
            if(opportunity.SyncedQuote.AVA_SFQUOTES__Non_Taxable__c===false && taxableCountry===true){
                var requirement2 = {
                    "Requirement":"Validate Address",
                    "Order" : 2,
                    "Completed" : opportunity.SyncedQuote.AVA_SFQUOTES__Shipping_Last_Validated__c!==undefined || opportunity.SyncedQuote.AVA_SFQUOTES__Tax_Now_Status__c==='Sales Tax Current'
                }
                requirements.push(requirement2);
                
                var requirement3 = {
                    "Requirement":"Calculate Taxes",
                    "Order" : 3,
                    "Completed" : opportunity.SyncedQuote.AVA_SFQUOTES__Tax_Now_Status__c==='Sales Tax Current'
                }
                requirements.push(requirement3);
            }
        }else{
            var requirement2 = {
                "Requirement":"Pending Synced Quote",
                "Order" : 2,
                "Completed" : false
            }
            requirements.push(requirement2);
            
            var requirement3 = {
                "Requirement":"Pending Synced Quote",
                "Order" : 3,
                "Completed" : false
            }
            requirements.push(requirement3);
        }        
        
        var requirement4 = {
            "Requirement":"Complete Onboarding Wizard",
            "Order" : 4,
            "Completed" : opportunity.OnboardingWizardUsed__c 
        }
        requirements.push(requirement4);
        
        var requirement5 = {
            "Requirement":"Select Billing Contact",
            "Order" : 5,
            "Completed" : billingContact === true
        }
        requirements.push(requirement5);
        
        var requirement6 = {
            "Requirement":"Add/Update Billing Contact Email Address",
            "Order" : 6,
            "Completed" : billingEmail === true
        }
        requirements.push(requirement6);
        
        var requirement7 = {
            "Requirement":"Complete Billing/Quoting Requirements",
            "Order" : 7,
            "Completed" : billAndQuoteReqComplete 
        }
        requirements.push(requirement7);
        
        if(totalServices > 0){            
            var requirement8 = {
                "Requirement":"Complete Service Requirements",
                "Order" : 8,
                "Completed" : serviceComplete                
            }
            requirements.push(requirement8);
            var requirement9 = {
                "Requirement":"Send for Payment",
                "Order" : 9,
                "Completed" : sentForPayment
            }
            requirements.push(requirement9);
        }else{
            var requirement8 = {
                "Requirement":"Send for Payment",
                "Order" : 8,
                "Completed" : sentForPayment
            }
            requirements.push(requirement8);
        }  
        helper.setReadyToSubmit(component,event,helper,'Credit Card');
        return requirements;
    },
    
    getNoContractRequirements : function(component,event,helper){
    //    console.log('RCH: getNoContractRequirements')
        var requirements=[];
        var opportunity = component.get('v.opportunity');        
        var contractDocument = component.get('v.contractDocument');
        var client = false;
        var requiredFields = false;
        var submitForInvoice = false;
        var signed = false;
        var totalServices = component.get('v.totalServices');
        var servicesCompleted = component.get('v.servicesCompleted');
        var serviceComplete = false;
        
        if(totalServices === servicesCompleted){
            serviceComplete = true;
        }
        if(opportunity.Client__c !== undefined){
            client = true;
        }
        if(opportunity.StageName==='Won' || opportunity.StageName==='Invoice'){
            submitForInvoice = true;   
        }        
        if(contractDocument.Quote_Type__c !== undefined 
           && contractDocument.Legal_Contact__c != undefined 
           && opportunity.Billing_Contact__c != undefined
           && opportunity.Qualification_Contact__c != undefined
           && contractDocument.Subscriber__c !== undefined 
           && contractDocument.Data_Center_Location__c !== undefined
           && opportunity.Payment_Terms__c !== undefined 
           && (contractDocument.MSA__c === false || opportunity.Client__r.MSA_Effective_Date__c !== undefined)
           && (opportunity.Payment_Terms__c !== "Custom" || opportunity.Custom_Payment_Terms__c !== undefined)){
            requiredFields = true;
        }        
        if(contractDocument.Status__c === 'Signed' ){
            signed = true;
        }
        
        var requirement1 = {
            "Requirement":"Select a Contract Type",
            "Order" : 1,
            "Completed" : false
        }
        requirements.push(requirement1);
        
        return requirements;
    },
    
    getStdEqsoRequirements : function(component,event,helper){
        console.log('RCH: getStdEqsoRequirements')
        var requirements=[];
        var opportunity = component.get('v.opportunity');        
        var contractDocument = component.get('v.contractDocument');
        var client = false;
        var requiredFields = false;
        var submitForInvoice = false;
        var signed = false;
        var totalServices = component.get('v.totalServices');
        var servicesCompleted = component.get('v.servicesCompleted');
        var serviceComplete = false;
        var billAndQuoteReqComplete = false;
        var multiYearCountMatch = component.get('v.multiYearCountMatch')
        
        if(component.get('v.hasInvoiceRequirements') === false && component.get('v.hasQuotingRequirements') === false ){
            billAndQuoteReqComplete = true;
        }
        if(totalServices === servicesCompleted){
            serviceComplete = true;
        }
        if(opportunity.Client__c !== undefined){
            client = true;
        }
        if(opportunity.StageName==='Won' || opportunity.StageName==='Invoice'){
            submitForInvoice = true;   
        }  
        if(opportunity.RecordType.Name === 'Panels'){
            if(contractDocument.Legal_Contact__c != undefined 
               && opportunity.Billing_Contact__c != undefined
               && opportunity.Qualification_Contact__c != undefined
               && contractDocument.Subscriber__c !== undefined           
               && opportunity.Payment_Terms__c !== undefined 
               && (opportunity.Payment_Terms__c !== "Custom" || opportunity.Custom_Payment_Terms__c !== undefined)){
                requiredFields = true;
            }       
        }else{
            if(contractDocument.Legal_Contact__c != undefined 
               && opportunity.Billing_Contact__c != undefined
               && opportunity.Qualification_Contact__c != undefined
               && contractDocument.Subscriber__c !== undefined 
               && contractDocument.Data_Center_Location__c !== undefined
               && opportunity.Payment_Terms__c !== undefined            
               && (opportunity.Payment_Terms__c !== "Custom" || opportunity.Custom_Payment_Terms__c !== undefined)
              && (contractDocument.MultiYear_Quote__c !== true || contractDocument.MultiYear_Number_of_Years__c !== undefined)){
                requiredFields = true;
            }       
        }
        
        if(contractDocument.Status__c === 'Signed' ){
            signed = true;
        }
        
        var requirement1 = {
            "Requirement":"Create/Link Client",
            "Order" : 1,
            "Completed" : client
        }
        requirements.push(requirement1);
        
        var requirement2 = {
            "Requirement":"Update Required Fields",
            "Order" : 2,
            "Completed" : requiredFields
        }
        requirements.push(requirement2);
        
        if(component.get('v.multiYear')){
            var requirement3 = {
            "Requirement":"Multi-Year Selections",
            "Order" : 3,
            "Completed" : multiYearCountMatch
        }
        requirements.push(requirement3);
        }
        
        var requirement4 = {
            "Requirement":"Preview and Send for Signature",
            "Order" : 4,
            "Completed" : signed
        }
        requirements.push(requirement4);
        
        var requirement5 = {
            "Requirement":"Complete Onboarding Wizard",
            "Order" : 5,
            "Completed" : opportunity.OnboardingWizardUsed__c 
        }
        requirements.push(requirement5);
        
        var requirement6 = {
            "Requirement":"Complete Billing/Quoting Requirements",
            "Order" : 6,
            "Completed" : billAndQuoteReqComplete 
        }
        requirements.push(requirement6);
        
        if(totalServices > 0){            
            var requirement7 = {
                "Requirement":"Complete Service Requirements",
                "Order" : 7,
                "Completed" : serviceComplete                
            }
            requirements.push(requirement7);
            var requirement8 = {
                "Requirement":"Submit for Invoice",
                "Order" : 8,
                "Completed" : submitForInvoice
            }
            requirements.push(requirement8);
        }else{
            var requirement7 = {
                "Requirement":"Submit for Invoice",
                "Order" : 7,
                "Completed" : submitForInvoice
            }
            requirements.push(requirement7);   
        }          
        helper.setReadyToSubmit(component,event,helper,'Standard Service Order');
        helper.setReadyToSend(component,event,helper,'Standard Service Order');
        return requirements;
    },
    
    getCustEqsoRequirements : function(component,event,helper){
        console.log('RCH: getCustEqsoRequirements')
        
        var requirements=[];
        var opportunity = component.get('v.opportunity');        
        var contractDocument = component.get('v.contractDocument');        
        var client = false;
        var requiredFields = false;
        var submitForInvoice = false;
        var signed = false;
        var downloaded = component.get('v.contractDocument').Contract_Downloaded__c;    
        var caseSent = component.get('v.contractDocument').Legal_Case_Created__c;
        var totalServices = component.get('v.totalServices');
        var servicesCompleted = component.get('v.servicesCompleted');
        var serviceComplete = false;
        var billAndQuoteReqComplete = false;
        var multiYearCountMatch = component.get('v.multiYearCountMatch')
        
        if(component.get('v.hasInvoiceRequirements') === false && component.get('v.hasQuotingRequirements') === false ){
            billAndQuoteReqComplete = true;
        }
        if(totalServices === servicesCompleted){
            serviceComplete = true;
        }
        if(opportunity.Client__c !== undefined){
            client = true;
        }        
        if(opportunity.StageName==='Won' || opportunity.StageName==='Invoice'){
            submitForInvoice = true;   
        }        
        if(opportunity.RecordType.Name === 'Panels'){
            if(contractDocument.Legal_Contact__c != undefined 
               && opportunity.Billing_Contact__c != undefined
               && opportunity.Qualification_Contact__c != undefined
               && contractDocument.Subscriber__c !== undefined           
               && opportunity.Payment_Terms__c !== undefined 
               && (opportunity.Payment_Terms__c !== "Custom" || opportunity.Custom_Payment_Terms__c !== undefined)){
                requiredFields = true;
            }       
        }else{
            if(contractDocument.Legal_Contact__c != undefined 
               && opportunity.Billing_Contact__c != undefined
               && opportunity.Qualification_Contact__c != undefined
               && contractDocument.Subscriber__c !== undefined 
               && contractDocument.Data_Center_Location__c !== undefined
               && opportunity.Payment_Terms__c !== undefined            
               && (opportunity.Payment_Terms__c !== "Custom" || opportunity.Custom_Payment_Terms__c !== undefined)
              && (contractDocument.MultiYear_Quote__c !== true || contractDocument.MultiYear_Number_of_Years__c !== undefined)){
                requiredFields = true;
            }       
        }   
        
        if(contractDocument.Status__c === 'Signed' ){
            signed = true;
        }
        //  component.set('v.readyToSend',readyToSend);
        // component.set('v.readyToSubmit',readyToSubmit);
        var requirement1 = {
            "Requirement":"Create/Link Client",
            "Order" : 1,
            "Completed" : client
        }
        requirements.push(requirement1);
        
        var requirement2 = {
            "Requirement":"Update Required Fields",
            "Order" : 2,
            "Completed" : requiredFields
        }
        requirements.push(requirement2);
        
        if(component.get('v.multiYear')){
            var requirement3 = {
            "Requirement":"Multi-Year Selections",
            "Order" : 3,
            "Completed" : multiYearCountMatch
        }
        requirements.push(requirement3);
        }
        
        var requirement4 = {
            "Requirement":"Download Contract",
            "Order" : 4,
            "Completed" : downloaded
        }
        requirements.push(requirement4);
        
        var requirement5 = {
            "Requirement":"Create Legal Case and Attach Contract",
            "Order" : 5,
            "Completed" : caseSent
        }
        requirements.push(requirement5);
        
        var requirement6 = {
            "Requirement":"Preview and Send for Signature",
            "Order" : 6,
            "Completed" : signed
        }
        requirements.push(requirement6);
        
        var requirement7 = {
            "Requirement":"Complete Onboarding Wizard",
            "Order" : 7,
            "Completed" : opportunity.OnboardingWizardUsed__c 
        }
        requirements.push(requirement7);
        var requirement8 = {
            "Requirement":"Complete Billing/Quoting Requirements",
            "Order" : 8,
            "Completed" : billAndQuoteReqComplete 
        }
        requirements.push(requirement8);
        
        if(totalServices > 0){            
            var requirement9 = {
                "Requirement":"Complete Service Requirements",
                "Order" : 9,
                "Completed" : serviceComplete                
            }
            requirements.push(requirement9);
            
            var requirement10 = {
                "Requirement":"Submit for Invoice",
                "Order" : 10,
                "Completed" : submitForInvoice
            }
            requirements.push(requirement10);
            
        }else{
            var requirement9 = {
                "Requirement":"Submit for Invoice",
                "Order" : 9,
                "Completed" : submitForInvoice
            }
            requirements.push(requirement9);
        }  
        helper.setReadyToDownload(component,event,helper,'Custom Service Order');
        helper.setReadyToSubmit(component,event,helper,'Custom Service Order');
        helper.setReadyToSend(component,event,helper,'Custom Service Order');
        return requirements;
    },
    
    getPaperEqsoRequirements : function(component,event,helper){
     //   console.log('RCH: getPaperEqsoRequirements')
        var requirements=[];
        var opportunity = component.get('v.opportunity');   
        //console.log(opportunity)
        //console.log(opportunity)
        var contractDocument = component.get('v.contractDocument');        
        var client = false;
        var requiredFields = false;
        var submitForInvoice = false;
        var signed = false;    
        var downloaded = component.get('v.contractDocument').Contract_Downloaded__c;    
        var caseSent = component.get('v.contractDocument').Legal_Case_Created__c;
        var qsoAttached = opportunity.QSO_Attached__c;
        var totalServices = component.get('v.totalServices');
        var servicesCompleted = component.get('v.servicesCompleted');
        var serviceComplete = false;
        var billAndQuoteReqComplete = false;
        var multiYearCountMatch = component.get('v.multiYearCountMatch')
        
     //   console.log(component.get('v.hasInvoiceRequirements'))
        if(component.get('v.hasInvoiceRequirements') === false && component.get('v.hasQuotingRequirements') === false ){
            billAndQuoteReqComplete = true;
        }
        if(totalServices === servicesCompleted){
            serviceComplete = true;
        } 
        if(opportunity.Client__c !== undefined){
            client = true;
        }        
        if(opportunity.StageName==='Won' || opportunity.StageName==='Invoice'){
            submitForInvoice = true;   
        }        
        if(opportunity.RecordType.Name === 'Panels'){
            if(contractDocument.Legal_Contact__c != undefined 
               && opportunity.Billing_Contact__c != undefined
               && opportunity.Qualification_Contact__c != undefined
               && contractDocument.Subscriber__c !== undefined           
               && opportunity.Payment_Terms__c !== undefined 
               && (opportunity.Payment_Terms__c !== "Custom" || opportunity.Custom_Payment_Terms__c !== undefined)){
                requiredFields = true;
            }       
        }else{
            if(contractDocument.Legal_Contact__c != undefined 
               && opportunity.Billing_Contact__c != undefined
               && opportunity.Qualification_Contact__c != undefined
               && contractDocument.Subscriber__c !== undefined 
               && contractDocument.Data_Center_Location__c !== undefined
               && opportunity.Payment_Terms__c !== undefined            
               && (opportunity.Payment_Terms__c !== "Custom" || opportunity.Custom_Payment_Terms__c !== undefined)
              && (contractDocument.MultiYear_Quote__c !== true || contractDocument.MultiYear_Number_of_Years__c !== undefined)){
                requiredFields = true;
            }       
        }
        if(contractDocument.Status__c === 'Signed' ){
            signed = true;
        }
        var requirement1 = {
            "Requirement":"Create/Link Client",
            "Optional" : true,
            "Order" : 1,
            "Completed" : client
        }
        requirements.push(requirement1);
        
        var requirement2 = {
            "Requirement":"Update Required Fields",
            "Optional" : true,
            "Order" : 2,
            "Completed" : requiredFields
        }
        requirements.push(requirement2);

        if(component.get('v.multiYear')){
            var requirement3 = {
            "Requirement":"Multi-Year Selections",
            "Order" : 3,
            "Completed" : multiYearCountMatch
        }
        requirements.push(requirement3);
        }
        
        var requirement4 = {
            "Requirement":"Download Contract",
            "Optional" : true,
            "Order" : 4,
            "Completed" : downloaded
        }
        requirements.push(requirement4);
        
        var requirement5 = {
            "Requirement":"Create Legal Case and Attach Contract",
            "Optional" : true,
            "Order" : 5,
            "Completed" : caseSent
        }
        requirements.push(requirement5);
        
        var requirement6 = {
            "Requirement":"Upload Signed Contract",
            "Optional" : false,
            "Order" : 6,
            "Completed" : qsoAttached
        }
        requirements.push(requirement6);
        
        var requirement7 = {
            "Requirement":"Complete Onboarding Wizard",
            "Optional" : false,
            "Order" : 7,
            "Completed" : opportunity.OnboardingWizardUsed__c 
        }
        requirements.push(requirement7);
        
        var requirement8 = {
            "Requirement":"Complete Billing/Quoting Requirements",
            "Order" : 8,
            "Completed" : billAndQuoteReqComplete 
        }
        requirements.push(requirement8);
        
        if(totalServices > 0){            
            var requirement9 = {
                "Requirement":"Complete Service Requirements",
                "Order" : 9,
                "Completed" : serviceComplete                
            }
            requirements.push(requirement9);
            
            var requirement10 = {
                "Requirement":"Submit for Approval/Invoice",
                "Optional" : false,
                "Order" : 10,
                "Completed" : submitForInvoice
            }
            requirements.push(requirement10);
        }else{
            var requirement9 = {
                "Requirement":"Submit for Approval/Invoice",
                "Optional" : false,
                "Order" : 9,
                "Completed" : submitForInvoice
            }
            requirements.push(requirement9);
        }          
        helper.setReadyToDownload(component,event,helper,'Paper Service Order');
        helper.setReadyToSubmit(component,event,helper,'Paper Service Order');
        
        return requirements;
    },
    
    getRetainerRequirements : function(component,event,helper){
      //  console.log('RCH: getPORequirements')
        var requirements=[];
        var opportunity = component.get('v.opportunity');
        var submitForInvoice = false;
        console.log(opportunity)
        if(opportunity.StageName === 'Invoice' || opportunity.StageName === 'Won'){
            submitForInvoice = true;
        }     
        var billAndQuoteReqComplete = false;
        if(component.get('v.hasInvoiceRequirements') === false && component.get('v.hasQuotingRequirements') === false ){
            billAndQuoteReqComplete = true;
        }
        var requirement1 = {
            "Requirement":"Complete Onboarding Wizard",
            "Order" : 1,
            "Completed" : opportunity.OnboardingWizardUsed__c 
        }
        requirements.push(requirement1);
        var requirement2 = {
            "Requirement":"Link Retainer",
            "Order" : 2,
            "Completed" : opportunity.Billed_Retainers_Linked__c 
        }
        requirements.push(requirement2);
        var requirement3 = {
            "Requirement":"Complete Billing/Quoting Requirements",
            "Order" : 3,
            "Completed" : billAndQuoteReqComplete 
        }
        requirements.push(requirement3);
        var requirement4 = {
            "Requirement":"Submit for Invoice",
            "Order" : 4,
            "Completed" : submitForInvoice
        }
        requirements.push(requirement4);
        helper.setReadyToSubmit(component,event,helper,'Retainer');
        return requirements;
    },
    
    setReadyToDownload : function(component,event,helper,type){
        console.log('setReadyToDownload')
        
        var readyToDownload = true;
        if(component.get('v.multiYear')=== true && component.get('v.multiYearCountMatch') === false){
            readyToDownload = false;
        }
        component.set('v.readyToDownload',readyToDownload); 
        console.log(readyToDownload)
    },
    
    setReadyToSend : function(component,event,helper,type){
        var readyToSend = true;
        var opportunity = component.get('v.opportunity');
        var contractDocument = component.get('v.contractDocument')
        if(component.get('v.multiYear')=== true && component.get('v.multiYearCountMatch') === false){
            readyToSend = false;
        }
        if(type === 'Custom Service Order'){            
            if(contractDocument.Custom_Contract_Status__c !== 'Ready for Signature'){
                readyToSend = false;
            }          
        }else if(type === 'Standard Service Order'){
            if(contractDocument.Status__c !== 'Ready for Signature'){
                readyToSend = false;
            }
        }
        component.set('v.readyToSend',readyToSend);   
    },
    
    setReadyToSubmit : function(component,event,helper,type){
     //   console.log('setReadyToSubmit')
        var readyToSubmit = true;
        var opportunity = component.get('v.opportunity');
        var taxableCountry = component.get('v.taxableCountry');
        
        //  console.log(readyToSubmit)
        if(opportunity.OnboardingWizardUsed__c === false){
            readyToSubmit = false;
        } 
      //  console.log(readyToSubmit)
        if(component.get('v.hasInvoiceRequirements') === true || component.get('v.hasQuotingRequirements') === true ){
            readyToSubmit = false;
        }
      //   console.log(readyToSubmit)
        if(type !== 'Retainer'){
            if(opportunity.Services_SOW_Not_Attached__c === true){
                readyToSubmit = false;
            } 
        }       
      //   console.log(readyToSubmit)
        if(type === 'Purchase Order' && opportunity.PO_Attached__c === false){
            readyToSubmit = false;
        }
       //   console.log(readyToSubmit)
        if(type === 'Credit Card'){
         //   console.log(taxableCountry)
        //    console.log(opportunity.SyncedQuote.AVA_SFQUOTES__Tax_Now_Status__c)
       //     console.log(opportunity.SyncedQuote.AVA_SFQUOTES__Non_Taxable__c)
            if(taxableCountry === true){
                if(opportunity.SyncedQuote.AVA_SFQUOTES__Tax_Now_Status__c==='Sales Tax Not Current'){
                    if(opportunity.SyncedQuote.AVA_SFQUOTES__Non_Taxable__c===false){
                        readyToSubmit = false;
                    }
                }
            }
        }
        
        
      //   console.log(readyToSubmit)
        if(type==='Standard Service Order' || type === 'Custom Service Order'){
            if(opportunity.eQSO_Attached__c === false && opportunity.QSO_Requirement_Override__c === false){
                readyToSubmit = false;
            }
        }
      //    console.log(readyToSubmit)
        if(type === 'Paper Service Order'){
            if(opportunity.QSO_Attached__c === false && opportunity.QSO_Requirement_Override__c === false){
                readyToSubmit = false;
            } 
            if(component.get('v.verified')===false){
                readyToSubmit = false;
            }        
        }
       //  console.log(opportunity.OnboardingWizardUsed__c)
       //   console.log(component.get('v.hasInvoiceRequirements'))
        //  console.log(component.get('v.hasQuotingRequirements'))
        //  console.log(opportunity.Services_SOW_Not_Attached__c)
        // console.log(taxableCountry)
        // console.log(opportunity.SyncedQuote.AVA_SFQUOTES__Tax_Now_Status__c)
        // console.log(opportunity.SyncedQuote.AVA_SFQUOTES__Non_Taxable__c)
       //   console.log(readyToSubmit)
        component.set('v.readyToSubmit',readyToSubmit);     
        
    }
})