({
    getData: function (cmp) {
        console.log('getdata');
        //   var counts = cmp.get('v.currentCount');
        //console.log(cmp.get("v.initialRows"));
        //console.log(cmp.get("v.rowNumberOffset"));
        
        var action = cmp.get('c.getCases');
        action.setParams({
            "recordLimit": cmp.get("v.initialRows"),
            "recordOffset": cmp.get("v.rowNumberOffset"),
            "oppId" : cmp.get('v.recordId')
        });
       // debugger;
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // cmp.set('v.mydata', response.getReturnValue());
                var dataObj= response.getReturnValue();
                dataObj.forEach(function(d){
                    d.OwnerName = d.Owner.Name;
                   d.ObjectId = '/'+d.Id;
                   d.ObjectName = d.Subject;
                })
                //console.log('DATA');
                //console.log(dataObj);
                cmp.set('v.data', dataObj);
                cmp.set("v.currentCount", cmp.get("v.initialRows"));
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
    },
    
    getTotal : function(cmp) {
        console.log('gettotal');
        var action = cmp.get("c.getTotal");
        action.setParams({
            "oppId" : cmp.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" ) {
                var resultData = response.getReturnValue();
                cmp.set("v.totalNumberOfRows", resultData);
            }
        });
        $A.enqueueAction(action);
    },
    
    getMoreApprovals: function(cmp , rows){
        console.log('getmoreapprovals');
        return new Promise($A.getCallback(function(resolve, reject) {
            var action = cmp.get('c.getCases');
            var recordOffset = cmp.get("v.currentCount");
            var recordLimit = cmp.get("v.initialRows");
            console.log(recordOffset);
            console.log(recordLimit);
        //    debugger;
            action.setParams({
                "recordLimit": recordLimit,
                "recordOffset": recordOffset,
            "oppId" : cmp.get('v.recordId') 
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log(state);
                console.log(response);
                if(state === "SUCCESS"){
                    var resultData = response.getReturnValue();
                    console.log(resultData);
                    resultData.forEach(function(d){
                        d.OwnerName = d.Owner.Name;
                        d.ObjectId = '/'+d.Id;
                   d.ObjectName = d.Subject;
                    })
                    resolve(resultData);
                    recordOffset = recordOffset+recordLimit;
                    cmp.set("v.currentCount", recordOffset);   
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    console.error(errors);
                }                
            });
            $A.enqueueAction(action);
        }));
    },
    
})