({
    doInit : function(cmp, event, helper) 
    {
        console.log('doinit');
         helper.getTotal(cmp);
        cmp.set('v.columns', [
          
            {label: 'Subject', fieldName: 'ObjectId', type: 'url', typeAttributes: {label: { fieldName: 'ObjectName'}}},
             {label: 'Status', fieldName: 'Status', type: 'text'},
            {label: 'Owner Name', fieldName: 'OwnerName', type: 'text'},
            {label: 'Primary Support Reason', fieldName: 'Primary_Support_Reason__c', type: 'text'},
            {label: 'Secondary Support Reason', fieldName: 'Secondary_Support_Reason__c', type: 'text'},
            {label: 'Date/Time Opened', fieldName: 'CreatedDate', type: 'datetime'}
            ]);
        helper.getData(cmp);
     
            },
    
    loadMoreData: function (cmp, event, helper) {
        console.log('loadmoredata');
        event.getSource().set("v.isLoading", true);
        cmp.set('v.loadMoreStatus', 'Loading....');
        helper.getMoreApprovals(cmp, cmp.get('v.rowsToLoad')).then($A.getCallback(function (data) {
            console.log(cmp.get('v.data').length);
            console.log(cmp.get('v.totalNumberOfRows'));
            
            if (cmp.get('v.data').length == cmp.get('v.totalNumberOfRows')) {
                console.log('thisworks');
                cmp.set('v.enableInfiniteLoading', false);
                cmp.set('v.loadMoreStatus', 'No more data to load');
            } else {
                var currentData = cmp.get('v.data');
                var newData = currentData.concat(data);
                cmp.set('v.data', newData);
                cmp.set('v.loadMoreStatus', 'Please scroll down to load more data');
            }
            event.getSource().set("v.isLoading", false);
        }));
    }
})