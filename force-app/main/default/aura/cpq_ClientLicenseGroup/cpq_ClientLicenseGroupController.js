({
    doInit : function(component, event, helper) {
       
    },
    returnClient : function(component, event, helper) {
        //console.log('client license group returnClient method running');
        
        if(component.get("v.box")){//If this client group is selected, return data in the object.
            var client = {};
            client.Name = component.get("v.clientname");
            client.Id = component.get("v.clientId");
            client.Bundle = component.get('v.bundlename');
            client.EndDate = component.get('v.end');
            //console.log('returning client' + JSON.stringify(client));
            return client;
        }
        else{
            //console.log('return false');
            return false;
        }
    },
    returnGroupLicenses : function(component, event, helper) {  
        //console.log('client license group returnGroupLicenses method running');
        if(component.get("v.box")){//If this client group is selected, return data in the array.
            //console.log('return group licenses data' + JSON.stringify(component.get("v.licenses")));
            return component.get("v.licenses");
        }
        else{
            //console.log('return false');
            return false;            
        }
    },
    sendPopLineDetail : function(component, event, helper) {
        //  console.log('ClientLicenseGroup Controller: sendPopLineDetail');
        var popEvent = component.getEvent("poplinedetail");
        // console.log('get client name' + component.get("v.clientname"));
        popEvent.setParams({
            "clientnameparam" : component.get("v.clientname"),
            "bundlenameparam" : component.get("v.bundlename"),
            "licensesparam" : component.get("v.licenses")
        });
        //  console.log('sending pop event: ' + JSON.stringify(popEvent.getParams()));
        popEvent.fire();
    },
    selectChange : function(component, event, helper) {
        //  console.log('ClientLicenseGroup Controller: selectchange');
        //  console.log(event.getSource().get("v.value"));
        var group = component.get('v.group');
        var expired = false;
        group = group.split('|');
        var expiredIndex = _.findIndex(group, function(g) {
            return g == 'Expired'
        });
        if(expiredIndex > -1) {
            expired = true;
        }
        var selected = component.get("v.box");
        if(selected){
            component.set("v.selectedclass", "selected");
            var selectEvent = component.getEvent("selectgroup");
            selectEvent.setParams({'selected' : selected,
                                   'selectedExperience' : component.get('v.experience'),
                                   'selectedUpgradeBundles' : component.get('v.upgradeBundles'),
                                   'selectedBundle' : component.get('v.bundlename'),
                                   'expired' : expired});
            selectEvent.fire();
        }
        else{
            var selectEvent = component.getEvent("selectgroup");
            selectEvent.setParams({'selected' : selected});
            selectEvent.fire();
            component.set("v.selectedclass", "");//Un-highlight the unselected row.
        }
    },   
    onSelection : function(component, event, helper) {
        //console.log('onSelection');
        var selectedUpgradeBundles = component.get('v.selectedUpgradeBundles');
        var bundlename = component.get('v.bundlename');
        var selectedBundle = component.get('v.selectedBundle');
        var upgradeBundles = component.get('v.upgradeBundles');
        var selectedExperience = component.get('v.selectedExperience');
        if(!upgradeBundles.includes(selectedBundle) && selectedExperience != undefined && !selectedUpgradeBundles.includes(bundlename)) {
            component.set('v.disabled', true);
        }
        else {
            component.set('v.disabled', false);
        }
    }
})