({
	buttonPress : function(component, buttontype) {
       // console.log('ClientLicenseGroup Helper: buttonPress');
     //   console.log('button press helper running with button type ' + buttontype);
		var sendEvent = $A.get("e.c:cpq_licensesToConfig");
        //var sendEvent = component.getEvent("sendlicenses");
        var licenses = component.get("v.licenses");
      
        sendEvent.setParams({
            "licenseCurrencyCode" : component.get('v.currencycode'),
            "licenses" : licenses,
            "upgradechoice" : buttontype,
            "start" : component.get('v.start'),
            "end" : component.get('v.end')
        });
       // console.log('sending license group to config event params: ' + JSON.stringify(sendEvent.getParams()));
        sendEvent.fire();
	}
})