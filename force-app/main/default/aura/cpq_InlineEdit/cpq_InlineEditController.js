({
    doInit : function(component,event,helper){
       // console.log('InlineEdit Controller: doInit');
        if(component.get('v.singleRec').Picklist_Value__c === true && component.get('v.singleRec').PricingType__c === 'UserD'){
            helper.fetchPickListVal(component, 'Maximum__c', 'dashboardUserTiersOptions');
        }else if(component.get('v.singleRec').Picklist_Value__c === true && component.get('v.singleRec').PricingType__c === 'PV'){
            helper.fetchPickListVal2(component, 'Maximum__c', 'pvTiersOptions');
        }
        helper.getEditablePrices(component, event, helper);
    },
    
    onBundleChange: function(component, event, helper) {
      //  console.log('InlineEdit Controller: BundleChange');
        helper.fetchPickListVal(component, 'Maximum__c', 'dashboardUserTiersOptions');
        helper.fetchPickListVal(component, 'Maximum__c', 'pvTiers');
    },
    
    onTierChange: function(component,event,helper){
        //console.log('InlineEdit Controller: onTierChange');
        helper.fetchPickListVal(component, 'Maximum__c', 'dashboardUserTiersOptions');
        helper.fetchPickListVal(component, 'Maximum__c', 'pvTiers');
    },
    
    inlineEditQuantity : function(component,event,helper){   
        //console.log('InlineEdit Controller: Quantity');
        component.set("v.quantityEditMode", true); 
        setTimeout(function(){ 
            component.find("Quantity").focus();
        }, 100);
    },
    
    inlineEditPrice : function(component,event,helper){   
        //console.log('InlineEdit Controller: Price');
        component.set("v.priceEditMode", true); 
        setTimeout(function(){ 
            component.find("PriceEach").focus();
        }, 100);
    },
    
    inlineEditQuantitySelect : function(component,event,helper){ 
        //console.log('InlineEdit Controller: QuantitySelect');
        component.set("v.quantitySelectEditMode", true); 
        if(component.get('v.singleRec').PricingType__c === 'UserD'){
            component.find("quantitySelect").set("v.options" , component.get("v.dashboardUserTiersOptions"));
        }else if(component.get('v.singleRec').PricingType__c === 'PV'){
            component.find("quantitySelect").set("v.options" , component.get("v.pvTiersOptions"));
        } setTimeout(function(){ 
            component.find("quantitySelect").focus();
        }, 100);
    },
    
    onQuantityChange : function(component,event,helper){
        //console.log('InlineEdit Controller: QuantityChange');
        var quantity = event.getSource().get("v.value").trim();
        var record = component.get('v.singleRec');
        var max = component.get('v.singleRec').Max_Cap__c;
        var min = component.get('v.singleRec').Min_Cap__c;
        if(quantity > max){
            alert('The quantity cannot be higher than '+max);
            quantity = max;
        }
        if(quantity < min){
            alert('The quantity cannot be lower than '+min);
            quantity = min;
        }
        component.find('Quantity').set('v.value',quantity);
        var p = record.PriceEach * quantity;
        component.set('v.singleRec.PriceTotal',p);
        helper.updateQlis(component, event, helper);
    },
    
    onPriceChange : function(component,event,helper){    
        //console.log('InlineEdit Controller: PriceChange');
        var price = event.getSource().get("v.value").trim();
        var record = component.get('v.singleRec');
        var p = price * record.Quantity__c;
        record.PriceEach = price;
        record.PriceTotal = p;
        component.set('v.singleRec',record);
        helper.updateQlis(component, event, helper);       
    },
    
    onQuantitySelectChange : function(component,event,helper){
        //console.log('InlineEdit Controller: QuantitySelectChange');
        var quantity = event.getSource().get("v.value").trim();
        var record = component.get('v.singleRec');
        var p = record.PriceEach * quantity;
        component.set('v.singleRec.PriceTotal',p);
        helper.updateQlis(component, event, helper);
    },     
    
    closeQuantityBox : function (component, event, helper) {
        //console.log('InlineEdit Controller: closeQuantity');
        component.set("v.quantityEditMode", false); 
        if(event.getSource().get("v.value").trim() == ''){
            component.set("v.showErrorClass",true);            
        }else{
            component.set("v.showErrorClass",false);
        }
    }, 
    
    closePriceBox : function (component, event, helper) {
        //console.log('InlineEdit Controller: closePricebox');
        component.set("v.priceEditMode", false); 
        if(event.getSource().get("v.value").trim() == ''){
            component.set("v.showErrorClass",true);
        }else{
            component.set("v.showErrorClass",false);
        }
    }, 
    
    closeQuantitySelectBox : function (component, event, helper) {
        //console.log('InlineEdit Controller: closeQuantitySelect');
        component.set("v.quantitySelectEditMode", false); 
    },
    
    openModel: function(component, event, helper) {   
        //console.log('Inline Controller: Open Modal');
        component.set("v.isOpen", true); 
    },
    
    onClose: function(component, event, helper) {
        //console.log('InlineEdit Controller: onClose');
        component.set("v.isOpen", false);
    },
    
    deleteFeature: function(component,event,helper){
        //console.log('InlineEdit Controller: deleteFeature');
        component.set('v.deleteFeature',true);
        helper.updateQlis(component, event, helper);
    }
})