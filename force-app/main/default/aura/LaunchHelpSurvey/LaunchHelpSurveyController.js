({
    launchHelpSurvey : function(component, event, helper) {
        //console.log('launch help survey');
        var recordId = component.get('v.recordId');
        var sObject = component.get('v.sobjecttype');
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var navEvt = $A.get("e.force:navigateToURL");
        if(sObject == 'Opportunity') {
            navEvt.setParams({
                "url" : "https://survey.qualtrics.com/jfe/form/SV_3aU9rGV04VCuHUF?userId="+userId+"&opportunityId="+recordId
            });
        } else if (sObject == 'Account') {
            navEvt.setParams({
                "url" : "https://survey.qualtrics.com/jfe/form/SV_3aU9rGV04VCuHUF?userId="+userId+"&accountId="+recordId
            });
        } else if (sObject == 'Quote') {
            navEvt.setParams({
                "url" : "https://survey.qualtrics.com/jfe/form/SV_3aU9rGV04VCuHUF?userId="+userId+"&quoteId="+recordId
            });
        } else if (sObject == 'Client__c') {
            navEvt.setParams({
                "url" : "https://survey.qualtrics.com/jfe/form/SV_3aU9rGV04VCuHUF?userId="+userId+"&clientId="+recordId
            });
        } else {
            navEvt.setParams({
                "url" : "https://survey.qualtrics.com/jfe/form/SV_3aU9rGV04VCuHUF?userId="+userId
            });
        }
        navEvt.fire();
    }
})