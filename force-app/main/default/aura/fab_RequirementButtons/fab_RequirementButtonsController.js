({
    doInit : function(component,event,helper){
       // console.log('RBC: doInit') 
        var opp = component.get('v.opportunity')
        
        if(opp.Client__c != undefined){
            if(opp.Client__r.Shipping_Zip_Postal_Code__c != undefined){
                component.set('v.readyToValidate',true);
            }  
        }
      //  console.log(component.get('v.readyToSubmit'))
    },
    
    goToBillingTab : function(component,event,helper){
      //  console.log('RBC: goToBillingTab')
        var cmpEvent = $A.get("e.c:fab_goToBillingTab");
        cmpEvent.fire();
    },
    
    goToBrandTab : function(component,event,helper){
     //   console.log('RBC: goToBrandTab')
        var cmpEvent = $A.get("e.c:fab_goToBrandTab");
        cmpEvent.fire();
    },
    
    goToSummaryTab : function(component,event,helper){
      //  console.log('RBC: goToSummaryTab')
        var cmpEvent = $A.get("e.c:fab_goToSummaryTab");
        cmpEvent.fire();
    },
    
    submitForInvoice : function(component,event,helper){
      //  console.log('RBC: submitForInvoice');
        //var context = component.get("v.UserContext");
        var opportunityId = component.get('v.opportunity').Id     
        
        var url = '/apex/RedirectionManager?sobject=opportunity&id='+component.get('v.opportunity').Id+'&action=submit_for_invoice'
      
        helper.urlDirection(component,event,helper,url)
      
    },
    
    validateAddress : function(component,event,helper){
       // console.log('RBC: validateAddress');       
        var url = '/apex/ava_AddressValidation?Id='+component.get('v.opportunity.SyncedQuoteId');
        //console.log(url);
      //  console.log('typeof sforce from controller ' + typeof sforce);
        helper.urlDirection(component,event,helper,url);
    },
    
    calculateTaxes : function(component,event,helper){
      //  console.log('RBC: calculateTaxes');   
      //  console.log('typeof sforce from controller ' + typeof sforce);
       // component.set('v.showSpinner',true)
        var url = '/apex/ava_Page?Id='+component.get('v.opportunity.SyncedQuoteId') ;
     //   + '?retURL=/apex/OnboardingLightning?Id='+component.get('v.opportunity.Id')
      //  console.log(url);
        helper.urlDirection(component,event,helper,url);
        // sforce.one.back()
    },
    
    reloadFab :function(component,event,helper){
      //  console.log('RBC: reloadFab')
        var cmpEvent = $A.get("e.c:fab_Reload");
        cmpEvent.fire();
    },
    
    send : function(component,event,helper){
      //  console.log('RBC: send')
        var target = event.getSource();
        var buttonName = target.get("v.value");
    
    },
    
    requestContractReview : function (component,event,helper){
     //   console.log('RBC: requestContractReview')
      //  var context = component.get("v.UserContext");
        var opportunityId = component.get('v.opportunity').Id       
        var userId = component.get('v.userId');
        var url = 'https://survey.qualtrics.com/jfe/form/SV_3aU9rGV04VCuHUF?opportunityId='+opportunityId+'&userId='+userId;
        
        // https://survey.qualtrics.com/jfe/form/SV_3aU9rGV04VCuHUF?opportunityId=0062J00000mqNVRQA2&userId=00550000005fPrMAAU
       helper.urlDirection(component,event,helper,url)
     
        var contractDocument = component.get('v.contractDocument');
        contractDocument.Legal_Case_Created__c = true;
        var cmpEvent = $A.get("e.c:fab_saveCdChanges");
        
        cmpEvent.setParams({"contractDocument": contractDocument});
        cmpEvent.fire();
    },
    
    requestEstimate : function (component,event,helper){
       // console.log('RBC: requestEstimate')
        //var context = component.get("v.UserContext");
        var opportunityId = component.get('v.opportunity').Id
        var quoteId = component.get('v.opportunity').SyncedQuoteId
        //  console.log(quoteId)
        var userId = component.get('v.userId');
        
        var url = 'https://services.qualtrics.com/jfe/form/SV_9o7r0gk556N0bGZ?QuoteID='+quoteId+'&caseId=&transferId=R_2QhMKdx02qD1x3j&SurveyTakerSFDCId='+userId;
        //var url = 'https://survey.qualtrics.com/jfe/form/SV_3aU9rGV04VCuHUF?Q_PopulateResponse={%22QID1%22:%224%22,%22QID5%22:%2223%22,%22QID12%22:%221%22}&Q_PopulateNext=1&Services=true&quoteId='+quoteId+'&userId='+userId
        
       helper.urlDirection(component,event,helper,url)
      
    },
    
    requestSOW : function (component,event,helper){
      //  console.log('RBC: requestSOW')
        //var context = component.get("v.UserContext");
        var opportunityId = component.get('v.opportunity').Id
        var userId = component.get('v.userId');
        var quoteId = component.get('v.opportunity').SyncedQuoteId
        var url = 'https://survey.qualtrics.com/jfe/form/SV_3aU9rGV04VCuHUF?Q_PopulateResponse={%22QID1%22:%224%22,%22QID5%22:%2223%22,%22QID12%22:%222%22}&Q_PopulateNext=1&Services=true&quoteId='+quoteId+'&userId='+userId
        helper.urlDirection(component,event,helper,url)
       
    },
    
    onVerify : function (component,event,helper){   
//        console.log('RBC: onVerify')
        component.set('v.verified',verified);
    },
    
    sendForApproval : function(component,event,helper){
       // console.log('submitForApproval2')
        component.set('v.showSpinner',true);
        var action = component.get('c.submitForApproval');
        
        var opportunityId = component.get('v.opportunity').Id      
        var accountId = component.get('v.opportunity').AccountId
        var clientId = component.get('v.opportunity').Client__c
        var quoteId = component.get('v.opportunity').SyncedQuoteId

        action.setParams({
            "opportunityId": opportunityId,
            "accountId" : accountId,
            "clientId" : clientId,
            "quoteId" :quoteId
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            // console.log(state)
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();   
                component.set('v.showSpinner',false)
                var url = "/" + component.get("v.opportunity").Id;
                
               helper.urlDirection(component,event,helper,url)
            } 
            else if (state === 'ERROR') {
                var header = 'Submit for Approval Error';
                helper.processErrors(component,response,header);
            }
        }));
        $A.enqueueAction(action);
    },
    
    cancelPayment : function(component,event,helper){
        console.log('cancelPayment')
        component.set('v.showSpinner',true)
        var opportunityId = component.get('v.opportunity').Id;
        console.log(opportunityId)
        var action = component.get('c.cancelCC');
        action.setParams({
            "opportunityId": opportunityId
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            console.log(state)
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                
                helper.reloadFab(component,event,helper);
                component.set('v.showSpinner',false)
            }  
            else if (state === 'ERROR') {
                var header = 'Contract Document Creation Error';
                   helper.processErrors(component,response,header);
                component.set('v.showSpinner',false)
            }
        }));
        $A.enqueueAction(action); 
    }
    
})