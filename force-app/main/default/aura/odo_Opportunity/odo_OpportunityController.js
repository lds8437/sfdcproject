({
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            var clientId = component.get('v.simpleRecord').Client__c;
            var action = component.get('c.getClient');
           // console.log(clientId);
            action.setParams({
                "clientId": clientId
            });
            
            action.setCallback(this, $A.getCallback(function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var dataObj= response.getReturnValue();
                    if(dataObj === null){
                        component.set('v.noClient',true);
                    }
                    else if(dataObj.Netsuite_External_Client_ID__c===undefined){
                        component.set('v.noLicense',true);
                    }else{
                        component.set('v.noClient',false);
                        component.set('v.vfHost','https://odo.corp.qualtrics.com/?a=Client&cid='+dataObj.Netsuite_External_Client_ID__c+'&b=ClientStory');
                    }
                    
                }  
                else if (state === 'ERROR') {
                    helper.handleErrors(component,response);
                }
            }));
                $A.enqueueAction(action);
        } 
    }
})