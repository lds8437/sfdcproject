({
	handleErrors : function(component,errors){
        if (errors) {
            if (errors[0] && errors[0].message) {
                component.find('notifLib').showNotice({
                    "variant": "error",
                    "header": "Something has gone wrong!",
                    "message": "Error message: " + errors[0].message,
                    closeCallback: function() {
                        return;
                    }
                });
            }
        } else {
            component.find('notifLib').showNotice({
                "variant": "error",
                "header": "Something has gone wrong!",
                "message": "Unknown error",
                closeCallback: function() {
                    return;
                }
            });
        }
    }
})