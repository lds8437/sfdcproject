({
	doInit : function(component, event, helper) {
       // console.log('ServicePartners Controller: doInit');
        component.set("v.showspinner", true);
        var partners = component.get('v.partners');
        var servicebundle = component.get("v.servicebundle");
        var indexOfPartner = _.findIndex(partners, {'Id' : servicebundle.Partner_Availability__c});
        if(indexOfPartner > -1) {
            if(servicebundle.Qualtrics_Invoicing__c === true) {
                component.set('v.qualtricsinvoice', true);
                component.set('v.selectedpartner', partners[indexOfPartner]);
            }
            else {
                component.set('v.qualtricsinvoice', false);
                component.set('v.selectedpartner', partners[indexOfPartner]);
            }
        }
        component.set("v.showspinner", false);
        //console.log('doinit for servicespartnerscontroller');
        
    },
    handleOptionSelect : function(component, event, helper) {
       // console.log('ServicePartners Controller: handleOptionSelect');
        var partner = event.getParam("partner");
        var qualtricsinvoice = event.getParam("qualtricsinvoice");
        component.set("v.selectedpartner", partner);
        component.set("v.qualtricsinvoice", qualtricsinvoice);
        component.set('v.partnerSelected', event.getParam('partnerSelected'));
    },
    getSelectedPartner : function(component, event, helper) {
       // console.log('ServicePartners Controller: getSelectedPartner');
        //console.log('running get selected partner function');
        var selected = {};
        var partneravailability = component.get("v.selectedpartner");
        var qualtricsinvoice = component.get("v.qualtricsinvoice");
        var servicebundle = component.get("v.servicebundle");
        //delete servicebundle.Partners;
        selected.partneravailability = partneravailability;
        selected.qualtricsinvoice = qualtricsinvoice;
        selected.servicebundle = servicebundle;
       // console.log('service bundle to be returned' + JSON.stringify(selected));
        return selected;
    }
 })