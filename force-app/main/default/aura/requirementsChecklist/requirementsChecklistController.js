({
	 handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
           // record is loaded (render other component which needs record data value)
            console.log("Record is loaded successfully.");
            var consolidatedScreeningHit = component.get('v.simpleRecord').Consolidated_Screening_Hit__c;
            var invoiceRequirements = component.get('v.simpleRecord').Ready_to_Invoice__c;
            var syncedQuoteId = component.get('v.simpleRecord').SyncedQuoteId;
            var billedAgainstRetainer = component.get('v.simpleRecord').Billed_Against_Retainer__c;
            //console.log(syncedQuoteId);
            component.set("v.consolidatedScreeningHit",consolidatedScreeningHit);
            if((syncedQuoteId === null || syncedQuoteId === undefined) && !billedAgainstRetainer){
                component.set('v.hasSyncedQuote',false);
            }else{
                component.set('v.hasSyncedQuote',true);
            }
            if(invoiceRequirements === null || invoiceRequirements === undefined){
                component.set('v.hasInvoiceRequirements',false);
            }else{
                var invoiceReq = invoiceRequirements.split('<br>');
            invoiceReq.pop();
            component.set('v.invoiceRequirements',invoiceReq);
            }
            
           
        } else if(eventParams.changeType === "CHANGED") {
            // record is changed
            //console.log("Record is changed.");
            var invoiceRequirements = component.get('v.simpleRecord').Ready_to_Invoice__c;
            var syncedQuoteId = component.get('v.simpleRecord').SyncedQuoteId;
            console.log(syncedQuoteId);
            if(syncedQuoteId === null || syncedQuoteId === undefined){
                component.set('v.hasSyncedQuote',false);
            }else{
                component.set('v.hasSyncedQuote',true);
            }
            //console.log(component.get('v.hasSyncedQuote'));
            if(invoiceRequirements === null || invoiceRequirements === undefined){
                component.set('v.hasInvoiceRequirements',false);
            }else{
                var invoiceReq = invoiceRequirements.split('<br>');
            invoiceReq.pop();
            component.set('v.invoiceRequirements',invoiceReq);
            }
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        }
    }
})