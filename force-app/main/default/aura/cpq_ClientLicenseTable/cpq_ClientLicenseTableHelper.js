({
    setUpGroupings : function(component) {
       // console.log('ClientLicenseTable Helper: setUpGroupings');
        //Get information set up for each group
        var groups = component.get("v.groupings");
        var groupobjects = [];
        var accountlicenses = component.get("v.licenses");
        var i = 0;
        for (i = 0; i < groups.length; i++) {
           // console.log('group iteration');
            var newgroupobject = {group : "", end : "", status : "", licenses : "", clientname : "", bundlename : "", start : "", currencycode : "", value : "", };//New Object for group info.
            var groupParams = groups[i].split("|");
            var clientid = groupParams[0];
            var bundleid = groupParams[1];
            var enddate = groupParams[2];
            var status = groupParams[3];
            newgroupobject.clientid = clientid;
            newgroupobject.bundleid = bundleid;
            newgroupobject.group = groups[i];
            newgroupobject.end = enddate;
            newgroupobject.status = status;
            
            //Get group licenses
            var licenses = [];
            var n = 0;
            for (n = 0; n < accountlicenses.length; n++) {
                var license = accountlicenses[n];
                if(!(license === undefined)){
                    //console.log('license under review');
                    //console.log('current license under review ' + license);
                    //console.log('matching paramters: ' + license.Client__c + clientid + license.Bundle__c + bundleid + license.License_End_Date__c + enddate + license.Status__c + status);
                    if(license.Client__c == clientid && license.Bundle__c == bundleid && license.License_End_Date__c == enddate && license.Status__c == status){
                        licenses.push(license);
                        //console.log('license pushed');
                    }
                }
            }
            
            newgroupobject.licenses = licenses;
            if(licenses.length > 0){
                //Get the client name
                
                if(clientid != 'null'){
                    var client = licenses[0].Client__r;
                    var clientname = client.Name;
                    newgroupobject.clientname = clientname;
                }
                
                //Get the bundle name
                if(bundleid != 'null'){
                    var bundle = licenses[0].Bundle__r;
                    var bundlename = bundle.Name;
                    var experience = bundle.Experience__c;
                    var upgradeBundles = bundle.Upgrade_Bundles__c;
                    newgroupobject.upgradeBundles = upgradeBundles;
                    newgroupobject.experience = experience;
                    newgroupobject.bundlename = bundlename;
                }
                //Get the earliest start date
                var starts = [];
                for (n = 0; n < licenses.length; n++) {
                    starts.push(licenses[n].License_Start_Date__c);
                }
                starts.sort();
                var firststart = starts[0];
                newgroupobject.start = firststart;
                
                //Find out if the licenses all share the same currency as the client.
                var clientcurrency = licenses[0].Client__r.CurrencyIsoCode;
                var currencymatch = 'true';
                for (n = 0; n < licenses.length; n++) {
                    if(licenses[n].CurrencyIsoCode != clientcurrency){
                        currencymatch = 'false';
                    }
                }                
                
                if(currencymatch == 'true' || currencymatch == 'false'){//If all licenses share the same currency as the client...
                    //console.log('currencymatch true');
                    //Get the currency code
                    var currencycode = licenses[0].CurrencyIsoCode;
                    newgroupobject.currencycode = currencycode;
                    
                    //Get summary of license value
                    var valuesum = 0;
                    for (n = 0; n < licenses.length; n++) {
                        valuesum += licenses[n].Invoice_Amount__c;
                    }
                    var locale = licenses[0].Culture_Code__c;
                    var valuesumformatted = valuesum.toLocaleString(locale, { style: 'currency', currency: currencycode });
                    newgroupobject.value = valuesumformatted;
                    newgroupobject.valuesum = valuesum;
                    groupobjects.push(newgroupobject);
                }
                else{//If at least one license has a different currency than the client...
                    //console.log('currencymatch false');
                    var currencycode = clientcurrency;
                    newgroupobject.currencycode = currencycode;
                    //Set up call to Apex to get the value summary in the client currency
                    var action = component.get("c.getLicenseValueSum");
                    action.setParams({ 
                        licenses : licenses
                    });
                    action.setCallback(this, function(response) {
                        var state = response.getState();
                        if (state === "SUCCESS") {
                            //Get summary of license value
                            var valuesum = response.getReturnValue();
                            //Convert to format of currency locale
                            var valuesumformatted = valuesum.toLocaleString(locale, { style: 'currency', currency: currencycode });
                            newgroupobject.value = valuesumformatted;
                            newgroupobject.valuesum = valuesum;
                            groupobjects.push(newgroupobject);
                        }
                        else if (state === "INCOMPLETE") {
                            console.log('get converted value sum operation incomplete');
                        }
                            else if (state === "ERROR") {
                                var errors = response.getError();
                                if (errors) {
                                    if (errors[0] && errors[0].message) {
                                        console.log("Error message: " + 
                                                    errors[0].message);
                                    }
                                } else {
                                    console.log("Unknown error");
                                }
                            }
                    });
                    $A.enqueueAction(action);
                }  
            }
        }
        groupobjects = _.sortBy(groupobjects, ['end']);
        component.set("v.groupobjects", groupobjects);
        //console.log('groupobjects' + JSON.stringify(groupobjects));
    },
    columnSort : function(component, column) {
       // console.log('ClientLicenseTable Helper: columnSort');
        //Get the current sort state of the column
        var sortstate = 'v.' + column + 'sort';
       // console.log('sortstate: ' + sortstate);
        var currentstate = component.get(sortstate);
       // console.log('currentstate: ' + currentstate);
        //Reset all the sort states to inactive
        if(column != 'client'){
            component.set("v.clientsort", "inactive");}
        if(column != 'bundle'){
            component.set("v.bundlesort", "inactive");}
        if(column != 'start'){
            component.set("v.startsort", "inactive");}
        if(column != 'end'){
            component.set("v.endsort", "inactive");}
        if(column != 'status'){
            component.set("v.statussort", "inactive");}
        if(column != 'value'){
            component.set("v.valuesort", "inactive");}
        var groupings = component.get("v.groupobjects");
      //  console.log('groupings before sort' + groupings);
        if(currentstate == 'asc'){
            component.set(sortstate,"desc");
            var sorted = groupings.sort(function(a,b){
                var x;
                var y;
                if(column == 'client'){
                    x = a.clientname;
                    y = b.clientname;
                }
                if(column == 'bundle'){
                    x = a.bundlename;
                    y = b.bundlename;
                }
                if(column == 'start'){
                    x = a.start;
                    y = b.start;
                }
                if(column == 'end'){
                    x = a.end;
                    y = b.end;
                }
                if(column == 'status'){
                    x = a.status;
                    y = b.status;
                }
                if(column == 'value'){
                    x = a.valuesum;
                    y = b.valuesum;
                }
                if (x < y) {return 1;}
                if (x > y) {return -1;}
                return 0;
            });
            component.set("v.groupobjects", sorted);
            //console.log('desc sort attempted');
        //    console.log('groupings after desc sort' + sorted);
        }            
        else{
            component.set(sortstate,"asc");
            var sorted = groupings.sort(function(a,b){
                var x;
                var y;
                if(column == 'client'){
                    x = a.clientname;
                    y = b.clientname;
                }
                if(column == 'bundle'){
                    x = a.bundlename;
                    y = b.bundlename;
                }
                if(column == 'start'){
                    x = a.start;
                    y = b.start;
                }
                if(column == 'end'){
                    x = a.end;
                    y = b.end;
                }
                if(column == 'status'){
                    x = a.status;
                    y = b.status;
                }
                if(column == 'value'){
                    x = a.valuesum;
                    y = b.valuesum;
                }
                if (x < y) {return -1;}
                if (x > y) {return 1;}
                return 0;
            });
            component.set("v.groupobjects", sorted);
            //console.log('asc sort attempted');
           // console.log('groupings after asc sort' + sorted);
        }
    }
})