({
    executeSelection : function(component) {
		var previouslySelected = component.get("v.criteria.isComplete");
        component.set("v.criteria.isComplete", !previouslySelected);
        var selectEvent = component.getEvent("selectCompletionCriteria");
		selectEvent.setParam("selectedCompletionCriteria", component.get("v.criteria"));
        selectEvent.setParam("selectedValue", !previouslySelected);
        selectEvent.fire();
    }
})