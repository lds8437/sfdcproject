({
    getPicklistOptions : function(component) {
        const picklistOptionAction = component.get("c.generatePicklistJSON");
        picklistOptionAction.setParams({"sObjectType": "Opportunity", "fieldApiNames": ["Invoice_Email_Language__c", "Payment_Terms__c", "ARR_NRR__c", "Auto_Renewal__c", "Renewal_Price_Increase__c","Study_Type__c","Primary_Competitor__c","Incumbent__c"]})
        picklistOptionAction.setCallback(this, function(response) {
            const state = response.getState();
            if (state === "SUCCESS") {
                const picklistJSON = JSON.parse(response.getReturnValue());
                if (picklistJSON.hasOwnProperty("Invoice_Email_Language__c")) {
                    component.set("v.emailLanguageOptions", picklistJSON["Invoice_Email_Language__c"]);
                }
                if (picklistJSON.hasOwnProperty("Payment_Terms__c")) {
                    component.set("v.paymentTermOptions", picklistJSON["Payment_Terms__c"]);
                }
                if (picklistJSON.hasOwnProperty("ARR_NRR__c")) {
                    component.set("v.recurringRevenueOptions", picklistJSON["ARR_NRR__c"]);
                }
                if (picklistJSON.hasOwnProperty("Auto_Renewal__c")) {
                    component.set("v.autoRenewOptions", picklistJSON["Auto_Renewal__c"]);
                }
                if (picklistJSON.hasOwnProperty("Renewal_Price_Increase__c")) {
                    component.set("v.autoPriceOptions", picklistJSON["Renewal_Price_Increase__c"]);
                }
                if (picklistJSON.hasOwnProperty("Study_Type__c")) {
                    component.set("v.studyTypeOptions", picklistJSON["Study_Type__c"]);
                }
                if (picklistJSON.hasOwnProperty("Primary_Competitor__c")) {
                    component.set("v.primaryCompetitorOptions", picklistJSON["Primary_Competitor__c"]);
                }
                if (picklistJSON.hasOwnProperty("Incumbent__c")) {
                    component.set("v.incumbentOptions", picklistJSON["Incumbent__c"]);
                }
            } else {
                console.log("There was a problem retrieving Opportunity picklist values: " + state + ", " + JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(picklistOptionAction);
    },
    reloadOpportunity : function(component) {
        component.find("oppRecordLoader").reloadRecord(false, $A.getCallback(function(reloadResult) {
            const consumptionTaxCountryValues = ['usa', 'us', 'united states of america', 'canada', 'united states'];
            let shippingCountry = component.get("v.tempOpp.Sales_Order_Shipping_Country__c");
            //let studyTypeSelections = component.get("v.tempOpp.Panel_Type_of_Project__c");
            shippingCountry = shippingCountry ? shippingCountry.toLowerCase().trim() : "";
            //studyTypeSelections = studyTypeSelections ? component.get("v.tempOpp.Panel_Type_of_Project__c").split(";"): [];
            component.set("v.showConsumptionTaxNumber", shippingCountry && consumptionTaxCountryValues.indexOf(shippingCountry) === -1);
            component.set("v.taxNumberDisabled", component.get("v.tempOpp.Not_Registered_for_Consumption_Tax__c") !== false);
            component.set("v.percentIncreaseDisabled", component.get("v.tempOpp.Renewal_Price_Increase__c") === "No");
            //Format incumbent value from ; separated string from DB into JS List/array.
            console.log('initial incumbent input from DB ' + component.get('v.tempOpp.Incumbent__c'));
            var incumbentvalue = component.get('v.tempOpp.Incumbent__c');
            if(incumbentvalue !== undefined && incumbentvalue !== null){
                var incumbentlist = incumbentvalue.split(';');}
            else{var incumbentlist = [];}            
            component.set('v.incumbentSelections', incumbentlist);            
            //component.set("v.studyTypeSelections", studyTypeSelections);
            //For recordtype = Panels
			//If Panel_Demographics_Invoice_Display__c IS BLANK, then pull in Panel_Demographics__c, otherwise pull in Panel_Demographics_Invoice_Display__c into the Invoice Display Text field
            if(component.get('v.tempOpp.RecordType.Name') == 'Panels' && component.get("v.tempOpp.Panel_Demographics_Invoice_Display__c") == null){
                component.set('v.tempOpp.Panel_Demographics_Invoice_Display__c', component.get('v.tempOpp.Panel_Demographics__c'));
                console.log('panel demographics invoice display set to panel demographics' + component.get('v.tempOpp.Panel_Demographics__c'));
            }
            component.set("v.showSpinner", false);
        }));
    },
    saveClient : function(component) {
        component.set("v.showSpinner", true);
        component.find("clientRecordLoader").saveRecord($A.getCallback(function(saveResult) {
            // Only show a toast if there's an error saving the record.
            if (saveResult.state === "INCOMPLETE") {
                if(component.get('v.isLightningOut') === false){
                    component.find('addBillingNotifLib').showToast({
                        "title": "There was a problem saving the Consumption Tax Number on the Client record",
                        "message": "User is offline, device doesn't support drafts.",
                        "variant": "error"
                    });
                }
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                const errors = saveResult.error;
                let errorMessage;
                if (errors && errors[0] && errors[0].message) {
                    errorMessage = errors[0].message;
                } else {
                    errorMessage = "Unknown error.";
                }
                if(component.get('v.isLightningOut') === false){
                    component.find('addBillingNotifLib').showToast({
                        "title": "There was a problem saving the Consumption Tax Number on the Client record",
                        "message": "Error: " + errorMessage,
                        "variant": "error"
                    });
                }
                console.log("Problem saving record, error: " + JSON.stringify(saveResult.error));
            } else if (saveResult.state !== "SUCCESS" && saveResult.state !== "DRAFT") {
                if(component.get('v.isLightningOut') === false){
                    component.find('addBillingNotifLib').showToast({
                        "title": "There was a problem saving the Consumption Tax Number on the Client record",
                        "message": "Error: " + "Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error),
                        "variant": "error"
                    });
                }
                console.log("Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error));
            }
            component.set("v.showSpinner", false);
        }));
    }
})