({
  afterRender: function (component, helper) {
    this.superAfterRender();
    /* Hack to get helptext label to function properly
    const helptextLabel = document.getElementById("abi-consumption-tax-number-label");
    const consumptionTaxInput = document.querySelector("input[name='abi-consumption-tax-number']");
    helptextLabel.htmlFor = consumptionTaxInput.id;
    */
  }
})