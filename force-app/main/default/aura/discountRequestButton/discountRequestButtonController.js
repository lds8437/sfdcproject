({
	doInit : function(component, event, helper) {
        var queryCall = component.get("c.getUser");
       // queryCall.setParams({
       //     "id": component.get("v.recordId")
       // })
        queryCall.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				var user = response.getReturnValue();
                component.set("v.doneProcessing", true);
                component.set("v.quote", user);
            } else if (state === "ERROR") {
                console.log("Error getting quoting requirements");
            } else {
                console.log("Unknown problem getting quoting requirements");
            }
        });
        $A.enqueueAction(queryCall);
	},
    
    requestDiscount : function(component, event, helper) {
        helper.redirectUser(component);
    },
    
   
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
           // record is loaded (render other component which needs record data value)
            console.log("Record is loaded successfully.");
            var oppOwner = component.get('v.simpleRecord').Opportunity_Owner__c;
            var delegatedRSRep = component.get('v.simpleRecord').Delegated_RS_Rep__c;
            var delegatedEIRep = component.get('v.simpleRecord').Delegated_EI_Rep__c;
            var products = component.get('v.simpleRecord').Products__c;
            var quotingRequirements = component.get('v.simpleRecord').Quoting_Requirements__c;
            var userName = component.get('v.user').Full_Name__c;
            var userId = component.get('v.user').Id;
            var userProfile = component.get('v.user').Profile.Name;
            
            if(userName === oppOwner || userProfile === 'System Administrator' || userProfile === 'Deal Desk' || userId === delegatedRSRep ||  userId === delegatedEIRep){
                component.set('v.approvedUser',true);
            }
            if(products !== null){
                component.set('hasProducts',true);
            }
            
            if(quotingRequirements === null || quotingRequirements === undefined){
                component.set('v.discountReady',true);
            }
            
            
           
        } else if(eventParams.changeType === "CHANGED") {
            // record is changed
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        }
    }

})