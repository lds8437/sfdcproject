({
    redirectUser : function(component) {
        var redirectEvent = $A.get("e.force:navigateToURL");
        redirectEvent.setParams({
            "url": "/flow/Q2C_Discount_Wizard_Chain?varQuoteID="+component.get("v.recordId")+"&retURL=/"+component.get("v.recordId")            
        });
        redirectEvent.fire();
        $A.get("e.force:closeQuickAction").fire();
    }
})