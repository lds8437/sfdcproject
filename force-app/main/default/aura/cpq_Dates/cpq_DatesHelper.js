({
    updateEndDate : function (component, event, helper){
        //console.log('DT HLPR: updateEndDate');
        var quoteStartDate = component.get('v.quoteStartDate');
        var quoteTerm = component.get('v.quoteTerm');
        var quoteType = component.get('v.quoteType');
        var upgradeType = component.get('v.upgradeType');
        var licenseEndDate = component.get('v.end');
        licenseEndDate = moment.utc(licenseEndDate);
        licenseEndDate = moment(licenseEndDate).format('YYYY-MM-DD');
        if (quoteType === 'New License Quote' || upgradeType === 'change-dates' || upgradeType === 'on-renewal' || upgradeType === 'merge') {
            var quoteEndDate = moment.utc(quoteStartDate).add(quoteTerm, 'months');
            if(quoteTerm >= 1){
                quoteEndDate = moment.utc(quoteEndDate).subtract(1, 'days').format('YYYY-MM-DD'); 
            }else{
                quoteEndDate = moment.utc(quoteEndDate).format('YYYY-MM-DD');
            }
            
            component.set('v.quoteEndDate', quoteEndDate);
        } else if (upgradeType === 'keep-dates' && component.get('v.dealDeskAccess')) {
            var quoteEndDate = moment.utc(quoteStartDate).add(quoteTerm, 'months');
            if(quoteTerm >= 1){
                quoteEndDate = moment.utc(quoteEndDate).subtract(1, 'days').format('YYYY-MM-DD'); 
            }else if (quoteTerm != undefined){
                quoteEndDate = moment.utc(quoteEndDate).format('YYYY-MM-DD');
            }
            else {
                quoteEndDate = licenseEndDate;
            }
            component.set('v.quoteEndDate', quoteEndDate);
            //console.log('DT HLPR: updateEndDate - CALL HLPR updateTermOnKeepDates');
            helper.updateTermOnKeepDates(component, event, helper);
        }
        else if (upgradeType === 'keep-dates') {
            component.set('v.quoteEndDate', licenseEndDate);
            //console.log('DT HLPR: updateEndDate - CALL HLPR updateTermOnKeepDates');
            helper.updateTermOnKeepDates(component, event, helper);
        }
    },
    updateTerm : function(component,event,helper){
       //console.log('DT HLPR: updateTerm');
        var quoteTerm = component.get('v.quoteTerm');
        var licenseEnd = component.get('v.licenseEndDate');
        var quoteStart = component.get('v.quoteStartDate');
        var newterm = moment.utc(licenseEnd).diff(moment.utc(quoteStart), 'days');
        var newterm2 = Math.round(newterm / 30)
        if(newterm2<0){
            newterm2 = .1;
        }
        component.set('v.quoteTerm',newterm2);
    },
    
    updateTermOnKeepDates : function(component, event, helper) {
        //console.log('DT HLPR: updateTermOnKeepDates');
        var licenseEnd = component.get('v.quoteEndDate');
        var licenseStart = component.get('v.quoteStartDate');
        var newterm = moment.utc(licenseEnd).diff(moment.utc(licenseStart), 'days');
        var newterm2 = Math.round(newterm / 30);
        if(newterm2<0){
            newterm2 = .1;
        }
        component.set('v.quoteTerm', newterm2);
    },
    
    updateMergeTerms : function(component, event, helper) {
        //console.log('DT HLPR: updateMergeTerms');
        var licenses = component.get('v.originalLicenses');
       // console.log(licenses);
        var startDate = component.get('v.quoteStartDate');
        if(licenses.length > 0) {
            _.forEach(licenses, function(license) {
                license.MergeTerm = moment(license.License_End_Date__c).month() - moment(startDate).month();
                if(license.MergeTerm <= 0) {
                    if(moment(license.License_End_Date__c).month() != moment(startDate).month() || moment(license.License_End_Date__c).year() != moment(startDate).year()) {
                        license.MergeTerm = license.MergeTerm + 12;
                    }
                }
            });
           // console.log(licenses);
            component.set('v.mergeTermsChanged', true);
        }
    }
})