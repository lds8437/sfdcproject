({
    doInit : function(component, event, helper) {
        //console.log('DT CTRL: doInit');
        var quoteName = component.get('v.quoteName');
        var numOfQuotes = component.get('v.numOfQuotes');
        var profileName = component.get('v.profileName');
       var oppName = component.get('v.oppName');
        if(component.get('v.actionType') !== 'Edit' && component.get('v.actionType') !== 'Add') {
            component.set('v.quoteName', oppName + ' - ' + quoteName + numOfQuotes);
        }
        var upgradeType = component.get('v.upgradeType');
        var quoteTerm = component.get('v.quoteTerm');
        var quoteType = component.get('v.quoteType');
        var licenseEndDate = component.get('v.end');
        var licenseStartDate = component.get('v.start');
        if(quoteTerm == undefined && upgradeType != 'keep-dates') {
            component.set('v.quoteTerm', 12);
        }
        licenseEndDate = moment.utc(licenseEndDate);
        if(upgradeType === 'on-renewal' && !component.get('v.dealDeskAccess')) {
            component.set('v.disableStartDate', true);
        }
        else if(upgradeType === 'keep-dates' && !component.get('v.dealDeskAccess')) {
            component.set('v.disableTerm', true);
        }
        if((upgradeType === 'keep-dates'&& moment(licenseStartDate) < moment()) || (quoteType === 'New License Quote' && component.get('v.quoteStartDate') === null) || upgradeType === 'change-dates' || upgradeType === 'merge') {
            component.set('v.quoteStartDate',moment.utc().format('YYYY-MM-DD'));
        }
        else if(upgradeType === 'keep-dates'&& moment(licenseStartDate) >= moment()){
            component.set('v.quoteStartDate',moment(licenseStartDate).utc().format('YYYY-MM-DD') );
        }
        else if (upgradeType === 'on-renewal') {
            component.set('v.quoteStartDate', moment(licenseEndDate).add(1, 'days').format('YYYY-MM-DD'));
        }
        if (upgradeType === 'change-dates') {
            component.set('v.disableCredits', false);
        }
        //console.log('DT CTRL: doInit - CALL HLPR updateEndDate');
        helper.updateEndDate(component, event, helper);
        var coreQli = _.filter(event.getParam('techQlis'),{'PricingType__c': 'Core'});
       // helper.setPriceList(component, event, helper);
    },
    
    changeQuoteName : function(component, event, helper){
        //console.log('DT CTRL: ChangeQuoteName');
        var target=event.getSource();
        var quoteName = target.get('v.value');
        component.set('v.quoteName',quoteName);
    },
    
    onTermChange : function(component, event, helper){
        //console.log('DT CTRL: TermChange');
        var quoteType = component.get('v.quoteType');
        var term = component.get('v.quoteTerm');
        if(term > 12 && !component.get('v.dealDeskAccess')) {
            component.set('v.preventSave', true);
        } else {
            component.set('v.preventSave', false);
        }
        //console.log('DT CTRL: TermChange - CALL HLPR updateEndDate');
        helper.updateEndDate(component, event, helper);
    },
    
    onValueChange : function(component, event, helper){
        //console.log('DT CTRL: ValueChange');
        var quoteType = component.get('v.quoteType');
        var upgradeType = component.get('v.upgradeType');
        //console.log('DT CTRL: ValueChange - CALL HLPR updateEndDate');
        helper.updateEndDate(component, event, helper);
        if(upgradeType == 'merge') {
           //console.log('DT CTRL: ValueChange - CALL HLPR updateMergeTerms');
            helper.updateMergeTerms(component, event, helper);
        }
    },
    
    showPartners : function(component, event, helper){
        //console.log('DT CTRL: ShowPartner');
        var appEvent = $A.get("e.c:cpq_ShowPartnerSelector");
        //console.log('DT CTRL: ShowPartner - FIRE cpq_ShowPartnerSelector');
        appEvent.fire();
    },
    
    changeCredits : function(component, event, helper) {
        //console.log('DT CTRL: changeCredits');
        var credits = component.get('v.unusedCredits');
    },
    
    onActionChange : function(component,event,helper){
        //console.log('DT CTRL: onActionChange');
        if(component.get('v.actionType')==='Add'){
            component.set('v.disableStartDate', true);
            component.set('v.disableTerm', true);
            component.set('v.disablePriceList', true);
        }
    },

    onUpgradeChange : function(component, event, helper){
        if(component.get('v.upgradeType')==='on-renewal'){
            component.set('v.disableStartDate', true);
        }
    }
    
})