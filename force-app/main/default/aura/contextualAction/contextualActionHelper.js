({
    performNavigation: function(component, helper) {
        var recordId = component.get("v.record.Id");
        var dynamicAction = component.get("v.dynamicAction");
        var destinationType = dynamicAction.destinationType;
        var goToDestination = dynamicAction.goToDestination;
        var openInNewWindow = dynamicAction.openInNewWindow;
        var recordIdOutputParameterName = dynamicAction.recordIdOutputParameterName;
        var userIdOutputParameterName = dynamicAction.userIdOutputParameterName;
        if (destinationType == 'URL') {
            helper.navToUrl(helper, recordId, goToDestination, openInNewWindow, recordIdOutputParameterName, userIdOutputParameterName);
        } else if (destinationType == 'Lightning Component') {
			helper.navToComponent(component, dynamicAction);
        }
    },
    navToUrl: function(helper, recordId, goToDestination, openInNewWindow, recordIdOutputParameterName, userIdOutputParameterName) {
        goToDestination += helper.appendRecordId(recordId, goToDestination, recordIdOutputParameterName);
        goToDestination += helper.appendUserId(goToDestination, userIdOutputParameterName);
        if (openInNewWindow) {
	        window.open(goToDestination);
        } else {
            // This will always open in the same window
            // if it's a local address.  External addresses
            // always open in a new window.
            var evt = $A.get("e.force:navigateToURL");
            evt.setParams({
                "url": goToDestination
            });
            evt.fire();
        }
    },
    appendRecordId: function(recordId, goToDestination, recordIdOutputParameterName) {
        if (recordIdOutputParameterName == undefined) {
            return "";
        } else {
            var concatChar = '?';
            if (goToDestination.includes('?')) {
                concatChar = '&';
            }
            return concatChar + recordIdOutputParameterName + '=' + recordId;
        }
    },
    appendUserId: function(goToDestination, userIdOutputParameterName) {
        if (userIdOutputParameterName == undefined) {
            return "";
        } else {
            var concatChar = '?';
            if (goToDestination.includes('?')) {
                concatChar = '&';
            }
            return concatChar + userIdOutputParameterName + '=' + $A.get("$SObjectType.CurrentUser.Id");
        }
    },
    navToComponent: function(component, dynamicAction) {
        var event = component.getEvent("displayModal");
        event.setParam("dynamicAction", dynamicAction);
        event.fire();
    }
})