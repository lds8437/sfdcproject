({
    showSpinner: function (cmp, statusFlag) {
        cmp.set('v.Spinner', statusFlag);
    },

    saveBrandRecord: function (cmp) {

        var action = cmp.get("c.SaveBrandRecord");

        var tmpArr = cmp.get("v.SelectedUsersToMove");
        var usrToMove = '';
        for (var i = 0; tmpArr.length > i; i++) {
            usrToMove += tmpArr[i] + ';'
        }
        cmp.set("v.newbrand.Users_To_Move__c", usrToMove);
        tmpArr = cmp.get("v.SelectedUsersToCreate");
        var usrToCreate = '';
        for (var i = 0; tmpArr.length > i; i++) {
            usrToCreate += tmpArr[i] + ';'
        }
        cmp.set("v.newbrand.Users_To_Create__c", usrToCreate);

        action.setParams({
            "dataString": JSON.stringify(cmp.get("v.newbrand")),
            "OppId": cmp.get("v.OpportunityId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                //cmp.set("v.NewContactObj", response.getReturnValue());
                //cmp.set("v.isContactSelected", true);
                window.scrollTo(0, 0);
                console.log('#### send brand event ');
                var brandData = response.getReturnValue()
                var cmpEvent = $A.get("e.c:OnboardingBrandEvent");
                cmpEvent.setParams({"Brand": brandData, "CurrentBrand": brandData.Id});
                cmpEvent.fire();
                this.showSpinner(cmp, false);
                if(component.get('v.isLightningOut') === false){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type": "success",
                        "message": "Brand Record Created.",
                        duration: ' 2000',
                        type: 'success',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
                }
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    }
})