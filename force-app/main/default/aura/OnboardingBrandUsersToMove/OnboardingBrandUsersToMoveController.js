({
    doInit: function (cmp, event, helper) {
        //var result = decodeURIComponent((new RegExp('[?|&]' + "Id" + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
        //cmp.set("v.OpportunityId", result);
        //cmp.set("v.UserTypeToMove", JSON.parse("[{\"label\": \"Standard\", \"value\" : \"Standard\"},{\"label\": \"Administrator\", \"value\" : \"Administrator\"}]"));
        //cmp.set("v.BrandOptions", JSON.parse("[{\"label\": \"Request Brand\", \"value\" : \"Id\"},{\"label\": \"Single User/Entry License\", \"value\" : \"single\"}]"));
        
        //helper.getBrandLabels(cmp);
        //helper.getClientList(cmp, event, helper);
        //helper.getContactRole(cmp);
        //helper.getContactLeadSource(cmp);
        //helper.getContactStatus(cmp);
        //helper.getContactLabels(cmp);
        //helper.getOpportunity(cmp);
        /*var newObject = {
            Name: "",
            Requested_Brand_Id__c: "",
            Data_Center__c: "",
            Users_To_Create__c: "",
            UserTypeToMove: "",
            Username: "",
            Users_To_Move__c: "",
            Brand_Type__c: "",
            Type__c: "",
            BrandToMerge: "",
            Client_Has_Existing_Brand__c: "",
            BrandOptions: "",
            Existing_Brand_Id__c: "",
            existingbranduser: "",
            Existing_Brand_Admin_Username__c: "",
            Brand_Admin__c: ""
        };
        */
        //cmp.set("v.newbrand", newObject);
        cmp.find("brandRecordLoader").reloadRecord();
        helper.showSpinner(cmp, false);
    },
    //var tst = str.split(';')
    //tst.splice(0,1) ;
    brandChange: function(cmp, event, helper) {
        cmp.find("brandRecordLoader").reloadRecord();
    },
    handleBrandRecordUpdated: function(cmp,event,helper){
        //Handles loading data from brand via LDS
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            //record loaded
            //Parse out user strings into arrays
            var brand = cmp.get("v.simpleBrandRecord");
            var userstomove = brand.Users_To_Move__c;
            var userstomovearray = [];
            if(!!userstomove){
                userstomovearray = userstomove.split(';');                
            }
            cmp.set('v.SelectedUsersToMove', userstomovearray);
            var userstocreate = brand.Users_To_Create__c;
            var userstocreatearray = [];
            if(!!userstocreate){
                userstocreatearray = userstocreate.split(';');                
            }
            cmp.set('v.SelectedUsersToCreate', userstocreatearray);
        } else if(eventParams.changeType === "CHANGED") {
            // record is changed
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        }  
    },
    handleBrandComponentEvent: function (cmp, event, helper) {
        /*
        console.log('#########  UsersToMove');
        console.log(event.getParam("Brand"));
        var brandObj = event.getParam("Brand");
        
        if (brandObj["Users_To_Move__c"] != undefined  ){
            var spltValue = brandObj["Users_To_Move__c"].split(';') ;
            var tmpArr = new Array();
            for (var i = 0 ; spltValue.length > i ; i++) {
                if (spltValue[i].length > 0) {
                    tmpArr.push(spltValue[i]);
                }
            }
            cmp.set('v.SelectedUsersToMove' , tmpArr ) ;
            
        }
        if (brandObj["Users_To_Create__c"] != undefined  ){
            var spltValue = brandObj["Users_To_Create__c"].split(';') ;
            var tmpArr = new Array();
            for (var i = 0 ; spltValue.length > i ; i++) {
                if (spltValue[i].length > 0) {
                    tmpArr.push(spltValue[i]);
                }
            }
            cmp.set('v.SelectedUsersToCreate' , tmpArr ) ;
        }
        //brandObj["Users_To_Create__c"] = "" ;
        cmp.set('v.newbrand', brandObj);
        */
    },
    
    UsersToMove: function (cmp, event, helper) {
        
        var allValid = true;
        
        if ( cmp.find('move_username').get('v.validity').valueMissing ) {
            allValid = false;
            cmp.find('move_username').showHelpMessageIfInvalid();
        }
        
        
        if (cmp.find('move_username').get('v.validity').patternMismatch) {
            allValid = false;
            cmp.find('move_username').showHelpMessageIfInvalid();
        }
        
        
        if (allValid) {
            var users = cmp.get("v.simpleBrandRecord.Users_To_Move__c");
            if (users == undefined) {
                users = "";
            }
            var userType = cmp.find("UserTypeToMove").get("v.value");
            if (userType == undefined) {
                userType = "Standard";
            }
            
            
            if (users.length > 0) {
                users += ';' + cmp.get("v.MoveUsername") + "," + userType;
            } else {
                users = cmp.get("v.MoveUsername") + "," + userType;
            }
            
            var tmpArr = cmp.get("v.SelectedUsersToMove");
            tmpArr.push( cmp.get("v.MoveUsername") + "," + userType ) ;
            cmp.set("v.SelectedUsersToMove", tmpArr );
            cmp.set("v.MoveUsername", "");
            cmp.set("v.MoveUserType", "Standard");
            const unsavedChangesEvent = cmp.getEvent("unsavedChangesEvent");
            unsavedChangesEvent.setParams({
                "tabName": "Users to Move",
                "showUnsavedIndicator": true
            });
            unsavedChangesEvent.fire();
        } else {
            if(cmp.get('v.isLightningOut') === false){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Required fields are missing. Please update and save again.",
                    duration:' 2000',
                    mode: 'dismissible'
                    
                });
                toastEvent.fire();
            }
        }
        
    } ,
    removeUserToMove : function (cmp, event, helper) {
        var dataToRemove = event.target.dataset.index ;
        var tmpArr = cmp.get("v.SelectedUsersToMove");
        var newArr = new Array();
        for (var i = 0 ; i < tmpArr.length; i++) {
            if (tmpArr[i] != dataToRemove) {
                newArr.push ( tmpArr[i] );
            }
        }
        cmp.set("v.SelectedUsersToMove", newArr );
        const unsavedChangesEvent = cmp.getEvent("unsavedChangesEvent");
        unsavedChangesEvent.setParams({
            "tabName": "Users to Move",
            "showUnsavedIndicator": true
        });
        unsavedChangesEvent.fire();
    },
    editUserToMove : function (cmp, event, helper) {
        var dataToRemove = event.target.dataset.index ;
        var tmpArr = cmp.get("v.SelectedUsersToMove");
        
        var idx = tmpArr.indexOf(dataToRemove) ;
        tmpArr.splice(idx, 1);
        cmp.set("v.SelectedUsersToMove", tmpArr );
        var splitString = dataToRemove.split(',');
        cmp.set("v.MoveUsername", splitString[0]);
        cmp.set("v.MoveUserType", splitString[1] );
    },
    removeUserToCreate : function (cmp, event, helper) {
        var dataToRemove = event.target.dataset.index ;
        var tmpArr = cmp.get("v.SelectedUsersToCreate");
        var newArr = new Array();
        for (var i = 0 ; i < tmpArr.length; i++) {
            if (tmpArr[i] != dataToRemove) {
                newArr.push ( tmpArr[i] );
            }
        }
        cmp.set("v.SelectedUsersToCreate", newArr );
    },
    editUserToCreate : function (cmp, event, helper) {
        var dataToRemove = event.target.dataset.index ;
        
        var tmpArr = cmp.get("v.SelectedUsersToCreate");
        var idx = tmpArr.indexOf(dataToRemove) ;
        tmpArr.splice(idx, 1);
        
        cmp.set("v.SelectedUsersToCreate", tmpArr );
        var splitString = dataToRemove.split(',');
        cmp.set("v.NewUserFirstName", splitString[0]);
        cmp.set("v.NewUserLastName", splitString[1]);
        cmp.set("v.NewUserEmail", splitString[2]);
        cmp.set("v.NewUserType", splitString[3]);
    },
    AddUsersToCreate : function (cmp, event, helper) {
        
        
        var allValid = true;
        
        if ( cmp.find('create_username').get('v.validity').valueMissing ) {
            allValid = false;
            cmp.find('create_username').showHelpMessageIfInvalid();
        }
        
        
        if (cmp.find('create_username').get('v.validity').patternMismatch) {
            allValid = false;
            cmp.find('create_username').showHelpMessageIfInvalid();
        }
        
        allValid = cmp.find('fieldId').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && !inputCmp.get('v.validity').valueMissing;
        }, true);
        
        
        if (allValid) {
            var users = cmp.get("v.simpleBrandRecord.Users_To_Create__c");
            if (users == undefined) {
                users = "";
            }
            if (cmp.get("v.NewUserType") == undefined) {
                cmp.set("v.NewUserType", "Standard");
            }
            /*
            if (users.length > 0) {
                users += ';' + cmp.get("v.NewUserFirstName") + "," + cmp.get("v.NewUserLastName") + "," + cmp.get("v.NewUserEmail") + "," + cmp.get("v.NewUserType");
            } else {
                users = cmp.get("v.NewUserFirstName") + "," + cmp.get("v.NewUserLastName") + "," + cmp.get("v.NewUserEmail") + "," + cmp.get("v.NewUserType");
            }
            */
            var tmpArr = cmp.get("v.SelectedUsersToCreate");
            users = cmp.get("v.NewUserFirstName") + "," + cmp.get("v.NewUserLastName") + "," + cmp.get("v.NewUserEmail") + "," + cmp.get("v.NewUserType");
            tmpArr.push(users);
            cmp.set("v.SelectedUsersToCreate", tmpArr );
            //cmp.set("v.newbrand.Users_To_Create__c", users);
            cmp.set("v.NewUserFirstName", "");
            cmp.set("v.NewUserLastName", "");
            cmp.set("v.NewUserEmail", "");
            cmp.set("v.NewUserType", "Standard");
            if (confirm("Do you want to create add another User?")) {
                var ele = document.getElementsByName("FirstName");
                //ele.focus();
                
            } else {
                
            }
            
        } else {
            if(cmp.get('v.isLightningOut') === false){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Required fields are missing. Please update and save again.",
                    duration:' 2000',
                    mode: 'dismissible'
                    
                });
                toastEvent.fire();
            }
        }
        
        /*
        var users = cmp.get("v.newbrand.Users_To_Create__c");
        if (users == undefined) {
            users = "";
        }
        if (cmp.get("v.NewUserType") == undefined) {
            cmp.set("v.NewUserType", "Standard");
        }
        if (cmp.get("v.NewUserFirstName") == undefined || cmp.get("v.NewUserLastName") == undefined || cmp.get("v.NewUserEmail") == undefined || cmp.get("v.NewUserType") == undefined) {
            alert('The user data needs to be entered');
            return;
        }
        */
        
        
    },
    SaveUsers : function (cmp, event, helper) {
        helper.showSpinner(cmp, true);
        
        //Concatenate Users to Move / Users to Create into strings and assign to the appropriate brand fields.
        var tmpArr = cmp.get("v.SelectedUsersToMove");
        var usrToMove = '';
        for (var i = 0; tmpArr.length > i; i++) {
            usrToMove += tmpArr[i] + ';'
        }
        usrToMove = usrToMove.substring(0, usrToMove.length - 1);
        cmp.set("v.simpleBrandRecord.Users_To_Move__c", usrToMove);
        
        tmpArr = cmp.get("v.SelectedUsersToCreate");
        var usrToCreate = '';
        for (var i = 0; tmpArr.length > i; i++) {
            usrToCreate += tmpArr[i] + ';'
        }
        usrToCreate = usrToCreate.substring(0, usrToCreate.length - 1);
        cmp.set("v.simpleBrandRecord.Users_To_Create__c", usrToCreate);
       
        //Save brand record via Lightning Data Service.
        cmp.find("brandRecordLoader").saveRecord($A.getCallback(function(saveResult) {
            helper.showSpinner(cmp, false);//Hide spinner
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                console.log('brand save result ' + saveResult.state);
                //Fire application event with ID of updated brand.
                var appEvent = $A.get("e.c:OnboardingBrandUpdatedEvent");
                appEvent.setParams({
                    "brandId" : cmp.get('v.selectedBrandId') });
                appEvent.fire();
                //Fire success toast for user
                if(cmp.get('v.isLightningOut') === false){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type" : "success",
                        "message": "User(s) to Move updated."
                    });
                    toastEvent.fire();
                }
                cmp.find("brandRecordLoader").reloadRecord(); 
                const unsavedChangesEvent = cmp.getEvent("unsavedChangesEvent");
                unsavedChangesEvent.setParams({
                    "tabName": "Users to Move",
                    "showUnsavedIndicator": false
                });
                unsavedChangesEvent.fire();               
            } else if (saveResult.state === "INCOMPLETE") {
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                console.log('Problem saving record, error: ' + JSON.stringify(saveResult.error));
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
            }
        }));
        
        //helper.saveBrandRecord(cmp);
    }
    
})