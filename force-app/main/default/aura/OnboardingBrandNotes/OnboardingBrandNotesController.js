({
    handleSubmit : function(component, event, helper) {
        component.set("v.showSpinner", true);
    },
    handleSuccess : function(component, event, helper) {
        component.set("v.showSpinner", false);
    },
    handleError : function(component, event, helper) {
        component.set("v.showSpinner", false);
    }
})