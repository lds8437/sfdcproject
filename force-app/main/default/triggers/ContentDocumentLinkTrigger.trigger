trigger ContentDocumentLinkTrigger on ContentDocumentLink ( before insert, before update, before delete,after insert, after update, after delete, after undelete) {
    
    ContentDocumentLinkTriggerHandler handler = new ContentDocumentLinkTriggerHandler();
    
    if(Trigger_Settings__c.getInstance().Disable_All_Triggers__c){
        return;
    }
    
    if(trigger.isAfter && trigger.isinsert){
		handler.OnAfterInsert(Trigger.new,trigger.newMap);
    }
    
}