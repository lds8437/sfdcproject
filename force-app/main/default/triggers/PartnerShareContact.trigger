trigger PartnerShareContact on Contact (after insert, after update){
    
    if(Trigger.isAfter && Trigger.isInsert){
        PartnerShareContactHelper.afterInsert(trigger.newMap);
    }
    
    if(Trigger.isAfter && Trigger.isUpdate){
        PartnerShareContactHelper.afterUpdate(Trigger.NewMap, Trigger.OldMap);
    }
    
    if(Trigger.isAfter && Trigger.isDelete){
    }    
	

}