trigger ServiceTrigger on Service__c (
    before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete) {


    if(Trigger_Settings__c.getInstance().Disable_All_Triggers__c){
        return;
    }

    
        
        
        if (trigger.isBefore && trigger.isInsert) {
            cdr_ServiceRecords.updateService(Trigger.new);
            cdr_ServiceRecords.updateImp(Trigger.new);            
        }
        
        if (trigger.isAfter && trigger.isUpdate) {
       
            cdr_ServiceRecords.updateImp(Trigger.new);            
        }
    }