trigger ContractDocTrigger on Contract_Document__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    ContractDocumentTriggerHandler handler = new ContractDocumentTriggerHandler();

    if (Trigger.isBefore) {
        if(Trigger.isInsert){
            //handler.OnBeforeInsert(Trigger.new);
        }
        else if(Trigger.isUpdate){
           // handler.OnBeforeUpdate(Trigger.old,Trigger.new,trigger.newMap, Trigger.oldMap);
        }
        else if(Trigger.isDelete){
           // handler.OnBeforeDelete(trigger.old,Trigger.newMap, Trigger.oldMap);
        }    
    } 
    else if (Trigger.isAfter) {
        if(Trigger.isInsert){
           // handler.OnAfterInsert(Trigger.new,trigger.newMap);
           }
           else if(Trigger.isUpdate){
           // handler.OnAfterUpdate(Trigger.old,Trigger.new,trigger.newMap, Trigger.oldMap);
        }
        else if(Trigger.isDelete){
           // handler.OnAfterDelete(trigger.old, Trigger.oldMap);
        }    
    }
}
//(
//    before insert,
//    before update,
//    before delete,
//    after insert,
//    after update,
//    after delete,
//    after undelete) {
        
//        if (trigger.isBefore && trigger.isUpdate) {           
            
//         //    cdr_ContractDocumentUpdates.updateImp(Trigger.new, Trigger.oldMap);
//        }
        
//        if (trigger.isAfter && trigger.isUpdate) {
//            cdrl_ContractTemplates.createImpLines(Trigger.new, Trigger.oldMap);
//            cdrl_ContractTemplates.createQSOLines(Trigger.new, Trigger.oldMap);  
//            cdrl_ContractTemplates.createQuoteLines(Trigger.new, Trigger.oldMap);
//            cdrl_ContractTemplates.createQSOLangLines(Trigger.new, Trigger.oldMap);
//            cdr_ContractDocumentUpdates.updateAccount(Trigger.new, Trigger.oldMap);
//            cdr_ContractDocumentUpdates.updateClient(Trigger.new, Trigger.oldMap);
//            cdr_ContractDocumentUpdates.updateMSA(Trigger.new, Trigger.oldMap);
//            cdr_ContractDocumentUpdates.updatePriCon(Trigger.new, Trigger.oldMap);
                
//        }
        
//        if (trigger.isAfter && trigger.isInsert) {           
//            cdrl_ContractTemplates.createQuoteLines(Trigger.new, Trigger.oldMap);
//        }
        
//    }