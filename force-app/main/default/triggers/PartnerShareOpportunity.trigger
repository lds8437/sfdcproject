trigger PartnerShareOpportunity on Opportunity (
    before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete) {
        

        if (Trigger.isBefore) {
            //call your handler.before method
        } else if (Trigger.isAfter) {
            //call handler.after method
            if(Trigger.isUpdate){
                PartnerShareOpportunityHelper.afterUpdate(Trigger.NewMap, Trigger.OldMap);
            }
            if(Trigger.isInsert){
                PartnerShareOpportunityHelper.afterInsert(trigger.newMap);
            }
        }
}