trigger Account_AddUser on Account (before insert, before update) {

	if(Trigger_Settings__c.getInstance().Disable_All_Triggers__c){
		return;
	}


	if(!Account_AddUserHandler.hasAlreadyfired()){
		if(Trigger.isBefore && Trigger.isInsert){
			Account_AddUserHandler objHandler = new Account_AddUserHandler();
			objHandler.onInsertAccount(trigger.new);
			Account_AddUserHandler.setAlreadyfired();
		}
	}
	
	if(!Account_UpdateTeamMemberHandler.hasAlreadyfired()){
		if(Trigger.isBefore && Trigger.isUpdate){
			Account_UpdateTeamMemberHandler objHandler = new Account_UpdateTeamMemberHandler();
			objHandler.updateTeamMember(trigger.newMap, trigger.oldMap); 
			Account_UpdateTeamMemberHandler.setAlreadyfired();
		}
	}
}