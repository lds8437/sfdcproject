/*
Revision History:
Version     Version Author    Date              Comments
2.0         Prajakta Sanap   21/05/2014         1. Trigger
2.1         Fredrick Bairn   11 NOV 2015        Added Random handler
*/

trigger OpportunityTrigger on Opportunity (
    before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete) {


    if(Trigger_Settings__c.getInstance().Disable_All_Triggers__c){
        return;
    }

    
        
        //OpportunityOwnerUpdateHandler objOpportunityOwnerUpdateHandler = new OpportunityOwnerUpdateHandler();
        
        //if (trigger.isBefore && trigger.isUpdate) {
        //    //objOpportunityOwnerUpdateHandler.updateOwner(trigger.new, trigger.oldMap);
        //    //    wls_CreateRandomNumber.createRandom(Trigger.new, trigger.oldMap);
            
        //}
        
        //if (trigger.isBefore && trigger.isInsert) {
        //    // system.debug('----------assignTeamMember-----Insert--------');
        //    //objOpportunityOwnerUpdateHandler.updateOwner(trigger.new, trigger.oldMap);
        //    //wls_CreateRandomNumber.createRandom(Trigger.new/*, trigger.oldMap*/);
        //}
        //if(trigger.isAfter && trigger.isInsert){
        //    //cdr_CreateContractRecord.createCdr(Trigger.new);
            
        //}
        
        //if (trigger.isAfter && trigger.isUpdate){
        //    //wls_CreateSurveyRecord.createSurvey(Trigger.new, trigger.oldMap);
        //    //PartnerSurveyClass.getOpportunity(Trigger.new);
        //    //cdr_CreateContractRecord.updateCdr(Trigger.new);
        //    //if(checkRecursive.runOnce()){
        //    //     SendemailController.sendInvoice(trigger.new, trigger.oldMap);
        //    //}
           
        //    //      PartnerSurveyClass.getPartnerOpportunity(Trigger.new);
            
        //}   
        
        
    }