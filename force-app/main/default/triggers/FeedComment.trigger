trigger FeedComment on FeedComment (before insert) {
    
    List<Case> casesToUpdate = new List<Case>();
    
    for(FeedComment comment : Trigger.new) {
        
        if(String.valueOf(comment.ParentId).startsWith('500')) {
			Case caseToUpdate = [SELECT Id, Status, OwnerId FROM Case WHERE Id = :comment.ParentId];
            //system.debug(caseToUpdate);
            if(caseToUpdate.Status != 'Closed' && comment.InsertedById != caseToUpdate.OwnerId) {
                caseToUpdate.Status = 'In Progress';
                casesToUpdate.add(caseToUpdate);
            }
        }
        
        
    }
    
    if(casesToUpdate.size() > 0) {
        update casesToUpdate;
    }

}