trigger PartnerShareCase on Case (
	before insert,
	before update,
	before delete,
	after insert,
	after update,
	after delete,
	after undelete) {

		if (Trigger.isBefore) {
	    	//call your handler.before method
		} else if (Trigger.isAfter) {
	    	//call handler.after method
			if(Trigger.isUpdate){
				PartnerShareCaseHelper.afterUpdate(Trigger.NewMap, Trigger.OldMap);
			}
			if(Trigger.isInsert){
				PartnerShareCaseHelper.afterInsert(trigger.newMap);
			}
		}
}