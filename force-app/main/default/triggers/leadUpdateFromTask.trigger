trigger leadUpdateFromTask on Task (after insert, after update){


    if(Trigger_Settings__c.getInstance().Disable_All_Triggers__c){
        return;
    }

    

    set<id> leadIDset = new set<id>();
    lead[] lList = new list<lead>();
    set<id> leadIDset2 = new set<id>();
    lead[] lList2 = new list<lead>();
    set<id> leadIDset3 = new set<id>();
    lead[] lList3 = new list<lead>();

    for (task t : trigger.new) {
        if (t.whoid != null) {
            if (t.whoid.getSObjectType() == lead.SObjectType && t.Status == 'Completed') {
                leadIDset.add(t.whoid);
                if (t.Classification__c != null) {
                    if (t.Classification__c == 'Contact - Progressed'){
                        leadIDset2.add(t.whoid);
                    }
                }
                if (t.Subject != null) {
                    if (t.Subject.containsIgnoreCase('Advancing Call') && !t.Subject.containsIgnoreCase('non')) {
                        leadIDset2.add(t.whoid);
                    }
                }
            }
        }
    }
    lList = [select CreatedDate, First_Activity_Date__c from lead where id in :leadIDset AND First_Activity_Date__c = NULL];
    for (lead l : lList) {
        if (l.First_Activity_Date__c == null) {
            l.First_Activity_Date__c = datetime.now();
            l.Hours_to_First_Activity2__c = WorkingHoursCalc.workingHoursCalc(l.createdDate, l.First_Activity_Date__c);
        }
    }
    lList2 = [select CreatedDate, First_Advancing_Call__c from lead where id in :leadIDset2 AND First_Advancing_Call__c = NULL];
    for (lead l : lList2) {
        if (l.First_Advancing_Call__c == null) {
            l.First_Advancing_Call__c = datetime.now();
            l.Hours_to_First_Advancing_Activity__c = WorkingHoursCalc.workingHoursCalc(l.createdDate, l.First_Advancing_Call__c);
        }
    }
    lList3 = [select First_Activity_after_MQL__c , MQL_Time_Stamp_hrs__c, Hours_to_First_Activity_after_MQL__c from lead where id in :leadIDset AND MQL__c = TRUE AND MQL_Time_Stamp_hrs__c != NULL];
    for(lead l : lList3){
        if(l.First_Activity_after_MQL__c == null) {
               l.First_Activity_after_MQL__c = DateTime.now();
            l.Hours_to_First_Activity_after_MQL__c = WorkingHoursCalc.workingHoursCalc(l.MQL_Time_Stamp_hrs__c, DateTime.now());
        }
    }
    
    
    update lList;
    update lList2;
    update lList3;
}