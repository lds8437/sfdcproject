trigger q2c_Employee_Trigger on Employee__c (before insert) {


    if(Trigger_Settings__c.getInstance().Disable_All_Triggers__c){
        return;
    }

    
        q2c_UpdateEmployeeSFDCId_Class.updateSFDCId(Trigger.new);
}