trigger Task_Set1stActivityNClosedTaskCount on Task (after insert, after update) {


	if(Trigger_Settings__c.getInstance().Disable_All_Triggers__c){
		return;
	}

	
    if((trigger.isAfter && trigger.isInsert) || (trigger.isAfter && trigger.isUpdate)){
         Task_Set1stActivityNClosedTaskCountCntlr objTask_Set1stActivityNClosedTaskCountCntlr = new Task_Set1stActivityNClosedTaskCountCntlr();
         objTask_Set1stActivityNClosedTaskCountCntlr.updateLead_FirstActivityAndClosedTaskCount (trigger.new);
    }
}