trigger ClientTrigger on Client__c (
    before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete) 
{
    if (trigger.isAfter && trigger.isUpdate) {
       // cdr_OnboardingUpdates.updateClient(Trigger.new, Trigger.oldMap);
    }
}