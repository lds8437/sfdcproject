trigger PanelsLoadUpdate on Opportunity (after update) {


	if(Trigger_Settings__c.getInstance().Disable_All_Triggers__c){
		return;
	}

	
	RecordType[] panelsList = [Select Id From RecordType WHERE Name='Panels' AND SobjectType='Opportunity'];
	Id panelsId = panelsList.get(0).Id;
	
	for(Opportunity o : trigger.new) {
		
		// Check if the opportunity was a panel.
		if (o.RecordTypeID == panelsId){
			
			// Get the project manager from before and after the update. (They may be the same.)
			Id oldProjectManager = trigger.oldmap.get(o.id).Project_Manager__c;
			Id newProjectManager = o.Project_Manager__c;
			
			// Get the old and new panel status, in case the project manager didn't change.
			String oldStatus = trigger.oldmap.get(o.id).Panel_Status__c;
			String newStatus = o.Panel_Status__c;
			
			if(oldProjectManager != newProjectManager){
				
				// Make sure that there was a project manager before the update.
				if(oldProjectManager != null){
					
					// Find all weight-carrying opportunities owned by the old project manager after the update.
					Opportunity[] o1 = [Select Panel_Status__c from Opportunity where Project_Manager__c = :oldProjectManager and Panel_Status__c IN ('Pre-launch','Live','Re-opened')];
					Integer load1 = 0;
					for(Opportunity opp1 : o1) {
						if (opp1.Panel_Status__c == 'Pre-launch') load1 += 5;
						else if (opp1.Panel_Status__c == 'Live') load1 += 1;
						else if (opp1.Panel_Status__c == 'Re-opened') load1 += 1;
					}
					
					// Find the user object that matches the old project manager's ID and update the load on their profile.
					user u1 = [select id from user where id = :oldProjectManager];
					u1.Project_Manager_Load__c = load1;
					update u1;
				}
				
				// Make sure that the change wasn't to delete the project manager.
				if(newProjectManager != null){
					
					// Find all weight-carrying opportunities owned by the new project manager after the update.
					Opportunity[] o2 = [Select Panel_Status__c from Opportunity where Project_Manager__c = :newProjectManager and Panel_Status__c IN ('Pre-launch','Live','Re-opened')];
					Integer load2 = 0;
					for(Opportunity opp1 : o2) {
						if (opp1.Panel_Status__c == 'Pre-launch') load2 += 5;
						else if (opp1.Panel_Status__c == 'Live') load2 += 1;
						else if (opp1.Panel_Status__c == 'Re-opened') load2 += 1;
					}
					
					// Find the user object that matches the new project manager's ID and update the load on their profile.
					user u1 = [select id from user where id = :newProjectManager];
					u1.Project_Manager_Load__c = load2;
					update u1;
				}
			}
			
			else if(newProjectManager != null && oldStatus != newStatus){
				// Run the same update as above for the current panel project manager.
				List<Opportunity> o2 = [Select Panel_Status__c from Opportunity where Project_Manager__c = :newProjectManager and Panel_Status__c IN ('Pre-launch','Live','Re-opened')];
				Integer load2 = 0;
				for(Opportunity opp1 : o2) {
					if (opp1.Panel_Status__c == 'Pre-launch') load2 += 5;
					else if (opp1.Panel_Status__c == 'Live') load2 += 1;
					else if (opp1.Panel_Status__c == 'Re-opened') load2 += 1;
				}
				user u1 = [select id from user where id = :newProjectManager];
				u1.Project_Manager_Load__c = load2;
				update u1;
			}
		}
	}
}