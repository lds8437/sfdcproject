trigger SOW_Attached on Attachment (before insert) {


    if(Trigger_Settings__c.getInstance().Disable_All_Triggers__c){
        return;
    }

    

 
 List<Service__c> ServiceList = new List<Service__c>();
    Set<String> ServIds = new Set<String>();
    for(Attachment att : trigger.New){
         //Check if added attachment is related to Service or not
         if(att.ParentId.getSobjectType() == Service__c.SobjectType){
              ServIds.add(att.ParentId);
         }
    }
    ServiceList = [select id, SOW_Attached__c  from Service__c where id in : ServIds];
    if(ServiceList!=null && ServiceList.size()>0){
        for(Service__c svc : ServiceList){
            svc.SOW_Attached__c = true;
        }
        update ServiceList;
    }

}