trigger AccountStatusUpdate on Task (after insert) {


	if(Trigger_Settings__c.getInstance().Disable_All_Triggers__c){
		return;
	}

	
	set<id> acctID = new set<id>();
	for (task t : trigger.new) {
		acctID.add(t.AccountId);
	}
	account[] alist = [select Account_Status__c from account where id in :acctID];
	for (account a : alist) {
		if (a.Account_Status__c != null) {
			if (a.Account_Status__c == 'Not Contacted') {
				a.Account_Status__c = 'No Open Opportunity';
			}
		}
	}
	update alist;
}