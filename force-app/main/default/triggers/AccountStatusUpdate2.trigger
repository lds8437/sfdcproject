trigger AccountStatusUpdate2 on Event (after insert) {


	if(Trigger_Settings__c.getInstance().Disable_All_Triggers__c){
		return;
	}

	
	set<id> acctID = new set<id>();
	for (event e : trigger.new) {
		acctID.add(e.AccountId);
	}
	account[] alist = [select Account_Status__c from account where id in :acctID];
	for (account a : alist) {
		if (a.Account_Status__c != null) {
			if (a.Account_Status__c == 'Not Contacted') {
				a.Account_Status__c = 'No Open Opportunity';
			}
		}
	}
	update alist;
}