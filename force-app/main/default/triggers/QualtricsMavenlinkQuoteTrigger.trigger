trigger QualtricsMavenlinkQuoteTrigger on Quote (after update) {
    try{
		//only perform this logic if it's a single update from false to true
		if (Trigger.old.size()==1&&Trigger.old[0].isSyncing==false&&Trigger.new[0].isSyncing==true){
			DateTime currentDateTime = DateTime.now();
			DateTime futureDateTime = currentDateTime.addSeconds(QualtricsMavenlinkSchedulable.OPPORTUNITY_UPDATE_DELAY);
			String cron = futureDateTime.second()+' '+futureDateTime.minute()+' '+futureDateTime.hour()+' '+futureDateTime.day()+' '+futureDateTime.month()+' ? '+futureDateTime.year();

			System.schedule('QualtricsMavenlinkSchedulable '+Trigger.new[0].OpportunityId+' '+DateTime.now().format('yyyy.MM.dd.HH.mm.ss.SSS'),
				cron,new QualtricsMavenlinkSchedulable(Trigger.new[0].OpportunityId));
		}
	} catch(Exception e){system.debug('Error in QualtricsMavenlinkQuoteTrigger ('+String.valueOf(Trigger.new[0].OpportunityId)+') = ('+e.getStackTraceString()+') '+e.getMessage());}
    
}