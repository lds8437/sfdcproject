trigger CaseTrigger on Case (after insert, after update) {

    // Define shared variables
    List<Id> caseIds = New List<Id>(Trigger.newMap.keySet());
    system.debug('caseIds');
    system.debug(caseIds);
    system.debug(Trigger.newMap);
    // Execute assignment rules.
    CaseAccess.executeAssignmentRules(caseIds);
}