public class cdr_OnboardingUpdates {
     public static void updateClient(List<Client__c> newList, map<Id,Client__c> OldMap){
        
        List<Id> lstCd = new List<Id>();
        
        for(Client__c obj:newList){
            if(OldMap != Null &&(obj.Shipping_Street__c  != OldMap.get(obj.Id).Shipping_Street__c  || obj.Shipping_City__c != OldMap.get(obj.Id).Shipping_City__c || obj.Shipping_State__c != OldMap.get(obj.Id).Shipping_State__c || obj.ShippingCountry2__c != OldMap.get(obj.Id).ShippingCountry2__c || obj.Shipping_Zip_Postal_Code__c != OldMap.get(obj.Id).Shipping_Zip_Postal_Code__c))
            {               
                lstCd.add(obj.Id);
                system.debug('lstCd: ' + lstCd);
            }
            else if (oldMap == NULL){
                lstCd.add(obj.Id);
            }
        }
        
        if(lstCd!=Null && lstCd.size()>0){
            
            //updates
           List<Contract_Document__c> cd = [SELECT ID, Synced_Quote__c FROM Contract_Document__c WHERE Client__c in: lstCd];
            
             Set<Id> qId = new Set<Id>();
            
            for(Contract_Document__c c:cd){
                qId.add(c.Synced_Quote__c);
            }
            
            Map<Id,Quote> q = new Map<Id,Quote>([SELECT Id, AVA_SFQUOTES__Tax_Now_Status__c, AVA_SFQUOTES__Tax_Date__c, AVA_SFQUOTES__Shipping_Last_Validated__c, Tax, RecordType.DeveloperName FROM Quote WHERE id in:qId]);
            Set<id> qids = q.keySet();
            
            if(!q.isEmpty()){
                List<Quote> qup = new List<Quote>();
            
            for(Id qs:qids){                
                    if(q.get(qs).RecordType.DeveloperName!='Closed Quote'){
                        Quote qnew = new Quote();
                        qnew.Id = q.get(qs).Id;
                        qnew.AVA_SFQUOTES__Shipping_Last_Validated__c = null;
                        qnew.Tax = null;
                        qnew.AVA_SFQUOTES__Tax_Now_Status__c = 'Sales Tax Not Current';
                        qnew.AVA_SFQUOTES__Tax_Date__c = null;
                        qup.add(qnew);
                    }               
            }
            update qup;
            }
            
        }       
    }
}