@isTest
public with sharing class PartnerShareContactTests {
	@isTest
    static void contactTest(){
        
        Account testAccount1 = TestFactory.createAccount();
        insert testAccount1;
        
        testAccount1.isPartner = true;
        update testAccount1;
        
        Account testAccount2 = TestFactory.createAccount();
        insert testAccount2;
        
        testAccount2.isPartner = true;
        update testAccount2;
        
        Q_Partner__c testPartner2 = TestFactory.createPartner(testAccount2.Id);
		insert testPartner2;
        
        Q_Partner__c testPartner = TestFactory.createPartner(testAccount1.Id);
        //testPartner.RelatedAccount__c = testPartner2.Id;
        insert testPartner;
        
        Lead testLead1 = new Lead(LastName='TestLead1Name', Company='TestLead1Company', Status = 'Prospect', Partner__c= testPartner.Id, LeadSource='Outbound - Sales');
		insert testLead1;
        
        Lead testLead2 = new Lead(LastName='TestLead2Name', Company='TestLead2Company', Status = 'Prospect', Partner__c= testPartner.Id, LeadSource='Outbound - Sales');
		insert testLead2;
        
        Contact testContact = new Contact(LastName = 'Test', AccountId = testAccount1.Id);
		insert testContact;
        
        
        
        
        
        //additional test code
        Id sysAdminpId = [select id from profile where name='System Administrator'].id;

		User tempUser = [SELECT Id FROM User WHERE ProfileId =: sysAdminpId AND IsActive = TRUE LIMIT 1];

		Id p = [select id from profile where name='Partner Community User'].id;
	
        String orgId = UserInfo.getOrganizationId();
	    String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    	Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
	    String uniqueName = orgId + dateString + randomInt;
        
		System.runAs(tempUser){
			User u1 = new User( email = uniqueName + '@test' + orgId + '.org',
	                profileid = p,
	                Username = uniqueName + '@test' + orgId + '.org',
	                Alias = 'GDS',
	                TimeZoneSidKey='America/New_York',
	                EmailEncodingKey='ISO-8859-1',
	                LocaleSidKey='en_US',
	                LanguageLocaleKey='en_US',
	                ContactId = testContact.Id,
	                PortalRole = 'Manager',
	                FirstName = 'Test',
	                LastName = 'User');
			insert u1;
		}
        
        //Contact testContact = new Contact(LastName = 'Test', AccountId = testAccount1.Id);
        testContact.LastName = 'Testing123';
        
        Test.startTest();
        update testContact;
        Test.stopTest();
    }
}