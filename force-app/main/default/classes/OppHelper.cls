public with sharing class OppHelper {
    public Opportunity opp;
    public OppHelper( ApexPages.StandardController stdController ) {
        opp = ( Opportunity )stdController.getRecord();        
    }
    public void rollupOppContacts(){
        OppHelper.rollupOppContactsFuture( opp.Id );    
    }
   public static void rollupOppContactsFuture( Id oppId ) {
        Contact[] contactList = [ SELECT Id, Converted_to_Contact__c FROM Contact
                             WHERE Id IN ( SELECT ContactId FROM OpportunityContactRole
                                         WHERE OpportunityId = :oppId ) ];
        Opportunity opp = [ SELECT Id, Qualification_Contact__c FROM Opportunity WHERE Id = :oppId And Qualification_Contact__c = null];
        //opp.Qualification_Contact__c = 0;
        
        for( Contact contact : contactList ) {
            opp.Qualification_Contact__c = contact.Id;
        }
        update opp;
    }
}