public with sharing class RedirectionManager {
    
    /**
     * In simplest terms, the redirection manager
     * creates a request and returns a response.
     */
    private RedirectionRequest request;
    private RedirectionResponse response;
    
    /**
     * Values provided, which are used to create the
     * request, are not visible to other classes (private).
     */
    private String sObjectName;
    private String id;
    private String action;
    private Boolean forcedDebugMode = false;
    
    /**
     * Class Constructor
     * 
     * Accepts all possible parameters, ensures all variables are
     * populated based on the values provided.
     */
    public RedirectionManager(String sObjectName, String id, String action, Boolean forcedDebugMode) {
        this.sObjectName = sObjectName;
        this.id = id;
        this.action = action;
        this.forcedDebugMode = forcedDebugMode;
        execute();
    }

    /**
     * Overloaded Constructor
     * 
     * This constructor is only provided with a subset of the
     * parameters needed to populate the primary constructor.
     * The additional property (forceDebugMode) is hardcoded
     * to false when this constructor is called.
     */
    public RedirectionManager(String sObjectName, String id, String action) {
        this(sObjectName, id, action, false);
    }
    
    /**
     * Overloaded Constructor
     * 
     * This constructor does not accept any parameters.  This is
     * the constructor called when this executes as a controller for
     * a Visualforce page.  All parameters are pulled from the
     * querystring.
     */
    public RedirectionManager() {
        /* WHY NOT USE THIS?
        this(ApexPages.currentPage().getParameters().get(RedirectionConstants.QUERYSTRING_PARAMETER_SOBJECT),
            	ApexPages.currentPage().getParameters().get(RedirectionConstants.QUERYSTRING_PARAMETER_ID),
            	ApexPages.currentPage().getParameters().get(RedirectionConstants.QUERYSTRING_PARAMETER_ACTION));
		*/
        this.sObjectName = ApexPages.currentPage().getParameters().get(RedirectionConstants.QUERYSTRING_PARAMETER_SOBJECT);
        this.id = ApexPages.currentPage().getParameters().get(RedirectionConstants.QUERYSTRING_PARAMETER_ID);
        this.action = ApexPages.currentPage().getParameters().get(RedirectionConstants.QUERYSTRING_PARAMETER_ACTION);
        execute();
    }
    
    /**
     * Execute (Main Method)
     * 
     * The execute method facilitates all of the operations
     * for this class.  Based on the state of the request
     * the proper next step is executed.
     */
    private void execute() {
        request = new RedirectionRequest(this);
        if (request.isDebugMode()) {
			executeDebugMode();
        } else if (request.isValid()) {
			executeIsValid();
        } else {
			executeIsInvalid();
        }
    }
    
    /**
     * Execute Debug Mode
     * 
     * In debug mode we perform all possible operations:
     * - Obtain the redirection URL
     * - Collect all error messages
     * - Collect all debug logs
     * Populate the response with all values.
     */
    private void executeDebugMode() {
        String redirectionUrl = request.getRedirectionUrl();
        Boolean redirectEntireWindow = request.getRedirectionSettings().getRedirectEntireWindow();
        List<String> errorMessages = request.getErrorMessages();
        RedirectionException redirectionException = new RedirectionException(errorMessages);
        List<String> logs = request.getLogs();
        response = new RedirectionResponse(redirectionUrl, redirectEntireWindow, redirectionException, logs);        
    }
    
    /**
     * Execute Is Valid
     * 
     * When the request is valid we only care about
     * the redirection URL.  Populate the response
     * with only the redirection URL.
     */
    private void executeIsValid() {
        String redirectionUrl = request.getRedirectionUrl();
        Boolean redirectEntireWindow = request.getRedirectionSettings().getRedirectEntireWindow();
        response = new RedirectionResponse(redirectionUrl, redirectEntireWindow);
    }
    
    /**
     * Execute Is Invalid
     * 
     * When the request is not valid we only care about
     * the error messages.  Populate the response
     * with only the list of error messages.
     */
    private void executeIsInvalid() {
        List<String> errorMessages = request.getErrorMessages();
        RedirectionException redirectionException = new RedirectionException(errorMessages);
        response = new RedirectionResponse(redirectionException);
    }
    
    /**
     * Standard Getters
     */
    public RedirectionRequest getRequest() {
        return request;
    }
    
    public RedirectionResponse getResponse() {
        return response;
    }
    
    public String getSObjectName() {
        return sObjectName;
    }
    
    public String getRecordId() {
        return id;
    }
    
    public String getAction() {
        return action;
    }
    
    public Boolean getForcedDebugMode() {
        return forcedDebugMode;
    }
    
    /**
     * Get Redirection Mode
     * 
     * Redirection mode is determined by the state of the
     * response.
     * - If the request is in debug mode or errors exist
	 *   no redirection should be performed.
	 * - If the request is valid and the settings state
	 *   the redirection should include the entire window
	 *   a JavaScript redirection is required.
	 * - If the request is valid and the settings state
	 *   the redirection should NOT include the entire window
	 *   a Page Reference redirection is required.
	 */
    public String getRedirectionMode() {
        return response.getRedirectionMode();
    }
    
    /**
     * Perform Redirection
     * 
     * This is called when the Visualforce page loads.
     * If the redirection mode is anything other than
     * page reference this method returns null and does
     * not redirect the user.
     */
    public PageReference performRedirection() {
        if (getRedirectionMode() == RedirectionConstants.REDIRECTION_MODE_PAGEREFERENCE) {
            PageReference pageReference = new PageReference(response.getRedirectionUrl());
            pageReference.setRedirect(true);
            return pageReference;
        }
        return null;
    }
}