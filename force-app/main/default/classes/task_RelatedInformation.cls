public class task_RelatedInformation {

     @AuraEnabled
    public static sObject getContact(Id whatId){
        system.debug('whatId'+whatId);
        sObject con;
        Task tsk = [SELECT Id, Who.Id, Who.Type FROM Task WHERE Id =:whatId ];
        
        if(tsk.Who.Type == 'Contact'){
            con = [SELECT Name, Phone, Email, Direct_Line__c  FROM Contact WHERE Id =:tsk.Who.Id];
        }
        else if(tsk.Who.Type == 'Lead'){
            con = [SELECT Name, Phone, Email FROM Lead WHERE Id =:tsk.Who.Id];
        }
        return con;
    }
}