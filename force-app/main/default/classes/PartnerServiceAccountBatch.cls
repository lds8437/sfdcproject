// PartnerServiceAccountBatch
// 
// One-time batch to initially populate the Partner_Account__c field on Partner Service records. - Tommy Noe (Bayforce)
global class PartnerServiceAccountBatch implements Database.Batchable<sObject> {
    global Database.queryLocator start(Database.BatchableContext BC) {
        if (Test.isRunningTest()) {
            return Database.getQueryLocator('SELECT Id, Partner_Account__c, Q_Partner__r.RelatedAccount__c FROM Partner_Service__c');
        }
        return Database.getQueryLocator('SELECT Id, Partner_Account__c, Q_Partner__r.RelatedAccount__c FROM Partner_Service__c WHERE Partner_Account__c = null AND Q_Partner__r.RelatedAccount__c != null');
    }
    
    global void execute(Database.BatchableContext BC, List<Partner_Service__c> scope) {
        for (Partner_Service__c ps : scope) {
            ps.Partner_Account__c = ps.Q_Partner__r.RelatedAccount__c;
        }
        update scope;
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
}