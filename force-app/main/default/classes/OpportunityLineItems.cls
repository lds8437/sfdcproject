public with sharing class OpportunityLineItems {

	public List<OpportunitylineItem> oliList {get;set;}

	public OpportunityLineItems(ApexPages.StandardController controller) {
		queryOli();
	}

	@testVisible
	private void queryOli(){
		String qry = 'SELECT ';
		for(Schema.FieldSetMember memberField : SObjectType.OpportunityLineItem.FieldSets.RelatedList.getFields()){
			qry += memberField.getFieldPath() + ',';
		}
		qry = qry.removeEnd(',');
		Id oppId = ApexPages.CurrentPage().getParameters().get('id');
		qry += ' FROM OpportunityLineItem WHERE OpportunityId = :oppId';
		system.debug(logginglevel.error, '***** qry='+qry);
		oliList = Database.Query(qry);
	}


}