public with sharing class ClientLicenses {

    @AuraEnabled
    public static List<Client_License__c> getClientLicenses(ID clientid) {
        //This method will return a list of all Client Licenses associated with a client.
        
        //Query and return the licenses
        List<Client_License__c> licenses = new List<Client_License__c>([SELECT Id,
                                                                        Name, 
                                                                        Status__c,
                                                                        Active__c,
                                                                        License_Start_Date__c, 
                                                                        License_End_Date__c,
                                                                        Quantitiy__c, 
                                                                        CurrencyIsoCode,
                                                                        Culture_Code__c,
                                                                        Invoice_Amount__c,
                                                                        Original_Amount__c, 
                                                                        Proration_Amount__c,
                                                                        Price__c,
                                                                        Bundle__c, 
                                                                        Bundle__r.Name, 
                                                                        Bundle__r.Experience__c,
                                                                        Bundle__r.Bundle__c,
                                                                        Bundle__r.Upgrade_Bundles__c,
                                                                        Bundle__r.A_la_Carte_Bundle__c,
                                                                        Bundle__r.Sort_Order__c,
                                                                        Bundle_Product__c,
                                                                        Bundle_Product__r.Availability__c, 
                                                                        Bundle_Product__r.Upgrade_Availability__c, 
                                                                        Bundle_Product__r.PricingType__c, 
                                                                        Bundle_Product__r.Price_Based_On__c,
                                                                        Bundle_Product__r.User_Input_Required__c, 
                                                                        Bundle_Product__r.Bundle_Key__c, 
                                                                        Bundle_Product__r.Display_Variable__c, 
                                                                        Bundle_Product__r.Display_Label__c, 
                                                                        Bundle_Product__r.Name, 
                                                                        Product__r.Name,
                                                                        Product__r.ProductCode, 
                                                                        Product__r.Revenue_Roll_up__c,
                                                                        Product__r.Revenue_Recognition_Rule__c,
                                                                        Product__r.Revenue_Type__c,
                                                                        Product__r.Upgrade_Comparison__c,
                                                                        Product__r.Eligible_for_Proration__c,
                                                                        Product__r.NS_Item_ID__c,
                                                                        Product__r.Merge_Comparison__c,
                                                                        Core_Product__c, 
                                                                        Client__c, 
                                                                        Client__r.Name,
                                                                        Client__r.CurrencyIsoCode,
                                                                        Tier__c
                                                                        FROM Client_License__c
                                                                        WHERE Client__c = :clientid
                                         								AND (Status__c = 'Active' OR Status__c = 'Expired')
                                         								AND Bundle__c != null
                                                                        AND Client__c != null]);
        system.debug('license count ' + licenses.size());
        return licenses;
    }

    @auraEnabled
    public static List<String> getLicenseGroupings(ID clientid){
        //This method will take a given Account ID and return a list of Strings representing Client License Groupings by Client, Bundle, and End Date.
        //All Client Licenses at the Account will be queried.
        //There will be one string for each grouping of Client Licenses for every Client, Bundle, and End Date combination that has data unless the data contains only Expired Status licenses and there is an Active set for that Client/Bundle combination as well.
        //Each string will have the format ClientId|BundleId|LicenseEndDate|Status|ClientName|BundleName.
        
        //First query summary of licenses grouped by client and bundle.
        List<AggregateResult> results = [SELECT Client__c, Bundle__c, License_End_Date__c, GROUPING(Client__c) grpClient, GROUPING(Bundle__c) grpBundle, GROUPING(License_End_Date__c) grpEnd, COUNT(id) num, MAX(Status__c) maxStatus, MAX(Client__r.Name) maxClientName, MAX(Bundle__r.Name) maxBundleName
                                         FROM Client_License__c
                                         WHERE Client__c = :clientid
                                         AND (Status__c = 'Active' OR Status__c = 'Expired')
                                         AND Bundle__c != null
                                         GROUP BY CUBE(Client__c, Bundle__c, License_End_Date__c)
                                         ORDER BY GROUPING(License_End_Date__c)];
        system.debug(results);
        List<String> groupings = new List<String>();//Set up the list of strings to be added to and eventually returned.
        Set<String> combinations = new Set<String>();//Set up a set for client/bundle combinations to check against for expired license sets.
        for(AggregateResult r : results){//Loop through query results
            if(r.get('grpClient') == 0 && r.get('grpBundle') == 0 && r.get('grpEnd') == 0){//Skip subtotals and grand total
                String enddate = String.valueOf(Date.valueOf(r.get('License_End_Date__c')));
                
                String s = r.get('Client__c') + '|' + r.get('Bundle__c') + '|' + enddate + '|' + r.get('maxStatus') + '|' + r.get('maxClientName') + '|' + r.get('maxBundleName');//Make the ClientId|BundleId|LicenseEndDate|Status|ClientName|BundleName string.
                String cb = r.get('Client__c') + '|' + r.get('Bundle__c');
                if(r.get('maxStatus') == 'Expired'){//If grouping contains only expired licenses.
                    if(!combinations.contains(cb)){//If the set of client/bundle combinations for active licenses does not contain this client/bundle combo.
                        groupings.add(s);//Add to the list of groupings.
                    }
                }
                else{//Grouping contains at least one active license.
                    groupings.add(s);//Add to the list of groupings.
                    combinations.add(cb);//Add to the list of client/bundle combinations.
                }
            }
        }
        //system.debug(groupings);
        return groupings;
    }

    @auraEnabled
    public static List<Boolean> getAccess() {
        List<Boolean> access = new List<Boolean>();
        access.add(Schema.SObjectType.Client_License__c.isAccessible());
        access.add(Schema.SObjectType.Client_License__c.isCreateable());
        access.add(Schema.SObjectType.Client_License__c.isUpdateable());
        access.add(Schema.SObjectType.Client_License__c.isDeletable());
        return access;
    }

    @auraEnabled
    public static void deleteRecord(ID recordId) {
        Client_License__c record = [SELECT Id FROM Client_License__c WHERE Id =: recordId];
        delete record;
    }
}