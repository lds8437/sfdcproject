/*-------------------------------------------------------------------------------------------------
Description: Test class for DealCoordination apex controller of the page DealCoordination
    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 03-10-2017       | 1. Added test methods to test the functionality
   =============================================================================================================================
*/

@isTest
private class DealCoordinationTest {
	static Account acct {get;set;}
    static Opportunity opp {get;set;}
	static Quote quoteObj;

	/*
     @ Description : The test setup runs when every test method starts.
	     			 Any objects you need to create that will be used by more than one method should be created here.
	*/
    @testsetup
    static void test_setup() {
    	TestFactory.disableTriggers();
    	acct = TestFactory.createAccount();
    	insert acct;
		opp = new Opportunity();
    	opp = TestFactory.createOpportunity(acct.Id, null);
    	insert opp;

		quoteObj = new Quote();
		quoteObj = TestFactory.createQuote(opp.Id, Test.getStandardPricebookId());
		quoteObj.Requested_Discount_Price__c = 100;
		quoteObj.Free_Months_Included__c = 2;
		quoteObj.License_Length_Input__c = 12;
		quoteObj.Summit_Discount_Amount__c = 12;
		insert quoteObj;
    }

	/*
     @ Description : this will query the static objects at the top and make them availabe in the test methods.
	*/
    static void QueryStartingItems(){
        acct = database.Query('select '+BuildQuery('Account')+' where name = \'test Account\'');
        opp = [SELECT Id from Opportunity where AccountId =: acct.Id LIMIT 1];
		// lstQLI = [SELECT Id FROM QuotelineItem WHERE QuoteId =: quoteObj.ID];
		// quoteObj = database.Query('select '+BuildQuery('Quote')+' where OpportunityId =: opp.Id LIMIT 1');
		quoteObj = [SELECT Id from Quote where OpportunityId =: opp.Id LIMIT 1];
    }

	/*
     @ Description : Method to form query
	 @Return:	   : Returns Query string
	*/
    static string BuildQuery(String obj){
        Map<String, Schema.SObjectField> SobjtField = Schema.getGlobalDescribe().get(obj).getDescribe().fields.getMap();
        string qry = '';
        for (Schema.SObjectField s : SobjtField.values()) {
            qry += s + ',';
        }
        qry = qry.removeEnd(',');
        qry = qry+' from '+ obj;
        return qry;
    }

	/*
     @ Description : Test method to test Quote objct functionality
	*/
	@isTest static void test_QuoteCalculations() {
		QueryStartingItems();

		// System.runAs(objUser) {
			System.Test.startTest();
			ApexPages.StandardController sc = new ApexPages.StandardController(quoteObj);
			PageReference pageRef = Page.DealCoordination;
			System.Test.setCurrentPage(pageRef);
			pageRef.getParameters().put('id',quoteObj.Id);
			DealCoordination controller = new DealCoordination(sc);
			System.Test.stopTest();
			System.assertEquals(true, controller.dateRequestedMap.size() > 0);
		// }
	}

}