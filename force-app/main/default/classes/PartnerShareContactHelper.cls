public class PartnerShareContactHelper {

  public static void afterInsert(Map<Id, Contact> newMap) {
    assignContactShares(newMap);
    } // end afterInsert

    public static void afterUpdate(Map<Id, Contact> newMap, Map<Id, Contact> oldMap) {
    assignContactShares(newMap);
    } // end afterUpdate


    private static void assignContactShares(Map<Id, Contact> newMap) {
    //Do PartnerExecutive Only.
    //Custom Setting to get Value...
        List<ContactShare> ContactShares  = new List<ContactShare>();
        ContactShare vShare;

    //you need the User's Partner Account in the trigger set
    //Get AssociatedAccounts
    Set<Id> setPartners = new Set<Id>();
        // List<Contact> enhancedContactList = [SELECT Id, Partner__r.RelatedAccount__c FROM Contact WHERE Partner__c <> null AND Id IN :newMap.keyset()];
    List<Contact> enhancedContactList = [SELECT Id, AccountId FROM Contact WHERE Id IN :newMap.keyset()];
        system.debug('+++++++++++++++THIS IS ' + enhancedContactList);
        for (Contact lp: enhancedContactList) {
            if (lp.AccountID != null && !setPartners.contains(lp.AccountId))
                setPartners.add(lp.AccountId);
        }

    //Get AssociatedRoles
    //Make Map of AccountIdsToListofRoleIds
    Map<Id, List<Id>> accountIdToRoleIdListMap = new Map<Id, List<Id>>();
    List<UserRole> associatedUserRoles = [SELECT Id, PortalAccountId FROM UserRole WHERE PortalAccountId IN: setPartners];
    Set<Id> userRoleIdSet = new Set<Id>();
    for(UserRole ur : associatedUserRoles){
      userRoleIdSet.add(ur.Id);
      if(accountIdToRoleIdListMap.get(ur.PortalAccountId) == null){
        List<Id> tempList = new List<Id>();
        tempList.add(ur.Id);
        accountIdToRoleIdListMap.put(ur.PortalAccountId, tempList);
      }
      else{
        accountIdToRoleIdListMap.get(ur.PortalAccountId).add(ur.Id);
      }
    }

    //Get AssociatedGroups
    //Make Map of RoleIdsToListofGroupIds
    List<Group> associatedGroups = [SELECT Id, RelatedId FROM Group WHERE RelatedId IN: userRoleIdSet AND Type = 'RoleAndSubordinates' AND DeveloperName LIKE '%PartnerExecutive%'];
    Map<Id, List<Id>> userRoleIdToGroupIdsMap = new Map<Id, List<Id>>();
    for(Group g : associatedGroups){
      if(userRoleIdToGroupIdsMap.get(g.RelatedId) == null){
        List<Id> tempList = new List<Id>();
        tempList.add(g.Id);
        userRoleIdToGroupIdsMap.put(g.RelatedId, tempList);
      }
      else{
        userRoleIdToGroupIdsMap.get(g.RelatedId).add(g.Id);
      }
    }

    //Make accountIdToGroupIdListMap
    Map<Id, List<Id>> accountIdToGroupIdListMap = new Map<Id, List<Id>>();
    for(Id accId : new List<Id>(setPartners)){
      if(accountIdToRoleIdListMap.get(accId) != null){
        for(Id roleId : accountIdToRoleIdListMap.get(accId)){
          if(accountIdToGroupIdListMap.get(accId) == null){
            if(userRoleIdToGroupIdsMap.get(roleId) != null){
              accountIdToGroupIdListMap.put(accId, userRoleIdToGroupIdsMap.get(roleId));
            }
          }
          else{
            if(userRoleIdToGroupIdsMap.get(roleId) != null){
              for(Id tempId : userRoleIdToGroupIdsMap.get(roleId)){
                accountIdToGroupIdListMap.get(accId).add(tempId);
              }
            }
          }
        }
      }
    }
    List<Id> ContactIdsToDeleteShares = new List<Id>();
        for (Contact l : enhancedContactList) {
      ContactIdsToDeleteShares.add(l.Id);
      if(accountIdToGroupIdListMap.get(l.AccountId) != null){
        for (Id r : accountIdToGroupIdListMap.get(l.AccountId)) {
          ContactIdsToDeleteShares.add(r);
          vShare = new ContactShare();
                  vShare.ContactId = l.Id;
                  vShare.UserOrGroupId = r;  // Jannard to set the group id here
                  vShare.ContactAccessLevel = 'Edit';

                  ContactShares.add(vShare);
                  system.debug('******Create Share:' + vShare);
              }
      }
        }

    List<ContactShare> lsToDeleteList = [SELECT Id FROM ContactShare WHERE ContactId IN: ContactIdsToDeleteShares];

        try {
      if(!lsToDeleteList.isEmpty()){
        Database.DeleteResult[] ldr = Database.Delete(lsToDeleteList,false);
        if(ldr[0].isSuccess()){
                  system.debug('**** Contact Shares deleted successfully');
              }
        else {
                  Database.Error err = ldr[0].getErrors()[0];
                  if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&
                     err.getMessage().contains('AccessLevel')){
                         system.debug('**** Failed successfully? ');
                     }
                  else{
                      system.debug('**** Contact Shares deleted failed' + err.getStatusCode() + ' ' + err.getMessage());
                  }
              }
      }

            Database.SaveResult[] lsr = Database.insert(ContactShares,false);
            if(lsr[0].isSuccess()){
                system.debug('**** Contact Shares added successfully');
            }
            else {
                Database.Error err = lsr[0].getErrors()[0];
                if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&
                   err.getMessage().contains('AccessLevel')){
                       system.debug('**** Failed successfully? ');
                   }
                else{
                    system.debug('**** Contact Shares added failed' + err.getStatusCode() + ' ' + err.getMessage());
                }
            }
        } catch (Exception e) {
            //handle your exception
        }

    }
}