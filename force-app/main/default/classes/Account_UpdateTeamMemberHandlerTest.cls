@isTest
private class Account_UpdateTeamMemberHandlerTest {

    static testMethod void test_Update360Rep() {
     
        Map<Id,Account> mapAccount = new Map<Id,Account> ();
        Map<Id,Account> mapOldAccount = new Map<Id,Account>();
        
        Profile objProfile = [Select p.Name, p.Id From Profile p Where p.Name = 'Q-OptDev'];
        
        List<User> l_users = new List<user>();
               
        for (integer i=0; i<8; i++) {
            User usertmp = createUser(objProfile.Id,i);
            l_users.add(usertmp);            
        }
        
        //When running this test class, and you get an error on inserting l_users, turn off User.Internal_User_Requirements validation rule.
        database.insert(l_users);

        
        //User user = createUser(objProfile.Id);
        //database.insert(user);
        // ->  User user = [select id from user where ProfileId = :objProfile.id and isactive=true limit 1];
      
      
        //User ObjUser = new User(   FirstName = 'Test1FirstName',
       //                     LastName = 'Test1LastName',
       //                     Username = 'Prachi123@gmail.com',
       //                     Email = 'Prachi123@username.com',
       //                     CommunityNickname = 'TestNickName1',
       //                     alias = 'Alias1',
       //                     emailencodingkey='UTF-8',
       //                     languagelocalekey='en_US',
       //                     localesidkey='en_US',
       //                     ProfileId = objProfile.Id,
       //                     timezonesidkey='America/Los_Angeles');
        
       // database.insert(ObjUser);  

       // -> User ObjUser = [select id from user where ProfileId = :objProfile.id AND isactive=true AND id != :user.id limit 1];
                         
        Account objAccount = new Account(); 
        objAccount.Name = 'test Account';
        objAccount.BillingState = 'Utah';
        objAccount.BillingCountry = 'USA';
        objAccount.BillingPostalCode = '84602';
        objAccount.Account_Notes__c = 'test notes';
        objAccount.OwnerId = userInfo.getUserId(); 
        objAccount.X360_EE_Rep__c = l_users[0].Id;
        objAccount.Panels_Rep__c = l_users[1].id;
        objAccount.Site_Intercept_Rep__c = l_users[2].id;
        ObjAccount.Target_Audience_Rep__c = l_users[3].id;
        ObjAccount.OpDev_Rep__c = l_users[4].id;
        ObjAccount.Client_Services_Rep__c = l_users[5].id;
        ObjAccount.Professional_Services_Rep__c = l_users[6].id;
        
        insert objAccount  ;
        system.debug('-======objAccount===='+objAccount);
        mapOldAccount.put(objAccount.ID, objAccount); 
                                     
        AccountTeamMember objTeam1 = new AccountTeamMember(TeamMemberRole = 'test',
                                                            AccountId = objAccount.Id,
                                                            UserId = l_users[0].Id);
        database.insert(objTeam1);
        system.debug('-======objTeam===='+objTeam1);
        
        
        Account objAcc = [Select Id, OwnerId, X360_EE_Rep__c From Account Where Id=:objAccount.Id];
        //objAcc.OwnerId = ObjUser.Id;
        system.debug('-======objAccount= update==='+objAcc + ':' + objProfile.id + ':' + user.id + ':' + l_users[1].id);
        objAcc.X360_EE_Rep__c = l_users[1].Id;
        objAcc.Panels_Rep__c = l_users[2].id;
        objAcc.Site_Intercept_Rep__c = l_users[3].id;
        ObjAcc.Target_Audience_Rep__c = l_users[4].id;
        ObjAcc.OpDev_Rep__c = l_users[5].id;
        ObjAcc.Client_Services_Rep__c = l_users[6].id;
        ObjAcc.Professional_Services_Rep__c = l_users[7].id;
        
        
        system.debug('-======objAccount= update==='+objAcc);
        update objAcc;
        
        mapAccount.put(objAcc.ID, objAcc); 
        system.debug('-======mapAccount= update==='+mapAccount);
        system.debug('-======mapOldAccount= update==='+mapOldAccount);
        
        Account_UpdateTeamMemberHandler objHandler = new Account_UpdateTeamMemberHandler();
        objHandler.updateTeamMember(mapAccount, mapOldAccount);
        
        test.startTest();  
       
       /* AccountTeamMember objTeam = [Select a.UserId, a.TeamMemberRole, a.Id, a.AccountId, a.AccountAccessLevel 
                                     From AccountTeamMember a 
                                     Where a.AccountId =: objAccount.Id
                                     limit 1];
        
        System.assertEquals(objAccount.X360_EE_Rep__c, objTeam.UserId); */           
        test.stopTest();
    }//test_Update360Rep
    
    public static User createUser(Id pProfileId,integer j){
        String ran = String.valueOf(Math.abs(Crypto.getRandomInteger())).left(5);
        String k = String.valueof(j);
        return  new User(   FirstName = 'TestFirstName',
                            LastName = 'TestLastName',
                            Username = 'test'+ k + ran + '@test' + ran + '.com',
                            Email = 'test' + k + ran + '@test' + ran + '.com',
                            CommunityNickname = 'TestNickName' + ran,
                            alias = 'Alias' + k,
                            emailencodingkey='UTF-8',
                            languagelocalekey='en_US',
                            localesidkey='en_US',
                            ProfileId = pProfileId,
                            timezonesidkey='America/Los_Angeles');
    }//createUser
}