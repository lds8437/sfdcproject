global class PartnerShareOpportunityBatch implements Database.Batchable<sObject> {
  global Database.QueryLocator start(Database.BatchableContext BC) {
    return Database.getQueryLocator('SELECT Id, Name, Partner__r.RelatedAccount__c, QPN_Recommended_Partner__r.RelatedAccount__c FROM Opportunity WHERE RecordTypeId != \'012500000009kLAAAY\' AND (Partner__c != null OR QPN_Recommended_Partner__c != null) AND (Partner__r.RelatedAccount__c != null OR QPN_Recommended_Partner__r.RelatedAccount__c != null)');
  }

  global void execute(Database.BatchableContext BC, List<Opportunity> scope) {
    Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>(scope);
    Set<Id> partnerAccountIds = new Set<Id>();

    for (Opportunity o : scope) {
      if (o.Partner__r.RelatedAccount__c != null) partnerAccountIds.add(o.Partner__r.RelatedAccount__c);
      if (o.QPN_Recommended_Partner__r.RelatedAccount__c != null) partnerAccountIds.add(o.QPN_Recommended_Partner__r.RelatedAccount__c);
    }

    Map<Id, Set<Id>> portalAccountToRolesMap = new Map<Id, Set<Id>>();
    Set<Id> userRoleIds = new Set<Id>();
    for (UserRole role : [SELECT Id, PortalAccountId FROM UserRole WHERE PortalAccountId IN :partnerAccountIds]) {
      if (portalAccountToRolesMap.containsKey(role.PortalAccountId)) {
        portalAccountToRolesMap.get(role.PortalAccountId).add(role.Id);
      } else {
        portalAccountToRolesMap.put(role.PortalAccountId, new Set<Id> { role.Id });
      }
      userRoleIds.add(role.Id);
    }

    Map<Id, Id> userRoleToGroupMap = new Map<Id, Id>();
    Map<Id, String> groupToAccessMap = new Map<Id, String>();
    for (Group roleGroup : [SELECT Id, RelatedId, DeveloperName FROM Group WHERE RelatedId IN :userRoleIds AND Type = 'Role']) {
      String accessLevel = 
        (roleGroup.DeveloperName.contains('PartnerExecutive') || roleGroup.DeveloperName.contains('PartnerManager')) ? 
        'Edit' :
        'Read';
      userRoleToGroupMap.put(roleGroup.RelatedId, roleGroup.Id);
      groupToAccessMap.put(roleGroup.Id, accessLevel);
    }

    List<OpportunityShare> oppShares = new List<OpportunityShare>();
    for (Id oppId : oppMap.keySet()) {
      Set<Id> accountIds = new Set<Id>();
      if (oppMap.get(oppId).Partner__r.RelatedAccount__c != null) accountIds.add(oppMap.get(oppId).Partner__r.RelatedAccount__c);
      if (oppMap.get(oppId).QPN_Recommended_Partner__r.RelatedAccount__c != null) accountIds.add(oppMap.get(oppId).QPN_Recommended_Partner__r.RelatedAccount__c);
      for (Id partnerAccountId : accountIds) {
        if (partnerAccountId != null && portalAccountToRolesMap.containsKey(partnerAccountId)) {
          for (Id userRoleId : portalAccountToRolesMap.get(partnerAccountId)) {
            Id groupId = userRoleToGroupMap.get(userRoleId);
            String access = groupToAccessMap.get(groupId);
            if (groupId != null && access != null) {
              OpportunityShare oppShare = new OpportunityShare();
              oppShare.OpportunityId = oppId;
              oppShare.UserOrGroupId = groupId;
              oppShare.OpportunityAccessLevel = groupToAccessMap.get(groupId);
              oppShares.add(oppShare);
            }
          }
        }
      }
    }

    Database.SaveResult[] srList = Database.insert(oppShares, false);
            
    for (Database.SaveResult sr : srList) {
      if (!sr.isSuccess()) {               
        for(Database.Error err : sr.getErrors()) {
          System.debug('The following error has occurred.');                    
          System.debug(err.getStatusCode() + ': ' + err.getMessage());
          System.debug('Account fields that affected this error: ' + err.getFields());
        }
      }
    }
  }

  global void finish(Database.BatchableContext BC) {

  }
}