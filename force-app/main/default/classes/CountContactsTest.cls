@isTest
public class CountContactsTest {
    public static testmethod void CountContactsTest(){
        User admin = [SELECT Id from User where FirstName = 'Admin'];
        account a = new account(name='test');
        a.Name = 'TestAccount3eqrr3543';
        a.SDR_RS__c = admin.Id;
        a.Number_of_Contacts__c = 0;
        Id newAccountId = a.Id;
        insert a;
        contact c = new contact(
            FirstName ='fname324f3fsdw43f',
            LastName ='lname',
            Account = a,
            AccountId = a.Id,
            SDR_RC_CX_Rep__c = admin.Id
        );
        insert c;
        List<Contact> contList = new List <Contact>{}; 
        contList.add(c);
        CountContacts.afterInsertTriggerUpdates(contList,null);
        Contact cont = [SELECT Id, FirstName from Contact where Id = :c.Id];
        cont.FirstName = 'fname324f3fsdw43f33';
        update cont; 
       
        delete cont;
    }
}