public with sharing class QuoteLineItems {
    public List<QuoteLineItem> oliList {get;set;}
    
    public QuoteLineItems(ApexPages.StandardController controller) {
        queryOli();
    }
    
    @testVisible
    private void queryOli(){
        String qry = 'SELECT ';
        for(Schema.FieldSetMember memberField : SObjectType.QuoteLineItem.FieldSets.RelatedList.getFields()){
            qry += memberField.getFieldPath() + ',';
        }
        qry = qry.removeEnd(',');
        Id quoteId = ApexPages.CurrentPage().getParameters().get('id');
        qry += ' FROM QuoteLineItem WHERE QuoteId = :quoteId';
        system.debug(logginglevel.error, '***** qry='+qry);
        oliList = Database.Query(qry);
    }
    
}