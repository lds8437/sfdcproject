/* 
 *  The class will change the Lead Status to "Reject" on click of Reject Button on Lead and will open a new popup 
 *  window to allow user to enter the Reject reason. 
 */

public class Leadextension {
	
    public Lead leadrecd {get;set;}
    public final static string LEAD_STATUS = system.label.Lead_Status;
    
    public Leadextension(ApexPages.StandardController stdController){
        
        this.leadrecd = (Lead)stdController.getRecord();
        system.debug('leadrecd ::'+leadrecd);
        leadrecd.status = LEAD_STATUS;
	}
    
   //Calling the method on click of updateLead button 
    public void updateLead (){
        try{
            update leadrecd;
        }catch(exception ex){
            system.debug('Insertion failed'+ ex.getMessage());
        }
	}
	
    //Calling the method on click of cancelUpdate button 
    public void cancelUpdate(){
    	system.debug('Cancel Completed');
    }
}