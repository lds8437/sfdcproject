public abstract class RedirectionBase {

    /**
     * All redirection objects (currently SObject and user)
     * require very similar concepts during execution.  Each
     * include a request, settings and an SObject.
     */
    protected RedirectionRequest request;
    protected RedirectionSettings settings;
    protected SObject record;
    
    /**
     * Class Constructor
     * 
     * The only way to instantiate this class is to pass a reference
     * to the request and settings.
     */
    public RedirectionBase(RedirectionRequest request, RedirectionSettings settings) {
        this.request = request;
        this.settings = settings;
        init();
    }
    
    /**
     * During initialization we verify the settings are valid.
     * Once we pass validation we initiate the SObject and
     * perform any custom validations against that record.
     */
    private void init() {
        if (!settings.isValid()) {
            return;
        }
        initRecord();
        validate();
    }

    /**
     * The record is initialized based on the SOQL statement
     * specified in the abstract method.  If a result was found
     * the record variable is updated to match that record.
     */
    private void initRecord() {
        try {
            String soql = getSoql();
            request.addLog(RedirectionConstants.LOG_PREFIX_SOQL + ' ' + soql);
            List<SObject> records = Database.query(soql);
            if (records.size() == 1) {
                request.addLog(RedirectionConstants.LOG_PREFIX_RECORD + ' ' + JSON.serialize(records[0]));
                record = records[0];
            }
        } catch (Exception e) {
            System.debug('Error getting SObject: ' + e.getMessage());
        }
    }
    
    /**
     * This is considered valid if the record variable is populated.
     */
    public Boolean isValid() {
        return record != null;
    }
    
    /**
     * Abstract Methods
     * 
     * Any class extending RedirectionBase (this) must implement
     * the following methods.
     */
    abstract String getSoql();
    abstract void validate();
    abstract List<String> getUrlNodes();
    abstract Map<String, String> getOutputQuerystringParamters();
}