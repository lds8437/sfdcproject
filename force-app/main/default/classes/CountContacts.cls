public with sharing class CountContacts {
    public static void afterInsertTriggerUpdates (List<Contact> newContacts, Map<Id,Contact> oldMap){
        Set<Id> parentIdsSet = new Set<Id>();
        List<Account> accountListToUpdate = new List<Account>();
        FOR(Contact c : newContacts){
            if(c.AccountId!=null){   
                parentIdsSet.add(c.AccountId); 
            }
        }
        System.debug('#### parentIdsSet = '+parentIdsSet);
        List<Account> accountList = new List<Account>([Select id ,Name, Number_of_Contacts__c, (Select id, Name From Contacts) from Account Where id in:parentIdsSet]);
        FOR(Account acc : accountList){
            List<Contact> contactList = acc.Contacts;
            acc.Number_of_Contacts__c = contactList.size();
            accountListToUpdate.add(acc);
        }
        try{
            update accountListToUpdate;
        }catch(System.Exception e){
            
        }
    }
    public static void beforeUpdateTriggerUpdates (List<Contact> newContacts, Map<Id,Contact> oldMap){
        Set<Id> parentIdsSet = new Set<Id>();
        List<Account> accountListToUpdate = new List<Account>();
        FOR(Contact c : newContacts){
            if(c.AccountId!=null){   
                parentIdsSet.add(c.AccountId); 
            }
        }
        System.debug('#### parentIdsSet = '+parentIdsSet);
        List<Account> accountList = new List<Account>([Select id ,Name, Number_of_Contacts__c, (Select id, Name From Contacts) from Account Where id in:parentIdsSet]);
        FOR(Account acc : accountList){
            List<Contact> contactList = acc.Contacts;
            acc.Number_of_Contacts__c = contactList.size();
            accountListToUpdate.add(acc);
        }
        try{
            update accountListToUpdate;
        }catch(System.Exception e){
            
        }
    }
    public static void afterDeleteTriggerUpdates (List<Contact> deletedContacts, Map<Id,Contact> oldMap){
        Set<Id> parentIdsSet = new Set<Id>();
        List<Account> accountListToUpdate = new List<Account>();
        FOR(Contact c : deletedContacts){
            if(c.AccountId!=null){   
                parentIdsSet.add(c.AccountId); 
            }
        }
        System.debug('#### parentIdsSet = '+parentIdsSet);
        List<Account> accountList = new List<Account>([Select id ,Name, Number_of_Contacts__c, (Select id, Name From Contacts) from Account Where id in:parentIdsSet]);
        FOR(Account acc : accountList){
            List<Contact> contactList = acc.Contacts;
            acc.Number_of_Contacts__c = contactList.size();
            accountListToUpdate.add(acc);
        }
        try{
            update accountListToUpdate;
        }catch(System.Exception e){
            
        }
    }
}