public class getProcessInstance {
    
    public static List<ProcessInstance> pi { get; set; } 
    @auraEnabled
    public static List<ProcessInstance> getApprovals(Integer recordLimit, Integer recordOffset){
        Id userId = UserInfo.getUserId();
        Integer intLimit = Integer.valueof(recordLimit);
        Integer intOffset = Integer.valueof(recordOffset);
        system.debug('userId');
        pi = [SELECT Id, TargetObject.Name, TargetObject.Type, SubmittedBy.Name, CreatedDate, 
            CompletedDate, Status, ProcessDefinition.Name 
            FROM ProcessInstance
            WHERE SubmittedById = :userId and CreatedDate = LAST_N_DAYS:30
              ORDER BY CreatedDate desc
             LIMIT :intLimit Offset :intOffset];
        return pi;        
    }
    
    //Get Total Number of Contacts
    @AuraEnabled
    public static Integer getTotal(){
        Id userId = UserInfo.getUserId();
        AggregateResult results = [SELECT Count(Id) ttl  FROM ProcessInstance
            WHERE SubmittedById = :userId and CreatedDate = LAST_N_DAYS:30];
        Integer ttl = (Integer)results.get('ttl') ; 
        return ttl;
    } 
}