public without sharing class opp_NewOpportunityController {
    public Opportunity opportunity{
        get{
            if(opportunity == null){
                opportunity = new Opportunity();
                opportunity.AccountId = ApexPages.currentPage().getParameters().get('accid');
                opportunity.StageName = 'Best Case';   
                opportunity.CloseDate = Date.Today()+90;
                opportunity.Created_by_CS_PS__c  = TRUE;
                opportunity.Approval_Status__c = 'Accepted';
                opportunity.Turn_Off_Initial_CX_EX_RC_Amounts__c = TRUE;
            }
            return opportunity;
        }
        set;
    }
    
    public List<SelectOption> types{
        get{
            string profile;
            
            for(User u: [SELECT Id, Profile.Name From User WHERE Id  =:Opportunity.Assign_To__c LIMIT 1]){
                profile = u.Profile.Name;         
            }  
            List<SelectOption> options = new List<SelectOption>();
            If(profile == 'Q-Sales-EI'){
                options.add(new SelectOption('Employee Insights','Employee Insights'));          
            }
            else If(profile == 'Q-Sales-Enterprise' || profile == 'XM Enterprise' || profile == 'XM Manager'){
                options.add(new SelectOption('Enterprise','Enterprise'));            
            }
            else If(profile == 'Q-Sales-K12'){
                options.add(new SelectOption('K-12','K-12'));            
            }
            else If(profile == 'Q-Sales-RS' || profile == 'XM AE'){
                options.add(new SelectOption('Research Suite Corp','Research Suite Corp'));            
            }
            else If(profile == 'Q-Sales-SLG'){
                options.add(new SelectOption('Enterprise','Enterprise'));            
            }
            else If(profile == 'Q-Sales–Academic'){
                options.add(new SelectOption('Academic','Academic'));
                options.add(new SelectOption('Enterprise','Enterprise'));
            } 
            else If(profile == 'Q-Sales-Academic-US'){
                options.add(new SelectOption('Academic','Academic'));
                options.add(new SelectOption('Enterprise','Enterprise'));
            } 
            else If(profile == 'Q-Sales–RS-Dallas'){
                options.add(new SelectOption('Research Suite Corp','Research Suite Corp'));                
            } 
            return options;
        }
        set;
    }
    
    public String quname {get; set;}
    public Quote quote {get;set;}
   
    
    public opp_NewOpportunityController(){
        quote = new Quote();
    }
    
    public String selectedType {get;set;}
    
    public PageReference save(){        
        Id devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(selectedType).getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get(selectedType).getRecordTypeId();
       // system.debug(devRecordTypeId);
      //  system.debug(quoteRecordTypeId);
        opportunity.RecordTypeId = devRecordTypeId;
        opportunity.Can_Change_Owner__c = TRUE;
        try{
            insert opportunity;
            quote.OpportunityId = opportunity.id;
            quote.Quote_Type__c = 'Services Only';            
            quote.Name = opportunity.name; 
            quote.RecordTypeId = quoteRecordTypeId;
            quote.Created_by_CS_PS__c = TRUE;
           // quote.Summit_Number_of_Passes__c = 0;
            insert quote;
            
            
        }catch (Exception ex){
            
            ApexPages.addMessages(ex);
            return null;
        }  
        
        
        return new PageReference('/flow/Q2C_Services?varQuoteID='+quote.Id+'&retURL=/'+opportunity.id);         
        
    }
    public PageReference cancel(){
        return new PageReference('/'+ApexPages.currentPage().getParameters().get('accid'));  
    }    
}