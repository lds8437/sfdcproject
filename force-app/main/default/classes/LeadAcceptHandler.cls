/* 
 *  The class will change the Lead owner to the current user and the Status to "Working" on click of Accept Button on Lead. 
 */
 
global with sharing class LeadAcceptHandler {
    
	public static final String STR_STATUS = 'Working';
    webservice static void updateLead(Id ObjId){
        
        Lead leadtoUpd= new Lead(Id = ObjId);
          
        if(leadtoUpd.Status <> STR_STATUS){
       		leadtoUpd.Status = STR_STATUS;
        }//if
        
        leadtoUpd.OwnerId=UserInfo.getUserId();
        update leadtoUpd;
        
    }//updateLead
}