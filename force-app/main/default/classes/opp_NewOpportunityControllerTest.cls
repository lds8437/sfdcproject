@isTest (SeeAllData=TRUE)
public class opp_NewOpportunityControllerTest {
@isTest static void NewOppTest()
    {
        PageReference pageRef = Page.opp_NewOpportunity;
        Test.setCurrentPage(pageRef);
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Q-Sales-EI'];
        
        User u1 = new User(Alias = 'standt1',Country='United Kingdom',Email='demo1@randomdemodomain.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='dprobertdemo1@camfed.org');
        insert u1;
        
        // now insert your test data

       
        
        Opportunity opp = (Opportunity)EB_Utility_CreateObjectForTesting.CreateObject('Opportunity');
        Opp.LID__LinkedIn_Company_Id__c = '5';
        opp.Forecast_PercentageV2__c = 0;
        opp.Opp_from_Lead_Conversion__c = false;        
        opp.Assign_To__c = u1.Id;
        insert opp;        
        
        string selectedType = 'Employee Insights';
        System.currentPagereference().getParameters().put('id',opp.id);
        opp_NewOpportunityController cont = new opp_NewOpportunityController();        
        
      //  cont.save();
        cont.cancel();
    }
}