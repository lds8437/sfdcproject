public class EmailHunter {

    //final static String BASE_URL = 'https://api.emailhunter.co/v2/email-finder';
    final static String BASE_URL = 'https://api.hunter.io/v2/email-finder';
    final static String API_KEY = '94c6256b158b15eba326c1189f1b85b8ad0e80de';
    final static String ENCODING_SCHEME = 'UTF-8';
    
    final static String METHOD_GET = 'GET';
    
    final static String KEY_DOMAIN = 'domain';
    final static String KEY_FIRST_NAME = 'first_name';
    final static String KEY_LAST_NAME = 'last_name';
    final static String KEY_API_KEY = 'api_key';
    final static String KEY_COMPANY = 'company';
    
    public static EmailHunterResponse callAPI(Contact record) {
        return callAPI(record.Domain__c, record.FirstName, record.LastName, record.Account.Name);
    }
    
    private static EmailHunterResponse callAPI(String domain, String firstName, String lastName, String company) {
        String requestURL = getRequestURL(domain, firstName, lastName, company);
		HttpRequest apiRequest = new HttpRequest();
        apiRequest.setEndpoint(requestURL);
        apiRequest.setMethod(METHOD_GET);
        
        Http http = new Http();
		HttpResponse apiResponse = http.send(apiRequest);
		EmailHunterResponse emailHunterResponse = (EmailHunterResponse)JSON.deserialize(apiResponse.getBody(), EmailHunterResponse.class);
        return emailHunterResponse;
    }
    
    private static String getRequestURL(String domain, String firstName, String lastName, String company) {
		return BASE_URL
            + '?' + KEY_DOMAIN + '=' + EncodingUtil.urlEncode(domain, ENCODING_SCHEME)
            + '&' + KEY_FIRST_NAME + '=' + EncodingUtil.urlEncode(firstName, ENCODING_SCHEME)
            + '&' + KEY_LAST_NAME + '=' + EncodingUtil.urlEncode(lastName, ENCODING_SCHEME)
            + '&' + KEY_API_KEY + '=' + API_KEY
            + '&' + KEY_COMPANY + '=' + EncodingUtil.urlEncode(company, ENCODING_SCHEME);
    }
}