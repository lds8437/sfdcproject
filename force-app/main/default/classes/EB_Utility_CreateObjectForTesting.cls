@isTest
public with sharing class EB_Utility_CreateObjectForTesting {
    public static sObject CreateObject(string sObjectName) {
        return CreateObject(sObjectName, new Set<string>());
    }

    public static sObject CreateObject(string sObjectName, Set<string> ignoreFields) {
        sObject sObj = Schema.getGlobalDescribe().get(sObjectName).newSObject() ;
        Map<String, Schema.SObjectField> SobjtField = Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap();
        for (string s : SobjtField.keySet()) {
            Schema.DescribeFieldResult field = SobjtField.get(s).getDescribe();
            if (field.isAccessible() == true && field.isCreateable() == true && IgnoreField(s, ignoreFields) == false) {
                if (field.getSoapType() == Schema.SoapType.String) {
                    if (field.getType() == Schema.DisplayType.Picklist || field.getType() == Schema.DisplayType.multipicklist) {
                        sObj.put(s, SobjtField.get(s).getDescribe().getPickListValues().get(0).getValue());
                    } else if (field.getType() == Schema.DisplayType.Email) {
                        sObj.put(s, 'test@eidebailly.com');
                    } else {
                        sObj.put(s, 'abcd');
                    }
                } else if (field.getSoapType() == Schema.SoapType.Boolean) {
                    sObj.put(s, True);
                } else if (field.getSoapType() == Schema.SoapType.Date) {
                    sObj.put(s, Date.today());
                } else if (field.getSoapType() == Schema.SoapType.DateTime) {
                    sObj.put(s, DateTime.now());
                } else if (field.getSoapType() == Schema.SoapType.Double) {
                    sObj.put(s, 1.2);
                } else if (field.getSoapType() == Schema.SoapType.Integer) {
                    sObj.put(s, 3);
                }
            }
        }
        return sObj;
    }

    public static Set<String> AddressFields() {
        return new Set<String> {'Billing*', 'Shipping*', 'Other*', 'Mailing*'};
    }

    private static Boolean IgnoreField(string fieldName, Set<string> ignoreFields) {
        //Loops through the set as set.contains is case sensitive.
        for (String iField : ignoreFields) {
            if (iField == fieldName) return true;
            if (iField.EndsWith('*') && fieldName.StartsWithIgnoreCase(iField.removeEnd('*'))) return true;
            if (iField.StartsWith('*') && fieldName.EndsWithIgnoreCase(iField.removeStart('*'))) return true;
        }
        return false;
    }
}