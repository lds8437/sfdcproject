/***********
Description: This class will be used to test 'CountNumberOfBids_db' batch class and 'Schedule_CountNumberOfBids_db' schedule class
***********/

@isTest
public class CountNumberOfBids_Test_db {

    //Description: This method is used for creating all the test data and work as testdata factory.
    @TestSetup
    private static void dataSetup() {
    Profile p = [SELECT Id FROM Profile WHERE Name='Q-Sales-EI'];
        User u1 = new User(Alias = 'standt1',Country='United Kingdom',Email='demo1@ryz.com',EmailEncodingKey='UTF-8', LastName='Testingryz', LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='dprobertdemo1@ryz.org', EmployeeID__c = 'EMP121212', EIN__c = 9999, FTE__c = 1, Sales_Level__c = 'NA', Rep_Tier__c = 'Non-Sales', Area__c = 'Global', Region__c = 'Non-Sales', Sales_Team__c = 'Non-Sales');
        insert u1;
        Opportunity opp = (Opportunity)EB_Utility_CreateObjectForTesting.CreateObject('Opportunity');
        Opp.LID__LinkedIn_Company_Id__c = '5';
        opp.Forecast_PercentageV2__c = 0;
        opp.Opp_from_Lead_Conversion__c = false;
        opp.RecordtypeId ='01250000000AQxm';
        opp.Assign_To__c = u1.Id;
        insert opp;
        system.debug('@@@'+opp);
        vendor__c v = new vendor__c(name = 'test vendor', CoupaID__c = 'abc', Type__c = 'Panels', Status__c = 'Active' );
        insert v;
        Panels_Project_Vendor__c ppv1 =  new Panels_Project_Vendor__c(
                                    VendorName__c = v.id, 
                    Opportunity__c = opp.id, 
                    Price_per_Response__c=6.00, 
                    Total_Vendor_Quoted_Cost__c=0.00, 
                    CurrencyIsoCode='USD'
                    ); 
        insert ppv1;
        Panels_Project_Vendor__c ppv2 =  new Panels_Project_Vendor__c(
                                    VendorName__c = v.id, 
                    Opportunity__c = opp.id, 
                    Price_per_Response__c=6.00, 
                    Total_Vendor_Quoted_Cost__c=0.00, 
                    CurrencyIsoCode='USD'
                    ); 
        insert ppv2;
    }

    //Description: The method will be used testing the batch class.
    private static testMethod void TestBatch() {
        Test.startTest();
        database.executeBatch(new CountNumberOfBids_db());
        Test.stopTest();
        vendor__c v = [select Number_of_times_Bided__c, Number_of_times_selected__c from vendor__c where name = 'test vendor' and CoupaID__c = 'abc' limit 1
        ];
        system.assertEquals(v.Number_of_times_Bided__c, 2);
        system.assertEquals(v.Number_of_times_selected__c, 0);

    }

    //Description: The method will be used testing the schedule class.
    private static testMethod void TestSchedule() {
        Test.startTest();
        string jodId = system.schedule('Test Territory Check', '0 0 23 * * ?', new Schedule_CountNumberOfBids_db());
        Test.StopTest();
        List < cronTrigger > cornList = [select id from cronTrigger where id =: jodId];
        system.assertEquals(cornList.size(), 1);
    }
    
    //Description: The method will be used testing the PanelProjectVendorTrigger trigger.
    private static testMethod void TestTrigger() {
        Test.startTest();
        List<Panels_Project_Vendor__c> pList = [select id from Panels_Project_Vendor__c limit 1];
        system.assertEquals(pList.size(), 1);
        delete pList;
        Test.StopTest();
    }
}