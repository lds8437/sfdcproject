@isTest
private class updateTaskTest {
  
    public static testMethod void testupdateTask(){
        Account a = new Account();
        a.Name = 'Test Account';
        a.SDR_RS__c = null;
        
        insert a;
        
        Contact c = new Contact();
        c.FirstName = 'fname';
        c.LastName = 'lname';
        c.Email	= 'email@email.com';
        c.Phone = '8018675309';
        c.AccountId = a.Id;
                               
        insert c;
        
        Task t = new Task();
        t.OwnerId = UserInfo.getUserId();
        t.Subject='Email Task';
        t.Status='Not Started';
        t.Priority='Normal';
        t.WhoId = c.Id;

        insert t;       
    }
}