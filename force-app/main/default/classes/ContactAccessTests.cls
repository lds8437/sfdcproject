@IsTest
public class ContactAccessTests {

    testMethod static void getEmailFromEmailHunterTest() {
        
        final String ACCOUNT_NAME = 'Test Account';
        final String ACCOUNT_WEBSITE = 'https://www.google.com';
        
        final String CONTACT_FIRST_NAME = 'John';
        final String CONTACT_LAST_NAME = 'Smith';
        
        // Prepare for tests
        Account testAccount = new Account();
        testAccount.Name = ACCOUNT_NAME;
        testAccount.Website = ACCOUNT_WEBSITE;
        insert testAccount;
        System.assertEquals(ACCOUNT_NAME, testAccount.Name);
        System.assertEquals(ACCOUNT_WEBSITE, testAccount.Website);
        System.assertNotEquals(null, testAccount.Id);
        
        Contact testContact = new Contact();
        testContact.AccountId = testAccount.Id;
        testContact.FirstName = CONTACT_FIRST_NAME;
        testContact.LastName = CONTACT_LAST_NAME;
        insert testContact;
        System.assertEquals(CONTACT_FIRST_NAME, testContact.FirstName);
        System.assertEquals(CONTACT_LAST_NAME, testContact.LastName);
        System.assertNotEquals(null, testContact.Id);
        
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new EmailHunterCalloutMock());
        EmailHunterResponse response = ContactAccess.getEmailFromEmailHunter(testContact.Id);
        
        Test.stopTest();
    }
    
    testMethod static void getEmailFromEmailHunterTest_Fail() {
        
        final String ACCOUNT_NAME = 'Test Account';
        final String ACCOUNT_WEBSITE = 'https://www.google.com';
        
        final String CONTACT_FIRST_NAME = 'John';
        final String CONTACT_LAST_NAME = 'Smith';
        
        // Prepare for tests
        Account testAccount = new Account();
        testAccount.Name = ACCOUNT_NAME;
        testAccount.Website = ACCOUNT_WEBSITE;
        insert testAccount;
        System.assertEquals(ACCOUNT_NAME, testAccount.Name);
        System.assertEquals(ACCOUNT_WEBSITE, testAccount.Website);
        System.assertNotEquals(null, testAccount.Id);
        
        try {
	        EmailHunterResponse response = ContactAccess.getEmailFromEmailHunter(testAccount.Id);
        } catch (QueryException e) {
            System.assertEquals('Contact not found', e.getMessage());
        }
    }

    testMethod static void updateEmailAddressTest() {
        
        final String ACCOUNT_NAME = 'Test Account';
        final String ACCOUNT_WEBSITE = 'https://www.google.com';
        
        final String CONTACT_FIRST_NAME = 'John';
        final String CONTACT_LAST_NAME = 'Smith';
        
        // Prepare for tests
        Account testAccount = new Account();
        testAccount.Name = ACCOUNT_NAME;
        testAccount.Website = ACCOUNT_WEBSITE;
        insert testAccount;
        System.assertEquals(ACCOUNT_NAME, testAccount.Name);
        System.assertEquals(ACCOUNT_WEBSITE, testAccount.Website);
        System.assertNotEquals(null, testAccount.Id);
        
        Contact testContact = new Contact();
        testContact.AccountId = testAccount.Id;
        testContact.FirstName = CONTACT_FIRST_NAME;
        testContact.LastName = CONTACT_LAST_NAME;
        insert testContact;
        System.assertEquals(CONTACT_FIRST_NAME, testContact.FirstName);
        System.assertEquals(CONTACT_LAST_NAME, testContact.LastName);
        System.assertNotEquals(null, testContact.Id);
        
		ContactAccess.updateEmailAddress(testContact.Id, 'jsmith@google.com');
    }
}