@isTest()
public class task_RelatedInformationTest {

    @IsTest static void testQuoteManipulation(){
        //Create Account
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        //Create Contact
        Contact con = new Contact();
        con.FirstName = 'Kruse';
        con.LastName = 'Collins';
        con.AccountId = acc.Id;
        con.LeadSource = 'Event';
        con.Contact_Role__c = 'VP';
        insert con;
        
          Lead l = new Lead();
       l.LastName = 'Baldazzi';
        l.Company = 'Qualtrics55';
        insert l;
        
      Task t = new Task();
        t.OwnerId = UserInfo.getUserId();
        t.Subject='Email Task';
        t.Status='Not Started';
        t.Priority='Normal';
        t.WhoId = con.Id;

        insert t; 
        
      Task t2 = new Task();
        t2.OwnerId = UserInfo.getUserId();
        t2.Subject='Email Task';
        t2.Status='Not Started';
        t2.Priority='Normal';
        t2.WhoId = l.Id;

        insert t2; 
        
        Test.startTest();
        sObject s = task_RelatedInformation.getContact(t.Id);
             sObject s2 = task_RelatedInformation.getContact(t2.Id);
        Test.stopTest();
    }
}