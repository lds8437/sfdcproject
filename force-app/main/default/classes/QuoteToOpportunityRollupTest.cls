@isTest
public with sharing class QuoteToOpportunityRollupTest {
	
	static Account acct {get;set;}
    static Opportunity opp {get;set;}
    static Quote quoteObj {get;set;}

    @testsetup 
    static void test_setup(){
    	TestFactory.disableTriggers();
    	acct = TestFactory.createAccount();
    	insert acct;
    	Pricebook2 pb = TestFactory.createPriceBook();
    	insert pb;
		Product2 prod = TestFactory.createProduct();
		insert prod;
    	opp = TestFactory.createOpportunity(acct.Id, pb.Id);
    	insert opp;
    	quoteObj = TestFactory.createQuote(opp.Id, pb.Id);
    	insert quoteObj;
    	opp.SyncedQuoteId = quoteObj.Id;
    	update opp;
    }

    static void QueryStartingItems(){
        acct = database.Query('select '+BuildQuery('Account')+' where name = \'test Account\'');
        opp = database.Query('select '+BuildQuery('Opportunity'));
        quoteObj = database.Query('select '+BuildQuery('Quote'));
    }

    static string BuildQuery(String obj){
        Map<String, Schema.SObjectField> SobjtField = Schema.getGlobalDescribe().get(obj).getDescribe().fields.getMap();
        string qry = '';
        for (Schema.SObjectField s : SobjtField.values()) {
            qry += s + ',';
        }
        qry = qry.removeend(',');
        qry = qry+' from '+ obj;
        return qry;
    }

    @isTest 
    static void Test_findSyncedQuotes(){
    	QueryStartingItems();
    	List<Quote> syncedQuoteList;
    	Test.startTest();
    		syncedQuoteList = QuoteToOpportunityRollup.findSyncedQuotes(new List<Quote>{quoteObj});
    	Test.stopTest();
    	system.assertEquals(1, syncedQuoteList.size());
    }

    @isTest 
    static void Test_findOpportunitiesFromSyncedQuotes(){
    	QueryStartingItems();
    	Map<Id,Opportunity> opportunityMap;
    	Test.startTest();
    		opportunityMap = QuoteToOpportunityRollup.findOpportunitiesFromSyncedQuotes(new List<Quote> {quoteObj});
    	Test.stopTest();
    	system.assertEquals(1, opportunityMap.size());
    	system.assert(opportunityMap.containsKey(opp.Id));
    }

    @isTest
    static void Test_updateOpportunitiesFromQuotes(){
        QueryStartingItems();
        quoteObj.Tax = 25;
        Test.startTest();
            QuoteToOpportunityRollup.updateOpportunitiesFromQuotes(new List<Quote>{quoteObj}, new Map<Id,Opportunity>{opp.Id => opp});
        Test.stopTest();
        Opportunity testOpp = [SELECT Tax_From_Quote__c FROM Opportunity WHERE Id = :opp.Id];
        system.assertEquals(25, testOpp.Tax_From_Quote__c);
    }

    @isTest
    static void Test_taxRollup(){
        QueryStartingItems();
        quoteObj.Tax = 25;
        Test.startTest();
            QuoteToOpportunityRollup.taxRollup(new List<Quote>{quoteObj});
        Test.stopTest();
        Opportunity testOpp = [SELECT Tax_From_Quote__c FROM Opportunity WHERE Id = :opp.Id];
        system.assertEquals(25, testOpp.Tax_From_Quote__c);
    }


}