@isTest
public with sharing class TaxExemptTest {
	
	static Opportunity opportunityObj {get;set;}
    static Quote quoteObj {get;set;}
    static Client__c clientObj {get;set;}

     @testsetup 
    static void test_setup(){
    	TestFactory.disableTriggers();
    	Account accountObj = TestFactory.createAccount();
    	insert accountObj;
    	Pricebook2 pb = TestFactory.createPriceBook();
    	insert pb;
		Product2 prod = TestFactory.createProduct();
		insert prod;
    	opportunityObj = TestFactory.createOpportunity(accountObj.Id, pb.Id);
    	insert opportunityObj;
    	quoteObj = TestFactory.createQuote(opportunityObj.Id, pb.Id);
    	insert quoteObj;
    	opportunityObj.SyncedQuoteId = quoteObj.Id;
    	update opportunityObj;
    	clientObj = TestFactory.createClient(accountObj.Id);
    	insert clientObj;
    }

    static void QueryStartingItems(){
        opportunityObj = database.Query('select '+BuildQuery('Opportunity'));
        quoteObj = database.Query('select '+BuildQuery('Quote'));
        clientObj = database.Query('select '+BuildQuery('Client__c'));
    }

    static string BuildQuery(String obj){
        Map<String, Schema.SObjectField> SobjtField = Schema.getGlobalDescribe().get(obj).getDescribe().fields.getMap();
        string qry = '';
        for (Schema.SObjectField s : SobjtField.values()) {
            qry += s + ',';
        }
        qry = qry.removeend(',');
        qry = qry+' from '+ obj;
        return qry;
    }

    @isTest
    static void Test_queryClient(){
    	QueryStartingItems();
    	opportunityObj.Client__c = clientObj.Id;
    	Map<Id,Client__c> clientMap = TaxExempt.queryClient(new List<Opportunity>{opportunityObj});
    	system.assertEquals(1, clientMap.size());
    	system.assert(clientMap.containsKey(clientObj.Id));
    }

    @isTest
    static void Test_queryOpportunities(){
    	QueryStartingItems();
    	Map<Id,Opportunity> opportunityMap = TaxExempt.queryOpportunities(new List<Quote>{quoteObj});
    	system.assertEquals(1,opportunityMap.size());
    	system.assert(opportunityMap.containsKey(quoteObj.OpportunityId));
    }

    @isTest
    static void Test_startFromQuote(){
    	QueryStartingItems();
    	clientObj.Tax_Exempt__c = true;
    	update clientObj;
    	opportunityObj.Client__c = clientObj.Id;
    	update opportunityObj;
    	TaxExempt.startFromQuote(new List<Quote>{quoteObj});
    	system.assertEquals(true, quoteObj.AVA_SFQUOTES__Non_Taxable__c);
    }

    @isTest
    static void Test_startFromOpportunity(){
    	QueryStartingItems();
    	clientObj.Tax_Exempt__c = true;
    	update clientObj;
    	opportunityObj.Client__c = clientObj.Id;
    	TaxExempt.startFromOpportunity(new List<Opportunity>{opportunityObj});
    	system.assertEquals(true,opportunityObj.AVA_SFCORE__NonTaxable__c);
    }

}