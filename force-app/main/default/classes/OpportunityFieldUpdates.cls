public with sharing class OpportunityFieldUpdates {
    
    private static final String ACCEPTED = Label.Accepted;
    private static final String ACCEPTED_AUTO = Label.Accepted_Auto;
    
    public static void beforeInsertTriggerUpdates(List<Opportunity> newOpps, Map<Id,Opportunity> oldMap){
        
        Map<Id,Contact> contactMap = getContactMapFromOpps(newOpps, null);
        
        for (Opportunity oppObj : newOpps) {
            updateOpportunityOwner(oppObj, oldMap);
            if(oppObj.Qualification_Contact__c != null && contactMap.containsKey(oppObj.Qualification_Contact__c)){
                updateOppFromContact(oppObj, contactMap.get(oppObj.Qualification_Contact__c));
            }
        }
        
    }
    
    public static void beforeUpdateTriggerUpdates(List<Opportunity> newOpps, Map<Id,Opportunity> oldMap){
        
        Map<Id,Contact> contactMap = getContactMapFromOpps(newOpps, oldMap);
        
        for (Opportunity oppObj : newOpps) {
            updateOpportunityOwner(oppObj, oldMap);
            addRandomNumberToOpp(oppObj);
            updateManagerJudgement(oppObj,oldMap);
            if(oppObj.Qualification_Contact__c != null && contactMap.containsKey(oppObj.Qualification_Contact__c)){
                updateOppFromContact(oppObj, contactMap.get(oppObj.Qualification_Contact__c));
            }
        }
        
    }
    
    @testVisible
    private static void updateOppFromContact(Opportunity oppObj, Contact contactObj){
        oppObj.Currently_engaged_in_data_collection__c = contactObj.Currently_engaged_in_data_collection__c;
        oppObj.Method_of_data_collection__c = contactObj.Method_of_data_collection__c;
        oppObj.Current_Solution__c = contactObj.Current_Solution__c;
        oppObj.Research_Target__c = contactObj.Research_Target__c;
        oppObj.Additional_Details__c = contactObj.Current_method_details__c;
        oppObj.Responses_per_Year__c = contactObj.Responses_per_Year__c;
        oppObj.Not_involved_in_data_collection__c = contactObj.Not_involved_in_data_collection__c;
        oppObj.Anticipated_Use_Case__c = contactObj.Anticipated_Use_Case__c;
        oppObj.Contract_Expiration__c = contactObj.Contract_Expiration__c;
        oppObj.Timeframe__c = contactObj.Timeframe__c;
        oppObj.Project_Status__c = contactObj.Project_Status__c;
        oppObj.Qualification_Notes__c = contactObj.Qualification_Notes__c;
        if(oppObj.Objectives__c == null){
            oppObj.Objectives__c = contactObj.Objectives__c;
        }
        if(oppObj.Impact__c == null){
            oppObj.Impact__c = contactObj.Impact__c;
        }
        if(oppObj.MEDDICC_Compelling_Event__c == null){
            oppObj.MEDDICC_Compelling_Event__c = contactObj.Compelling_Event__c;
        }
        if(oppObj.DA_Decision_Process__c == null){
            oppObj.DA_Decision_Process__c = contactObj.Decision_Process__c;
        }
        if(oppObj.Problems_Barriers__c == null){
            oppObj.Problems_Barriers__c = contactObj.Problems_Barriers__c;
        }
        if(oppObj.Needs__c == null){
            oppObj.Needs__c = contactObj.Needs__c;
        }
        if(oppObj.DA_Budget__c == null){
            oppObj.DA_Budget__c = contactObj.Budget__c;
        }
        if(oppObj.Notes__c == null){
            oppObj.Notes__c = contactObj.Notes__c;
        }
    }
    
    @testVisible
    private static Map<Id,Contact> getContactMapFromOpps(List<Opportunity> opps, Map<Id,Opportunity> oldMap){
        Set<Id> contactIdSet = new Set<Id>();
        for(Opportunity oppObj : opps){
            if(
                oppObj.Qualification_Contact__c != null 
                && (
                    oldMap == null
                    || (
                        oldMap.get(oppObj.Id) != null
                        && oldMap.get(oppObj.Id).Qualification_Contact__c == null
                    )
                )
            ){
                contactIdSet.add(oppObj.Qualification_Contact__c);
            }
        }
        Map<Id,Contact> contactMap = new Map<Id,Contact>();
        contactMap.putall([SELECT Currently_engaged_in_data_collection__c,Method_of_data_collection__c,Current_Solution__c,Research_Target__c,Current_method_details__c,Responses_per_Year__c,Not_involved_in_data_collection__c,Anticipated_Use_Case__c,Contract_Expiration__c,Timeframe__c,Project_Status__c,Qualification_Notes__c,
                           Objectives__c,Impact__c,Compelling_Event__c,Decision_Process__c,Problems_Barriers__c,Needs__c,
                           Budget__c,Notes__c FROM Contact WHERE Id in :contactIdSet]);
        return contactMap;
    }
    
    @testVisible
    private static void addRandomNumberToOpp(Opportunity oppObj){
        oppObj.Random_Number__c = Decimal.valueOf(Math.roundToLong(Math.Random()*9 +1));
    }
    
    @testVisible
    private static void updateOpportunityOwner(Opportunity oppObj, Map<Id,Opportunity> oldMap){
        if (
            oldMap != null
            && oppObj.Approval_Status__c != oldMap.get(oppObj.Id).Approval_Status__c
            && oppObj.Assign_To__c != null
            && (
                oppObj.Approval_Status__c == ACCEPTED
                || oppObj.Approval_Status__c == ACCEPTED_AUTO
            )
        ) {
            oppObj.OwnerId = oppObj.Assign_To__c;
        } else if (
            oldMap == null
            && oppObj.Assign_To__c != null
            && (
                oppObj.Approval_Status__c == ACCEPTED
                || oppObj.Approval_Status__c == ACCEPTED_AUTO
            )
        ) {
            oppObj.OwnerId = oppObj.Assign_To__c;
        }
    }
    
    @testVisible
    private static void updateManagerJudgement(Opportunity oppObj, Map<Id,Opportunity> oldMap){
        if (
            // oldMap != null
            // && oppObj.StageName != oldMap.get(oppObj.Id).StageName
            
            // && (
            oppObj.StageName == 'Lost'
            || oppObj.StageName == 'Cancelled'
            || oppObj.StageName == 'Invoice'
            || oppObj.StageName == 'Omit from Forecast'
            // )
        ) {
            oppObj.Manager_Judgement__c = oppObj.StageName;
        } 
    }
    @testVisible
    Public static void reformatSAPInformation(List<Opportunity> newOpps){
        for(Opportunity o : newOpps){
            if(o.SAP_Referral_Info__c != null){
                String unformatedInfo = o.SAP_Referral_Info__c;
                String formattedInfo = unformatedInfo.replace('_BR_ENCODED_','\n');
                o.SAP_Referral_Info__c = formattedInfo; 
            }
        }
    }
    
}