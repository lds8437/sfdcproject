public class ava_OverrideAddressValidation extends AVA_SFQUOTES.Quote_AddrValidation {
    private ApexPages.StandardController std;
    public Quote quote;
    private List<Client__c> clopps;     
    public String countryBillingName;
    public String countryFieldName;
    public String stateBillingName;
    public String stateFieldName;
    public id cId;
    
    // query for the additional custom fields to be used in FetchDestinationAddress and FetchTaxData
    
    public ava_OverrideAddressValidation(ApexPages.StandardController controller)
    {
        std = controller;
        getQuote();
        clopps = [select Id, Shipping_Street__c, Shipping_City__c, Shipping_Country__c,
                  Shipping_State__c, Shipping_Zip_Postal_Code__c
                  from Client__c where Id=:Quote.Client__c LIMIT 1];   
    }
    
    public Quote getQuote() {
        Quote = [SELECT Id, Client__c, Name, AVA_SFQUOTES__Shipping_Last_Validated__c, OpportunityId  from Quote WHERE ID =:ApexPages.currentPage().GetParameters().Get('Id')];
        system.debug('Quote: '+Quote.Id);
        return Quote;        
    }  
    
    public ava_OverrideAddressValidation(Id currAcct) {        
    }
    
    public System.PageReference CopyBillingToShipping() {
        return null;
    }
    
    public virtual override void FetchOriginalAddress()
    {        
        
        if(null != clopps[0].Shipping_Street__c)
        {
            List<String> Lines = clopps[0].Shipping_Street__c.split('\r\n');            
            oaOriginal.Line1 = Lines[0].trim();            
        }        
        oaOriginal.City = clopps[0].Shipping_City__c;
        oaOriginal.Region = clopps[0].Shipping_State__c;
        oaOriginal.PostalCode = clopps[0].Shipping_Zip_Postal_Code__c;
        oaOriginal.Country = clopps[0].Shipping_Country__c; 
    }
    
    public override System.PageReference UpdateAddress() {
        return null;
    }
    public override void UpdateStdController(AVA_SF_SDK.AddressSvc.ValidAddress ovaValAddr) {        
    }
    
    public void UpdateCAddress(){
        Client__c c = new Client__c();
        c.Id = clopps[0].Id;
        c.Shipping_Street__c = oaValidated.Line1;
        c.Shipping_City__c = oaValidated.City;
        c.Shipping_Country__c = oaValidated.Country;
        c.Shipping_State__c = oaValidated.Region;
        c.Shipping_Zip_Postal_Code__c = oaValidated.PostalCode;
        update c;
        
        Quote q = new Quote();
        q.Id = quote.Id;
        q.AVA_SFQUOTES__Shipping_Last_Validated__c  = string.valueof(date.today());
        update q;
    }
}