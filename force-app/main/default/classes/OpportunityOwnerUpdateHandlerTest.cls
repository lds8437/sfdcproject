/*
	Revision History:
    Version     Version Author    Date          Comments
	1.0			Prajakta Sanap	 21/05/2014   1. Test class for OpportunityOwnerUpdateHandlerTest

*/
@isTest
public class OpportunityOwnerUpdateHandlerTest {
	public static User objUser1;
	public static User objUser2;
	public static Opportunity objOpportunity;


	public static User initializeUser() {
		ID ProfileID = [ Select id from Profile where name = 'System Administrator'].id;
		User objUser = new User(email = 'test-user@fakeemail1.com',
		                        profileid = ProfileID, UserName = 'test-user@fakeemail1.com',
		                        alias = 'tuser11', CommunityNickName = 'tuser1', TimeZoneSidKey = 'America/New_York',
		                        LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',
		                        FirstName = 'Test_Parent_Fname1', LastName = 'Test_Parent_Lname1');
		return objUser;
	}

	public static Opportunity initOpportunity(User objUser) {
		Opportunity O = new Opportunity(Name = 'Test Opportunity'
		                                       , Assign_To__c = objUser2.Id
		                                               , OwnerId = objUser.Id
		                                                       , StageName = 'Solution Presentation'
		                                                               , CloseDate = system.today());
		return O;
	}

	@isTest
	public static void TestData() {
		List<User> lstUser = new List<User>();

		objUser1 = initializeUser();
		lstUser.add(objUser1);

		objUser2 = initializeUser();
		objUser2.UserName = 'test-user@fakeemail.com';
		objUser2.FirstName = 'Test_Parent_Fname2';
		objUser2.LastName = 'Test_Parent_Lname3';
		objUser2.CommunityNickname = 'tuser2';
		objUser2.Alias = 'tuser12';
		objUser2.email = 'usertest-user@fakeemail1.com';
		lstUser.add(objUser2);
		insert lstUser;

		//objOpportunity = initOpportunity(objUser1);
		//insert objOpportunity;
	}

	@isTest
	public static void updateOwnerTest() {
		//initializeUser();
		TestData();
		CreateSobjectForTesting csft = new CreateSobjectForTesting();
		objOpportunity = (Opportunity)csft.CreateObject('Opportunity');
		objOpportunity.Name = 'Test Opportunity';
		objOpportunity.Assign_To__c = objUser2.Id;
		//objOpportunity.StageName = 'Solution Presentation';
		objOpportunity.OwnerId = objUser1.Id;
		objOpportunity.CloseDate = system.today();
		objOpportunity.LID__LinkedIn_Company_Id__c = null;
		objOpportunity.StageName = 'Won';
        objOpportunity.ForecastCategoryName = 'Won';
        objOpportunity.Forecast_PercentageV2__c = 0;
        objOpportunity.Opp_from_Lead_Conversion__c = false;
		insert objOpportunity;


		Test.startTest();
		objOpportunity.Approval_Status__c = 'Accepted';
		update objOpportunity;
		Test.stopTest();
	}
}