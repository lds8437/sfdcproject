public class wls_CreateSurveyRecord {
    public static void createSurvey(Opportunity[] opps, Map<id,Opportunity> old, Map<id,Opportunity> oppMap){
        
        //Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>([
        //    SELECT Id, StageName, Amount, OwnerId, Products__c, Qualification_Contact__r.Email, Qualification_Contact__r.Name, Count_Of_Surveys__c, 
        //    Account_Name_Text__c, CloseDate, CreatedBy.Name, LeadSource, Owner.Name, Owner.Rep_Tier__c, Owner.Global_Region__c, Owner.Area__c, 
        //    Owner.Region__c, Owner.Sales_Team__c, RecordType.Name, Pricebook2.Name, Territory2.Name, of_Products__c, of_Responses__c, of_Users__c, 
        //    Account_Tier__c, Account_Type__c, Discount__c, Lead_Source_Detail__c, Lead_Type__c, Partner__r.Name, Product__c, 
        //    CreatedBy.Rep_Tier__c, CreatedBy.Global_Region__c, CreatedBy.Area__c, CreatedBy.Region__c, CreatedBy.Sales_Team__c, Owner.Team_Lead__r.Email 
        //    FROM Opportunity WHERE Id in : opps and Amount >0
        //    
        //    
        //]);
        

        //Set<Id> oppSet = oppMap.keySet();
        
        Set<Id> oppSet = new Set<Id>();
        for(Opportunity oppObj : oppMap.values()){
            if(oppObj.Amount > 0){
                oppSet.add(oppObj.Id);
            }
        }


        List<Survey__c> sur = new List<Survey__c>();
        
        
        for(Id o:oppSet){
            Opportunity oldopp = old.get(oppMap.get(o).Id);            
            
            if((oppMap.get(o).StageName == 'Won' || oppMap.get(o).StageName == 'Lost') && oppMap.get(o).Count_Of_Surveys__c == 0 && oppMap.get(o).RecordType.Name != 'Renewals'){
                if(oldOpp.StageName != oppMap.get(o).StageName){
                    Survey__c s = new Survey__c();
                    s.Opp_Amount__c = oppMap.get(o).Amount;
                    s.Opportunity__c = o;                   
                    s.Product_s__c = oppMap.get(o).Products__c;
                    s.Status__c = 'New';
                    s.Name = oppMap.get(o).StageName + ' Survey ' + Date.Today();
                    s.Contact_Email__c = oppMap.get(o).Qualification_Contact__r.Email;
                    s.Account__c = oppMap.get(o).Account_Name_Text__c;
                    s.Account_Type__c = oppMap.get(o).Account_Type__c;
                    s.Area__c = oppMap.get(o).Owner.Area__c;
                    s.CloseDate__c = oppMap.get(o).CloseDate;
                    s.Contact_Name__c = oppMap.get(o).Qualification_Contact__r.Name;
                    s.Discount__c = oppMap.get(o).Discount__c;
                    s.ForecastCategory__c = oppMap.get(o).StageName;
                    s.Global_Region__c = oppMap.get(o).Owner.Global_Region__c;
                    s.Historic_Product__c = oppMap.get(o).Product__c;
                    s.Lead_Source__c = oppMap.get(o).LeadSource;
                    s.Lead_Source_Detail__c = oppMap.get(o).Lead_Source_Detail__c;
                    s.Lead_Type__c = oppMap.get(o).Lead_Type__c;
                    s.Number_of_Products__c = oppMap.get(o).of_Products__c;
                    s.Number_of_Responses__c = oppMap.get(o).of_Responses__c;
                    s.Number_of_Users__c = oppMap.get(o).of_Users__c;
                    s.Opportunity_Owner__c = oppMap.get(o).Owner.Name;
                    s.Partner__c = oppMap.get(o).Partner__r.Name;
                    s.Pricebook__c = oppMap.get(o).Pricebook2.Name;
                    s.Record_Type__c = oppMap.get(o).RecordType.Name;
                    s.Sales_Rep_Region__c = oppMap.get(o).Owner.Region__c;
                    s.Sales_Team__c = oppMap.get(o).Owner.Sales_Team__c;
                    s.Sales_Tier__c = oppMap.get(o).Owner.Rep_Tier__c;
                    s.Territory__c = oppMap.get(o).Territory2.Name;
                    s.Created_By_Area__c = oppMap.get(o).CreatedBy.Area__c;
                    s.Created_By_Global_Region__c = oppMap.get(o).CreatedBy.Global_Region__c;
                    s.Created_By_Region__c = oppMap.get(o).CreatedBy.Region__c;
                    s.Created_By_Team__c = oppMap.get(o).CreatedBy.Sales_Team__c;
                    s.Created_By_Tier__c = oppMap.get(o).CreatedBy.Rep_Tier__c;
                    s.Team_Lead_Email__c = oppMap.get(o).Owner.Team_Lead__r.Email;
                    sur.add(s);              
                    
                }                
            }
        }           
        
        insert sur;
        
    }
}