public without sharing class Event_AttachODRsPersonalCampaign {
    /* ---
    private static final String EVENT_SUBJECTNAME = Label.Event_SubjectName;
    private static final String PROFILE_NAME = Label.Profile_Name;
    private static final String CONTACT_OBJECTLABEL = Label.Contact_ObjectLabel;
    private static final String LEAD_OBJECTLABEL = Label.Lead_ObjectLabel;
    private static final String EVENT_PROFILENAME = Label.Event_ProfileName;
    
    private static boolean flagvalue = false;
    
    public ID profileId { set; get; }
    public list<Event> eventOldList { set; get; }
    public list<Event> eventNewList { set; get; }
    public map<Id, Event> eventOldListMap { set; get; }
    public map<Id, Event> eventNewListMap { set; get; }
    
    public Event_AttachODRsPersonalCampaign(Event[] eventOldList, Event[] eventNewList) {
        this.eventNewList = eventNewList == null ? new Event[] {} : eventNewList.clone();
        this.eventOldList = eventOldList == null ? new Event[] {} : eventOldList.clone();
        this.eventOldListMap = new Map<Id, Event> {};
        this.eventNewListMap = new Map<Id, Event> {};
        
        // Build the old map
        for(Event eventOld : this.eventOldList) {
            if (eventOld.Id != null) {
                eventOldListMap.put(eventOld.Id, eventOld);
            }
        }
        //Building the new mapk
        for(Event eventNew : this.eventNewList){
            if(eventNew.id != null){
                eventNewListMap.put(eventNew.id, eventNew);
            }
        }
        
        //this.profileId = [Select Name, Id From Profile where Name =: PROFILE_NAME].Id;
    }
    
    public static boolean hasAlreadyfired() {
         return flagvalue;
      }

    public static void setAlreadyfired(){
        flagvalue = true;
    }
    
    public void execute() {
        map<ID, ID> userIdToContactIdMap = new map<ID, ID>{};
        map<ID, list<Campaign>> contactIdToCampaignsMap = new map<ID, list<Campaign>>{};
        
        map<ID, ID> userIdToLeadIdMap = new map<ID, ID>{};
        map<ID, list<Campaign>> leadIdToCampaignsMap = new map<ID, list<Campaign>>{};
        
        for (Event event : eventNewList) {
            if(executable(event)) {
                String objName = getObjectAPIName(event.WhoId);
                if(objName != null && objName == CONTACT_OBJECTLABEL){
                    System.debug('This is the Contact Object Execute Code------------------------');
                    //userIdToContactIdMap.put(event.OwnerId, event.WhoId);
                    userIdToContactIdMap.put(event.CreatedByID, event.WhoId);                    
                }
                else if(objName != null && objName == LEAD_OBJECTLABEL){
                    System.debug('This is the Lead Object Execute Code------------------------');
                    //userIdToLeadIdMap.put(event.OwnerId, event.WhoId);
                    userIdToLeadIdMap.put(event.CreatedByID, event.WhoId);                    
                }
            }
        }
        
        if(!userIdToContactIdMap.isEmpty() && userIdToContactIdMap != null) {
            for(String userId : userIdToContactIdMap.keySet()) {
                list<Campaign> campaignList = getCampaignList(userId);
                contactIdToCampaignsMap.put(userIdToContactIdMap.get(userId), campaignList);
            }
        }
        
        if(!userIdToLeadIdMap.isEmpty() && userIdToLeadIdMap != null) {
            for(String userId : userIdToLeadIdMap.keySet()) {
                list<Campaign> campaignList = getCampaignList(userId);
                leadIdToCampaignsMap.put(userIdToLeadIdMap.get(userId), campaignList);
            }
        }
        
        if(!contactIdToCampaignsMap.isEmpty() && contactIdToCampaignsMap != null) {
            createCampaignMembersForContact(contactIdToCampaignsMap);
        }
        
        if(!leadIdToCampaignsMap.isEmpty() && leadIdToCampaignsMap != null) {
            createCampaignMembersForLead(leadIdToCampaignsMap);
        }
    }
    
    public Boolean executable(Event eventNew) {
        Boolean Ex;
        list<Profile> profileList = [Select Name, Id From Profile where Name LIKE: EVENT_PROFILENAME];
        system.debug('--- profileList --->' + profileList);
        if(profileList != null && profileList.size() > 0){
            
        }
        Id ownerProfileId = userinfo.getProfileId();
        
        //Check to see if current user profile is in the ProfileList.
        For(Profile Pro: profileList){
            if(Pro.ID == ownerProfileID){
                Ex = True;
            }
        }    --- */
        /*
        system.debug('--- ownerProfileId --->' + ownerProfileId);
        if(ownerProfileId == this.profileId){
            system.debug('--- inside --->');
            if(eventNew.Subject != null && eventNew.Subject == EVENT_SUBJECTNAME) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }*/
        
        /* ---
        if(eventNew.Subject != null && Ex == True)// && eventNew.Subject == EVENT_SUBJECTNAME) 
        {
            return true;
        } else {
            return false;
        }
    }
    
    public string getObjectAPIName(String whoId){
        String objectName;
        String keyCode  = whoId.subString(0,3);
        map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
        for(Schema.SObjectType objectInstance : globalDescribe.values()) {
            if(objectInstance.getDescribe().getKeyPrefix() == keyCode) {
                objectName = objectInstance.getDescribe().getName();
            }
        }
        return objectName;
    }
    
    public list<Campaign> getCampaignList(String OwnerId){
        list<Campaign> campaignLst = new list<Campaign>();
        if(OwnerId != null) {
            campaignLst = [
                    Select OwnerId
                         , Id
                      From Campaign
                     where OwnerId =: OwnerId
            ];
        }
        return campaignLst;
    }
    
    public void createCampaignMembersForContact(map<ID, list<Campaign>> contactIdToCampaignsMap){
        list<CampaignMember> campaignMemberForContactList = new list<CampaignMember>();
        
        list<CampaignMember> campaignMemberList = [
                                            Select LeadId
                                                 , Id
                                                 , ContactId
                                                 , CampaignId
                                              From CampaignMember
                                             where ContactId =: contactIdToCampaignsMap.keySet()
        ];
        
        map<Id, Id> camToContactCMMap = new Map<Id, Id>();
        if(campaignMemberList != null && campaignMemberList.size() > 0) {
            for (CampaignMember cm : campaignMemberList) {
                camToContactCMMap.put(cm.CampaignId, cm.ContactId);
            }
        }
        
        for(String contactId : contactIdToCampaignsMap.keySet()) {
            for(Campaign campaign : contactIdToCampaignsMap.get(contactId)) {
                if(!camToContactCMMap.isEmpty() && camToContactCMMap != null){
                    if(!camToContactCMMap.containsKey(campaign.Id) && !(camToContactCMMap.get(campaign.Id) == contactId)){
                        CampaignMember campaignMember = new CampaignMember(CampaignId = campaign.Id, ContactId= contactId);
                        campaignMemberForContactList.add(campaignMember);
                    }
                }
                else {
                    CampaignMember campaignMember = new CampaignMember(CampaignId = campaign.Id, ContactId= contactId);
                    campaignMemberForContactList.add(campaignMember);
                }
            }
        }
        
        if(campaignMemberForContactList != null && campaignMemberForContactList.size() > 0){
            upsert campaignMemberForContactList;
        }
    }
    
    public void createCampaignMembersForLead(map<ID, list<Campaign>> leadIdToCampaignsMap){
        list<CampaignMember> campaignMemberForLeadList = new list<CampaignMember>();
        
        list<CampaignMember> campaignMemberList = [
                                            Select LeadId
                                                 , Id
                                                 , ContactId
                                                 , CampaignId
                                              From CampaignMember
                                             where LeadId =: leadIdToCampaignsMap.keySet()
        ];
        
        map<Id, Id> camTOLeadCMMap = new Map<Id, Id>();
        if(campaignMemberList != null && campaignMemberList.size() > 0) {
            for (CampaignMember cm : campaignMemberList) {
                camTOLeadCMMap.put(cm.CampaignId, cm.LeadId);
            }
        }
        
        for(String leadId : leadIdToCampaignsMap.keySet()) {
            for(Campaign campaign : leadIdToCampaignsMap.get(leadId)) {
                if(!camTOLeadCMMap.isEmpty() && camTOLeadCMMap != null){
                    if(!camTOLeadCMMap.containsKey(campaign.Id) && !(camTOLeadCMMap.get(campaign.Id) == leadId)){
                        CampaignMember campaignMember = new CampaignMember(CampaignId = campaign.Id, LeadId= leadId);
                        campaignMemberForLeadList.add(campaignMember);
                    }
                }
                else {
                    CampaignMember campaignMember = new CampaignMember(CampaignId = campaign.Id, LeadId= leadId);
                    campaignMemberForLeadList.add(campaignMember);
                }
            }
        }
        
        if(campaignMemberForLeadList != null && campaignMemberForLeadList.size() > 0){
            upsert campaignMemberForLeadList;
        }
    }  --- */
}