public with sharing class ContractDocumentTriggerHandler {
	public static boolean firstRunOnBeforeInsert = true;
	public static boolean firstRunOnAfterInsert = true;
	public static boolean firstRunOnBeforeUpdate = true;
	public static boolean firstRunOnAfterUpdate = true;
	public static boolean firstRunOnBeforeDelete = true;
	public static boolean firstRunOnAfterDelete = true;
	public static boolean firstRunOnUndelete = true;
	
	
	public void OnBeforeInsert(Contract_Document__c[] newObjects){
		if(firstRunOnBeforeInsert){
			firstRunOnBeforeInsert = false;
		}
	}
	
	public void OnAfterInsert(Contract_Document__c[] newObjects, map<id,Contract_Document__c> MapNewMap){
		if(firstRunOnAfterInsert){
			firstRunOnAfterInsert = false;
			cdrl_ContractTemplates.createQuoteLines(newObjects, null);
            cdrl_ContractTemplates.createMultiYearQuoteLines(newObjects, null);
            cdrl_ContractTemplates.createQSOStandalone(newObjects, null);
            cdrl_ContractTemplates.createQSOLines(newObjects, null); 
		//	ClientAddressUpdate.startAddressUpdateProcess(MapNewMap, null);
		}
	}
	
	public void OnBeforeUpdate(Contract_Document__c[] oldObjects, Contract_Document__c[] updatedObjects, map<id,Contract_Document__c> MapNewMap, map<id,Contract_Document__c> MapOldMap){
		if(firstRunOnBeforeUpdate){
			firstRunOnBeforeUpdate = false;
			
		}
	}
	
	public void OnAfterUpdate(Contract_Document__c[] oldObjects, Contract_Document__c[] updatedObjects, map<id,Contract_Document__c> MapNewMap, map<id,Contract_Document__c> MapOldMap){
		if(firstRunOnAfterUpdate){
			firstRunOnAfterUpdate = false;
			cdrl_ContractTemplates.createImpLines(updatedObjects, MapOldMap);
            cdrl_ContractTemplates.createQuoteLines(updatedObjects, MapOldMap);
            cdrl_ContractTemplates.createMultiYearQuoteLines(updatedObjects, MapOldMap);
            cdrl_ContractTemplates.createQSOLangLines(updatedObjects, MapOldMap);
            cdr_ContractDocumentUpdates.updateMSA(updatedObjects, MapOldMap);
         //   cdr_ContractDocumentUpdates.updateAccount(updatedObjects, MapOldMap);
         //   cdr_ContractDocumentUpdates.updateClient(updatedObjects, MapOldMap);            
         //   cdr_ContractDocumentUpdates.updatePriCon(updatedObjects, MapOldMap);
		//	ClientAddressUpdate.startAddressUpdateProcess(MapNewMap, MapOldMap);
		}
	}
	
	public void OnBeforeDelete(Contract_Document__c[] ObjectsToDelete, map<id,Contract_Document__c> MapNewMap, map<id,Contract_Document__c> MapOldMap){
		if(firstRunOnBeforeDelete){
			firstRunOnBeforeDelete = false;
	
		}
	}
	
	public void OnAfterDelete(Contract_Document__c[] deletedObjects, map<id,Contract_Document__c> MapOldMap){
		if(firstRunOnAfterDelete){
			firstRunOnAfterDelete = false;
	
		}
	}
	
	public void OnUndelete(Contract_Document__c[] restoredObjects){
		if(firstRunOnUndelete){
			firstRunOnUndelete = false;
			
		}
	}
	
}