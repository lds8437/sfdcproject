@IsTest
public class cpq_RulesTestClass {
	@isTest static void testServiceBundleProductQuery() {
		//Create new bundle
		Bundle__c b = new Bundle__c();
        b.Name = 'Test bundle';
        b.Experience__c = 'CX';
        b.Bundle__c = '0';
        b.Active__c = TRUE;
        insert b;
        
        //Create new bundle product
		Product_Bundle_Junction__c bp = new Product_Bundle_Junction__c();
        bp.Name = 'Test Bundle Product';
        bp.Related_Bundle__c = b.Id;
        insert bp;
        
        //Create new service bundle
        Service_Bundle__c sb = new Service_Bundle__c();
        sb.Name = 'Test Service Bundle';
        sb.Bundle_Product_Prerequisite__c = bp.Id;
        insert sb;
        
        //Create new service bundle product
        Service_Bundle_Product_Junction__c sbpj = new Service_Bundle_Product_Junction__c();
        sbpj.Name = 'Test service bundle product junction';
        sbpj.Availability__c = 'Available';
        sbpj.Related_Service_Bundle__c = sb.Id;
        insert sbpj;
        
        Service_Bundle_Product_Junction__c[] newQuery = cpq_Rules.getServiceBundleProducts();
    }
    
    @isTest static void testBundleProductsQuery() {
        //Create new bundle
        Bundle__c b = new Bundle__c();
        b.Name = 'Test bundle';
        b.Experience__c = 'CX';
        b.Bundle__c = '0';
        b.Active__c = TRUE;
        insert b;
        
        //Create new bundle product
        Product_Bundle_Junction__c bp = new Product_Bundle_Junction__c();
        bp.Name = 'Test Bundle Product';
        bp.Related_Bundle__c = b.Id;
        insert bp;
        
        Product_Bundle_Junction__c[] newQuery = cpq_Rules.getBundleProducts();
    }   
    
    @isTest static void testBundleNamesById(){
        //Create new bundle
        Bundle__c b = new Bundle__c();
        b.Name = 'Test bundle';
        b.Experience__c = 'CX';
        b.Bundle__c = '0';
        b.Active__c = TRUE;
        insert b;
        
        Map<String, String> newQuery = cpq_Rules.getBundleNamesById();
    }
}