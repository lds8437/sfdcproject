@isTest 
public without sharing class PartnerSurveyClassTest {
    
    @istest static void testGetOpportunityWithOpenOpp() {
        
        TestFactory.disableTriggers();

        Account acctObj = TestFactory.createAccount();
        insert acctObj;

        Q_Partner__c partnerObj = TestFactory.createPartner(acctObj.Id);
        insert partnerObj;

        Pricebook2 pb2Obj = TestFactory.createPriceBook();
        insert pb2Obj;

        Product2 prodObj = TestFactory.createProduct();
        insert prodObj;

        PricebookEntry standardPbe = TestFactory.createPricebookEntry(Test.getStandardPricebookId(), prodObj.Id);
        insert standardPbe;

        PricebookEntry customPbe = TestFactory.createPricebookEntry(pb2Obj.Id, prodObj.Id);
        insert customPbe;

        Opportunity oppObj = TestFactory.createOpportunity(acctObj.Id, pb2Obj.Id);
        oppObj.Partner__c = partnerObj.Id;
        insert oppObj;        

        Quote quoteObj = TestFactory.createQuote(oppObj.Id, pb2Obj.Id);
        insert quoteObj;

        QuoteLineItem qliObj = TestFactory.createQuoteLineItem(quoteObj.Id, customPbe.Id);
        qliObj.Partner__c = partnerObj.Id;
        insert qliObj;
        
        

        List<Partner_Product_Opportunity__c> ppoList = [SELECT Id FROM Partner_Product_Opportunity__c];
        system.assertEquals(0, ppoList.size());

        Test.startTest();
            PartnerSurveyClass.getOpportunity(new List<Opportunity>{oppObj});
        Test.stopTest();
        ppoList = [SELECT Id FROM Partner_Product_Opportunity__c];
        system.assertEquals(0, ppoList.size());        
    }

    @istest static void testGetOpportunityWithClosedOpp() {
        
        TestFactory.disableTriggers();

        Account acctObj = TestFactory.createAccount();
        insert acctObj;

        Q_Partner__c partnerObj = TestFactory.createPartner(acctObj.Id);
        insert partnerObj;

        Pricebook2 pb2Obj = TestFactory.createPriceBook();
        insert pb2Obj;

        Product2 prodObj = TestFactory.createProduct();
        insert prodObj;

        PricebookEntry standardPbe = TestFactory.createPricebookEntry(Test.getStandardPricebookId(), prodObj.Id);
        insert standardPbe;

        PricebookEntry customPbe = TestFactory.createPricebookEntry(pb2Obj.Id, prodObj.Id);
        insert customPbe;

        Opportunity oppObj = TestFactory.createOpportunity(acctObj.Id, pb2Obj.Id);
        oppObj.Partner__c = partnerObj.Id;
        oppObj.StageName = 'Won';
        insert oppObj;        

        Quote quoteObj = TestFactory.createQuote(oppObj.Id, pb2Obj.Id);
        insert quoteObj;

        QuoteLineItem qliObj = TestFactory.createQuoteLineItem(quoteObj.Id, customPbe.Id);
        qliObj.Partner__c = partnerObj.Id;
        insert qliObj;
        
        

        List<Partner_Product_Opportunity__c> ppoList = [SELECT Id FROM Partner_Product_Opportunity__c];
        system.assertEquals(0, ppoList.size());

        Test.startTest();
            PartnerSurveyClass.getOpportunity(new List<Opportunity>{oppObj});
        Test.stopTest();
        ppoList = [SELECT Id FROM Partner_Product_Opportunity__c];
        system.assertEquals(1, ppoList.size());
        
    }


}