public with sharing class RedirectionValidation {

    /**
     * Convenience method to perform a validation directly
     * against a custom metadata record and an SObject.  The
     * proper validation is performed based on the comparison
     * operator specified on the custom metadata record.
     */
    public static Boolean validateField(Redirection_Field__mdt field, SObject record) {
        if (field.Comparison_Operator__c == RedirectionConstants.COMPARISON_NULL) {
			return validateIsNull(field, record);
        } else if (field.Comparison_Operator__c == RedirectionConstants.COMPARISON_NOT_NULL) {
            return validateIsNotNull(field, record);
        } else if (field.Comparison_Operator__c == RedirectionConstants.COMPARISON_EQUAL_TO) {
            return validateEqualTo(field, record);
        } else if (field.Comparison_Operator__c == RedirectionConstants.COMPARISON_NOT_EQUAL_TO) {
            return validateNotEqualTo(field, record);
        }
        return false;
    }
    
    /**
     * Validate if the record field value is null.
     */
    private static Boolean validateIsNull(Redirection_Field__mdt field, SObject record) {
        if (record.get(field.Field_Name__c) == null) {
			return true;
        }
        return false;
    }
    
    /**
     * Validate if the record field value is NOT null.
     */
    private static Boolean validateIsNotNull(Redirection_Field__mdt field, SObject record) {
        if (record.get(field.Field_Name__c) != null) {
			return true;
        }
        return false;
    }
    
    /**
     * Validate if the record field value is equal to the
     * value specified in the custom metadata record.
     */
    private static Boolean validateEqualTo(Redirection_Field__mdt field, SObject record) {
        String fieldType = RedirectionUtilities.getFieldType(record.get(field.Field_Name__c));
        if (record.get(field.Field_Name__c) == null
            	|| (fieldType == RedirectionConstants.DATA_TYPE_BOOLEAN && (record.get(field.Field_Name__c) == Boolean.valueOf(field.Field_Value__c)))
        		|| (fieldType == RedirectionConstants.DATA_TYPE_DECIMAL && (record.get(field.Field_Name__c) == Decimal.valueOf(field.Field_Value__c)))
        		|| (fieldType == RedirectionConstants.DATA_TYPE_OBJECT && (record.get(field.Field_Name__c) == field.Field_Value__c))) {
			return true;
        }
        return false;
    }
    
    /**
     * Validate if the record field value is NOT equal to the
     * value specified in the custom metadata record.
     */
    private static Boolean validateNotEqualTo(Redirection_Field__mdt field, SObject record) {
        String fieldType = RedirectionUtilities.getFieldType(record.get(field.Field_Name__c));
        if (record.get(field.Field_Name__c) == null
            	|| (fieldType == RedirectionConstants.DATA_TYPE_BOOLEAN && (record.get(field.Field_Name__c) != Boolean.valueOf(field.Field_Value__c)))
        		|| (fieldType == RedirectionConstants.DATA_TYPE_DECIMAL && (record.get(field.Field_Name__c) != Decimal.valueOf(field.Field_Value__c)))
        		|| (fieldType == RedirectionConstants.DATA_TYPE_OBJECT && (record.get(field.Field_Name__c) != field.Field_Value__c))) {
			return true;
        }
        return false;
    }
}