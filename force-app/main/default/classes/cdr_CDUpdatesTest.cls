@isTest
public class cdr_CDUpdatesTest {
    public static testMethod void testcdup(){
        
        List<APXTConga4__Conga_Template__c> conga = new List<APXTConga4__Conga_Template__c>();
        
        APXTConga4__Conga_Template__c conga1 = new APXTConga4__Conga_Template__c ();
        conga1.APXTConga4__Description__c = '1';
        conga1.APXTConga4__Template_Group__c = 'Quote';
        conga1.APXTConga4__Name__c = 'Quote';
        conga.add(conga1);
        
        APXTConga4__Conga_Template__c conga2 = new APXTConga4__Conga_Template__c ();
        conga2.APXTConga4__Description__c = '1';
        conga2.APXTConga4__Template_Group__c = 'QSO Required';
        conga2.APXTConga4__Name__c = 'QSO Required';
        conga.add(conga2);
        
        APXTConga4__Conga_Template__c conga3 = new APXTConga4__Conga_Template__c ();
        conga3.APXTConga4__Description__c = '1';
        conga3.APXTConga4__Template_Group__c = 'QSO';
        conga3.APXTConga4__Name__c = 'QSO with MSA';
        conga.add(conga3);
        
        APXTConga4__Conga_Template__c conga4 = new APXTConga4__Conga_Template__c ();
        conga4.APXTConga4__Description__c = '1';
        conga4.APXTConga4__Template_Group__c = 'QSO';
        conga4.APXTConga4__Name__c = 'QSO Standalone';
        conga.add(conga4);
        
        APXTConga4__Conga_Template__c conga5 = new APXTConga4__Conga_Template__c ();
        conga5.APXTConga4__Description__c = '1';
        conga5.APXTConga4__Template_Group__c = 'Implementation';
        conga5.APXTConga4__Name__c = 'MRCXSTD';
        conga.add(conga5);
        
        APXTConga4__Conga_Template__c conga6 = new APXTConga4__Conga_Template__c ();
        conga6.APXTConga4__Description__c = '1';
        conga6.APXTConga4__Template_Group__c = 'Implementation';
        conga6.APXTConga4__Name__c = 'MRCXPLU';
        conga.add(conga6);
        
        APXTConga4__Conga_Template__c conga7 = new APXTConga4__Conga_Template__c ();
        conga7.APXTConga4__Description__c = '1';
        conga7.APXTConga4__Template_Group__c = 'Quote';
        conga7.APXTConga4__Name__c = 'Quote - Itemized';
        conga.add(conga7);
        
        insert conga;
        
        Opportunity opp = (Opportunity)EB_Utility_CreateObjectForTesting.CreateObject('Opportunity');
        Opp.LID__LinkedIn_Company_Id__c = '5';
        opp.Forecast_PercentageV2__c = 0;
        opp.Opp_from_Lead_Conversion__c = false; 
        opp.Client_Use_Case__c = 'Market Research';        
        opp.CurrencyIsoCode = 'USD';
        insert opp;
        system.debug('opp: '+opp);
        
        Account acc = new Account();
        acc.Name = 'TEST ACCOUNT';
        acc.Tier__c ='A';
        insert acc;
        
        Client__c cli = New Client__c();
        cli.Name = 'TEST CLIENT';
        cli.Billing_City__c = 'Provo';
        cli.Account__c = acc.Id;
        insert cli;  
        
        Contract_Document__c cd = New Contract_Document__c();
        cd.Opportunity__c = opp.id;        
        insert cd;
        system.debug('cd: '+cd);
        
        cd.MSA__c = TRUE;
        cd.Client__c = cli.Id;
        cd.Implementation__c = 'MRCXSTD';
        update cd;
        
        Contract_Document__c cd2 = New Contract_Document__c();
        cd2.Opportunity__c = opp.id; 
        cd2.MSA__c = TRUE;        
        insert cd2;
        system.debug('cd2: '+cd2);
        
        cd2.MSA__c = FALSE;
        update cd2;
        
        
        
        
    }
    
    public static testMethod void testcdup2(){
        
        List<APXTConga4__Conga_Template__c> conga = new List<APXTConga4__Conga_Template__c>();
        
        APXTConga4__Conga_Template__c conga1 = new APXTConga4__Conga_Template__c ();
        conga1.APXTConga4__Description__c = '1';
        conga1.APXTConga4__Template_Group__c = 'Quote';
        conga1.APXTConga4__Name__c = 'Quote';
        conga.add(conga1);
        
        APXTConga4__Conga_Template__c conga2 = new APXTConga4__Conga_Template__c ();
        conga2.APXTConga4__Description__c = '1';
        conga2.APXTConga4__Template_Group__c = 'QSO Required';
        conga2.APXTConga4__Name__c = 'QSO Required';
        conga.add(conga2);
        
        APXTConga4__Conga_Template__c conga3 = new APXTConga4__Conga_Template__c ();
        conga3.APXTConga4__Description__c = '1';
        conga3.APXTConga4__Template_Group__c = 'QSO';
        conga3.APXTConga4__Name__c = 'QSO with MSA';
        conga.add(conga3);
        
        APXTConga4__Conga_Template__c conga4 = new APXTConga4__Conga_Template__c ();
        conga4.APXTConga4__Description__c = '1';
        conga4.APXTConga4__Template_Group__c = 'QSO';
        conga4.APXTConga4__Name__c = 'QSO Standalone';
        conga.add(conga4);
        
        APXTConga4__Conga_Template__c conga5 = new APXTConga4__Conga_Template__c ();
        conga5.APXTConga4__Description__c = '1';
        conga5.APXTConga4__Template_Group__c = 'Implementation';
        conga5.APXTConga4__Name__c = 'MRCXSTD';
        conga.add(conga5);
        
        APXTConga4__Conga_Template__c conga6 = new APXTConga4__Conga_Template__c ();
        conga6.APXTConga4__Description__c = '1';
        conga6.APXTConga4__Template_Group__c = 'Implementation';
        conga6.APXTConga4__Name__c = 'MRCXPLU';
        conga.add(conga6);
        
        APXTConga4__Conga_Template__c conga7 = new APXTConga4__Conga_Template__c ();
        conga7.APXTConga4__Description__c = '1';
        conga7.APXTConga4__Template_Group__c = 'Quote';
        conga7.APXTConga4__Name__c = 'Quote - Itemized';
        conga.add(conga7);
        
        insert conga;
        
        Opportunity opp = (Opportunity)EB_Utility_CreateObjectForTesting.CreateObject('Opportunity');
        Opp.LID__LinkedIn_Company_Id__c = '5';
        opp.Forecast_PercentageV2__c = 0;
        opp.Opp_from_Lead_Conversion__c = false; 
        opp.Client_Use_Case__c = 'Market Research';        
        opp.CurrencyIsoCode = 'USD';
        insert opp;
        system.debug('opp: '+opp);
        
        Account acc = new Account();
        acc.Name = 'TEST ACCOUNT';
        acc.Tier__c ='A';
        insert acc;
        
        Client__c cli = New Client__c();
        cli.Name = 'TEST CLIENT';
        cli.Billing_City__c = 'Provo';
        cli.Account__c = acc.Id;
        insert cli;  
        
        Contract_Document__c cd2 = New Contract_Document__c();
        cd2.Opportunity__c = opp.id; 
        cd2.MSA__c = TRUE;
		cd2.Status__c = '';        
        insert cd2;
        system.debug('cd2: '+cd2);
        
        Contract_Document__c cd3 = New Contract_Document__c();
        cd3.Opportunity__c = opp.id; 
        cd3.MSA__c = TRUE;   
        cd3.Status__c = 'Review Contract Details';
      
        
        cd2.MSA__c = FALSE;
        cd2.Language_Type__c = 'Standalone';
        update cd2;
        
        Contact con = new Contact();
        con.FirstName = 'Gina';
        con.LastName = 'TestQualtrics543';
        con.AccountId = acc.Id;
        
        string var1 = '123456789';
        Boolean conDoc = fab_Controller.createContractDocument(opp.Id);
        Contract_Document__c conDoc2 = fab_Controller.getContractDocument(opp.Id);
        Opportunity opp2 = fab_Controller.getOpportunity(opp.Id);
        List<APXTConga4__Conga_Merge_Query__c> conQue = fab_Controller.getCongaQueries();
        List<String> options = fab_Controller.getselectOptions(cd2, 'Status__c');
       // List<Contact> ct = fab_Controller.search('Gina', new list<string>{var1}, acc.Id) ;
        sObject conDoc3 = fab_Controller.updateRecord(cd2);
            List<sObject> conDoc4 = fab_Controller.insertRecords(new list<sObject>{cd3});
            List<APXTConga4__Conga_Template__c> ct2 = fab_Controller.getCongaTemplates();
            Boolean cdt = fab_Controller.deleteTemplates(cd2.Id);
        
    }
        
    
}