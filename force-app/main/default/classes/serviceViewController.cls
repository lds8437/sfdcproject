/*
Last Edited by: Kruse Collins
12 March, 2018
Related to serviceView.vfp
-------------------------------------------------------------------------------------------------
Description: Test class for XM_DiscountsController apex controller of the page XM_Discounts
============================================================================================================================
    Version | Date(DD-MM-YYYY) | Comments
----------------------------------------------------------------------------------------------------------------------------
    1.0     | 22-03-2018       | 1. Finished building class, VR page, and test class.
=============================================================================================================================
*/

public class serviceViewController {
    
    public Opportunity opp; //Create opp var
    public List<QService__c> getQServices{get; set;} //List to pull in values from Qservice__c for related list.
    public boolean section1{get; set;} //used for deciding what to render on VF page.
    public boolean section2{get; set;} //used for deciding what to render on VF page.

    
    //Controller Extension
    public serviceViewController(ApexPages.StandardController controller) {
        this.opp = (Opportunity)controller.getRecord();
     
        //Pull in columns for QService__c and load into getQServices
        getQServices = [SELECT Id, Name, Total_Service_Cost__c, Status__c, Q_Partner__c, Service_Rep__c, Service_Type__c  
                        FROM QService__c
                        WHERE Opportunity__c = :opp.Id 
                        AND Synced_Service__c = TRUE
                        ];
		
        //Set to TRUE if there IS services on the synced quote.
        boolean recordsExist = ![SELECT Id FROM QService__c WHERE Opportunity__c = :opp.Id AND Synced_Service__c = TRUE LIMIT 1].isEmpty();
		
        //If records do exist, set section1 to TRUE to render the related list, otherwise display catch error VF display.
        if(recordsExist == TRUE){
            section1 = TRUE;
          	section2 = FALSE;
        } else if(recordsExist == FALSE){
            section1 = FALSE;
            section2 = TRUE;
        }
    }
}