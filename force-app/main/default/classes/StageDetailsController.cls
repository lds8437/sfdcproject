public with sharing class StageDetailsController {
    
    /**
     * Returns a list of completion criteria related to the specified:
     * - scope: Type of user (Sales or Research)
     * - record id: Id of the SObject being viewed.
     * - field name: Picklist field name on the SObject representing
     *      its current state.  Guided_Sales_Path__c, for example.
     * - field value: Picklist field value on the SObject representing
     *      its current state.  Discovery, for example.
     */
	@AuraEnabled
    public static List<CompletionCriteria> getCompletionCriteriaRecords(String scope, Id recordId, String fieldName, String fieldValue) {
        
        // Exit immediately if any of the required properties are
        // not provided.
        if (scope == null || recordId == null || fieldName == null || fieldValue == null) {
            return new List<CompletionCriteria>();
        }
        
        // Populate a list of completion criteria records matching
        // the properties provided.
        List<Completion_Criteria__mdt> completionCriteriaRecords = getCompletionCriteriaRecords(scope, String.valueOf(recordId.getSobjectType()), fieldName, fieldValue);
        
        // Obtain a list of unique field names from the configurations.
		List<String> distinctFieldNames = getDistinctFieldNames(completionCriteriaRecords);
        
        // Query for the SObject based on the record id provided.  Include
        // all fields to match the unique field names compiled above.
        SObject sObj = getSObject(recordId, distinctFieldNames);
        
        // Convert the list of completion criteria records to a wrapped
        // and formatted version that checks the boxes as appropriate.
        List<CompletionCriteria> formattedCompletionRecords = new List<CompletionCriteria>();
        for (Completion_Criteria__mdt record : completionCriteriaRecords) {
            formattedCompletionRecords.add(new CompletionCriteria(record, sObj));
        }
        
        // Sort and return the results.
        formattedCompletionRecords.sort();
        return formattedCompletionRecords;
    }
    
    /**
     * Returns a list of stage progresses related to the specified:
     * - scope: Type of user (Sales or Research)
     * - record id: Id of the SObject being viewed.
     * All related field values and the calculated completion
     * percentage for the given record id are returned.
     */
	@AuraEnabled
    public static List<StageProgress> getStageProgress(String scope, Id recordId) {
        
        // Exit immediately if either of the required properties
        // are not provided.
        if (scope == null || recordId == null) {
            return new List<StageProgress>();
        }
        
        // Exit immediately if the current user does not have the
        // proper permissions to perform this action.
        if (!Schema.SObjectType.Stage_Details__mdt.isQueryable()
	            || !Schema.SObjectType.Stage_Details__mdt.fields.Id.isAccessible()
	            || !Schema.SObjectType.Stage_Details__mdt.fields.Field_Value__c.isAccessible()
	            || !Schema.SObjectType.Stage_Details__mdt.fields.Display_Order__c.isAccessible()
	            || !Schema.SObjectType.Stage_Details__mdt.fields.Scope__c.isAccessible()
	            || !Schema.SObjectType.Stage_Details__mdt.fields.Object_Name__c.isAccessible()
           		|| !Schema.SObjectType.Completion_Criteria__mdt.isQueryable()
	            || !Schema.SObjectType.Completion_Criteria__mdt.fields.Id.isAccessible()
	            || !Schema.SObjectType.Completion_Criteria__mdt.fields.Label__c.isAccessible()
	            || !Schema.SObjectType.Completion_Criteria__mdt.fields.Results_Field_Name__c.isAccessible()
	            || !Schema.SObjectType.Completion_Criteria__mdt.fields.Display_Order__c.isAccessible()) {
            throw new InvalidPermissionsException(InvalidPermissionsException.STANDARD_MESSAGE);
        }
        
        // Obtain the object name based on the record id provided.
        String objectName = String.valueOf(recordId.getSobjectType());
        
        // Obtain a list of unique field names from the configurations.
		List<String> distinctFieldNames = getDistinctFieldNames(getAllCompletionCriteriaRecords());
        
        // Query for the SObject based on the record id provided.  Include
        // all fields to match the unique field names compiled above.
        SObject sObj = getSObject(recordId, distinctFieldNames);
        
        // Compile a list of stage progresses matching the scope
        // and SObject type.
        List<StageProgress> records = new List<StageProgress>();
		for (Stage_Details__mdt stageDetails :
	            [SELECT Id, Field_Value__c, Display_Order__c,
    	         	(SELECT Id, Label__c, Results_Field_Name__c, Display_Order__c
                	 FROM Completion_Criteria__r)
	             FROM Stage_Details__mdt
    	         WHERE Scope__c = : scope
        	     AND Object_Name__c = : String.valueOf(recordId.getSObjectType())
            	 LIMIT 100]) {

			// Add the wrapped version of the stage progress to the
			// list of records to be returned.
			records.add(new StageProgress(stageDetails, sObj));
		}
        
        // Sort and return the results.
        records.sort();
        return records;
    }

    /**
     * Returns a list of dynamic actions (buttons and hyperlinks)
     * related to the values provided:
     * - scope: Type of user (Sales or Research)
     * - record id: Id of the SObject being viewed.
     * - field name: Picklist field name on the SObject representing
     *      its current state.  Guided_Sales_Path__c, for example.
     * - field value: Picklist field value on the SObject representing
     *      its current state.  Discovery, for example.
     */
    @AuraEnabled
    public static List<DynamicAction> getGlobalDynamicActions(String scope, Id recordId, String fieldName, String fieldValue) {
        
        // Exit immediately if either of the required properties
        // are not provided.
        if (scope == null || recordId == null || fieldName == null || fieldValue == null) {
            return new List<DynamicAction>();
        }
        
        // Compile a list of dynamic actions matching the values
        // provided.
        List<DynamicAction> dynamicActions = new List<DynamicAction>();
        List<Dynamic_Action_Location__mdt> globalDynamicActionLocationRecords = getGlobalDynamicActionLocations(scope, String.valueOf(recordId.getSobjectType()), fieldName, fieldValue);
        
        // Obtain a list of unique field names from the configurations.
		List<String> distinctFieldNames = getDistinctFieldNames(globalDynamicActionLocationRecords);
        
        // Query for the SObject based on the record id provided.  Include
        // all fields to match the unique field names compiled above.
        SObject sObj = getSObject(recordId, distinctFieldNames);
        
        // Add the wrapped version of the dynamic action to the list of
        // records to be returned.
        for (Dynamic_Action_Location__mdt record : globalDynamicActionLocationRecords) {
            dynamicActions.add(new DynamicAction(record, sObj));
        }
        
        // Sort and return the results.
        dynamicActions.sort();
		return dynamicActions;
    }
    
    /**
     * Returns a filtered list of global dynamic action locations.
     * This method loops through all global dynamic action locations
     * and adds only the relevant records to the list to be returned.
     */
    private static List<Dynamic_Action_Location__mdt> getGlobalDynamicActionLocations(String scope, String objectName, String fieldName, String fieldValue) {
        
        // Create a list to be populated and returned with relevant
        // records.
        List<Dynamic_Action_Location__mdt> dynamicActionLocations = new List<Dynamic_Action_Location__mdt>();
        
        // Loop through all global dynamic action locations.
		for (Dynamic_Action_Location__mdt record : getAllGlobalDynamicActionLocations()) {
            
            // Only add records that match all properties provided.
			if (record.Stage_Details__r.Scope__c == scope
                    && record.Stage_Details__r.Object_Name__c == objectName
                    && record.Stage_Details__r.Field_Name__c == fieldName
                    && record.Stage_Details__r.Field_Value__c == fieldValue) {
				dynamicActionLocations.add(record);
			}
		}
        return dynamicActionLocations;
    }
    
    /**
     * Returns a list of all global dynamic action locations.
     */
    private static List<Dynamic_Action_Location__mdt> getAllGlobalDynamicActionLocations() {
        
        // Exit immediately if the current user does not have the
        // proper permissions to perform this action.
        if (!Schema.SObjectType.Dynamic_Action_Location__mdt.isQueryable()
	            || !Schema.SObjectType.Dynamic_Action_Location__mdt.fields.Id.isAccessible()
	            || !Schema.SObjectType.Dynamic_Action_Location__mdt.fields.Display_Order__c.isAccessible()
	            || !Schema.SObjectType.Dynamic_Action_Location__mdt.fields.Stage_Details__c.isAccessible()
           		|| !Schema.SObjectType.Stage_Details__mdt.isQueryable()
	            || !Schema.SObjectType.Stage_Details__mdt.fields.Id.isAccessible()
	            || !Schema.SObjectType.Stage_Details__mdt.fields.Scope__c.isAccessible()
	            || !Schema.SObjectType.Stage_Details__mdt.fields.Object_Name__c.isAccessible()
	            || !Schema.SObjectType.Stage_Details__mdt.fields.Field_Name__c.isAccessible()
	            || !Schema.SObjectType.Stage_Details__mdt.fields.Field_Value__c.isAccessible()
           		|| !Schema.SObjectType.Dynamic_Action__mdt.isQueryable()
	            || !Schema.SObjectType.Dynamic_Action__mdt.fields.Id.isAccessible()
	            || !Schema.SObjectType.Dynamic_Action__mdt.fields.Label__c.isAccessible()
	            || !Schema.SObjectType.Dynamic_Action__mdt.fields.User_Interface__c.isAccessible()
	            || !Schema.SObjectType.Dynamic_Action__mdt.fields.Destination_Type__c.isAccessible()
	            || !Schema.SObjectType.Dynamic_Action__mdt.fields.Go_To_Destination__c.isAccessible()
	            || !Schema.SObjectType.Dynamic_Action__mdt.fields.Open_in_New_Window__c.isAccessible()
	            || !Schema.SObjectType.Dynamic_Action__mdt.fields.Modal_Title__c.isAccessible()
	            || !Schema.SObjectType.Dynamic_Action__mdt.fields.Record_Id_Output_Parameter_Name__c.isAccessible()
	            || !Schema.SObjectType.Dynamic_Action__mdt.fields.User_Id_Output_Parameter_Name__c.isAccessible()
	            || !Schema.SObjectType.Dynamic_Action__mdt.fields.Tooltip__c.isAccessible()
	            || !Schema.SObjectType.Dynamic_Action__mdt.fields.Controlling_Checkbox_Field_Name__c.isAccessible()) {
            throw new InvalidPermissionsException(InvalidPermissionsException.STANDARD_MESSAGE);
        }
        
        // Return all fields necessary from the following custom
        // metadata objects:
        // - dynamic action location
        // - stage details
        // - dynamic actions
		return [SELECT Id, Display_Order__c,
                	Stage_Details__r.Scope__c, Stage_Details__r.Object_Name__c,
                	Stage_Details__r.Field_Name__c, Stage_Details__r.Field_Value__c,
					Dynamic_Action__r.Id, Dynamic_Action__r.Label__c,
					Dynamic_Action__r.User_Interface__c, Dynamic_Action__r.Destination_Type__c,
					Dynamic_Action__r.Go_To_Destination__c, Dynamic_Action__r.Open_in_New_Window__c,
					Dynamic_Action__r.Modal_Title__c, Dynamic_Action__r.Record_Id_Output_Parameter_Name__c,
	                Dynamic_Action__r.User_Id_Output_Parameter_Name__c, Dynamic_Action__r.Tooltip__c,
                	Dynamic_Action__r.Controlling_Checkbox_Field_Name__c
				FROM Dynamic_Action_Location__mdt
                WHERE Stage_Details__c <> null];
    }

    /**
     * Returns a list of all completion criteria records
     */
    private static List<Completion_Criteria__mdt> getAllCompletionCriteriaRecords() {
        
        // Exit immediately if the current user does not have the
        // proper permissions to perform this action.
        if (!Schema.SObjectType.Completion_Criteria__mdt.isQueryable()
	            || !Schema.SObjectType.Completion_Criteria__mdt.fields.Id.isAccessible()
	            || !Schema.SObjectType.Completion_Criteria__mdt.fields.Label__c.isAccessible()
	            || !Schema.SObjectType.Completion_Criteria__mdt.fields.Display_Order__c.isAccessible()
	            || !Schema.SObjectType.Completion_Criteria__mdt.fields.Results_Field_Name__c.isAccessible()
           		|| !Schema.SObjectType.Stage_Details__mdt.isQueryable()
	            || !Schema.SObjectType.Stage_Details__mdt.fields.Scope__c.isAccessible()
	            || !Schema.SObjectType.Stage_Details__mdt.fields.Object_Name__c.isAccessible()
	            || !Schema.SObjectType.Stage_Details__mdt.fields.Field_Name__c.isAccessible()
	            || !Schema.SObjectType.Stage_Details__mdt.fields.Field_Value__c.isAccessible()) {
            throw new InvalidPermissionsException(InvalidPermissionsException.STANDARD_MESSAGE);
        }
        
        // Return all fields necessary from the following custom
        // metadata objects:
        // - completion criteria
        // - stage details
		return [SELECT Id, Label__c, Display_Order__c, Results_Field_Name__c,
                         Stage_Details__r.Scope__c, Stage_Details__r.Object_Name__c,
                         Stage_Details__r.Field_Name__c, Stage_Details__r.Field_Value__c
                 FROM Completion_Criteria__mdt
                 LIMIT 1000];
    }
    
    /**
     * Returns a filtered list of completion criteria records matching
     * the values provided:
     * - scope: Type of user (Sales or Research)
     * - object name: Name of the SObject (ie: Opportunity)
     * - field name: Picklist field name on the SObject representing
     *      its current state.  Guided_Sales_Path__c, for example.
     * - field value: Picklist field value on the SObject representing
     *      its current state.  Discovery, for example.
     */
    private static List<Completion_Criteria__mdt> getCompletionCriteriaRecords(String scope, String objectName, String fieldName, String fieldValue) {
        
        // Create a list to be populated and returned
        List<Completion_Criteria__mdt> relevantCriteria = new List<Completion_Criteria__mdt>();
        
        // Loop through all completion criteria records.
		for (Completion_Criteria__mdt record : getAllCompletionCriteriaRecords()) {
            
            // Only add records to the list being returned that match
            // the criteria provided.
			if (record.Stage_Details__r.Scope__c == scope
                    && record.Stage_Details__r.Object_Name__c == objectName
                    && record.Stage_Details__r.Field_Name__c == fieldName
                    && record.Stage_Details__r.Field_Value__c == fieldValue) {
				relevantCriteria.add(record);
			}
		}
        return relevantCriteria;
    }
    
    /**
     * Returns an SObject (Salesforce record) matching the values
     * provided.
     */
    private static SObject getSObject(Id recordId, List<String> distinctFieldNames) {
        
        // Execute the query using the values provided.
		List<SObject> records = Database.query(getSoql(recordId, distinctFieldNames));
        
        // Return the result from the query if it exists.
        if (records.size() == 1) {
            return records[0];

		// Create a new SObject if the query did not produce
		// any results.
        } else {
            return createSObject(recordId);
        }
    }
    
    /**
     * Returns the SOQL statement used to query for existing
     * records.  The statement returned is specific to the
     * values provided.
     */
    private static String getSoql(Id recordId, List<String> distinctFieldNames) {
        
        // Exit immediately if the current user does not have the
        // proper permissions to perform this action.
        if (!Schema.SObjectType.Guided_Sales__c.isQueryable()
	            || !Schema.SObjectType.Guided_Sales__c.fields.Opportunity__c.isAccessible()) {
            throw new InvalidPermissionsException(InvalidPermissionsException.STANDARD_MESSAGE);
        }
       
        // Concatenate multiple components to create the SOQL statement.
		String soql = getSoqlSelect(distinctFieldNames)
            + ' FROM ' + GuidedSellingConstants.SOBJECT_GUIDED_SALES
            + ' WHERE ' + GuidedSellingConstants.RECORD_ID_FIELD_NAME + ' = : recordId'
            + ' LIMIT 1';
        return soql;
    }
    
    /**
     * Returns a SOQL select statement based on the unique list
     * of fields to be queried.
     */
    private static String getSoqlSelect(List<String> distinctFieldNames) {
        
        // Start the SOQL statement.
        String soqlSelect = 'SELECT ';
        
        // Loop through each unique field name.
        for (Integer i = 0; i < distinctFieldNames.size(); i++) {
            
            // Add the unique field name to the SOQL statement.
            soqlSelect += distinctFieldNames[i];
            
            // Add a comma if this is not the last unique field name.
            if (i < distinctFieldNames.size() - 1) {
                soqlSelect += ',';
            }
        }
        return soqlSelect;
    }
    
    /**
     * Returns a new SObject populated with only the related
     * record id.
     */
    private static SObject createSObject(Id parentId) {
        
        // Exit immediately if the current user does not have the
        // proper permissions to perform this action.
        if (!Schema.SObjectType.Guided_Sales__c.isCreateable()
	            || !Schema.SObjectType.Guided_Sales__c.fields.Opportunity__c.isCreateable()) {
            throw new SecurityException(InvalidPermissionsException.STANDARD_MESSAGE);
        }
       
        // Create a new SObject.  The type matches the constant
        // used for queries.
        Type sObjType = Type.forName(GuidedSellingConstants.SOBJECT_GUIDED_SALES);
        SObject sObj = (SObject)sObjType.newInstance();
        sObj.put(GuidedSellingConstants.RECORD_ID_FIELD_NAME, parentId);
        
        // Insert and return the new SObject.
        insert sObj;
        return sObj;
    }

    /**
     * Convenience method to simplify getting a list of unique
     * field names related to a list of completion criteria
     * records.
     */
    private static List<String> getDistinctFieldNames(List<Completion_Criteria__mdt> completionCriteriaRecords) {
        
        // Call the method which performs the process to get
        // unique field names.
        return getDistinctFieldNames(completionCriteriaRecords, GuidedSellingConstants.FIELD_RESULTS_FIELD_NAME);
    }

    /**
     * Convenience method to simplify getting a list of unique
     * field names related to a list of global dynamic action
     * location records.
     */
    private static List<String> getDistinctFieldNames(List<Dynamic_Action_Location__mdt> globalDynamicActionLocationRecords) {
        
        // Field names are not populated on global dynamic action
        // location records.  We need to create a list of related
        // dynamic action records, which do include field names.
        List<Dynamic_Action__mdt> dynamicActions = new List<Dynamic_Action__mdt>();
        for (Dynamic_Action_Location__mdt globalDynamicActionLocationRecord : globalDynamicActionLocationRecords) {
            dynamicActions.add(globalDynamicActionLocationRecord.Dynamic_Action__r);
        }
        
        // Call the method which performs the process to get
        // unique field names.
        return getDistinctFieldNames(dynamicActions, GuidedSellingConstants.FIELD_CONTROLLING_CHECKBOX_FIELD_NAME);
    }
    
    /**
     * Returns a list of unique field names based on the SObjects
     * provided.  The proper name (field name) is provided so the
     * software knows which field to return for the SObject type
     * provided.
     */
    private static List<String> getDistinctFieldNames(List<SObject> sObjects, String propertyName) {
        
        // Start the set with the Id field.
        Set<String> distinctFieldNames = new Set<String>{ GuidedSellingConstants.FIELD_ID };

		// Get a list of all field names for the given SObject type.
		// This is based on the schema describes, which will always
		// be accurate.
		Set<String> guidedSalesFieldNames = Schema.getGlobalDescribe().get(GuidedSellingConstants.SOBJECT_GUIDED_SALES).getDescribe().fields.getMap().keySet();
        String fieldName;
        
        // Loop through each SObject provided.
        for (SObject sObj : sObjects) {
            
            // Define the field name based on the property name provided.
            fieldName = (String)sObj.get(propertyName);
            
            // Validate the field actual exists before attempting to add
            // it to the list of unique field names.
            if (fieldName != null && guidedSalesFieldNames.contains(fieldName.toLowerCase())) {
	            distinctFieldNames.add(fieldName);
            }
        }
        
        // Convert the set to a list and return the results.
        return new List<String>(distinctFieldNames);
    }

    /**
     * Updates a field value on an SObject (Salesforce record).
     */
	@AuraEnabled
    public static void updateFieldValue(Id recordId, String fieldName, Object fieldValue) {

        // Exit immediately if any of the required properties are
        // not provided.
        if (recordId == null || fieldName == null || fieldValue == null) {
            return;
        }
        
        // Query for and update the record based on the values provided.
        SObject record = getSObject(recordId, new List<String>{ GuidedSellingConstants.FIELD_ID });
        record.put(fieldName, fieldValue);
        update record;
    }
}