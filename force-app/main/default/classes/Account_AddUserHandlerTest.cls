@isTest
private class Account_AddUserHandlerTest {

    static testMethod void test_Sales360Profile() {
        
        Account_AddUserHandler objHandler = new Account_AddUserHandler();
        List<Account> lstAcc = new List<Account>();
        
        Profile objProfile = [Select p.Name, p.Id From Profile p Where p.Name = 'Q-Sales-EI'];
        
        User user = createUser(objProfile.Id);
        database.insert(user);
       
        Account objAccount = new Account();
        objAccount.Name = 'test Account';
        objAccount.Account_Notes__c = 'test notes';
        objAccount.OwnerId = user.Id; 
        objAccount.BillingState = 'Utah';
        objAccount.BillingCountry = 'USA';
        objAccount.BillingPostalCode = '84602';
        lstAcc.add(objAccount);

        test.startTest();       //test start
            objHandler.onInsertAccount(lstAcc);
            database.insert(lstAcc);
            Account objAccNew = [Select Id,X360_EE_Rep__c From Account Where Id =:lstAcc[0].Id];
       //     System.assertEquals(user.Id, objAccNew.X360_EE_Rep__c);
        test.stoptest();        //test stop
    }//test_Sales360Profile
    
    static testMethod void test_PanelsProfile() {
        
        Account_AddUserHandler objHandler = new Account_AddUserHandler();
        List<Account> lstAcc = new List<Account>();
        
        Profile objProfile = [Select p.Name, p.Id From Profile p Where p.Name = 'Q-Panels'];
        
        User user = createUser(objProfile.Id);
        database.insert(user);
       
        Account objAccount = new Account();
        objAccount.Name = 'test Account';
        objAccount.Account_Notes__c = 'test notes';
        objAccount.OwnerId = user.Id; 
        objAccount.BillingState = 'Utah';
        objAccount.BillingCountry = 'USA';
        objAccount.BillingPostalCode = '84602';
        lstAcc.add(objAccount);
        database.insert(lstAcc);
        
        test.startTest();       //test start
            objHandler.onInsertAccount(lstAcc);
            Account objAccNew = [Select Id,Panels_Rep__c From Account Where Id =:lstAcc[0].Id];
            System.assertEquals(user.Id, objAccNew.Panels_Rep__c);
        test.stoptest();        //test stop
    }//test_PanelsProfile
    
     static testMethod void test_OptDevProfile() {
        
        Account_AddUserHandler objHandler = new Account_AddUserHandler();
        List<Account> lstAcc = new List<Account>();
        
        Profile objProfile = [Select p.Name, p.Id From Profile p Where p.Name = 'Q-OptDev'];
        
        User user = createUser(objProfile.Id);
        database.insert(user);
       
        Account objAccount = new Account();
        objAccount.Name = 'test Account';
        objAccount.Account_Notes__c = 'test notes';
        objAccount.OwnerId = user.Id;
        objAccount.BillingState = 'Utah';
        objAccount.BillingCountry = 'USA';
        objAccount.BillingPostalCode = '84602';
        lstAcc.add(objAccount);
        database.insert(lstAcc);
        
        test.startTest();       //test start
            objHandler.onInsertAccount(lstAcc);
            Account objAccNew = [Select Id,OpDev_Rep__c From Account Where Id =:lstAcc[0].Id];
            System.assertEquals(user.Id, objAccNew.OpDev_Rep__c);
        test.stoptest();        //test stop
    }//test_OptDevProfile
    
    static testMethod void test_SaleRSLinked() {
        
        Account_AddUserHandler objHandler = new Account_AddUserHandler();
        List<Account> lstAcc = new List<Account>();
        
        Profile objProfile = [Select p.Name, p.Id From Profile p Where p.Name = 'Q-Sales-RS'];
        
        User user = createUser(objProfile.Id);
        database.insert(user);
       
        Account objAccount = new Account();
        objAccount.Name = 'test Account';
        objAccount.Account_Notes__c = 'test notes';
        objAccount.OwnerId = user.Id; 
        objAccount.BillingState = 'Utah';
        objAccount.BillingCountry = 'USA';
        objAccount.BillingPostalCode = '84602';
        lstAcc.add(objAccount);
        database.insert(lstAcc);
        
        test.startTest();       //test start
            objHandler.onInsertAccount(lstAcc);
            Account objAccNew = [Select Id,Target_Audience_Rep__c From Account Where Id =:lstAcc[0].Id];
        //    System.assertEquals(user.Id, objAccNew.Target_Audience_Rep__c);
        test.stoptest();        //test stop
    }//test_SaleRSLinked
    
    static testMethod void test_ClientSuccessRep() {
        
        Account_AddUserHandler objHandler = new Account_AddUserHandler();
        List<Account> lstAcc = new List<Account>();
        
        Profile objProfile = [Select p.Name, p.Id From Profile p Where p.Name = 'Client Success Rep'];
        
        User user = createUser(objProfile.Id);
        database.insert(user);
       
        Account objAccount = new Account();
        objAccount.Name = 'test Account';
        objAccount.Account_Notes__c = 'test notes';
        objAccount.OwnerId = user.Id; 
        objAccount.BillingState = 'Utah';
        objAccount.BillingCountry = 'USA';
        objAccount.BillingPostalCode = '84602';
        lstAcc.add(objAccount);
        database.insert(lstAcc);
        
        test.startTest();       //test start
            objHandler.onInsertAccount(lstAcc);
            Account objAccNew = [Select Id,Client_Services_Rep__c From Account Where Id =:lstAcc[0].Id];
            System.assertEquals(user.Id, objAccNew.Client_Services_Rep__c);
        test.stoptest();        //test stop
    }//test_ClientSuccessRep
    
    public static User createUser(Id pProfileId){
        
        return  new User(   FirstName = 'TestFirstName',
                            LastName = 'TestLastName',
                            Username = 'userhcdjkfhkjslf@gmail.com',
                            Email = 'test@username.com',
                            CommunityNickname = 'TestNickName',
                            alias = 'Alias',
                            emailencodingkey='UTF-8',
                            languagelocalekey='en_US',
                            localesidkey='en_US',
                            ProfileId = pProfileId,
                            timezonesidkey='America/Los_Angeles');
    }//createUser
}