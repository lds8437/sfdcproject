public class clientLicenseChart {
   
    @AuraEnabled
    public static String getClientLicensesJSON(Id accountId){
        
        AggregateResult[] arcl = 
            [SELECT Bundle__r.Experience__c, SUM(Invoice_Amount__c) invoice FROM Client_License__c
             WHERE Client__r.Account__c =:accountId and Active__c = true
             GROUP BY Bundle__r.Experience__c];
        
        AggregateResult[] aropp = 
            [SELECT SUM(TotalPrice) price FROM OpportunityLineItem
             WHERE Opportunity.AccountId =:accountId and Opportunity.stageName = 'Won'
             and Opportunity.RecordTypeId = '012500000009kLAAAY'
             and Opportunity.CloseDate = LAST_N_MONTHS:12
             ];
        
        Map<String,Decimal> mapLeadSource = new Map<String,Decimal>();
        system.debug('mapleadsource: '+mapLeadSource);
        for(AggregateResult l : arcl)
        {
            String exp = String.valueOf(l.get('Experience__c'));
            Double inv = Double.valueOf(l.get('invoice'));
            mapLeadSource.put(exp,inv);
        }
        for(AggregateResult o : aropp)
        {
            String exp = 'Research Services';
            Double inv = Double.valueOf(o.get('price'));
            mapLeadSource.put(exp,inv);
        }
        system.debug('map values--'+mapLeadSource);
        list<RadarDataWrapper> radarData = new list<RadarDataWrapper>();
        
        for(String key : mapLeadSource.keySet())
        {            
            RadarDataWrapper rdw = new RadarDataWrapper();
            rdw.name=key;
            rdw.y=mapLeadSource.get(key);
            radarData.add(rdw);
        }
        system.debug('rdw---'+radarData);
        return System.json.serialize(radarData);
        //return null;
    }
    
    /**
* Wrapper class to serialize as JSON as return Value
* */
    class RadarDataWrapper
    {
        @AuraEnabled
        public String name;
        @AuraEnabled
        public decimal y;
        
    }
}