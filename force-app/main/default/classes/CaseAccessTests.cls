@IsTest
public class CaseAccessTests {

    testMethod static void testExecuteAssignmentRules() {
        
        // Prepare for tests
		final String RECORD_TYPE_DEAL_STRATEGY_NAME = 'Deal Strategy';
		final String RECORD_TYPE_DEAL_STRATEGY_ID;
		final String DEAL_STRATEGY_OWNER_NAME = 'Deal Strategy';
        
        Map<String, Id> mapOfRecordTypeIds = new Map<String, Id>();
        for (RecordType recordType : 
                [SELECT Id, Name
                 FROM RecordType
                 WHERE SObjectType = 'Case'
                 LIMIT 50]) {
			mapOfRecordTypeIds.put(recordType.Name, recordType.Id);
		}
        
        if (mapOfRecordTypeIds.containsKey(RECORD_TYPE_DEAL_STRATEGY_NAME)) {
            RECORD_TYPE_DEAL_STRATEGY_ID = mapOfRecordTypeIds.get(RECORD_TYPE_DEAL_STRATEGY_NAME);
        }
        
        // Create account and case records
        Account testAccount = new Account();
        testAccount.Name='Test Account';
        insert testAccount;
        
        Case testCase = new Case();
        testCase.AccountId = testAccount.Id;
		testCase.RecordTypeId = RECORD_TYPE_DEAL_STRATEGY_ID;
        insert testCase;

        // Organize the list of ids to pass to the method
        List<Id> caseIds = new List<Id>();
        caseIds.add(testCase.Id);
        
        Test.startTest();
        
        CaseAccess.executeAssignmentRules(caseIds);
        caseAccess.prepareToExecuteAssignmentRules(caseIds);
        
        // Validate the results
        if (RECORD_TYPE_DEAL_STRATEGY_ID != null) {

            List<Case> cases =
                [SELECT Id, RecordType.Name
                 FROM Case
                 WHERE Id IN : CaseIds
                 LIMIT 1];
            if (cases.size() == 1) {
                System.assertEquals(RECORD_TYPE_DEAL_STRATEGY_NAME, cases[0].RecordType.Name);
            }
        }
        
        Test.stopTest();
    }
}