/***********
Description: This batch class is used for calculating the no bid made by a vendor(i.e no. of panels project vendors per vendor)
and the no. of bid own by a vendor(i.e no. of panels project vendor slected for use per vendor). 
The class will run iteratively, based on a pre-defined time lapse. 
This batch script at each periodic iteration will read only the vendor details that have been inserted or last modified since the timestamp of the previous batch iteration.
This will help us achieve batch execution at a minimized time. 
***********/

public class CountNumberOfBids_db implements Database.Batchable<sObject>{
    
    //Description: Start method will fetch details of all the panels project vendor which have newly insertd or modified scince the last time batch job executed.
    public Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT ID,NAME,Number_of_times_Bided__c, Number_of_times_selected__c FROM Vendor__c';
        return Database.getQueryLocator(query);
    }
    
    //Description: Excute method is to calculate the no bid made by a vendor and the no. of bid own by a vendor and update the count in the vendor record.
    public void execute(Database.BatchableContext BC, List<Vendor__c> scope){
        try{
            Set<Id> vendorIds = new Set<Id>();
            Map<Id, List<Panels_Project_Vendor__c>> panelsProjectVendorSMap = new Map<Id, List<Panels_Project_Vendor__c>>();
            Map<Id, List<Panels_Project_Vendor__c>> panelsProjectVendorSelectedMap = new Map<Id, List<Panels_Project_Vendor__c>>();
            for(Vendor__c vendor : scope){
                vendorIds.add(vendor.Id);
            }
            List<Panels_Project_Vendor__c> panelsProjectVendorList = [SELECT Id,Selected_for_Use__c,VendorName__c,Last_Processed_Time__c from Panels_Project_Vendor__c where VendorName__c IN:vendorIds AND Is_Eligible_for_Batch_Process__c = true];
            system.debug('ChildLit@@@@@@'+panelsProjectVendorList);
            for(Panels_Project_Vendor__c eachPanelProjectVendor : panelsProjectVendorList){
                
                if(panelsProjectVendorSMap.containsKey(eachPanelProjectVendor.VendorName__c))
                {
                    panelsProjectVendorSMap.get(eachPanelProjectVendor.VendorName__c).add(eachPanelProjectVendor);    
                }
                else{
                    panelsProjectVendorSMap.put(eachPanelProjectVendor.VendorName__c,new List<Panels_Project_Vendor__c>{eachPanelProjectVendor});
                }
                if(eachPanelProjectVendor.Selected_for_Use__c){
                    if(panelsProjectVendorSelectedMap.containsKey(eachPanelProjectVendor.VendorName__c))
                    {
                        panelsProjectVendorSelectedMap.get(eachPanelProjectVendor.VendorName__c).add(eachPanelProjectVendor); 
                    }
                    else{
                        panelsProjectVendorSelectedMap.put(eachPanelProjectVendor.VendorName__c,new List<Panels_Project_Vendor__c>{eachPanelProjectVendor});
                    }
                }
            }
            system.debug('ChildLit@@@@@@'+panelsProjectVendorSMap);
            system.debug('ChildLit@@@@@@'+panelsProjectVendorSelectedMap);
            List<vendor__c> lstUpdVendors = new List<vendor__C>();
            for(Vendor__c vendorUpdate : scope){
                
                //Vendor__C vd = new Vendor__C();
                if(panelsProjectVendorSMap.containsKey(vendorUpdate.Id)){
                 vendorUpdate.Number_of_times_Bided__c = panelsProjectVendorSMap.get(vendorUpdate.Id).size();
                 }
                 if(panelsProjectVendorSelectedMap.containsKey(vendorUpdate.Id)){
                vendorUpdate.Number_of_times_selected__c = panelsProjectVendorSelectedMap.get(vendorUpdate.Id).size();
                }
                lstUpdVendors.add(vendorUpdate);
            }
            
            system.debug('updTperList@@@@@@'+lstUpdVendors);
            
            database.update(lstUpdVendors,false);
           
            
        }
        catch(Exception e){
            System.debug('****Exception Message*** '+e.getMessage());
            System.debug('****Exception Stacktrace*** '+e.getStackTraceString());
            System.debug('****Exception*** '+e);
        }
    } 
    
    // no logic needed for finish method
    public void finish(Database.BatchableContext BC)
    {
    }
}