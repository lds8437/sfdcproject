@isTest
public class q2c_UpdateEmployeeSFDCId_Test {
    @testSetup
    static void setup(){
        List<Employee__c> elist = new List<Employee__c>();
        
        Employee__c e = new Employee__c();
        e.Name = 'Test Employee';
        e.EmployeeIndentificationNumber__c = 9999;
        e.NSInternalDepartmentID__c = 'Test Dept 1234';
        elist.add(e);
        
        Employee__c e2 = new Employee__c();
        e2.Name = 'Test Employee2';     
        e2.NSInternalDepartmentID__c = 'Test Dept 5678';
        elist.add(e2);
        
        Employee__c e3 = new Employee__c();
        e3.Name = 'Test Employee2';  
        e3.EmployeeIndentificationNumber__c = 1111;        
        elist.add(e3);
        
        insert elist;
    }
    
    static testmethod void testUpdateUserName(){
        List<Employee__c> employees = [SELECT ID, EmployeeIndentificationNumber__c, UserName__c FROM Employee__c WHERE Username__c = NULL AND EmployeeIndentificationNumber__c != Null];
        
        Test.startTest();
        q2c_UpdateEmployeeSFDCId_Class.updateSFDCId(employees);
        Test.stopTest();
    }
    
    
}