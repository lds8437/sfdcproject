public class QualtricsMavenlinkCustomWSAllocScheduler {
	
    public static void executeSync(){
    		DateTime currentDateTime = DateTime.now();
    		Date startingDay = Date.today();

    		
    		//mavenlink__PackageConfiguration__c config=[select QualtricsCustomJobId__c from mavenlink__PackageConfiguration__c limit 1];
    		String jobId = null;
    		try{
	    		CronJobDetail CJD =[select Id, Name from CronJobDetail Where Name=:QualtricsMavenlinkWSAllocSchedulable.jobName Limit 1];
	    		
	    		CronTrigger CT = [select Id, NextFireTime, PreviousFireTime, StartTime from CronTrigger Where CronJobDetailId=:CJD.Id Limit 1]; 
	    		jobId = CT.Id;
	    }catch(Exception e){
    			if(e.getTypeName()=='System.QueryException') jobId = null;
    		}
    		
    		if(jobId!=null){
    			System.abortJob(jobId);
    			System.debug(QualtricsMavenlinkWSAllocSchedulable.jobName+':Aborted jobId='+jobId);
    		}
    		
    		String cron = getCronExpression(String.valueOf(currentDateTime.hour()),String.valueOf(currentDateTime.minute()+1),'hourly',startingDay,'');
    		
    		jobId=System.schedule(QualtricsMavenlinkWSAllocSchedulable.jobName,
				cron,new QualtricsMavenlinkWSAllocSchedulable());
				
		//config.QualtricsCustomJobId__c = jobId;
		//update config;
    }
    
    
    /**
    * @description Builds a cron expression based on the user selected values
    * @return String representation of the Cron expression
    * @param startHour The hour of the day the job will start on
    * @param startingMinutes The minute of the hour the job will start on
    * @param frequency The frequency with which the job will run
    * @param startingDate The day the cronjob will start
    * @param weekday The day of the week the cronjob will start
    */
    public static String getCronExpression(String startingHour, String startingMinutes, String frequency, Date startingDate, String weekday){
        String m = 'getCronExpression';
        // second minute hour day-of-month month day-of-week year
        if(frequency == 'monthly'){         
            return '0 ' + startingMinutes + ' ' + startingHour + ' ' + startingDate.day() +' ' + startingDate.month() +'/1 ? *';
            
        }else if(frequency == 'weekly'){        
            return '0 ' + startingMinutes + ' ' + startingHour + ' ? * ' + weekday + ' *';
           
        }else if(frequency == 'daily'){
            return '0 ' + startingMinutes + ' ' + startingHour +' ? * * *';
            
        }else if(frequency == 'twiceDaily'){            
            Integer startTime = Integer.valueOf(startingHour);
            if(startTime > 11) startTime -=  12;

            return '0 ' + startingMinutes + ' ' + startTime + ',' + (startTime + 12) + ' ? * * *';
            
        }else if(frequency == 'threeDaily'){
            Integer startTime = Integer.valueOf(startingHour);
            
            while(startTime > 7){  startTime -= 8;}
            return '0 ' + startingMinutes + ' ' + startTime + ',' + (startTime + 8) + ',' + (startTime + 16) + ' ? * * *';
            
        }else if(frequency == 'hourly'){
            return '0 ' + startingMinutes + ' * ? * * *';       }       return '';
    }
}