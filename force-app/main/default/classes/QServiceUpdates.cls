public class QServiceUpdates {

    public static void updateCongaTempalte(List<QService__c> serviceList){
		//Set<Id> oppIds = new Set<Id>();
        Map<Id,String> impMap = new Map<Id,String>();
        system.debug('serviceList: '+serviceList);
		for(QService__c serviceObj : serviceList){
            system.debug('status: '+serviceObj.Status__c);
            system.debug('status: '+serviceObj.Custom__c);
			if(
				serviceObj.Status__c == 'Request Cancelled'
				&& serviceObj.Custom__c == false
				
			){
			//	oppIds.add(serviceObj.Opportunity__c);
                impMap.put(serviceObj.Opportunity__c,serviceObj.Template_Codes__c);
			}
		}
       
        List<Contract_Document__c> cdList = new List<Contract_Document__c>();
        for(Id oppId : impMap.keyset()){
            Contract_Document__c cd = [SELECT Id, Implementation__c 
                                       FROM Contract_Document__c 
                                       WHERE Opportunity__c = :oppId];
            cd.Implementation__c = null;
            cdList.add(cd);
        }
       update cdList;
	}

}