public class PartnerSurveyClass {
    public static void getOpportunity(Opportunity[] opps){
        
        Set<Id> closedop = new Set<Id>();
        //get a set of closed opportunities from trigger
        For(Opportunity o:opps){
            if(o.StageName == 'Won' || o.StageName == 'Lost'){
                closedop.add(o.Id);
            //    system.debug(o.Id);
            }
        }
        
        
        //get the sum of the partner specific revenue from the related products
        AggregateResult[] groupResults = [SELECT SUM(TotalPrice), Partner__c, Quote.Opportunity.Id opp, Quote.Opportunity.Name oppn, Partner__r.Name parn,
                                          AVG(Quote.Opportunity.Amount) amt, Quote.Opportunity.StageName stnm, Quote.Opportunity.Client__r.Name client,
                                          Quote.Opportunity.OwnerId owner, Quote.Opportunity.Owner.Name ownn, Quote.Opportunity.Owner.FirstName owfn, 
                                          Quote.Opportunity.Owner.LastName owln, Quote.Opportunity.Owner.Email owem, Quote.Opportunity.Owner.LanguageLocaleKey owlg
                                          FROM QuoteLineItem
                                          WHERE Quote.OpportunityId in: closedop and Quote.IsSyncing = True and Partner__c != Null
                                          GROUP By Partner__c, Quote.Opportunity.Id, Quote.Opportunity.Name, Partner__r.Name,
                                          Quote.Opportunity.StageName, Quote.Opportunity.Client__r.Name, Quote.Opportunity.OwnerId, Quote.Opportunity.Owner.Name, 
                                         Quote.Opportunity.Owner.FirstName, Quote.Opportunity.Owner.LastName, Quote.Opportunity.Owner.Email, Quote.Opportunity.Owner.LanguageLocaleKey];
        
      //  system.debug('ar: '+groupResults);
        //create an empty list to hold the new partner product opportunities and populate it with new records
        List<Partner_Product_Opportunity__c> ppo = new List<Partner_Product_Opportunity__c>();
        if(!groupResults.isEmpty()){
            for(AggregateResult ar:groupResults){            
                Partner_Product_Opportunity__c ppoNew = new Partner_Product_Opportunity__c();
                ppoNew.Opportunity__c = (id) ar.get('opp');
                ppoNew.Name = (string) ar.get('oppn');
                ppoNew.Q_Partner__c = (id) ar.get('Partner__c');
                ppoNew.Partner_Name__c = (string) ar.get('parn');
                ppoNew.Amount__c = (decimal) ar.get('expr0');
                ppoNew.Total_Opportunity_Amount__c = (decimal) ar.get('amt');
                ppoNew.Opportunity_Status__c = (string) ar.get('stnm');
                ppoNew.Client_Name__c = (string) ar.get('client');
                ppoNew.Opportunity_Owner_Id__c = (id) ar.get('owner');
                ppoNew.Opportunity_Owner_Name__c = (string) ar.get('ownn');
                ppoNew.Type__c = 'Product';   
                ppoNew.Opp_Partner_Unique_Id__c = (id) ar.get('opp') +'-' + (id) ar.get('Partner__c');
                ppoNew.Opportunity_Id__c = (string) ar.get('opp');
                ppoNew.Owner_Email__c = (string) ar.get('owem');
                ppoNew.Owner_First_Name__c = (string) ar.get('owfn');
                ppoNew.Owner_Last_Name__c = (string) ar.get('owln');
                ppoNew.Owner_Language__c = (string) ar.get('owlg');
                ppo.add(ppoNew);
            }
            Try{
                Upsert ppo Opp_Partner_Unique_Id__c;
            }
            Catch(exception e){
                system.debug(logginglevel.error, '*****'+e);
            }
        }        
    }
    
 /*   public static void getPartnerOpportunity(Opportunity[] opps){
        
        Set<Id> closedop = new Set<Id>();
        //get a set of closed opportunities from trigger
        For(Opportunity o:opps){
            if((o.StageName == 'Won' || o.StageName == 'Lost') && o.Partner__c !=Null){
                closedop.add(o.Id);
                system.debug(o.Id);
            }
        }
        Map<id,Opportunity> opp = new Map<Id,Opportunity>([
            SELECT Id, Partner__c, Name, StageName, Amount, Client__r.Name, OwnerId, Owner.Name, Partner__r.Name FROM Opportunity where id in : closedop 
        ]);     
        
        
        List<Partner_Product_Opportunity__c> ppo = new List<Partner_Product_Opportunity__c>();
        //get a set of closed opportunities from trigger
        For(Id o:closedop){            
            Partner_Product_Opportunity__c ppoNew = new Partner_Product_Opportunity__c();
            ppoNew.Opportunity__c = opp.get(o).Id;
            ppoNew.Q_Partner__c = opp.get(o).Partner__c;
            ppoNew.Partner_Name__c = opp.get(o).Partner__r.Name;
            ppoNew.Name = opp.get(o).Name;
            ppoNew.Opportunity_Status__c = opp.get(o).StageName;
            ppoNew.Opp_Partner_Unique_Id__c = opp.get(o).Id + '-' +opp.get(o).Partner__c;
            ppoNew.Total_Opportunity_Amount__c = opp.get(o).Amount;
            ppoNew.Client_Name__c = opp.get(o).Client__r.Name;
            ppoNew.Opportunity_Id__c = opp.get(o).Id;
            ppoNew.Opportunity_Owner_Id__c = opp.get(o).OwnerId;
            ppoNew.Opportunity_Owner_Name__c = opp.get(o).Owner.Name;
            ppoNew.Type__c = 'Referral';
            ppoNew.Amount__c = opp.get(o).Amount;
            ppo.add(ppoNew);
            
        }
        
         try{
                Upsert ppo Opp_Partner_Unique_Id__c;
            }
            catch(DMLException e){
                System.debug('Exception');
           }
    }  */                 
    
}