@isTest
private class ParentAccountUpdateTest {

    // Test the case of a new acct. being created without imm. parent DUNS. Expected result: Trigger doesn't modify acct.
    static testMethod void NewAccountNoImmParentDuns() {
        Account a1 = new Account(Name = 'Alphabet Factory');
        a1.BillingCountry = 'USA';
        a1.BillingState = 'Utah';
        a1.BillingPostalCode = '84604';
        insert a1;
        Account aCheck = [select Temp_Parent_Account__c from Account where Name = 'Alphabet Factory'];
        system.assertequals(aCheck.Temp_Parent_Account__c, null);
    }
    
    // Test the case of a new acct. being created with imm. parent DUNS, but no matching acct. in the database. Expected result: Trigger doesn't modify acct.
    /*static testMethod void NewAccountImmParentDunsButParentDNE() {
        Account a1 = new Account(Name = 'Bento Factory', Immediate_Parent_D_U_N_S__c = '617264288');
        insert a1;
        Account aCheck = [select Temp_Parent_Account__c from Account where Name = 'Bento Factory'];
        system.assertequals(aCheck.Temp_Parent_Account__c, null);
    }
    
    // Test the case of a new acct. being created with imm. parent DUNS and a matching acct. in the database. Expected result: Matching acct. set as Parent Account.
    static testMethod void NewAccountImmParentDunsSuccess() {
        Account a1 = new Account(Name = 'Cadmium Conservatory', DunsNumber = '617264288');
        insert a1;
        Account aCheck = [select Id from Account where Name = 'Cadmium Conservatory'];
        
        Account a2 = new Account(Name = 'Cadmium Factory', Immediate_Parent_D_U_N_S__c = '617264288');
        insert a2;
        Account aCheck2 = [select Temp_Parent_Account__c from Account where Name = 'Cadmium Factory'];
        system.assertequals(aCheck2.Temp_Parent_Account__c, aCheck.Id);
    }
    
    // Test the case of an old acct. being edited without imm. parent DUNS and no parent acct. set. Expected result: Trigger doesn't modify acct.
    static testMethod void UpdateAccountNoImmParentDunsAndNoParentListed() {
        Account a1 = new Account(Name = 'Denim Factory');
        insert a1;
        Account aCheck1 = [select Id from Account where Name = 'Denim Factory'];
        
        aCheck1.Account_Notes__c = 'Denim dalmations diving.';
        update aCheck1;
        aCheck1 = [select Temp_Parent_Account__c from Account where Name = 'Denim Factory'];
        system.assertequals(aCheck1.Temp_Parent_Account__c, null);
    }
    
    // Test the case of an old acct. being edited without imm. parent DUNS but having parent acct. set. Expected result: Trigger doesn't modify acct.
    static testMethod void UpdateAccountNoImmParentDunsButParentListed() {
        Account a1 = new Account(Name = 'Eagle Conservatory', DunsNumber = '617264288');
        insert a1;
        Account aCheck = [select Id from Account where Name = 'Eagle Conservatory'];
        
        Account a2 = new Account(Name = 'Eagle Factory', Temp_Parent_Account__c = aCheck.Id);
        insert a2;
        Account aCheck2 = [select Id from Account where Name = 'Eagle Factory'];
        
        aCheck2.Account_Notes__c = 'Eagle editors evolving.';
        update aCheck2;
        aCheck2 = [select Temp_Parent_Account__c from Account where Name = 'Eagle Factory'];
        system.assertequals(aCheck2.Temp_Parent_Account__c, aCheck.Id);
    }
    
    // Test the case of an old acct. being edited with imm. parent DUNS, but no matching acct. in the database. Expected result: Trigger doesn't modify acct.
    static testMethod void UpdateAccountImmParentDunsButParentDNE() {
        Account a1 = new Account(Name = 'Farmer Factory');
        insert a1;
        Account aCheck1 = [select Id from Account where Name = 'Farmer Factory'];
        
        aCheck1.Immediate_Parent_D_U_N_S__c = '617264288';
        update aCheck1;
        aCheck1 = [select Temp_Parent_Account__c from Account where Name = 'Farmer Factory'];
        system.assertequals(aCheck1.Temp_Parent_Account__c, null);
    }
    
    // Test the case of an old acct. being edited with imm. parent DUNS and no parent acct. set.
    static testMethod void UpdateAccountImmParentDunsFresh() {
        Account a1 = new Account(Name = 'Garden Conservatory', DunsNumber = '617264288');
        insert a1;
        Account aCheck = [select Id from Account where Name = 'Garden Conservatory'];
        
        Account a2 = new Account(Name = 'Garden Factory');
        insert a2;
        Account aCheck2 = [select Id from Account where Name = 'Garden Factory'];
        
        aCheck2.Immediate_Parent_D_U_N_S__c = '617264288';
        update aCheck2;
        aCheck2 = [select Temp_Parent_Account__c from Account where Name = 'Garden Factory'];
        system.assertequals(aCheck2.Temp_Parent_Account__c, aCheck.Id);
    }
    
    // Test the case of an old acct. being edited with imm. parent DUNS and a parent acct. already set.
    static testMethod void UpdateAccountImmParentDunsOverwrite() {
        // Create parent account #1
        Account a1 = new Account(Name = 'Honorific Conservatory', DunsNumber = '617264288');
        insert a1;
        Account aCheck = [select Id from Account where Name = 'Honorific Conservatory'];
        
        // Create parent account #2
        Account a2 = new Account(Name = 'Honorific HQ', DunsNumber = '451329999');
        insert a2;
        Account aCheck2 = [select Id from Account where Name = 'Honorific HQ'];
        
        // Create new account, setting its parent to #1
        Account a3 = new Account(Name = 'Honorific Factory', Immediate_Parent_D_U_N_S__c = '617264288');
        insert a3;
        Account aCheck3 = [select Id, Temp_Parent_Account__c, Immediate_Parent_D_U_N_S__c from Account where Name = 'Honorific Factory'];
        system.assertequals(aCheck3.Temp_Parent_Account__c, aCheck.Id);
        
        // Edit same account and set its parent to #2
        aCheck3.Immediate_Parent_D_U_N_S__c = '451329999';
        update aCheck3;
        aCheck3 = [select Temp_Parent_Account__c from Account where Name = 'Honorific Factory'];
        system.assertequals(aCheck3.Temp_Parent_Account__c, aCheck2.Id);
    }*/
}