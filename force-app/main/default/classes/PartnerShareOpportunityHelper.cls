public class PartnerShareOpportunityHelper {
	public static void afterInsert(Map<Id, Opportunity> newMap) {
        	//assignOpportunityShares(newMap);
    	shareOpportunity(newMap, null);
    } // end afterInsert

    public static void afterUpdate(Map<Id, Opportunity> newMap, Map<Id, Opportunity> oldMap) {
		//assignOpportunityShares(newMap);
		shareOpportunity(newMap, oldMap);
    } // end afterUpdate

    private static void shareOpportunity(Map<Id, Opportunity> newMap, Map<Id, Opportunity> oldMap) {
        Id panelsRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Panels').getRecordTypeId();
        Map<Id, Id> newPartnerIdMap = new Map<Id, Id>();
        Map<Id, Id> newQPNPartnerIdMap = new Map<Id, Id>();
        Map<Id, Id> oldPartnerIdMap = new Map<Id, Id>();
        Map<Id, Id> oldQPNPartnerIdMap = new Map<Id, Id>();

        // If the Partner Sourced Opportunity or QPN Recommended Partner fields change
        // create OpportunityShares for the Partner User Roles related to the new Partner's Account
        // and remove Opportunity shares from the Partner User Roles related to the old Partner's Account
         
        // Create Opportunity ID to Partner ID maps related to each field
        for (Opportunity opp : newMap.values()) {
            if (opp.RecordTypeId != panelsRTId) {
                if (opp.Partner__c != null) {
                    newPartnerIdMap.put(opp.Id, opp.Partner__c);
                }
                if (opp.QPN_Recommended_Partner__c != null) {
                    newQPNPartnerIdMap.put(opp.Id, opp.QPN_Recommended_Partner__c);
                }
                if (oldMap != null && (opp.Partner__c != oldMap.get(opp.Id).Partner__c || opp.QPN_Recommended_Partner__c != oldMap.get(opp.Id).QPN_Recommended_Partner__c)) {
                    if (oldMap.get(opp.Id).Partner__c != null) {
                        oldPartnerIdMap.put(opp.Id, oldMap.get(opp.Id).Partner__c);
                    }
                    if (oldMap.get(opp.Id).QPN_Recommended_Partner__c != null) {
                        oldQPNPartnerIdMap.put(opp.Id, oldMap.get(opp.Id).QPN_Recommended_Partner__c);
                    }
                }
            }
        }
        
        // Deleting OpportunityShare records from old Partners
        if (!oldPartnerIdMap.isEmpty() || !oldQPNPartnerIdMap.isEmpty()) {
            Set<Id> oldPartnerIds = new Set<Id>();
            Set<Id> oldOppIds = new Set<Id>();
            oldPartnerIds.addAll(oldPartnerIdMap.values());
        	oldPartnerIds.addAll(oldQPNPartnerIdMap.values());
            oldOppIds.addAll(oldPartnerIdMap.keySet());
            oldOppIds.addAll(oldQPNPartnerIdMap.keySet());
            Map<Id, Q_Partner__c> partnerIdToAccountMap = new Map<Id, Q_Partner__c> ([SELECT Id, RelatedAccount__c FROM Q_Partner__c WHERE Id IN :oldPartnerIds AND RelatedAccount__c != null]);
            
            // Adjust maps to be Opportunity ID to Partner Account ID
            Set<Id> partnerAccountIds = new Set<Id>();
            for (Id oldOppId : oldPartnerIdMap.keySet()) {
                Id partnerId = oldPartnerIdMap.get(oldOppId);
                if (partnerIdToAccountMap.containsKey(partnerId)) {
                    Id partnerAccountId = partnerIdToAccountMap.get(partnerId).RelatedAccount__c;
                    oldPartnerIdMap.put(oldOppId, partnerAccountId);
                    partnerAccountIds.add(partnerAccountId);
                }
            }
            
            for (Id oldOppId : oldQPNPartnerIdMap.keySet()) {
                Id partnerId = oldQPNPartnerIdMap.get(oldOppId);
                if (partnerIdToAccountMap.containsKey(partnerId)) {
                    Id partnerAccountId = partnerIdToAccountMap.get(partnerId).RelatedAccount__c;
                    oldQPNPartnerIdMap.put(oldOppId, partnerAccountId);
                    partnerAccountIds.add(partnerAccountId);
                }
            }
            
            // Find all Role Groups related to the old Partner Account IDs
            Set<Id> groupIds = new Set<Id>();
            for (Group roleGroup : [SELECT Id FROM Group WHERE RelatedId IN (SELECT Id FROM UserRole WHERE PortalAccountId IN :partnerAccountIds) AND Type = 'Role']) {
                groupIds.add(roleGroup.Id);
            }
            
            // Find all Opportunity Shares related to the Opportunities and old Partner Account Ids and delete them
            List<OpportunityShare> oppSharesToDelete = [SELECT Id FROM OpportunityShare WHERE OpportunityId IN :oldOppIds AND UserOrGroupId IN :groupIds];
            Database.DeleteResult[] deleteResults = Database.delete(oppSharesToDelete, false);
            
            for (Database.DeleteResult dr : deleteResults) {
                if (!dr.isSuccess()) {               
                    for(Database.Error err : dr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Account fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
        
        // Inserting OpportunityShares for new Partners
        if (!newPartnerIdMap.isEmpty() || !newQPNPartnerIdMap.isEmpty()) {
            Set<Id> newPartnerIds = new Set<Id>();
            Set<Id> newOppIds = new Set<Id>();
            newPartnerIds.addAll(newPartnerIdMap.values());
        	newPartnerIds.addAll(newQPNPartnerIdMap.values());
            newOppIds.addAll(newPartnerIdMap.keySet());
            newOppIds.addAll(newQPNPartnerIdMap.keySet());
            Map<Id, Q_Partner__c> partnerIdToAccountMap = new Map<Id, Q_Partner__c> ([SELECT Id, RelatedAccount__c FROM Q_Partner__c WHERE Id IN :newPartnerIds AND RelatedAccount__c != null]);
            
            // Adjust maps to be Opportunity ID to Partner Account ID
            Set<Id> partnerAccountIds = new Set<Id>();
            for (Id newOppId : newPartnerIdMap.keySet()) {
                Id partnerId = newPartnerIdMap.get(newOppId);
                if (partnerIdToAccountMap.containsKey(partnerId)) {
                    Id partnerAccountId = partnerIdToAccountMap.get(partnerId).RelatedAccount__c;
                    newPartnerIdMap.put(newOppId, partnerAccountId);
                    partnerAccountIds.add(partnerAccountId);
                }
            }
            
            for (Id newOppId : newQPNPartnerIdMap.keySet()) {
                Id partnerId = newQPNPartnerIdMap.get(newOppId);
                if (partnerIdToAccountMap.containsKey(partnerId)) {
                    Id partnerAccountId = partnerIdToAccountMap.get(partnerId).RelatedAccount__c;
                    newQPNPartnerIdMap.put(newOppId, partnerAccountId);
                    partnerAccountIds.add(partnerAccountId);
                }
            }
            
            // Map the Partner Account IDs to the set of related User Role IDs
            Map<Id, Set<Id>> portalAccountToRolesMap = new Map<Id, Set<Id>>();
            Set<Id> userRoleIds = new Set<Id>();
            for (UserRole role : [SELECT Id, PortalAccountId FROM UserRole WHERE PortalAccountId IN :partnerAccountIds]) {
                if (portalAccountToRolesMap.containsKey(role.PortalAccountId)) {
                    portalAccountToRolesMap.get(role.PortalAccountId).add(role.Id);
                } else {
                	portalAccountToRolesMap.put(role.PortalAccountId, new Set<Id> { role.Id });
                }
                userRoleIds.add(role.Id);
            }
            
            // Map the Partner Role Group IDs to that Group's access level
            //   Executive and Manager Roles receive edit access
            //   Any other role is Read Only
            Map<Id, Id> userRoleToGroupMap = new Map<Id, Id>();
            Map<Id, String> groupToAccessMap = new Map<Id, String>();
            for (Group roleGroup : [SELECT Id, RelatedId, DeveloperName FROM Group WHERE RelatedId IN :userRoleIds AND Type = 'Role']) {
                String accessLevel = 
                    (roleGroup.DeveloperName.contains('PartnerExecutive') || roleGroup.DeveloperName.contains('PartnerManager')) ? 
                    'Edit' :
                	'Read';
                userRoleToGroupMap.put(roleGroup.RelatedId, roleGroup.Id);
                groupToAccessMap.put(roleGroup.Id, accessLevel);
            }
            
            // Using the maps above, create OpportunityShare records for each Opportunity ID, Role Group ID and access level
            List<OpportunityShare> oppShares = new List<OpportunityShare>();
            for (Id oppId : newPartnerIdMap.keySet()) {
                Id partnerAccountId = newPartnerIdMap.get(oppId);
                if (portalAccountToRolesMap.containsKey(partnerAccountId)) {
                    for(Id userRoleId : portalAccountToRolesMap.get(partnerAccountId)) {
                        Id groupId = userRoleToGroupMap.get(userRoleId);
                        String access = groupToAccessMap.get(groupId);
                        if (groupId != null && access != null) {
                            OpportunityShare oppShare = new OpportunityShare();
                            oppShare.OpportunityId = oppId;
                            oppShare.UserOrGroupId = groupId;
                            oppShare.OpportunityAccessLevel = groupToAccessMap.get(groupId);
                            oppShares.add(oppShare);
                        }
                    }
                } 
            }
            
            for (Id oppId : newQPNPartnerIdMap.keySet()) {
                Id partnerAccountId = newQPNPartnerIdMap.get(oppId);
                if (portalAccountToRolesMap.containsKey(partnerAccountId)) {
                    for(Id userRoleId : portalAccountToRolesMap.get(partnerAccountId)) {
                        Id groupId = userRoleToGroupMap.get(userRoleId);
                        String access = groupToAccessMap.get(groupId);
                        if (groupId != null && access != null) {
                            OpportunityShare oppShare = new OpportunityShare();
                            oppShare.OpportunityId = oppId;
                            oppShare.UserOrGroupId = groupId;
                            oppShare.OpportunityAccessLevel = groupToAccessMap.get(groupId);
                            oppShares.add(oppShare);
                        }
                    }
                } 
            }
            
            Database.SaveResult[] srList = Database.insert(oppShares, false);
            
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {               
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Account fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
    }
}