public class ConsolidatedScreeningApproval {
    
    private ConsolidatedScreeningManager manager; 
    private Id opportunityId;
    private List<Object> results;
    
    public ConsolidatedScreeningApproval(ConsolidatedScreeningManager manager){
        system.debug('APPROVAL');
        this.manager = manager;
        this.opportunityId = manager.getOpportunityId();
        this.results = manager.getResults();
        createApproval(opportunityId);
    }
	
    //Create approval on opportunity
    private void createApproval(Id opportunityId){
        try {
            Approval.ProcessSubmitRequest req1 =new Approval.ProcessSubmitRequest();
            req1.setComments(createComments(results));
            req1.setObjectId(opportunityId);
            req1.setSubmitterId(UserInfo.getUserId());
            req1.setProcessDefinitionNameOrId('Consolidated_Screening_Approval');
            req1.setSkipEntryCriteria(false);
            Approval.ProcessResult result = Approval.process(req1);
        }
        catch(System.DmlException excp) {
            system.debug(excp);
        }
    }
    
    //Convert results object list into a string for use in approval comments
    private String createComments(List<Object> results){
        String comments = 'Search Results:\n';
        for(Object result : results){
            comments += result+'\n';
        }
        return comments;
    }
}