@isTest(seealldata=true)
public class Q2C_AttachmentsExtensionTest {
    static testMethod void myPage_Test() {
        PageReference pageRef = Page.Q2C_AttachmentsPage;
        Test.setCurrentPage(pageRef);
        
        Profile P = new Profile();
        P = [Select ID from Profile where Name = 'System Administrator'];
        List<User> AdmnUser = new List<User>();
        AdmnUser = [Select Id, ProfileID, Alias, TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, 
                    LanguageLocaleKey from User where ProfileID = :P.ID Limit 1];
        List<Lead> newLeads = new List<Lead>();
        
        User u = new User();
        
        For(User AU: AdmnUser){
            u.FirstName = 'Ron';
            u.LastName = 'Howard';
            u.ProfileID = P.ID;
            u.UserName = 'ron@qualtrics.com.q';
            u.email = 'ron@qualtrics.com';
            u.Alias = AU.Alias;
            u.TimeZoneSidKey = AU.TimeZoneSidKey;
            u.LocaleSidKey = AU.LocaleSidKey;
            u.EmailEncodingKey = AU.EmailEncodingKey;
            u.LanguageLocaleKey = AU.LanguageLocaleKey;
            u.Region__c = 'NA';
            u.Tier_A__c = True;
            u.Tier_B__c = True;
            u.Tier_C__c = True;
            u.Tier_D__c = True;
            u.Tier_E__c = True;
            u.Contract_Automation_Pilot__c = True;
        }
        insert u;
        
        Opportunity opp = (Opportunity)EB_Utility_CreateObjectForTesting.CreateObject('Opportunity');
        Opp.LID__LinkedIn_Company_Id__c = '5';
        opp.Forecast_PercentageV2__c = 0;
        opp.Opp_from_Lead_Conversion__c = false;
        opp.OwnerId = u.Id;
        insert opp;    
        
        
        ApexPages.currentPage().getParameters().put('id',opp.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        Q2C_AttachmentsExtension cls  = new Q2C_AttachmentsExtension(sc);
        cls.filename = 'Test File';
        cls.description = 'Test Description';
        String fileBody = 'Test Body';
        cls.fileBody = Blob.valueof(fileBody);
        //       ApexPages.StandardController sc = new ApexPages.standardController(opp);
        
        //     Q2C_AttachmentsExtension cls = new Q2C_AttachmentsExtension(sc);
        //     
        cls.closeAtt();
        cls.deleteAttachment();
        cls.getAlert();
        cls.processUpload();
    }
}