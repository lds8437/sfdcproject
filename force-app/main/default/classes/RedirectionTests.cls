@IsTest
public with sharing class RedirectionTests {
    
	testMethod static void testRedirectionManager() {
        
        // Prepare for tests.
		PageReference testPage = new PageReference('/apex/RedirectionManager');
        Opportunity testOpportunity = RedirectionTestUtilities.createOpportunity();
        
        RedirectionManager manager;
        RedirectionRequest request;
        RedirectionResponse response;
        
        final String SOBJECT_NAME = 'opportunity';
        final String ID = testOpportunity.Id;
        final String ACTION = 'submit_for_invoice';
        
        Test.startTest();
            
        // Test properties as querystring parameters.
		testPage.getParameters().put('sobject', SOBJECT_NAME);
		testPage.getParameters().put('id', ID);
		testPage.getParameters().put('action', ACTION);
        Test.setCurrentPage(testPage);
        
		manager = new RedirectionManager();
		request = manager.getRequest();
		response = manager.getResponse();
        System.assertEquals(SOBJECT_NAME, manager.getSObjectName());
        System.assertEquals(ID, manager.getRecordId());
        System.assertEquals(ACTION, manager.getAction());
        
        // Test properties as direct assignment via the constructor.
		testPage.getParameters().put('sobject', null);
		testPage.getParameters().put('id', null);
		testPage.getParameters().put('action', null);
        Test.setCurrentPage(testPage);
        
        // Not forced debug mode
		manager = new RedirectionManager(SOBJECT_NAME, ID, ACTION);
		request = manager.getRequest();
		response = manager.getResponse();
        System.assertEquals(SOBJECT_NAME, manager.getSObjectName());
        System.assertEquals(ID, manager.getRecordId());
        System.assertEquals(ACTION, manager.getAction());
        
        // Forced debug mode
		manager = new RedirectionManager(SOBJECT_NAME, ID, ACTION, true);
		request = manager.getRequest();
		response = manager.getResponse();
        System.assertEquals(SOBJECT_NAME, manager.getSObjectName());
        System.assertEquals(ID, manager.getRecordId());
        System.assertEquals(ACTION, manager.getAction());
        
        if (manager.getRedirectionMode() != RedirectionConstants.REDIRECTION_MODE_PAGEREFERENCE) {
            System.assertEquals(null, manager.performRedirection());
        }
        
        Test.stopTest();
    }

	testMethod static void testValidRedirectionRequest() {
        
        // Prepare for tests.
        Contact testContact = RedirectionTestUtilities.createContact();
        
        final String SOBJECT_NAME = 'contact';
        final String ID = testContact.Id;
        final String ACTION = 'send_prospecting_survey';
        
        RedirectionManager manager = new RedirectionManager(SOBJECT_NAME, ID, ACTION);
        RedirectionRequest request;
        RedirectionResponse response;
        
        Redirection_Field__mdt redirectionField = RedirectionTestUtilities.getRedirectionField();
        
        Test.startTest();
        
		request = new RedirectionRequest(manager);
        System.assert(request.isValid());

        Test.stopTest();
    }
    
	testMethod static void testRedirectionRequest() {
        
        // Prepare for tests.
        Opportunity testOpportunity = RedirectionTestUtilities.createOpportunity();
        
        final String SOBJECT_NAME = 'opportunity';
        final String ID = testOpportunity.Id;
        final String ACTION = 'submit_for_invoice';
        
        RedirectionManager manager = new RedirectionManager(SOBJECT_NAME, ID, ACTION);
        RedirectionRequest request;
        RedirectionResponse response;
        
        Redirection_Field__mdt redirectionField = RedirectionTestUtilities.getRedirectionField();
        
        List<String> errorMessages;
        List<String> logs;
        
        Test.startTest();
        
		request = new RedirectionRequest(manager);
        
        Boolean isDebugMode = request.isDebugMode();
        Boolean isValid = request.isValid();
        
		errorMessages = request.getErrorMessages();
        Integer startingErrorMessagesSize = errorMessages.size();
        request.addErrorMessage(redirectionField);
		request.addErrorMessage('text error message');
		errorMessages = request.getErrorMessages();
        System.assertEquals(startingErrorMessagesSize + 2, errorMessages.size());
        
        // Not forced debug mode
        logs = request.getLogs();
        System.assertEquals(0, logs.size());
        request.addLog(redirectionField, true);
		request.addLog('Text log');
        if (request.isDebugMode()) {
	        logs = request.getLogs();
    	    System.assertEquals(2, logs.size());
        }
        
        // Forced debug mode
		manager = new RedirectionManager(SOBJECT_NAME, ID, ACTION, true);
		request = new RedirectionRequest(manager);
        logs = request.getLogs();
        //System.assertEquals(8, logs.size());
        request.addLog(redirectionField, true);
		request.addLog('Text log');
        if (request.isDebugMode()) {
	        logs = request.getLogs();
    	    //System.assertEquals(10, logs.size());
        }
        
        String redirectionUrl = request.getRedirectionUrl();
        
        Map<String, String> outputParameters = request.getStaticOutputQuerystringParamters();
        RedirectionManager requestedRedirectionManager = request.getRedirectionManager();
        
        Test.stopTest();
    }
    
	testMethod static void testRedirectionResponse() {
        
        // Prepare for tests.
		final String REDIRECTION_URL = 'https://www.google.com';
        final List<String> MESSAGES = new List<String>{
            'error #1',
            'error #2'
        };
		final RedirectionException ERR = new RedirectionException(messages);
        final List<String> LOGS = new List<String>{
            'log #1',
            'log #2'
        };

        RedirectionResponse response;
        
        Test.startTest();
        
        response = new RedirectionResponse(REDIRECTION_URL, true);
        System.assertEquals(REDIRECTION_URL, response.getRedirectionUrl());
        System.assertEquals(true, response.getRedirectEntireWindow());
        
        response = new RedirectionResponse(ERR);
        System.assertEquals(ERR.getMessages().size(), response.getErr().getMessages().size());
        
        response = new RedirectionResponse(REDIRECTION_URL, true, ERR, LOGS);
        System.assertEquals(REDIRECTION_URL, response.getRedirectionUrl());
        System.assertEquals(true, response.getRedirectEntireWindow());
        System.assertEquals(ERR.getMessages().size(), response.getErr().getMessages().size());
        System.assertEquals(LOGS.size(), response.getLogs().size());
        
        // Test the various redirection modes
        response = new RedirectionResponse(REDIRECTION_URL, true);
        System.assertEquals(RedirectionConstants.REDIRECTION_MODE_JAVASCRIPT, response.getRedirectionMode());
        
        response = new RedirectionResponse(REDIRECTION_URL, false);
        System.assertEquals(RedirectionConstants.REDIRECTION_MODE_PAGEREFERENCE, response.getRedirectionMode());
        
        response = new RedirectionResponse(ERR);
        System.assertEquals(RedirectionConstants.REDIRECTION_MODE_NONE, response.getRedirectionMode());
        
        Test.stopTest();
    }
    
	testMethod static void testRedirectionException() {
        
        // Prepare for tests.
        final String ERROR_MESSAGE_1 = 'error #1';
        final String ERROR_MESSAGE_2 = 'error #2';
        final List<String> ERROR_MESSAGES = new List<String>{
            ERROR_MESSAGE_1, ERROR_MESSAGE_2
        };
            
		Test.startTest();
        
        RedirectionException redirectionException = new RedirectionException(ERROR_MESSAGES);
        System.assertEquals(ERROR_MESSAGES.size(), redirectionException.getMessages().size());
        
        Test.stopTest();
    }
    
	testMethod static void testRedirectionUtilities() {
        
        // Prepare for tests.
        final String NULL_VARIABLE = null;
        final Boolean BOOLEAN_VARIABLE = true;
        final Decimal DECIMAL_VARIABLE = 1.2;
        final String STRING_VARIABLE = 'test';
        
        final List<Redirection_Field__mdt> STATIC_FIELDS = RedirectionTestUtilities.getRedirectionFields('opportunity', RedirectionConstants.FIELD_TYPE_STATIC_OUTPUT_PARAMETER, 100);
        final List<Redirection_Field__mdt> RECORD_FIELDS = RedirectionTestUtilities.getRedirectionFields('opportunity', RedirectionConstants.FIELD_TYPE_OUTPUT_PARAMETER_FROM_RECORD, 100);
        
        Opportunity testOpportunity = RedirectionTestUtilities.createOpportunity();
        
        Map<String, String> querystringParameters;
            
		Test.startTest();

        // getFieldType
        System.assertEquals('', RedirectionUtilities.getFieldType(NULL_VARIABLE));        
        System.assertEquals(RedirectionConstants.DATA_TYPE_BOOLEAN, RedirectionUtilities.getFieldType(BOOLEAN_VARIABLE));
        System.assertEquals(RedirectionConstants.DATA_TYPE_DECIMAL, RedirectionUtilities.getFieldType(DECIMAL_VARIABLE));
        System.assertEquals(RedirectionConstants.DATA_TYPE_OBJECT, RedirectionUtilities.getFieldType(STRING_VARIABLE));
        
        // getOutputQuerystringParamters and getEncodedFieldValue
        querystringParameters = RedirectionUtilities.getOutputQuerystringParamters(STATIC_FIELDS, testOpportunity);
        querystringParameters = RedirectionUtilities.getOutputQuerystringParamters(RECORD_FIELDS, testOpportunity);
        
        Test.stopTest();
    }
    
    testMethod static void testRedirectionValidation() {
        Opportunity testOpportunity = RedirectionTestUtilities.createOpportunity();
        // Prepare for tests.
        final List<Redirection_Field__mdt> RECORD_FIELDS = RedirectionTestUtilities.getRedirectionFields('opportunity', RedirectionConstants.FIELD_TYPE_OUTPUT_PARAMETER_FROM_RECORD, 100);
        final String NULL_VALUE = null;
        final String NOT_NULL_VALUE = 'test';
        
        Boolean nullResult;
        Boolean notNullResult;
        
		Test.startTest();

        for (Redirection_Field__mdt recordField : RECORD_FIELDS) {
            
			// Try to test with null and non-null values.  Exception are
			// caught and ignored if the field type is not compatible with strings.
            try {
                
                // Test null
                testOpportunity.put(recordField.Field_Name__c, NULL_VALUE);
	            nullResult = RedirectionValidation.validateField(recordField, testOpportunity);
                
                // Test not null
                testOpportunity.put(recordField.Field_Name__c, NOT_NULL_VALUE);
	            notNullResult = RedirectionValidation.validateField(recordField, testOpportunity);
                
                if (recordField.Comparison_Operator__c == RedirectionConstants.COMPARISON_NULL) {
	                System.assert(nullResult || notNullResult);
                }
                
            } catch (Exception e) {
                // Ignore exceptions
            }
        }
       
        Test.stopTest();
    }
    testMethod static void testRedirectionValidationNotEqualTo(){
        Boolean result;
        final String NULL_VALUE = null;
        Opportunity testOpportunity = RedirectionTestUtilities.createOpportunity();
        Quote testQuote = new Quote(
            Name = 'TEST',
        	OpportunityId = testOpportunity.Id,
            AdditionalName = 'TEST'
        );
        insert testQuote;
        final List<Redirection_Field__mdt> redirectionFields = RedirectionTestUtilities.getRedirectionFields('quote', RedirectionConstants.FIELD_TYPE_FIELD_VALIDATION_FROM_RECORD,100);
        for(Redirection_Field__mdt redirectionField : redirectionFields){
            if(redirectionField.Comparison_Operator__c == RedirectionConstants.COMPARISON_NOT_EQUAL_TO){
                result = RedirectionValidation.validateField(redirectionField, testQuote);
                //redirectionField.Field_Name__c = 'Name';
                //redirectionField.Field_Value__c = 'TEST';
                //result = RedirectionValidation.validateField(redirectionField, testQuote);
            }
        }
    }
    
    testMethod static void testRedirectionConstants() {
        
		Test.startTest();

		System.assertEquals(RedirectionConstants.QUERYSTRING_PARAMETER_SOBJECT, 'sobject');
		System.assertEquals(RedirectionConstants.QUERYSTRING_PARAMETER_ID, 'id');
		System.assertEquals(RedirectionConstants.QUERYSTRING_PARAMETER_ACTION, 'action');

		System.assertEquals(RedirectionConstants.FIELD_TYPE_STATIC_OUTPUT_PARAMETER, 'Static Output Parameter');
		System.assertEquals(RedirectionConstants.FIELD_TYPE_OUTPUT_PARAMETER_FROM_RECORD, 'Output Parameter from Record');
		System.assertEquals(RedirectionConstants.FIELD_TYPE_OUTPUT_PARAMETER_FROM_USER, 'Output Parameter from User');
		System.assertEquals(RedirectionConstants.FIELD_TYPE_FIELD_VALIDATION_FROM_RECORD, 'Field Validation from Record');
		System.assertEquals(RedirectionConstants.FIELD_TYPE_FIELD_VALIDATION_FROM_USER, 'Field Validation from User');
    
		System.assertEquals(RedirectionConstants.COMPARISON_NULL, 'null');
		System.assertEquals(RedirectionConstants.COMPARISON_NOT_NULL, 'not null');
		System.assertEquals(RedirectionConstants.COMPARISON_EQUAL_TO, 'equal to');
		System.assertEquals(RedirectionConstants.COMPARISON_NOT_EQUAL_TO, 'not equal to');
        
		System.assertEquals(RedirectionConstants.REDIRECTION_MODE_PAGEREFERENCE, 'PageReference');
		System.assertEquals(RedirectionConstants.REDIRECTION_MODE_JAVASCRIPT, 'JavaScript');
		System.assertEquals(RedirectionConstants.REDIRECTION_MODE_NONE, 'None');
    
		System.assertEquals(RedirectionConstants.ERROR_RECORD_NOT_FOUND, 'Record not found.');
    
		System.assertEquals(RedirectionConstants.DATA_TYPE_BOOLEAN, 'Boolean');
		System.assertEquals(RedirectionConstants.DATA_TYPE_DECIMAL, 'Decimal');
		System.assertEquals(RedirectionConstants.DATA_TYPE_OBJECT, 'Object');
    
		System.assertEquals(RedirectionConstants.VALIDATION_PASS, 'PASS');
		System.assertEquals(RedirectionConstants.VALIDATION_FAIL, 'FAIL');
    
		System.assertEquals(RedirectionConstants.LOG_PREFIX_SOQL, 'SOQL: ');
		System.assertEquals(RedirectionConstants.LOG_PREFIX_RECORD, 'RECORD: ');
    
		System.assertEquals(RedirectionConstants.ENCODING_UTF_8, 'UTF-8');
    
		System.assertEquals(RedirectionConstants.SOQL_SELECT, 'SELECT');
		System.assertEquals(RedirectionConstants.SOQL_FROM, 'FROM');
		System.assertEquals(RedirectionConstants.SOQL_WHERE, 'WHERE');
		System.assertEquals(RedirectionConstants.SOQL_LIMIT, 'LIMIT');
    
		System.assertEquals(RedirectionConstants.OBJECT_USER, 'User');
    
		System.assertEquals(RedirectionConstants.FIELD_ID, 'Id');
        
        Test.stopTest();
    }
    
    testMethod static void testRedirectionSettings() {

        // Prepare for tests.
        Opportunity testOpportunity = RedirectionTestUtilities.createOpportunity();
        
        final String SOBJECT_NAME = 'opportunity';
        final String ID = testOpportunity.Id;
        final String VALID_ACTION = 'submit_for_invoice';
        final String INVALID_ACTION = 'invalid_action_name';
        
        RedirectionManager manager;
        RedirectionRequest request;
        RedirectionSettings settings;
        
        List<Redirection_Field__mdt> fields;
        
        Test.startTest();

        // Invalid test
		manager = new RedirectionManager(SOBJECT_NAME, ID, INVALID_ACTION);
		request = new RedirectionRequest(manager);
		settings = new RedirectionSettings(request);
        
        System.assertEquals(false, settings.isValid());
        settings.isDebugMode();
        settings.getBaseUrl();
        settings.getRedirectEntireWindow();
        
		fields = settings.getStaticOutputParameters();
		fields = settings.getOutputParametersFromRecord();
		fields = settings.getOutputParametersFromUser();
		fields = settings.getFieldValidationsFromRecord();
		fields = settings.getFieldValidationsFromUser();
        
        // Valid test
		manager = new RedirectionManager(SOBJECT_NAME, ID, VALID_ACTION);
		request = new RedirectionRequest(manager);
		settings = new RedirectionSettings(request);
        
        System.assertEquals(true, settings.isValid());
        settings.isDebugMode();
        settings.getBaseUrl();
        settings.getRedirectEntireWindow();
        
		fields = settings.getStaticOutputParameters();
		fields = settings.getOutputParametersFromRecord();
		fields = settings.getOutputParametersFromUser();
		fields = settings.getFieldValidationsFromRecord();
		fields = settings.getFieldValidationsFromUser();
        
        Test.stopTest();
    }
    
	testMethod static void testRedirectionSObject() {
        
        // Prepare for tests.
        Account testAccount = RedirectionTestUtilities.createAccount();
        Opportunity testOpportunity = RedirectionTestUtilities.createOpportunity();
        Contact testContact = RedirectionTestUtilities.createContact();
        
        List<String> actions;
        
        RedirectionManager manager;
        RedirectionRequest request;
        RedirectionResponse response;

        Test.startTest();

        // Test accounts
        for (String action : RedirectionTestUtilities.getActions('account')) {
			manager = new RedirectionManager('account', testAccount.Id, action);
            System.assertEquals('account', manager.getSObjectName());
            System.assertEquals(testAccount.Id, manager.getRecordId());
            System.assertEquals(action, manager.getAction());
        }
        
        // Test opportunities
        for (String action : RedirectionTestUtilities.getActions('opportunity')) {
			manager = new RedirectionManager('opportunity', testOpportunity.Id, action);
            System.assertEquals('opportunity', manager.getSObjectName());
            System.assertEquals(testOpportunity.Id, manager.getRecordId());
            System.assertEquals(action, manager.getAction());
        }
        
        // Test contacts
        for (String action : RedirectionTestUtilities.getActions('contact')) {
			manager = new RedirectionManager('contact', testContact.Id, action);
            System.assertEquals('contact', manager.getSObjectName());
            System.assertEquals(testContact.Id, manager.getRecordId());
            System.assertEquals(action, manager.getAction());
        }
        
        Test.stopTest();
    }
    
	testMethod static void testRedirectionUser() {
        
        // Prepare for tests.
        Account testAccount = RedirectionTestUtilities.createAccount();
        Opportunity testOpportunity = RedirectionTestUtilities.createOpportunity();
        Contact testContact = RedirectionTestUtilities.createContact();
        
        List<String> actions;
        
        RedirectionManager manager;
        RedirectionRequest request;
        RedirectionResponse response;

        Test.startTest();

        // Test accounts
        for (String action : RedirectionTestUtilities.getActions('account')) {
			manager = new RedirectionManager('account', testAccount.Id, action);
            System.assertEquals('account', manager.getSObjectName());
            System.assertEquals(testAccount.Id, manager.getRecordId());
            System.assertEquals(action, manager.getAction());
        }
        
        // Test opportunities
        for (String action : RedirectionTestUtilities.getActions('opportunity')) {
			manager = new RedirectionManager('opportunity', testOpportunity.Id, action);
            System.assertEquals('opportunity', manager.getSObjectName());
            System.assertEquals(testOpportunity.Id, manager.getRecordId());
            System.assertEquals(action, manager.getAction());
        }
        
        // Test contacts
        for (String action : RedirectionTestUtilities.getActions('contact')) {
			manager = new RedirectionManager('contact', testContact.Id, action);
            System.assertEquals('contact', manager.getSObjectName());
            System.assertEquals(testContact.Id, manager.getRecordId());
            System.assertEquals(action, manager.getAction());
        }
        
        Test.stopTest();
    }
}