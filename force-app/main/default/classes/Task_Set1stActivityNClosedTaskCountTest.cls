/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Task_Set1stActivityNClosedTaskCountTest {

    	public  static Lead insert_Lead() {
        return ( new Lead(LastName = 'test Lead',
                            LeadSource = 'Referral',
                            Status = 'Suspect',
                            Company = 'test Company',
                            curr_Assigned_to_Follow_Up_Queue__c = system.now().addDays(-4)));
    	}
    	
        public  static Task insert_Task(Id pUserId, Id pLeadId) {
        	return( new Task (OwnerId = pUserId,
        	                   Subject = 'Call',
        	                   Priority = 'Normal',
        	                   Status = 'Completed',
        	                   ActivityDate =System.today().addDays(-2),
        	                   WhoId = pLeadId));
        }
    
    static testMethod void updateLead_FirstActivityAndClosedTaskCount() {
    	
    	Lead objLead = insert_Lead();
        database.insert(objLead);
        
    	User objUser = new User(Alias = 'standt', Email='standarduser123baldazzi@test.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = userInfo.getProfileId(), 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser123baldazzi@test.com');
        database.insert(objUser);
        
        Task objTask = insert_Task(objUser.Id , objLead.Id);
        database.insert(objTask);
        
        Lead lead =[select Id, curr_First_Activity__c, curr_Activity_cnt_from_Disp_Status__c from Lead];       
        system.assertNotEquals(lead.curr_Activity_cnt_from_Disp_Status__c, 0);
        system.assertNotEquals(lead.curr_First_Activity__c, null);
    }
    static testMethod void updateLead() {
        
        Lead objLead = insert_Lead();
        objLead.curr_Final_Disposition_Time_Stamp__c = system.now();
        database.insert(objLead);
        
        User objUser = new User(Alias = 'standt', Email='standarduser123baldazzi@test.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = userInfo.getProfileId(), 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser123baldazzi@test.com');
        database.insert(objUser);
        
        Task objTask = insert_Task(objUser.Id , objLead.Id);
        database.insert(objTask);
        
    }
}