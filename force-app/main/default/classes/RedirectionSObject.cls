public with sharing class RedirectionSObject extends RedirectionBase {

    /**
     * Class Constructor
     * 
     * This constructor does nothing more than implement the
     * constructor from the abstract class it's extending.
     */
    public RedirectionSObject(RedirectionRequest request, RedirectionSettings settings) {
        super(request, settings);
    }

    /**
     * Returns a dynamic SOQL statement to query for this
     * specific record.  The query itself is performed within
     * the abstract class.
     */
    private String getSoql() {
        return getSoqlSelect()
            + ' ' + RedirectionConstants.SOQL_FROM + ' ' + String.escapeSingleQuotes(request.getRedirectionManager().getSObjectName())
            + ' ' + RedirectionConstants.SOQL_WHERE + ' ' + RedirectionConstants.FIELD_ID + ' = \'' + String.escapeSingleQuotes(request.getRedirectionManager().getRecordId()) + '\''
            + ' ' + RedirectionConstants.SOQL_LIMIT + ' 1';
    }
    
    /**
     * Returns the dynamic SELECT statement for the dynamic
     * SOQL statement in the getSoql method.
     */
    private String getSoqlSelect() {
        String soqlSelect = RedirectionConstants.SOQL_SELECT + ' ';
        
        // Loop through every unique field name related to this SObject.
        List<String> distinctFieldNames = getDistinctFieldNames();
        for (Integer i = 0; i < distinctFieldNames.size(); i++) {
            soqlSelect += distinctFieldNames[i];
            
            // If this is not the last field being added
            // a comma is required.
            if (i < distinctFieldNames.size() - 1) {
                soqlSelect += ',';
            }
        }
        return soqlSelect;
    }
    
    /**
     * Returns a unique list of field names related to this SObject.
     */
    private List<String> getDistinctFieldNames() {
        
        // All field names are added to a set, which by definition
        // removes any duplicates.
        Set<String> distinctFieldNames = new Set<String>();
        
        // Add all URL nodes.
        for (Redirection_Field__mdt field : settings.getUrlNodesFromRecord()) {
            distinctFieldNames.add(field.Field_Name__c);
        }
        
        // Add all output parameters.
        for (Redirection_Field__mdt field : settings.getOutputParametersFromRecord()) {
            distinctFieldNames.add(field.Field_Name__c);
        }
        
        // Add all field validations.
        for (Redirection_Field__mdt field : settings.getFieldValidationsFromRecord()) {
            distinctFieldNames.add(field.Field_Name__c);
        }
        
        // If no fields exist, add the Id to ensure the SOQL
        // statement is considered valid.
        if (distinctFieldNames.size() == 0) {
            distinctFieldNames.add('Id');
        }
        
        // Add all unique field names to a list and return the
        // results.
        return new List<String>(distinctFieldNames);
    }
    
    /**
     * This is considered value if:
     * 1) The record variable has been populated
     * and
     * 2) The record field values satisfy all custom field validations.
     */
    private void validate() {
        if (record == null) {
            request.addErrorMessage(RedirectionConstants.ERROR_RECORD_NOT_FOUND);
        } else {
	        for (Redirection_Field__mdt field : settings.getFieldValidationsFromRecord()) {
                if (RedirectionValidation.validateField(field, record)) {
                    request.addLog(field, false);
					request.addErrorMessage(field);
                } else {
                    request.addLog(field, true);
                }
	        }
        }
    }
    
    /**
     * Returns the URL nodes related to the current record
     * as specified in the settings custom metadata.
     */
    public List<String> getUrlNodes() {
        return RedirectionUtilities.getUrlNodes(settings.getUrlNodesFromRecord(), record);
    }

    /**
     * Returns the querystring parameters related to the
     * current record as specified in the settings custom metadata.
     */
    public Map<String, String> getOutputQuerystringParamters() {
        return RedirectionUtilities.getOutputQuerystringParamters(settings.getOutputParametersFromRecord(), record);
    }
}