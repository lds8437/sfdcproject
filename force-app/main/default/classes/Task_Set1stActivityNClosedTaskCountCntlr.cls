public without sharing class Task_Set1stActivityNClosedTaskCountCntlr {
    
    public void updateLead_FirstActivityAndClosedTaskCount(list<Task> taskNewList){
        set<Id> leadIdSet = new set<Id>();
        for(Task task : taskNewList){
            if(task.WhoId != null){
                if(task.IsClosed && task.WhoId.getSObjectType() == Schema.Lead.SObjectType){
                    leadIdSet.add(task.WhoId);
                }
            }
        }
        
        if(leadIdSet!= null && leadIdSet.size() > 0){
             map<Id,Lead> leadIdToLeadMap = new map<Id,Lead>([
                                                       select Id
                                                            , curr_First_Activity__c
                                                            , curr_Assigned_to_Follow_Up_Queue__c
                                                            , curr_Final_Disposition_Time_Stamp__c
                                                         from Lead
                                                        where Id IN : leadIdSet
                                                          and curr_Assigned_to_Follow_Up_Queue__c != null
            ]);
            
            list<Task> allTasksList = new list<Task>();
            if(!leadIdToLeadMap.isEmpty() && leadIdToLeadMap.size() > 0){
                allTasksList = [
                         Select Id
                              , Subject
                              , WhoId
                              , Date_Time_Completed__c
                              , Type
                              , IsClosed
                           From Task
                          where WhoId IN: leadIdToLeadMap.keySet()
                            and IsClosed =: true
                ];
            }
            
            map<Id,list<Task>> leadIdToTasksListMap = new map<Id,list<Task>>();
            if(allTasksList != null && allTasksList.size() > 0) {
                for(Task taskObj : allTasksList) {
                    if(leadIdToTasksListMap != null && leadIdToTasksListMap.containsKey(taskObj.WhoId))
                        leadIdToTasksListMap.get(taskObj.WhoId).add(taskObj);
                    else
                        leadIdToTasksListMap.put(taskObj.WhoId, new list<Task>{taskObj});
                }
            }
            
            list<Lead> leadToBeUpdatedList = new list<Lead>();
            if(!leadIdToTasksListMap.isEmpty() && leadIdToTasksListMap.size() > 0) {
                for(Id leadId : leadIdToTasksListMap.keySet()) {
                    System.debug('Updating Lead ' + leadId);
                    Lead lead = leadIdToLeadMap.get(leadId);
                    System.debug(lead.curr_Assigned_to_Follow_Up_Queue__c);
                    System.debug(lead.curr_Final_Disposition_Time_Stamp__c);
                    Integer tasksCount = 0;
                    
                    if(lead.curr_First_Activity__c == null) {
                        datetime myDateTime = datetime.now();
                        lead.curr_First_Activity__c = myDateTime;
                    }
                    
                    if(lead.curr_Final_Disposition_Time_Stamp__c == null) {
                        for(Task objTask : leadIdToTasksListMap.get(leadId)){
                            if(objTask.Date_Time_Completed__c != null
                            && objTask.Date_Time_Completed__c >= lead.curr_Assigned_to_Follow_Up_Queue__c ) {
                                tasksCount ++;
                            }
                            System.debug(objTask.Date_Time_Completed__c);
                        }
                    }
                    else if(lead.curr_Final_Disposition_Time_Stamp__c != null) {
                        for(Task objTask : leadIdToTasksListMap.get(leadId)){
                            if(objTask.Date_Time_Completed__c != null
                            && objTask.Date_Time_Completed__c >= lead.curr_Assigned_to_Follow_Up_Queue__c 
                            && objTask.Date_Time_Completed__c <= lead.curr_Final_Disposition_Time_Stamp__c) {
                                tasksCount ++;
                            }
                            System.debug(objTask.Date_Time_Completed__c);
                        }
                    }
                    lead.curr_Activity_cnt_from_Disp_Status__c = tasksCount;
                    leadToBeUpdatedList.add(lead);
                }
            }
            
            if(leadToBeUpdatedList != null && leadToBeUpdatedList.size() > 0) {
                update leadToBeUpdatedList;
            }
        }
    }
}