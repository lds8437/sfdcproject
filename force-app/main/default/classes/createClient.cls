//Controller for createClient Lightning Component
public with sharing class createClient {

    @AuraEnabled
    public static ID newClient(Id accountid, Client__c client, Boolean newbillingadd, Boolean newendclientadd){
        client.Account__c = accountid;
        if(!newbillingadd){//If the user has not selected to enter a billing address different from the account HQ address, populate new client billing address with HQ address
            Account a = [SELECT BillingStreet,BillingCity,BillingState,BillingPostalCode FROM Account WHERE ID = :accountid];
            client.Billing_Street_Address__c = a.BillingStreet;
            client.Billing_City__c = a.BillingCity;
            client.Billing_State_Province__c = a.BillingState;
            client.Billing_Zip_Postal_Code__c = a.BillingPostalCode;
        }
        if(!newendclientadd){//If the user has not selected to enter a different end user address than the Client billing address, populate new client end user/shipping address to match billing address
            client.Shipping_Street__c = client.Billing_Street_Address__c;
            client.Shipping_City__c = client.Billing_City__c;
            client.Shipping_State__c = client.Billing_State_Province__c;
            client.Shipping_Zip_Postal_Code__c = client.Billing_Zip_Postal_Code__c;
            client.ShippingCountry2__c = client.BillingCountry2__c;
        }
        try{
            insert client;
            return client.Id;
        }
        catch(exception e){
            system.debug('error while trying to insert client: ' + e);
            return null;
        }
    }
}