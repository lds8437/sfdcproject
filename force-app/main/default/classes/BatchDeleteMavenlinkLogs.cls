global class BatchDeleteMavenlinkLogs implements Database.Batchable<Integer>, Database.AllowsCallouts, Database.Stateful {
    
	private Integer page{get;set;}
	private Boolean processMore{get;set;}
	private Integer maxPages{get;set;}	
	
	public BatchDeleteMavenlinkLogs(Integer page,Integer maxPages){
		this.page = page;
		this.maxPages = maxPages;
	}
	
    global List<Integer> start(Database.BatchableContext context) {
        String m = 'start';
		try{
  		
    		return new Integer[]{page};		} catch(Exception e){System.debug('Error = ('+e.getStackTraceString()+') '+e.getMessage()); return null;}
    }

	global void execute(Database.BatchableContext context, List<Integer> pages){
        String m = 'execute';
		try{
			processMore = true;
			List<mavenlink__Mavenlink_Log__c> deleteRecords= [select id,name from mavenlink__Mavenlink_Log__c LIMIT 10000];
			if (deleteRecords.size()>0){
				System.debug('Deleting '+deleteRecords.size()+' records from Mavenlink_Log__c (Page '+page+')');
				delete deleteRecords;
				if(page>=maxPages) processMore = false;
			}
			else{
				System.debug('No records to delete from Mavenlink_Log__c (Page '+page+')');	processMore = false;
			}
		} catch(Exception e){System.debug('Error = ('+e.getStackTraceString()+') '+e.getMessage()); }
	}

    global void finish(Database.BatchableContext ctx) {
        String m = 'finish';
		try{
			if (processMore != null && processMore){
				if(!Test.isRunningTest()) {Database.executeBatch(new BatchDeleteMavenlinkLogs((page+1),maxPages), 1);}
			}
	  	} catch(Exception e){System.debug('Error = ('+e.getStackTraceString()+') '+e.getMessage());}
    }
}