@IsTest
public class CentralizedBuyingRequestTest {
    
    testMethod static void centralizedBuyingRequest(){
        
        CentralizedBuyingRequestController request;
        PageReference testPage = new PageReference('/apex/centralizedBuyingRequest');
        Opportunity testOpportunity = RedirectionTestUtilities.createOpportunity();
        
        Test.startTest();
        
        testPage.getParameters().put('oppId',testOpportunity.Id);
        Test.setCurrentPage(testPage);
        request = new CentralizedBuyingRequestController();
        request.redirectToSurvey();
        
        Test.stopTest();
    }
	
}