public class CloneOppController {
    
    public Flow.Interview.Q2C_Clone_Opportunity_Custom myFlow {get; set;}
    
    public String getmyID() {
        
        string returnURL;
        
        if (myFlow==null) 
            return '';
        else 
            returnURL= '/'+ myFlow.varNewOppID; /*+'/e?retURL=/'+myFlow.varNewOppID;*/
        return returnURL;
          
    }
    
    public PageReference getQID(){ 
        
        PageReference p = new PageReference(getmyID());
        p.setRedirect(true);
        return p;
       
    }
}