@istest
public class leadProductsTest{
public static testmethod void leadProductsTest(){
	lead l = new lead(firstname='fname', lastname='lname', Product__c='Research Suite;360', company='cpny');
	insert l;
	l.Product__c='Panels';
	update l;
	lead l1 = [select Product__c from lead where id = :l.id];
	system.assertequals(l1.Product__c, 'Research Suite;360;Panels');

}
}