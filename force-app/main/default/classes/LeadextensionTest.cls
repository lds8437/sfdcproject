@isTest
public with sharing class LeadextensionTest {
	
	static testMethod void LeadextensionTest() {
		
		test.startTest();
        
        //Creating the Lead Record
        lead leadRecd = new lead(firstname='Test ', lastname='Lead', Product__c='Research Suite;360', company='Test Company', Status='Qualified');
        insert leadRecd;
        
        PageReference pageRef = new PageReference('/LeadStatus?id='+leadRecd.Id);
       
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController standardController = new ApexPages.StandardController(leadRecd);
      
        Leadextension objController = new Leadextension(standardController);
        
        objController.cancelUpdate();
        
        list<Lead> leadrecds= new list<Lead>();
        leadrecds=[Select id,Status,OwnerId from Lead where id=:leadRecd.id];
        system.debug('*****'+leadrecds[0].Status);
        //Checking assert for the Cancel Button
        system.assertequals(leadrecds[0].Status, 'Qualified');
        
        objController.updateLead();
        list<Lead> leadrecds1= new list<Lead>();
        leadrecds1=[Select id,Status,OwnerId from Lead where id=:leadRecd.id];
        //Checking assert for the Update Lead Button
        //system.assertequals(leadrecds1[0].Status, 'Reject');
        
        test.stopTest();
        
        
	}

}