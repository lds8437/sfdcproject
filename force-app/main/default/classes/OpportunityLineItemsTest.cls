@isTest
public with sharing class OpportunityLineItemsTest {
	static Account acct {get;set;}
    static Opportunity opp {get;set;}

     @testsetup
    static void test_setup(){
    	TestFactory.disableTriggers();
    	acct = TestFactory.createAccount();
    	insert acct;

    	opp = TestFactory.createOpportunity(acct.Id, null);
    	insert opp;
    }

    static void QueryStartingItems(){
        acct = database.Query('select '+BuildQuery('Account')+' where name = \'test Account\'');
        opp = [SELECT Id from Opportunity where AccountId =: acct.Id LIMIT 1];
    }

    static string BuildQuery(String obj){
        Map<String, Schema.SObjectField> SobjtField = Schema.getGlobalDescribe().get(obj).getDescribe().fields.getMap();
        string qry = '';
        for (Schema.SObjectField s : SobjtField.values()) {
            qry += s + ',';
        }
        qry = qry.removeEnd(',');
        qry = qry+' from '+ obj;
        return qry;
    }

    @isTest static void test_queryOli() {
		QueryStartingItems();

		ApexPages.StandardController sc = new ApexPages.StandardController(opp);
		PageReference pageRef = Page.OpportunityLineItems;
		Test.setCurrentPage(pageRef);
		pageRef.getParameters().put('id',opp.Id);
		OpportunityLineItems controller = new OpportunityLineItems(sc);

		Test.startTest();
			controller.queryOli();
		Test.stopTest();
		system.assertNotEquals(controller.oliList,null);
	}
}