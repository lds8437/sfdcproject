public with sharing class PartnerShareQPartnerHelper {
	public static void afterInsert(Map<Id, Q_Partner__c> newMap) {
		assignQPartnerShares(newMap);
    } // end afterInsert

    public static void afterUpdate(Map<Id, Q_Partner__c> newMap, Map<Id, Q_Partner__c> oldMap) {
		assignQPartnerShares(newMap);
    } // end afterUpdate


    private static void assignQPartnerShares(Map<Id, Q_Partner__c> newMap) {
		//Do PartnerExecutive Only.
		//Custom Setting to get Value...
        List<Q_Partner__Share> leadShares  = new List<Q_Partner__Share>();
        Q_Partner__Share vShare;

		//you need the User's Partner Account in the trigger set
		//Get AssociatedAccounts
		Set<Id> setPartners = new Set<Id>();
        // List<Lead> enhancedLeadList = [SELECT Id, Partner__r.RelatedAccount__c FROM Lead WHERE Partner__c <> null AND Id IN :newMap.keyset()];
		List<Q_Partner__c> enhancedLeadList = [SELECT Id, RelatedAccount__c FROM Q_Partner__c WHERE Id IN :newMap.keyset()];
        for (Q_Partner__c lp: enhancedLeadList) {
            if (lp.RelatedAccount__c != null && !setPartners.contains(lp.RelatedAccount__c))
                setPartners.add(lp.RelatedAccount__c);
        }

		//Get AssociatedRoles
		//Make Map of AccountIdsToListofRoleIds
		Map<Id, List<Id>> accountIdToRoleIdListMap = new Map<Id, List<Id>>();
		List<UserRole> associatedUserRoles = [SELECT Id, PortalAccountId FROM UserRole WHERE PortalAccountId IN: setPartners];
		Set<Id> userRoleIdSet = new Set<Id>();
		for(UserRole ur : associatedUserRoles){
			userRoleIdSet.add(ur.Id);
			if(accountIdToRoleIdListMap.get(ur.PortalAccountId) == null){
				List<Id> tempList = new List<Id>();
				tempList.add(ur.Id);
				accountIdToRoleIdListMap.put(ur.PortalAccountId, tempList);
			}
			else{
				accountIdToRoleIdListMap.get(ur.PortalAccountId).add(ur.Id);
			}
		}
		System.debug(accountIdToRoleIdListMap);
		//Get AssociatedGroups
		//Make Map of RoleIdsToListofGroupIds
		// List<Group> associatedGroups = [SELECT Id, RelatedId FROM Group WHERE RelatedId IN: userRoleIdSet AND Type = 'RoleAndSubordinates' AND DeveloperName LIKE '%PartnerExecutive%'];
		List<Group> associatedGroups = [SELECT Id, RelatedId, DeveloperName FROM Group WHERE RelatedId IN: userRoleIdSet AND Type = 'Role'];
		System.debug('Size: ' + associatedGroups.size());
		Map<Id, Map<String, Id>> userRoleIdToGroupIdsMap = new Map<Id, Map<String, Id>>();
		for(Group g : associatedGroups){
			if(userRoleIdToGroupIdsMap.get(g.RelatedId) == null){
				Map<String, Id> tempList = new Map<String, Id>();
				if(g.DeveloperName.contains('PartnerExecutive')){
					tempList.put('PartnerExecutive', g.Id);
				}
				if(g.DeveloperName.contains('PartnerManager')){
					tempList.put('PartnerManager', g.Id);
				}
				if(g.DeveloperName.contains('PartnerUser')){
					tempList.put('PartnerUser', g.Id);
				}
				userRoleIdToGroupIdsMap.put(g.RelatedId, tempList);
			}
			else{
				Map<String, Id> tempList = new Map<String, Id>();
				if(g.DeveloperName.contains('PartnerExecutive')){
					userRoleIdToGroupIdsMap.get(g.RelatedId).put('PartnerExecutive', g.Id);
				}
				if(g.DeveloperName.contains('PartnerManager')){
					userRoleIdToGroupIdsMap.get(g.RelatedId).put('PartnerManager', g.Id);
				}
				if(g.DeveloperName.contains('PartnerUser')){
					userRoleIdToGroupIdsMap.get(g.RelatedId).put('PartnerUser', g.Id);
				}
				// userRoleIdToGroupIdsMap.get(g.RelatedId).put();
			}
		}
		System.debug(userRoleIdToGroupIdsMap);
		//Make accountIdToGroupIdListMap
		Map<Id, Map<String, Id>> accountIdToGroupIdListMap = new Map<Id, Map<String, Id>>();
		for(Id accId : new List<Id>(setPartners)){
			if(accountIdToRoleIdListMap.get(accId) != null){
				for(Id roleId : accountIdToRoleIdListMap.get(accId)){
					if(accountIdToGroupIdListMap.get(accId) == null){
						if(userRoleIdToGroupIdsMap.get(roleId) != null){
							accountIdToGroupIdListMap.put(accId, userRoleIdToGroupIdsMap.get(roleId));
						}
					}
					else{
						if(userRoleIdToGroupIdsMap.get(roleId) != null){
							for(String tempId : userRoleIdToGroupIdsMap.get(roleId).keySet()){
								Map<String, Id> tempMap = new Map<String, Id>();
								tempMap.put(tempId, userRoleIdToGroupIdsMap.get(roleId).get(tempId));
								accountIdToGroupIdListMap.get(accId).putall(tempMap);
							}
						}
					}
				}
			}
		}
		System.debug(accountIdToGroupIdListMap);
		List<Id> leadIdsToDeleteShares = new List<Id>();
        for (Q_Partner__c l : enhancedLeadList) {
			leadIdsToDeleteShares.add(l.Id);
			if(accountIdToGroupIdListMap.get(l.RelatedAccount__c) != null){
				for (String s : accountIdToGroupIdListMap.get(l.RelatedAccount__c).keySet()) {
					Id r = accountIdToGroupIdListMap.get(l.RelatedAccount__c).get(s);
					if(s == 'PartnerExecutive' || s == 'PartnerManager'){
						leadIdsToDeleteShares.add(r);
						vShare = new Q_Partner__Share();
		                vShare.ParentId = l.Id;
		                vShare.UserOrGroupId = r;  // Jannard to set the group id here
		                vShare.AccessLevel = 'Edit';
					}
					else{
						leadIdsToDeleteShares.add(r);
						vShare = new Q_Partner__Share();
		                vShare.ParentId = l.Id;
		                vShare.UserOrGroupId = r;  // Jannard to set the group id here
		                vShare.AccessLevel = 'Read';
					}


	                leadShares.add(vShare);
	                system.debug('******Create Share:' + vShare);
	            }
			}
        }

		List<Q_Partner__Share> lsToDeleteList = [SELECT Id FROM Q_Partner__Share WHERE ParentId IN: leadIdsToDeleteShares];

        try {
			if(!lsToDeleteList.isEmpty()){
				Database.DeleteResult[] ldr = Database.Delete(lsToDeleteList,false);
				if(ldr[0].isSuccess()){
	                system.debug('**** Lead Shares deleted successfully');
	            }
				else {
	                Database.Error err = ldr[0].getErrors()[0];
	                if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&
	                   err.getMessage().contains('AccessLevel')){
	                       system.debug('**** Failed successfully? ');
	                   }
	                else{
	                    system.debug('**** Lead Shares deleted failed' + err.getStatusCode() + ' ' + err.getMessage());
	                }
	            }
			}

            Database.SaveResult[] lsr = Database.insert(leadShares,false);
            if(lsr[0].isSuccess()){
                system.debug('**** Lead Shares added successfully');
            }
            else {
                Database.Error err = lsr[0].getErrors()[0];
                if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&
                   err.getMessage().contains('AccessLevel')){
                       system.debug('**** Failed successfully? ');
                   }
                else{
                    system.debug('**** Lead Shares added failed' + err.getStatusCode() + ' ' + err.getMessage());
                }
            }
        } catch (Exception e) {
            //handle your exception
        }

    }
}