@isTest()
public class getRelatedCasesTest {
static Account a;
    static Opportunity o;
    
    @IsTest static void testRelatedCases(){
        a = new Account(Name = 'Test Account',
                        BillingStreet = '123 Billing Street',
                        BillingCity = 'Billville',
                        BillingState = 'BL',
                        BillingPostalCode = '12345');
        insert a;
        RecordType xmtype = [SELECT ID FROM RecordType WHERE sObjectType = 'Opportunity' AND Name = 'XM'];
        o = new Opportunity(Name = 'Test Opportunity',
                            AccountId = a.Id,
                            StageName = 'Discover and Assess',
                            CloseDate = system.today(),
                            RecordTypeId = xmtype.Id,
                            Turn_Off_Initial_CX_EX_RC_Amounts__c = TRUE);
        insert o;
        
        List<Case> cse = getRelatedCases.getCases(5, 1,o.Id);
        Integer ttl = getRelatedCases.getTotal(o.Id);
    }
}