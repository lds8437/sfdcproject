public with sharing class RedirectionUtilities {

    /**
     * Returns field types related to the generic object
     * provided.  The only field types we need to react
     * to at this time are boolean and decimal.  All other
     * types are returned as generic SObjects and treated
     * like strings.
     */
    public static String getFieldType(Object o) {
        if (o == null) return '';
        if (o instanceof Boolean)			return RedirectionConstants.DATA_TYPE_BOOLEAN;
        if (o instanceof Decimal)			return RedirectionConstants.DATA_TYPE_DECIMAL;
        return RedirectionConstants.DATA_TYPE_OBJECT;
    }

    /**
     * Returns the URL nodes relating to the URL node fields
     * and SObject provided.
     */
    public static List<String> getUrlNodes(List<Redirection_Field__mdt> urlNodeFields, SObject record) {
        List<String> urlNodes = new List<String>();
        Redirection_Field__mdt urlNodeField;
        for (Integer i = 0; i < urlNodeFields.size(); i++) {
            urlNodeField = urlNodeFields[i];
            
            // Try to return the encoded value of the record.
            // This is dynamic and could easily be configured
            // wrong.  We protect ourselves by wrapping this
            // in a try/catch block.
            try {
	            urlNodes.add(getEncodedFieldValue(urlNodeField, record));
            } catch (Exception e) {
                System.debug('Error adding url node: ' + e.getMessage());
            }
		}
        return urlNodes;
    }
    
    /**
     * Returns the querystring parameters relating to the output fields
     * and SObject provided.
     */
    public static Map<String, String> getOutputQuerystringParamters(List<Redirection_Field__mdt> outputFields, SObject record) {
        Map<String, String> mapOfParameters = new Map<String, String>();
        Redirection_Field__mdt outputField;
        for (Integer i = 0; i < outputFields.size(); i++) {
            outputField = outputFields[i];
            
            // Try to return the encoded value of the record.
            // This is dynamic and could easily be configured
            // wrong.  We protect ourselves by wrapping this
            // in a try/catch block.
            try {
	            mapOfParameters.put(outputField.Property_Name__c, getEncodedFieldValue(outputField, record));
            } catch (Exception e) {
                System.debug('Error adding field: ' + e.getMessage());
            }
		}
        return mapOfParameters;
    }
    
    /**
     * Returns the encoded value of the field on the SObject
     * related to the redirection field provided.
     */
    private static String getEncodedFieldValue(Redirection_Field__mdt redirectionField, SObject record) {
        String unencodedFieldValue;
        
        // For static output parameters we simply set the variable
        // to be the field value from the redirection field record.
        if (redirectionField.Type__c == RedirectionConstants.FIELD_TYPE_STATIC_OUTPUT_PARAMETER) {
			unencodedFieldValue = redirectionField.Field_Value__c;
            
        // For all other types we set the variable to be the field
        // value from the SObject relating to the redirection field
        // record.
        } else {
            unencodedFieldValue = (String)record.get(redirectionField.Field_Name__c);
        }
        
        // If a prefix should be applied we prepend the prefix value
        // to the field value variable.
        if (redirectionField.Value_Prefix__c != null) {
            unencodedFieldValue = redirectionField.Value_Prefix__c + unencodedFieldValue;
        }
        
        // If a suffix should be applied we append the suffix value
        // to the field value variable.
        if (redirectionField.Value_Suffix__c != null) {
            unencodedFieldValue = unencodedFieldValue + redirectionField.Value_Suffix__c;
        }
        
        // In all scenarios we return the URL encoded version of the
        // field value variable.
        return EncodingUtil.urlEncode(unencodedFieldValue, RedirectionConstants.ENCODING_UTF_8);
    }
}