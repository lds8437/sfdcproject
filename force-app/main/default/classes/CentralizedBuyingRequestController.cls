public class CentralizedBuyingRequestController {
    
    private Id oppId;
    private Opportunity opp;
    private String redirectLink;
    
    Private String BASE_URL = 'https://customersatisfaction.qualtrics.com/jfe/form/SV_0JqQSR9B9qLqKq1?oppId';
    private String DATA_QUERY = 'SELECT Id, SyncedQuoteId, SyncedQuote.Number_of_Custom_Services__c FROM Opportunity WHERE Id =: oppId LIMIT 1';
    Private String QUOTE_PARAM = '&QuoteId=';
    Private String SERVICE_PARAM = '&customService=';
    Private String USER_PARAM = '&SurveyTakerSFDCId=';
  
    public CentralizedBuyingRequestController(){
        this.oppId = ApexPages.currentPage().getParameters().get('oppId');
        execute();
    }
    
    private void execute(){
        opp = getData(oppId);
        buildRedirectLink(opp);
    }
    
    private Opportunity getData(Id oppId){
       return database.query(DATA_QUERY);
    }
    
    private String buildRedirectLink(Opportunity opp){
        redirectLink = BASE_URL+opp.Id+QUOTE_PARAM+opp.SyncedQuoteId+SERVICE_PARAM
            	       +opp.SyncedQuote.Number_of_Custom_Services__c
                       +USER_PARAM+UserInfo.getUserId(); 
        return redirectLink;
    }
    
    public PageReference redirectToSurvey(){
        PageReference pageReference = new PageReference(redirectLink);
        pageReference.setRedirect(true);
        return pageReference;
    }
}