@isTest
public class q2c_Employee_TestClass {
    static testMethod void createEmployee(){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        User u = new user();
        u.LastName = 'Test Code';
        u.Email = 'test@test.com';
        u.Alias = 'Tcode';
        u.Username = 'test534859@test.com.qualtrics';
        u.CommunityNickname = 'test12';
        u.LocaleSidKey = 'en_US';
        u.TimeZoneSidKey = 'GMT';
        u.ProfileID = profileId.Id;
        u.LanguageLocaleKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.EIN__c = 9999;
        insert u;
                
        Department__c d = new Department__c();
        d.NSInternalDepartmentID__c = 'Test Dept 1234';
        d.Name = 'Test Department';
        insert d;
        
        List<Employee__c> elist = new List<Employee__c>();
        
        Employee__c e = new Employee__c();
        e.Name = 'Test Employee';
        e.EmployeeIndentificationNumber__c = 9999;
        e.NSInternalDepartmentID__c = 'Test Dept 1234';
        elist.add(e);
        
        Employee__c e2 = new Employee__c();
        e2.Name = 'Test Employee2';     
        e2.NSInternalDepartmentID__c = 'Test Dept 5678';
        elist.add(e2);
        
        Employee__c e3 = new Employee__c();
        e3.Name = 'Test Employee2';  
        e3.EmployeeIndentificationNumber__c = 1111;        
        elist.add(e3);
        
        insert elist;
        
        User u2 = new user();
        u2.LastName = 'Test Code2';
        u2.Email = 'test@test2.com';
        u2.Alias = 'Tcode2';
        u2.Username = 'test534859@test.com.qualtrics2';
        u2.CommunityNickname = 'test122';
        u2.LocaleSidKey = 'en_US';
        u2.TimeZoneSidKey = 'GMT';
        u2.ProfileID = profileId.Id;
        u2.LanguageLocaleKey = 'en_US';
        u2.EmailEncodingKey = 'UTF-8';
        u2.EIN__c = 1111;
        insert u2;
    }
    
}