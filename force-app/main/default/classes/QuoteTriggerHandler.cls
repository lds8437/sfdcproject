public with sharing class QuoteTriggerHandler {
	public static boolean firstRunOnBeforeInsert = true;
	public static boolean firstRunOnAfterInsert = true;
	public static boolean firstRunOnBeforeUpdate = true;
	public static boolean firstRunOnAfterUpdate = true;
	public static boolean firstRunOnBeforeDelete = true;
	public static boolean firstRunOnAfterDelete = true;
	public static boolean firstRunOnUndelete = true;
	
	
	public void OnBeforeInsert(List<Quote> newObjects){
		if(firstRunOnBeforeInsert){
			firstRunOnBeforeInsert = false;
			TaxExempt.startFromQuote(newObjects);
		} 
	}
	
	public void OnAfterInsert(List<Quote> newObjects, map<Id,Quote> MapNewMap){
		if(firstRunOnAfterInsert){
			firstRunOnAfterInsert = false;
			QuoteToOpportunityRollup.taxRollup(newObjects);
		}
	}
	
	public void OnBeforeUpdate(List<Quote> oldObjects, List<Quote> updatedObjects, map<Id,Quote> MapNewMap, map<Id,Quote> MapOldMap){
		if(firstRunOnBeforeUpdate){
			firstRunOnBeforeUpdate = false;
			TaxExempt.startFromQuote(updatedObjects);
		}
	}
	
	public void OnAfterUpdate(List<Quote> oldObjects, List<Quote> updatedObjects, map<Id,Quote> MapNewMap, map<Id,Quote> MapOldMap){
		if(firstRunOnAfterUpdate){
			firstRunOnAfterUpdate = false;
			QuoteToOpportunityRollup.taxRollup(updatedObjects);
		}
	}
	
	public void OnBeforeDelete(List<Quote> ObjectsToDelete, map<Id,Quote> MapNewMap, map<Id,Quote> MapOldMap){
		if(firstRunOnBeforeDelete){
			firstRunOnBeforeDelete = false;
	
		}
	}
	
	public void OnAfterDelete(List<Quote> deletedObjects, map<Id,Quote> MapOldMap){
		if(firstRunOnAfterDelete){
			firstRunOnAfterDelete = false;
	
		}
	}
	
	public void OnUndelete(List<Quote> restoredObjects){
		if(firstRunOnUndelete){
			firstRunOnUndelete = false;
			
		}
	}
	
}