@isTest
public class SendEmailControllerTest {

 /*   @isTest
    static void testSendInvoice(){
        TestFactory.disableTriggers();
        Opportunity oppObj = TestFactory.createOpportunity(null, null);
        insert oppObj;
        Opportunity oppObjClone = oppObj.clone(true, true, true, true);
        oppObj.Sales_Process_Stage__c = 'Invoice';
        Map<Id,Opportunity> oldOppMap = new Map<Id,Opportunity>();
        oldOppMap.put(oppObjClone.Id, oppObjClone);
        Attachment attObj = TestFactory.createAttachment(oppObj.Id);
        insert attObj;
        Test.startTest();
            SendemailController.sendInvoice(new List<Opportunity>{oppObj}, oldOppMap);
        Test.stopTest();
    }*/

    @isTest static void SendEmailTestMethod() {
        // PageReference pageRef = Page.InvoiceEmailPage;
        //  Test.setCurrentPage(pageRef);

        Opportunity opp = (Opportunity)EB_Utility_CreateObjectForTesting.CreateObject('Opportunity');
        Opp.LID__LinkedIn_Company_Id__c = '5';
        opp.Forecast_PercentageV2__c = 0;
        opp.Opp_from_Lead_Conversion__c = false;
        opp.Sales_Process_Stage__c = 'Contract Negotiations';
        insert opp;

        Attachment objAtt = new Attachment();
        objAtt.Name = 'QSO';
        objAtt.body = Blob.valueof('string');
        objAtt.ParentId = opp.Id;
        insert objAtt;

        opp.Sales_Process_Stage__c = 'Invoice';
        update opp;

        //  System.currentPagereference().getParameters().put('id',opp.id);
        //  SendemailController cont = new SendemailController();

        // cont.getOpp();
        // cont.sendEmailFunction();
    }

    @isTest static void QSOTestMethod() {
        Opportunity opp = (Opportunity)EB_Utility_CreateObjectForTesting.CreateObject('Opportunity');
        Opp.LID__LinkedIn_Company_Id__c = '5';
        opp.Forecast_PercentageV2__c = 0;
        opp.Opp_from_Lead_Conversion__c = false;
        insert opp;

        Attachment objAtt = new Attachment();
        objAtt.Name = 'QSO';
        objAtt.body = Blob.valueof('string');
        objAtt.ParentId = opp.Id;
        insert objAtt;

        delete objAtt;
    }

    @isTest static void AttachmentTestMethod() {
        PageReference pageRef = Page.Q2C_AttachmentsPage;
        Test.setCurrentPage(pageRef);

        Opportunity opp = (Opportunity)EB_Utility_CreateObjectForTesting.CreateObject('Opportunity');
        Opp.LID__LinkedIn_Company_Id__c = '5';
        opp.Forecast_PercentageV2__c = 0;
        opp.Opp_from_Lead_Conversion__c = false;
        insert opp;

        Attachment objAtt = new Attachment();
        objAtt.Name = 'QSO';
        objAtt.body = Blob.valueof('string');
        objAtt.ParentId = opp.Id;
        insert objAtt;

        System.currentPagereference().getParameters().put('id', opp.id);
        ApexPages.StandardController stdOpp = new ApexPages.StandardController(opp);
        Q2C_AttachmentsExtension attcon = new Q2C_AttachmentsExtension(stdOpp);

        attcon.processUpload();
        attcon.closeAtt();
        attcon.deleteAttachment();
    }
}