public class RecordUtilities {

    /**
     * Returns the boolean field value of the SObject
     * provided relating to the field name provided.  If
     * either critical property are null or an error
     * occurs the default value is returned.
     */
    public static Boolean getBooleanFieldValue(SObject sObj, String fieldName, Boolean defaultValue) {
        
        // Exit immediately if any of the required properties are
        // not provided.
        if (sObj == null || fieldName == null) {
            return defaultValue;
        }
        
        // By default, set the return value to the default value provided.
        Boolean value = defaultValue;
        
        // Attempt to set the return value to the field value related
        // field name and SObject provided.  This is wrapped in a try/
        // catch block to protect against errors if incorrect configurations
        // are made.
        try {
            value = Boolean.valueOf(sObj.get(fieldName));
            System.debug('sObj: ' + sObj.Id + ' - ' + fieldName + ' - ' + sObj.get(fieldName));
        } catch (Exception e) {
            System.debug('Error getting boolean value: ' + e);
        }
        return value;
    }

    /**
     * Convenience method to reduce the properties needed to
     * get a boolean field value.  This executes the method
     * with the same name, but sets the default value to false.
     */
    public static Boolean getBooleanFieldValue(SObject sObj, String fieldName) {
        return getBooleanFieldValue(sObj, fieldName, false);
    }
}