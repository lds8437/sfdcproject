public class cdr_CreateContractRecord {

    public static void createCdr(Opportunity[] opps){
       
        Map<Id,Opportunity> op = new Map<Id,Opportunity>([
            SELECT Id, 
            AccountId, 
            Client__c, 
            SyncedQuoteId, 
            Qualification_Contact__c, 
            Billing_Contact__c,             
            Products__c, 
            Account.Name, 
            Client__r.BillToName__c, 
            OwnerId, 
            CurrencyISOCode, 
            SyncedQuote.ExpirationDate,
            SyncedQuote.Multi_Year_Quote__c,
            SyncedQuote.Service_Template_Codes__c,
            SyncedQuote.Partner_Email__c,
            RecordType.DeveloperName
            FROM Opportunity where id in:opps and RecordType.DeveloperName!='Panels' and RecordType.DeveloperName!='Panels Closed'
        ]);
    
        //Set<Id> opp = op.keySet();
        String rec;
        List<Contract_Document__c> cds = new List<Contract_Document__c>();
        List<Id> cdIds = new List<Id>();
        for(Id o : op.keySet()){
            Contract_Document__c cd = new Contract_Document__c();  
            cd.Opportunity__c = op.get(o).Id;            
            cd.Synced_Quote__c = op.get(o).SyncedQuoteId;
            cd.Primary_Contact__c = op.get(o).Qualification_Contact__c;             
            cd.Legal_Contact__c = op.get(o).Qualification_Contact__c;             
            cd.Billing_Contact__c = op.get(o).Billing_Contact__c;
            cd.Products__c = op.get(o).Products__c;  
            cd.Account__c = op.get(o).AccountId;
            cd.Client__c = op.get(o).Client__c;
            cd.Subscriber__c = op.get(o).Account.Name;
            cd.Implementation__c = op.get(o).SyncedQuote.Service_Template_Codes__c;
            cd.Partner_Email__c = op.get(o).SyncedQuote.Partner_Email__c;
          /*  if(op.get(o).OwnerId!=NULL){
                 cd.OwnerId = op.get(o).OwnerId;
            }  */ 
             //   cd.Quote_Type__c = 'Quote';
            cd.Status__c = 'Create/Sync Quote'; 
            cd.QSO_Type__c = 'eQSO (Standard)';
            cd.CurrencyIsoCode = op.get(o).CurrencyISOCode;
          //  cd.Expiration_Date__c = op.get(o).SyncedQuote.ExpirationDate;           
            cd.Language_Type__c = 'Standalone';
            cd.MultiYear_Quote__c = op.get(o).SyncedQuote.Multi_Year_Quote__c;
            cds.add(cd);
        }
        insert cds;
        for (Contract_Document__c cd : cds){
            cdIds.add(cd.Id);
        }
        updateLegalContact(cdIds);
    }
    
    @future
    Public Static Void updateLegalContact(List<Id> cdIds){
        List<Signatory_Contact__mdt> legalContact = [SELECT Contact_Id__c FROM Signatory_Contact__mdt WHERE Contact_Type__c = 'Legal'];
        List<Contract_Document__c> cds = [SELECT Id, Qualtrics_Legal_Contact__c FROM Contract_Document__c WHERE Id in :cdIds];
        for(Contract_Document__c cd : cds){
            cd.Qualtrics_Legal_Contact__c = legalContact[0].Contact_Id__c;
        }
        update cds;
    }
    
    public static void updateCdr(Opportunity[] opps){
        system.debug(logginglevel.error, '***** opps='+opps);
        Map<Id,Contract_Document__c> cdMap = new Map<Id,Contract_Document__c>([
            SELECT 
            Id, 
            Opportunity__c, 
            Status__c, 
            Data_Center_Location__c, 
            OwnerId
            FROM Contract_Document__c WHERE Opportunity__c in: opps]);
        
        //Set<Id> cdSet = cdMap.keySet();
        system.debug(logginglevel.error, '***** cdMap='+cdMap);
        Map<Id,Opportunity> op = new Map<Id,Opportunity>([
            SELECT 
            Id, 
            AccountId, 
            Client__c, 
            SyncedQuoteId, 
            SyncedQuote.ExpirationDate, 
            SyncedQuote.Multi_Year_Quote__c,
            Qualification_Contact__c, 
            Products__c, 
            OnboardingWizardUsed__c, 
            CurrencyISOCode, 
            Owner.Id, 
            Owner.IsActive,
            SyncedQuote.Service_Template_Codes__c,
            SyncedQuote.Partner_Email__c,
            Billing_Contact__c            
            FROM Opportunity where id in:opps and RecordType.DeveloperName!='Panels' and RecordType.DeveloperName!='Panels Closed'
        ]);
        
        //Set<Id> opp = op.keySet();
        system.debug(logginglevel.error, '***** op='+op);
        List<Contract_Document__c> cds = new List<Contract_Document__c>();

        Map<Id,Id> opportunityIdToContractDocumentIdMap = new Map<Id,Id>();
        for(Contract_Document__c contractDocumentObj : cdMap.values()){
            opportunityIdToContractDocumentIdMap.put(contractDocumentObj.Opportunity__c, contractDocumentObj.Id);
        }
         if(!cdMap.isEmpty()){
        for(Opportunity opportunityObj : op.values()){
            Contract_Document__c cd = new Contract_Document__c();
            cd.Id = opportunityIdToContractDocumentIdMap.get(opportunityObj.Id);
            cd.Synced_Quote__c = opportunityObj.SyncedQuoteId;
          //  cd.Expiration_Date__c = opportunityObj.SyncedQuote.ExpirationDate;
            cd.CurrencyIsoCode = opportunityObj.CurrencyISOCode;
           
                 if(cdMap.get(opportunityIdToContractDocumentIdMap.get(opportunityObj.Id)).Status__c == ''){
                cd.Status__c = 'Create/Sync Quote';}
            else if(cdMap.get(opportunityIdToContractDocumentIdMap.get(opportunityObj.Id)).Status__c == 'Create/Sync Quote' && opportunityObj.SyncedQuoteId!=Null){
                cd.Status__c = 'Review Contract Details';
            }else if(cdMap.get(opportunityIdToContractDocumentIdMap.get(opportunityObj.Id)).Status__c == 'Review Contract Details' && cdMap.get(opportunityIdToContractDocumentIdMap.get(opportunityObj.Id)).Data_Center_Location__c != NULL ){
                cd.Status__c = 'Ready for Signature';
            }
           
            cd.Primary_Contact__c = opportunityObj.Qualification_Contact__c; 
            cd.Billing_Contact__c = opportunityObj.Billing_Contact__c;
            cd.Products__c = opportunityObj.Products__c;  
            cd.Account__c = opportunityObj.AccountId;
            cd.Client__c = opportunityObj.Client__c;
            cd.MultiYear_Quote__c = opportunityObj.SyncedQuote.Multi_Year_Quote__c;
            cd.Implementation__c = opportunityObj.SyncedQuote.Service_Template_Codes__c;
            cd.Partner_Email__c = opportunityObj.SyncedQuote.Partner_Email__c;
            if(opportunityObj.Owner.IsActive == TRUE){
                cd.OwnerId = opportunityObj.Owner.Id;                
            }
            
            cds.add(cd);
        }}

    
        if(!cds.isEmpty()){
             update cds;
        }
       
    }    
}