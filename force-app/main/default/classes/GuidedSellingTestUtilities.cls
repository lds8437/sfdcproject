public with sharing class GuidedSellingTestUtilities {

    public static void createSObjects(List<SObject> sObjs) {
        List<Database.SaveResult> saveResults = Database.insert(sObjs, false);
        for (Database.SaveResult saveResult : saveResults) {
            System.debug(saveResult);
        }
    }
        
    public static List<Stage_Details__mdt> getAllStageDetails(String objectName) {
        return [SELECT Id, Field_Value__c, Display_Order__c,
    	         	(SELECT Id, Label__c, Display_Order__c, Results_Field_Name__c
                	 FROM Completion_Criteria__r)
	             FROM Stage_Details__mdt
        	     WHERE Object_Name__c = : objectName
                 ORDER BY Display_Order__c ASC
            	 LIMIT 100];
    }
    
    public static List<Completion_Criteria__mdt> getAllCompletionCriteriaRecords() {
        return [SELECT Id, Label__c, Display_Order__c, Results_Field_Name__c,
						Stage_Details__r.Scope__c, Stage_Details__r.Object_Name__c,
						Stage_Details__r.Field_Name__c, Stage_Details__r.Field_Value__c
                 FROM Completion_Criteria__mdt
                 LIMIT 1000];
    }
    
    public static List<Dynamic_Action_Location__mdt> getAllGlobalDynamicActionLocations() {
		return [SELECT Id, Display_Order__c,
                	Stage_Details__r.Scope__c, Stage_Details__r.Object_Name__c,
                	Stage_Details__r.Field_Name__c, Stage_Details__r.Field_Value__c,
					Dynamic_Action__r.Id, Dynamic_Action__r.Label__c,
					Dynamic_Action__r.User_Interface__c, Dynamic_Action__r.Destination_Type__c,
					Dynamic_Action__r.Go_To_Destination__c, Dynamic_Action__r.Open_in_New_Window__c,
					Dynamic_Action__r.Modal_Title__c, Dynamic_Action__r.Record_Id_Output_Parameter_Name__c,
	                Dynamic_Action__r.User_Id_Output_Parameter_Name__c, Dynamic_Action__r.Tooltip__c,
	                Dynamic_Action__r.Controlling_Checkbox_Field_Name__c
				FROM Dynamic_Action_Location__mdt
                WHERE Stage_Details__c <> null];
    }
    
    public static List<Dynamic_Action__mdt> getAllDynamicActionRecords() {
        return [SELECT Id, Label__c, User_Interface__c, Destination_Type__c,
					Go_To_Destination__c, Open_in_New_Window__c, Modal_Title__c,
                	Record_Id_Output_Parameter_Name__c
				FROM Dynamic_Action__mdt
				LIMIT 1000];
    }
    
    public static Account createAccount() {
        return createAccounts(1)[0];
    }
    
    public static List<Account> createAccounts(Integer qty) {
        List<Account> records = new List<Account>();
        Account record;
        for (Integer i = 0; i < qty; i++) {
			record = new Account();
            record.Name = 'Test';
            records.add(record);
        }
        createSObjects(records);
        return records;
    }
    
    public static Opportunity createOpportunity() {
        return createOpportunities(1)[0];
    }
    
    public static List<Opportunity> createOpportunities(Integer qty) {
        List<Opportunity> records = new List<Opportunity>();
        Account parentRecord = createAccount();
        Opportunity record;
        for (Integer i = 0; i < qty; i++) {
			record = new Opportunity();
            record.AccountId = parentRecord.Id;
            record.Name = 'Test';
            record.StageName = 'Pending';
            record.CloseDate = System.today();
            record.Turn_Off_Initial_CX_EX_RC_Amounts__c = true;
            records.add(record);
        }
        createSObjects(records);
        return records;
    }
    
    public static Guided_Sales__c createGuidedSalesRecord() {
        return createGuidedSalesRecords(1)[0];
    }
    
    public static List<Guided_Sales__c> createGuidedSalesRecords(Integer qty) {
        List<Guided_Sales__c> records = new List<Guided_Sales__c>();
        Opportunity parentRecord = createOpportunity();
        Guided_Sales__c record;
        for (Integer i = 0; i < qty; i++) {
			record = new Guided_Sales__c();
            record.Opportunity__c = parentRecord.Id;
            records.add(record);
        }
        createSObjects(records);
        return records;
    }
}