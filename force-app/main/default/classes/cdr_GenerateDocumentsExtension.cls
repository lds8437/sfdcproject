global with sharing class cdr_GenerateDocumentsExtension {
    
    private ApexPages.StandardController std;
    public List<Quote> lstQuote {get; set;}
    public Boolean isRendered {get; set;}
    public List<String> options{get; set;}
    public String opts {get; set;}
    public Boolean qrocr = FALSE;
    public Boolean qropo = FALSE;
    public Boolean qr = FALSE;
    public Boolean nr = FALSE;
    public decimal conversionrate;
    public decimal amtusd;
    public Opportunity opps {get; set;}
    public Contract_Document__c cd;
    public List<Quote> Quotes;
    public List<Attachment> att;
    public Client__c client;
    public Quote quote;
    public boolean nc = FALSE;
    public List<String> qreq;
    public Blob fileBody {get;set;}//Get file body
    public String fileName {get;set;}
    public List<Attachment> cusQSO {get;set;}
    public List<SelectOption> qsoTypeOptions {get;set;}
    public Quote qte {get;set;}
    
    public cdr_GenerateDocumentsExtension(ApexPages.StandardController stdCtrl) {
        if(!Test.isRunningTest()){
            stdCtrl.addFields(new List<String>{
                'Payment_Terms__c','Custom_Payment_Terms__c','Amount','Client__c','Qualification_Contact__c','Opportunity_Type__c','Sales_Process_Stage__c','CurrencyIsoCode','SyncedQuoteId','QSO_Override_Reason__c','StageName', 'AccountId' 
                    });
            
        }        
        std = stdCtrl;
        //getOpportunity();
        getCDStages();
        lstQuote = getQuotes();
       
        getOpps();
        if(opps.Client__c!=null){
            getClient();
        }
        getct();
        getAmtusd();
        if(opps.Client__c!=null){
            getqrocr();
        }
        if(opps.Client__c!=null){
            getqropo();
        }
        if(opps.Client__c!=null){
            getQr();
        }
        if(opps.Client__c!=null){
            getNr();
        }
        getQuote();
         qte = getQuote();
        getNc();
        //  getCusQSO();
          populateQsoTypeOptions();  
    }
    
    
/*    public pageReference updatePage(){
        
        getCDStages();
        lstQuote = getQuotes();
        
        getOpps();
        if(opps.Client__c!=null){
            getClient();
        }
        getct();
        getAmtusd();
        if(opps.Client__c!=null){
            getqrocr();
        }
        if(opps.Client__c!=null){
            getqropo();
        }
        if(opps.Client__c!=null){
            getQr();
        }
        if(opps.Client__c!=null){
            getNr();
        }
        getQuote();
        qte = getQuote();
        getNc();
        //  getCusQSO();
        populateQsoTypeOptions();  
        return null;
    }*/
    
    //POPULATE PICKLIST WITH QSO OPTIONS
    public void populateQsoTypeOptions(){
        qsoTypeOptions = new List<SelectOption>();
        for(Schema.PicklistEntry f : Contract_Document__c.QSO_Type__c.getDescribe().getPicklistValues()) {
            
            if(quote != null && opps != null && (opps.Payment_Terms__c == 'Custom' || quote.Multi_Year_Quote__c == true) && f.getValue() == 'eQSO (Standard)'){
                qsoTypeOptions.add(new SelectOption(f.getValue(), f.getLabel(), true));
            } else {
                qsoTypeOptions.add(new SelectOption(f.getValue(), f.getLabel()));
            }
            
        }
    }
    
    public void getCDStages() {
        options = new List<String>();
        //getting the Object's picklist's values and adding them into an array
        for(Schema.PicklistEntry f : Contract_Document__c.Status__c.getDescribe().getPicklistValues()) {
            options.add(f.getLabel());
        }
        //Stringifying the array and assigning it to a string
        opts = JSON.serialize(options);
    }   
    
    public String selectedQuoteType{get;set;}
    public String selectedXMQuoteType{get;set;}
    
    //To get Quote and attachment to the Quote
    public pageReference addQuoteAttachment() {
        //Get quote for the opportunity
        ContentVersion cv = new ContentVersion();
        cv.title = fileName;
        cv.PathOnClient = this.filename;
        cv.VersionData = this.fileBody;
        /*Attachment attachment = new Attachment();
attachment.body = this.fileBody;
attachment.parentId = opps.SyncedQuoteId;
attachment.description = this.filename;
attachment.ContentType = 'application/pdf';
attachment.Name= fileName;*/
        // insert the attahcment
        try {
            insert cv;
            Approval.ProcessSubmitRequest req1 =new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval.');
            req1.setObjectId(opps.SyncedQuoteId);
            req1.setSubmitterId(UserInfo.getUserId());
            req1.setProcessDefinitionNameOrId('Tax_Exempt');
            req1.setSkipEntryCriteria(true);
            Approval.ProcessResult result = Approval.process(req1);
        }
        catch(System.DmlException excp) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, excp.getMessage()));
        }
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.Id].ContentDocumentId;
        cdl.LinkedEntityId = opps.Id;
        cdl.ShareType = 'I';
        cdl.Visibility = 'InternalUsers';
        try {
            insert cdl;
        }
        catch(System.DmlException excp) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, excp.getMessage()));
        }
        // reset the file for the view state
        fileBody = Blob.valueOf(' ');
        PageReference generateDocsPage = Page.cdr_GenerateDocs;
        generateDocsPage.getParameters().put('id', opps.Id);
        return generateDocsPage;
    }//End addQuoteAttachment
    
    //To get PO and attachment to the Opp
    public pageReference addPOAttachment() {
        //Get PO for the opportunity
        ContentVersion cv = new ContentVersion();
        cv.title = 'PO';
        cv.PathOnClient = this.filename;
        cv.VersionData = this.fileBody;
        /*Attachment attachment = new Attachment();
attachment.body = this.fileBody;
attachment.parentId = opps.Id;
attachment.description = this.filename;
attachment.ContentType = 'application/pdf';
attachment.Name= 'PO';*/
        opps.PO_Attached__c  = TRUE;
        // insert the attahcment
        try {
            update opps;
            insert cv;              
        }
        catch(System.DmlException excp) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, excp.getMessage()));
        }
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.Id].ContentDocumentId;
        cdl.LinkedEntityId = opps.Id;
        cdl.ShareType = 'I';
        cdl.Visibility = 'InternalUsers';
        try {
            insert cdl;
        }
        catch(System.DmlException excp) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, excp.getMessage()));
        }
        // reset the file for the view state
        opps = getOpps();
        fileBody = Blob.valueOf(' ');
        PageReference generateDocsPage = Page.cdr_GenerateDocs;
        generateDocsPage.getParameters().put('id', opps.Id);
        generateDocsPage.setRedirect(true);
        return generateDocsPage;
    }//End addPOAttachment
    
    //To get QSO and attachment to the Opp
    public pageReference addQSOAttachment() {
        
        //Get PO for the opportunity
        ContentVersion cv = new ContentVersion();
        cv.title = 'QSO';
        cv.PathOnClient = this.filename;
        cv.VersionData = this.fileBody;
        /*Attachment attachment = new Attachment();
attachment.body = this.fileBody;
attachment.parentId = opps.Id;
attachment.description = this.filename;
attachment.ContentType = 'application/pdf';
attachment.Name= 'QSO';*/
        opps.QSO_Attached__c  = TRUE;
        // insert the attahcment
        try {
            insert cv;
            update opps;
        }
        catch(System.DmlException excp) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, excp.getMessage()));
        }
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.Id].ContentDocumentId;
        cdl.LinkedEntityId = opps.Id;
        cdl.ShareType = 'I';
        cdl.Visibility = 'InternalUsers';
        try {
            insert cdl;
        }
        catch(System.DmlException excp) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, excp.getMessage()));
        }
        // reset the file for the view state
        fileBody = Blob.valueOf(' ');
        PageReference generateDocsPage = Page.cdr_GenerateDocs;
        generateDocsPage.getParameters().put('id', opps.Id);
        generateDocsPage.setRedirect(true);
        return generateDocsPage;
    }//End addPOAttachment    
    
    public pageReference redirectQuoteAttachment() {
        PageReference pageRef = Page.QuoteAttachment;
        pageRef.getParameters().put('id', opps.Id);
        return pageRef;
    }
    
    public pageReference redirectPOAttachment() {
        PageReference pageRef = Page.POAttachement;
        pageRef.getParameters().put('id', opps.Id);
        return pageRef;
    }
    
    public pageReference redirectQSOAttachment() {
        PageReference pageRef = Page.QSOAttachment;
        pageRef.getParameters().put('id', opps.Id);
        return pageRef;
    }
    
    public pageReference redirectGenerateDocPage() {
        PageReference pageRef = Page.cdr_GenerateDocs;
        pageRef.getParameters().put('id', opps.Id);
        return pageRef;
    }
    
    public Client__c getClient() {
        if(opps.Client__c!=null){
            client = [SELECT Id, In_NetSuite__c, ShippingCountry2__c, Shipping_Street__c, Shipping_City__c, Shipping_State__c, Shipping_Zip_Postal_Code__c FROM Client__c WHERE ID =:opps.Client__c];
        }
        
        return client;
    }
    
    public void updateQuotes() {
        system.debug('this worked');
        try {
            update lstQuote;
        } catch(Exception e) {
            System.debug(e.getMessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }
    
    public void updateSyncedQuote() {
        system.debug('this worked');
        try {
            update qte;
        } catch(Exception e) {
            System.debug(e.getMessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }
    
    private Boolean updateCd() {
        Boolean result = true;
        if (null != cd) {
            try {
                update cd;
            } catch (Exception e) {
                System.debug(e.getMessage());
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, e.getMessage()));
                /*  String msg = e.getMessage();
Integer pos;
if(-1 == (pos - msg.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION, '))) {
ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
}*/
                result = false;
            }
        }
        return result;
    }
    
    public PageReference save() {
        Boolean result = true;
        if (opps.Id != null) {
            result = updateCd();
        }
        if (result) {
            std.save();
            if(opps.Client__c!=null){
                getClient();
            }
            getct();
            getAmtusd();
            if(opps.Client__c!=null){
                getqrocr();
            }
            if(opps.Client__c!=null){
                getqropo();
            }
            if(opps.Client__c!=null){
                getQr();
            }
            if(opps.Client__c!=null){
                getNr();
            }
            getNc();
            
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Changes saved'));
        }
        cd = getCd();
        system.debug(logginglevel.error, '***** cd='+cd);
        system.debug(logginglevel.error, '***** cd.Legal_Contact__r.Name='+cd.Legal_Contact__r.Name);
        isRendered = false;
        return null;
    }
    
    public PageReference cancel() {
        isRendered = false;
        return null;
    }
    
    public Opportunity getOpps() {
        
        if(std != null){
            opps = (Opportunity) std.getRecord();
        } else {
            opps = [SELECT Amount,Client__c,Qualification_Contact__c,
                    Opportunity_Type__c,Sales_Process_Stage__c,CurrencyIsoCode,
                    SyncedQuoteId,QSO_Override_Reason__c,StageName,Payment_Terms__c, Custom_Payment_Terms__c 
                    FROM Opportunity 
                    WHERE ID =:ApexPages.currentPage().GetParameters().Get('Id')];
        }
        
        
        return opps;
    }
    
    public Contract_Document__c getCd() {
        if ((opps.Id) != null) {
            cd = [  SELECT  Id,
                  Name,
                  MSA_Date__c,
                  MSA__c,
                  Quote_Type__c,
                  Contract_Status__c,
                  Subscriber__c,
                  Validation_Type__c,
                  Status__c,
                  Custom_Contract_Status__c,
                  Data_Center_Location__c,
                  Synced_Quote__c,
                  Legal_Contact__c,
                  Legal_Contact__r.Name,
                  Payment_Choice__c,
                  Payment_Choice_2__c,
                  QSO_Type__c,
                  Billing_Contact__c,
                  Billing_Contact__r.Name
                  FROM    Contract_Document__c
                  WHERE   Opportunity__c =:opps.Id LIMIT 1];
        }
        return cd;
    }
    
    public List<Quote> getQuotes() {
        if(opps == null){
            getOpps();
        }
        if (opps.Id != null) {
            quotes = [  SELECT  Id,
                      Name,
                      Select_for_PDF__c,
                      TotalPrice,
                      CurrencyISOCode,
                      IsSyncing,
                      RecordType.DeveloperName
                      FROM    Quote
                      WHERE   OpportunityId =:opps.Id];
        }
        return quotes;
    }
    
    public Quote getQuote() {
        if (opps.Id != null && opps.SyncedQuoteId !=null) {
            quote = [  SELECT  Id,
                     Name,
                     IsSyncing,
                     AVA_SFQUOTES__Shipping_Last_Validated__c,
                     AVA_SFQUOTES__Non_Taxable__c,
                     AVA_SFQUOTES__Tax_Date__c,
                     AVA_SFQUOTES__Tax_Now_Status__c,
                     Product_Copy__c,
                     Services__c,
                     Services_Count__c, 
                     TotalPrice,
                     ExpirationDate,
                     License_Start_Date__c,
                     Quote_Type__c,
                     Current_License_End_Date__c,
                     Deal_Desk_Status__c,
                     Discount_Approval_Status__c,
                     WaiveImplementationServices__c,
                     ImplementationServicesRequired__c,
                     IMP_Services__c,
                     SME_Services__c,
                     PS_Services__c,
                     RecordType.DeveloperName,
                     Exclude_0_Items_From_PDF__c,
                     Multi_Year_Quote__c 
                     FROM Quote
                     WHERE OpportunityId =:opps.Id and IsSyncing = TRUE];
        }
        return quote;
    }
    
    public map<string,decimal> ct = new map<string,decimal>();
    
    public map<string,decimal> getct(){
        for(CurrencyType c : [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE and ISOCode =: opps.CurrencyIsoCode]){
            ct.put(c.ISOCode,c.ConversionRate);
        }
        return ct;
    }
    
    public decimal getAmtusd(){
        for (STRING d : ct.keySet()){
            if(opps.CurrencyIsoCode == d){
                conversionrate = ct.get(d);
                system.debug('conversion: '+conversionrate);
                if(opps.Amount != null){
                    amtusd = opps.Amount.divide(conversionrate,2);
                }else{
                    amtusd = 0;
                }
            }
        }
        system.debug('amt: '+amtusd);
        return amtusd;
    }
    
    //CC PO or QSO (1)
    public Boolean getqrocr(){
        if(opps.Client__c!=null){
            
            // if(quote.Services_Count__c !=null){
            //   qrocr = FALSE;
            // }else
            if(amtusd<10000 && opps.Opportunity_Type__c != 'Services Only'){
                qrocr = TRUE;
            }
        }
        return qrocr;
    }
    
    //PO or QSO (2)
    public Boolean getqropo(){
        if(opps.Client__c!=null){
            //   if(quote.Services_Count__c !=null){
            //     qrocr = FALSE;
            //  }else
            if(amtusd >= 10000 && amtusd < 75000 && opps.Opportunity_Type__c != 'Services Only'){
                qropo = TRUE;
            }
            //  if(amtusd < 75000 && opps.Opportunity_Type__c != 'Services Only'){
            //      qropo = TRUE;
            //   }
        }
        return qropo;
    }
    
    //QSO (3) 
    public Boolean getQr(){
        if(opps.Client__c!=null){
            if(amtusd >= 75000 && opps.Opportunity_Type__c != 'Services Only'){
                qr = TRUE;
            }
        }
        return qr;
    }
    
    //Nothing Required
    public Boolean getNr(){
        if(opps.Client__c != null){
            if(opps.Opportunity_Type__c == 'Services Only'){
                nr = TRUE;
            }
        }
        return nr;
    }
    
    public Boolean getNc(){
        if(opps.Client__c==null){
            nc = TRUE;
        }
        return nc;
    }
    
    public PageReference showPanel() {
        isRendered = true;
        return null;
    }
    
    public PageReference quoteOnly() {
        cd.Validation_Type__c = 'Quote Only';
        Boolean result = true;
        if (opps.Id != null) {
            result = updateCd();
        }
        if (result) {
            std.save();
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Changes saved'));
        }
        return null;
    }
    
    public PageReference updateQSO() {
        
        if(cd.Payment_Choice__c == 'Credit Card'){
            cd.Payment_Choice_2__c = '';
            cd.QSO_Type__c = '';
            
        }   else{
            cd.Payment_Choice_2__c = '';
        }
        
        Boolean result = true;
        if (opps.Id != null) {
            result = updateCd();
        }
        if (result) {
            std.save();
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Changes saved'));
        }
        return null;
    }
    
    public PageReference updateQSO2() {
        if(cd.Payment_Choice_2__c != ''){
            cd.Payment_Choice__c = '';
        }
        Boolean result = true;
        if (opps.Id != null) {
            result = updateCd();
        }
        if (result) {
            std.save();
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Changes saved'));
        }
        return null;
    }
    
    public PageReference qsoView() {
        cd.Validation_Type__c = 'QSO View';
        Boolean result = true;
        
        if (opps.Id != null) {
            result = updateCd();
        }
        if (result) {
            std.save();
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Changes saved'));
        }
        return null;
    }
    
    public PageReference qsoAdobe() {
        cd.Validation_Type__c = 'QSO Adobe';
        Boolean result = true;
        if (opps.Id != null) {
            result = updateCd();
        }
        if (result) {
            std.save();
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Changes saved'));
        }
        return null;
    }
    
    public PageReference qsoDownload() {
        cd.Validation_Type__c = 'QSO Download';
        cd.Custom_Contract_Status__c = 'Download Document';
        Boolean result = true;
        if (opps.Id != null) {
            result = updateCd();
        }
        if (result) {
            std.save();
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Changes saved'));
        }
        return null;
    }
    
     public void paperQSO() {
        opps.Sales_Process_Stage__c='Pending Internal QSO Approval';
        opps.StageName = 'Invoice';
        opps.ForecastCategoryName = 'Won';
        opps.Status__c = '';
        //  Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Closed Opps').getRecordTypeId();
        //   opps.RecordTypeId=oppRecordTypeId;
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Finance').getRecordTypeId();
        Entitlement ent;
        try{
            ent = [SELECT Id from Entitlement where Name = 'Finance'];
        }
        catch(Exception e) {
            system.debug(logginglevel.error, '***** e.getmessage()='+e.getmessage());
        }
        Case newCase;
        If(ent != null){
            newCase = new Case(RecordTypeId = caseRecordTypeId, Primary_Support_Reason__c = 'Finance', Secondary_Support_Reason__c = 'Paper QSO',
                                Subject = 'Paper QSO Approval', Related_Opportunity__c = opps.Id, AccountId = opps.AccountId, 
                                Original_Case_Creator__c = UserInfo.getUserId(), EntitlementId = ent.Id, Execute_Assignment_Rules__c = TRUE, 
                                Related_Client__c = opps.Client__c, Related_Quote__c = opps.SyncedQuoteId);
        }
        else{
            newCase = new Case(RecordTypeId = caseRecordTypeId, Primary_Support_Reason__c = 'Finance', Secondary_Support_Reason__c = 'Paper QSO',
                                Subject = 'Paper QSO Approval', Related_Opportunity__c = opps.Id, AccountId = opps.AccountId, 
                                Original_Case_Creator__c = UserInfo.getUserId(), Execute_Assignment_Rules__c = TRUE, Related_Client__c = opps.Client__c,
                                Related_Quote__c = opps.SyncedQuoteId);
        }
      	try {
            insert newCase;
            update opps;
            /*Approval.ProcessSubmitRequest req1 =new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval.');
            req1.setObjectId(opps.id);
            req1.setSubmitterId(UserInfo.getUserId());
            req1.setProcessDefinitionNameOrId('Paper_QSO');
            req1.setSkipEntryCriteria(false);
            Approval.ProcessResult result = Approval.process(req1);*/
        }
        catch(System.DMLException e) {
            system.debug(logginglevel.error, '***** e.getmessage()='+e.getmessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId  =: opps.Id AND ContentDocument.Title = 'QSO' ORDER BY ContentDocument.CreatedDate DESC LIMIT 1].ContentDocumentId;
        cdl.LinkedEntityId = newCase.Id;
        cdl.ShareType = 'I';
        cdl.Visibility = 'InternalUsers';
        system.debug(cdl);
        try {
            insert cdl;
        }
        catch(System.DmlException excp) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, excp.getMessage()));
        }
        
    }
    
    public PageReference cancelContractButton(){
        
       dsfs__DocuSign_Status__c docs = [SELECT Id   
                                         FROM dsfs__DocuSign_Status__c  
                                         WHERE Contract_Document__c =:cd.Id and (dsfs__Envelope_Status__c = 'Sent' or dsfs__Envelope_Status__c = 'Delivered' or dsfs__Envelope_Status__c = 'Completed' or dsfs__Envelope_Status__c = 'Declined') LIMIT 1];
        Id statusId = docs.Id;
        
        PageReference pageRef = Page.fab_StatusRecords;
        pageRef.getParameters().put('id', statusId);
        return pageRef;
        
   /*     dfsle__Envelope__c env = new dfsle__Envelope__c ();
        env.dfsle__DocuSignId__c = docs.dsfs__DocuSign_Envelope_ID__c;
        env.dfsle__SourceId__c = cd.Id;
        env.dfsle__Sent__c = Datetime.now();
        insert env;
        
        dfsle__EnvelopeStatus__c stat = new dfsle__EnvelopeStatus__c ();
        stat.dfsle__DocuSignId__c = docs.dsfs__DocuSign_Envelope_ID__c;
        stat.dfsle__Status__c  = docs.dsfs__Envelope_Status__c;
        
        insert stat;*/
        
     
        
     /*   dfsle.UUID myEnvelopeId = dfsle.UUID.Parse(docs.dsfs__DocuSign_Envelope_ID__c);
        String reason = 'This is my reason';
        boolean myStatus = dfsle.StatusService.voidEnvelope(
            myEnvelopeId,reason
        );*/
        
        
        
  //    return null;
     
       // return myStatus;
    }
    
    public PageReference sendforCC() {
        cd.Validation_Type__c = 'Credit Card';
        opps.Sales_Process_Stage__c = 'Awaiting Client Payment';
        opps.StageName = 'Invoice';
        opps.ForecastCategoryName = 'Won';        
        Boolean result = true;
        if (opps.Id != null) {
            result = updateCd();
        }
        system.debug(logginglevel.error, '***** result='+result);
        if (result) {
            std.save();
            opps = (Opportunity) std.getRecord();
            //update opps;
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Changes saved'));
        }
        return null;
    }
    
    public static List<Opportunity> setCancelFields(List<Opportunity> lstOpportunity) {
        for (Opportunity opportunity : lstOpportunity) {
            opportunity.Sales_Process_Stage__c = 'Contract Negotiations';
            opportunity.StageName = 'Commit';
            opportunity.ForecastCategoryName = 'Commit';
        }
        return lstOpportunity;
    }
    
    @InvocableMethod
    public static void staticCancelPay(List<Opportunity> lstOpportunity) {
        lstOpportunity = setCancelFields(lstOpportunity);
        update lstOpportunity;
    }
    
    public PageReference cancelPay() {
        //opps = setCancelFields(new List<Opportunity>{opps})[0];
        opps.Sales_Process_Stage__c = 'Contract Negotiations';
        opps.StageName = 'Commit';
        opps.ForecastCategoryName = 'Commit';
        Boolean result = true;
        if (opps.Id != null) {
            result = updateCd();
        }
        if (result) {
            std.save();
            opps = (Opportunity) std.getRecord();
            //update opps;
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Changes saved'));
        }
        return null;
    }
    
    public PageReference onboardingButton(){
        Pagereference MyPage = new Pagereference('/apex/Q2C_RedirectOnboarding?varOppID='+ApexPages.CurrentPage().getParameters().get('id') + '&returnToOpp=false');
        MyPage.setRedirect(true);
        return MyPage;
    }
    
    public List<Attachment> getFileId() {
        List<Attachment> fileId;
        fileId = [select Id, Name, Description, LastModifiedDate from Attachment where parentId =:cd.Id order By LastModifiedDate DESC limit 1];
        if( fileId != null && fileId.size() > 0 ) {
            
        }
        return fileId;    
    }
    
    //EDIT QUOTE
    @RemoteAction    
    global static Database.SaveResult editCd(Contract_Document__c c)
    {
        
        return Database.update(c);        
    } 
}