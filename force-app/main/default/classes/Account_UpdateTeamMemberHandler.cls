public with sharing class Account_UpdateTeamMemberHandler {
    
    public Account_UpdateTeamMemberHandler(){}
    public AccountTeamMember objTeam{get;set;}
    public List<User> lstUser{get;set;}
    public Map<Id,String> mapUser = new Map<Id,String>();
    
    public static final String STR_SALES_OPS = Label.Account_AddUser_SalesOps;                  //'Sales Ops';
    public static final String STR_360_REP = '360/EE Rep';
    public static final String STR_PANELS_REP = 'Panels Rep';
    public static final String STR_SITE_REP = 'Site Intercept Rep';
    public static final String STR_TARGET_REP = 'Target Audience Rep';
    public static final String STR_OPDEV_REP = 'OpDev Rep';
    public static final String STR_RESEARCH_REP = 'Research Suite Rep';
    public static final String STR_CLIENT_REP = 'Client Success Rep';
    public static final String STR_SERVICE_REP ='Professional Services Rep';
    private static boolean flagvalue = false;
    
    public void updateTeamMember(Map<Id,Account> mapAccount, Map<Id,Account> mapOldAccount){
        
        User currentUser = [Select Id, ContactId from User where Id = :UserInfo.getUserId() ] ;
        if (String.isNotBlank(currentUser.ContactId)) {
            return;
        }

        Set<Id> accId = new Set<Id>();
        Set<Id> setOwerId = new Set<ID>();
        List<AccountTeamMember> lstAccountTeam = new List<AccountTeamMember>();
        List<AccountTeamMember> lstDelAccountTeam = new List<AccountTeamMember>();
        Map<Id,AccountTeamMember> mapDelAccountTeam = new Map<Id,AccountTeamMember>();
        lstUser = new List<User>();
        /*Code to Filter out Inactive Users*/ List<Id> userAccUpdateList = new List<Id>();
        User objUserSales = [Select Name,
                                    Profile.Name,
                                    ProfileId 
                            From User 
                            Where Name =: STR_SALES_OPS];
                            
        for(Account objAcc : mapAccount.values()){
            accId.add(objAcc.Id);
            setOwerId.add(objAcc.OwnerId);
        }//for  
                        
        List<AccountTeamMember> lstTeam = [Select a.UserId, a.TeamMemberRole, a.Id, a.AccountId, a.AccountAccessLevel 
                                                    From AccountTeamMember a 
                                                    Where  a.AccountId IN : accId];
        
        lstUser = [Select Name From User Where Name !=: STR_SALES_OPS];
        if(lstUser.size() > 0){
            for(User objUser : lstUser){
                mapUser.put(objUser.Id, objUser.Name);
            }
        }
        
        for(Account objAcc : mapAccount.values()){
            
            if(mapOldAccount.get(objAcc.Id).X360_EE_Rep__c != objAcc.X360_EE_Rep__c ||
                mapOldAccount.get(objAcc.Id).Panels_Rep__c != objAcc.Panels_Rep__c ||
                mapOldAccount.get(objAcc.Id).Site_Intercept_Rep__c != objAcc.Site_Intercept_Rep__c ||
                mapOldAccount.get(objAcc.Id).Target_Audience_Rep__c != objAcc.Target_Audience_Rep__c ||
                mapOldAccount.get(objAcc.Id).OpDev_Rep__c != objAcc.OpDev_Rep__c ||
                mapOldAccount.get(objAcc.Id).Client_Services_Rep__c != objAcc.Client_Services_Rep__c ||
                mapOldAccount.get(objAcc.Id).CX_CS_Rep__c != objAcc.CX_CS_Rep__c ||
                mapOldAccount.get(objAcc.Id).EX_CS_Rep__c != objAcc.EX_CS_Rep__c ||
                mapOldAccount.get(objAcc.Id).Supporting_CS_Rep__c != objAcc.Supporting_CS_Rep__c ||
                mapOldAccount.get(objAcc.Id).Professional_Services_Rep__c != objAcc.Professional_Services_Rep__c ||
                mapOldAccount.get(objAcc.Id).OwnerId != objAcc.OwnerId ){
                    
                    if(lstTeam.size() >= 0){
                        for(AccountTeamMember objTeam : lstTeam){
                            //lstDelAccountTeam.add(objTeam);
                            mapDelAccountTeam.put(objTeam.Id,objTeam);
                        }
                        
                        if(mapOldAccount.get(objAcc.Id).X360_EE_Rep__c != objAcc.X360_EE_Rep__c && 
                                mapOldAccount.get(objAcc.Id).X360_EE_Rep__c != Null && 
                                mapOldAccount.get(objAcc.Id).X360_EE_Rep__c != objUserSales.Id){
                                
                                    //objAcc.Prior_360_EE_Rep__c = fetchPriorUser(mapOldAccount.get(objAcc.Id).X360_EE_Rep__c);
                                    if(mapUser.containsKey(mapOldAccount.get(objAcc.Id).X360_EE_Rep__c)){
                                        objAcc.Prior_360_EE_Rep__c = mapUser.get(mapOldAccount.get(objAcc.Id).X360_EE_Rep__c);
                                    }
                        }//if
                        
                        if(mapOldAccount.get(objAcc.Id).Panels_Rep__c != objAcc.Panels_Rep__c &&
                                mapOldAccount.get(objAcc.Id).Panels_Rep__c != Null &&
                                mapOldAccount.get(objAcc.Id).Panels_Rep__c != objUserSales.Id){
                                
                                    //objAcc.Prior_Panels_Rep__c = fetchPriorUser(mapOldAccount.get(objAcc.Id).Panels_Rep__c);
                                    if(mapUser.containsKey(mapOldAccount.get(objAcc.Id).Panels_Rep__c)){
                                        objAcc.Prior_Panels_Rep__c = mapUser.get(mapOldAccount.get(objAcc.Id).Panels_Rep__c);
                                    }
                        }//if
                        
                        if(mapOldAccount.get(objAcc.Id).Site_Intercept_Rep__c != objAcc.Site_Intercept_Rep__c && 
                                mapOldAccount.get(objAcc.Id).Site_Intercept_Rep__c != Null && 
                                mapOldAccount.get(objAcc.Id).Site_Intercept_Rep__c != objUserSales.Id){
                                    //objAcc.Prior_Site_Intercept_Rep__c = fetchPriorUser(mapOldAccount.get(objAcc.Id).Site_Intercept_Rep__c);
                                    
                                    if(mapUser.containsKey(mapOldAccount.get(objAcc.Id).Site_Intercept_Rep__c)){
                                        objAcc.Prior_Site_Intercept_Rep__c = mapUser.get(mapOldAccount.get(objAcc.Id).Site_Intercept_Rep__c);
                                    }
                        }//if
                        
                        if(mapOldAccount.get(objAcc.Id).Target_Audience_Rep__c != objAcc.Target_Audience_Rep__c && 
                                mapOldAccount.get(objAcc.Id).Target_Audience_Rep__c != Null && 
                                mapOldAccount.get(objAcc.Id).Target_Audience_Rep__c != objUserSales.Id){
                                    //objAcc.Prior_Target_Audience_Rep__c = fetchPriorUser(mapOldAccount.get(objAcc.Id).Target_Audience_Rep__c);
                                    
                                    if(mapUser.containsKey(mapOldAccount.get(objAcc.Id).Target_Audience_Rep__c)){
                                        objAcc.Prior_Target_Audience_Rep__c = mapUser.get(mapOldAccount.get(objAcc.Id).Target_Audience_Rep__c);
                                    }
                        }//if
                        
                        if(mapOldAccount.get(objAcc.Id).OpDev_Rep__c != objAcc.OpDev_Rep__c && 
                                mapOldAccount.get(objAcc.Id).OpDev_Rep__c != Null && 
                                mapOldAccount.get(objAcc.Id).OpDev_Rep__c != objUserSales.Id){ 
                                
                                    //objAcc.Prior_OpDev_Rep__c = fetchPriorUser(mapOldAccount.get(objAcc.Id).OpDev_Rep__c);
                                    if(mapUser.containsKey(mapOldAccount.get(objAcc.Id).OpDev_Rep__c)){
                                        objAcc.Prior_OpDev_Rep__c = mapUser.get(mapOldAccount.get(objAcc.Id).OpDev_Rep__c);
                                    }
                        }//if
                        
                        if(mapOldAccount.get(objAcc.Id).OwnerId != objAcc.OwnerId && 
                                mapOldAccount.get(objAcc.Id).OwnerId != Null && 
                                mapOldAccount.get(objAcc.Id).OwnerId != objUserSales.Id){
                                    
                                //objAcc.Prior_Owner__c = fetchPriorUser(mapOldAccount.get(objAcc.Id).OwnerId);
                                if(mapUser.containsKey(mapOldAccount.get(objAcc.Id).OwnerId)){
                                    objAcc.Prior_Owner__c = mapUser.get(mapOldAccount.get(objAcc.Id).OwnerId);
                                }   
                        }//if
                        
                        if(mapOldAccount.get(objAcc.Id).Client_Services_Rep__c != objAcc.Client_Services_Rep__c && 
                              mapOldAccount.get(objAcc.Id).Client_Services_Rep__c != Null &&
                               mapOldAccount.get(objAcc.Id).Client_Services_Rep__c != objUserSales.Id){
                                //objAcc.Prior_Client_Services_Rep__c = fetchPriorUser(mapOldAccount.get(objAcc.Id).Client_Services_Rep__c);
                                
                                if(mapUser.containsKey(mapOldAccount.get(objAcc.Id).Client_Services_Rep__c)){
                                        objAcc.Prior_Client_Services_Rep__c = mapUser.get(mapOldAccount.get(objAcc.Id).Client_Services_Rep__c);
                                }
                        }//if
                        
                        if(mapOldAccount.get(objAcc.Id).CX_CS_Rep__c != objAcc.CX_CS_Rep__c && 
                              mapOldAccount.get(objAcc.Id).CX_CS_Rep__c != Null &&
                               mapOldAccount.get(objAcc.Id).CX_CS_Rep__c != objUserSales.Id){
                                //objAcc.Prior_CX_CS_Rep__c = fetchPriorUser(mapOldAccount.get(objAcc.Id).CX_CS_Rep__c);
                                
                                if(mapUser.containsKey(mapOldAccount.get(objAcc.Id).CX_CS_Rep__c)){
                                        objAcc.Prior_CX_CS_Rep__c = mapUser.get(mapOldAccount.get(objAcc.Id).CX_CS_Rep__c);
                                }
                        }//if
                        
                        if(mapOldAccount.get(objAcc.Id).EX_CS_Rep__c != objAcc.EX_CS_Rep__c && 
                              mapOldAccount.get(objAcc.Id).EX_CS_Rep__c != Null &&
                               mapOldAccount.get(objAcc.Id).EX_CS_Rep__c != objUserSales.Id){
                                //objAcc.Prior_EX_CS_Rep__c = fetchPriorUser(mapOldAccount.get(objAcc.Id).EX_CS_Rep__c);
                                
                                if(mapUser.containsKey(mapOldAccount.get(objAcc.Id).Client_Services_Rep__c)){
                                        objAcc.Prior_EX_CS_Rep__c = mapUser.get(mapOldAccount.get(objAcc.Id).EX_CS_Rep__c);
                                }
                        }//if
                        
                        if(mapOldAccount.get(objAcc.Id).Supporting_CS_Rep__c != objAcc.Supporting_CS_Rep__c && 
                              mapOldAccount.get(objAcc.Id).Supporting_CS_Rep__c != Null &&
                               mapOldAccount.get(objAcc.Id).Supporting_CS_Rep__c != objUserSales.Id){
                                //objAcc.Prior_Supporting_CS_Rep__c = fetchPriorUser(mapOldAccount.get(objAcc.Id).Supporting_CS_Rep__c);
                                
                                if(mapUser.containsKey(mapOldAccount.get(objAcc.Id).Client_Services_Rep__c)){
                                        objAcc.Prior_Supporting_CS_Rep__c = mapUser.get(mapOldAccount.get(objAcc.Id).Supporting_CS_Rep__c);
                                }
                        }//if
                        
                        if(mapOldAccount.get(objAcc.Id).Professional_Services_Rep__c != objAcc.Professional_Services_Rep__c && 
                              mapOldAccount.get(objAcc.Id).Professional_Services_Rep__c != Null &&
                              mapOldAccount.get(objAcc.Id).Professional_Services_Rep__c != objUserSales.Id){
                                
                                //objAcc.Prior_Prof_Services_Rep__c = fetchPriorUser(mapOldAccount.get(objAcc.Id).Professional_Services_Rep__c);
                                if(mapUser.containsKey(mapOldAccount.get(objAcc.Id).Professional_Services_Rep__c)){
                                        objAcc.Prior_Prof_Services_Rep__c = mapUser.get(mapOldAccount.get(objAcc.Id).Professional_Services_Rep__c);
                                }
                        }//if
                            
                        if(objAcc.X360_EE_Rep__c  != Null && objAcc.X360_EE_Rep__c != objUserSales.Id){
                            /*Code to Filter out Inactive Users*/ userAccUpdateList.add(objAcc.X360_EE_Rep__c);
                            objTeam = createAccountTeam(objAcc.Id, objAcc.X360_EE_Rep__c, STR_360_REP);
                            lstAccountTeam.add(objTeam);
                        }
                        
                        if(objAcc.Panels_Rep__c  != Null && objAcc.Panels_Rep__c != objUserSales.Id){
                            /*Code to Filter out Inactive Users*/ userAccUpdateList.add(objAcc.Panels_Rep__c);
                            objTeam = createAccountTeam(objAcc.Id, objAcc.Panels_Rep__c, STR_PANELS_REP);
                            lstAccountTeam.add(objTeam);
                        }
                        
                        if(objAcc.Site_Intercept_Rep__c  != Null && objAcc.Site_Intercept_Rep__c != objUserSales.Id){
                            /*Code to Filter out Inactive Users*/ userAccUpdateList.add(objAcc.Site_Intercept_Rep__c);
                            objTeam = createAccountTeam(objAcc.Id, objAcc.Site_Intercept_Rep__c, STR_SITE_REP);
                            lstAccountTeam.add(objTeam);
                        }
                        
                        if(objAcc.Target_Audience_Rep__c  != Null && objAcc.Target_Audience_Rep__c != objUserSales.Id){
                            /*Code to Filter out Inactive Users*/ userAccUpdateList.add(objAcc.Target_Audience_Rep__c);
                            objTeam = createAccountTeam(objAcc.Id, objAcc.Target_Audience_Rep__c, STR_TARGET_REP);
                            lstAccountTeam.add(objTeam);
                        }
                        
                        if(objAcc.OpDev_Rep__c  != Null && objAcc.OpDev_Rep__c != objUserSales.Id){
                            /*Code to Filter out Inactive Users*/ userAccUpdateList.add(objAcc.OpDev_Rep__c);
                            objTeam = createAccountTeam(objAcc.Id, objAcc.OpDev_Rep__c, STR_OPDEV_REP);
                            lstAccountTeam.add(objTeam);
                        }
                        
                        if(objAcc.OwnerId  != Null && objAcc.OwnerId != objUserSales.Id){
                            objTeam = createAccountTeam(objAcc.Id, objAcc.OwnerId, STR_RESEARCH_REP);
                            lstAccountTeam.add(objTeam);
                        }
                        
                        if(objAcc.Client_Services_Rep__c  != Null && objAcc.Client_Services_Rep__c != objUserSales.Id){
                            /*Code to Filter out Inactive Users*/ userAccUpdateList.add(objAcc.Client_Services_Rep__c);
                            objTeam = createAccountTeam(objAcc.Id, objAcc.Client_Services_Rep__c, STR_CLIENT_REP);
                            lstAccountTeam.add(objTeam);
                        }
                        if(objAcc.CX_CS_Rep__c  != Null && objAcc.CX_CS_Rep__c != objUserSales.Id){
                            /*Code to Filter out Inactive Users*/ userAccUpdateList.add(objAcc.CX_CS_Rep__c);
                            objTeam = createAccountTeam(objAcc.Id, objAcc.CX_CS_Rep__c, STR_CLIENT_REP);
                            lstAccountTeam.add(objTeam);
                        }
                        if(objAcc.EX_CS_Rep__c  != Null && objAcc.EX_CS_Rep__c != objUserSales.Id){
                            /*Code to Filter out Inactive Users*/   userAccUpdateList.add(objAcc.EX_CS_Rep__c);
                            objTeam = createAccountTeam(objAcc.Id, objAcc.EX_CS_Rep__c, STR_CLIENT_REP);
                            lstAccountTeam.add(objTeam);
                        }
                        if(objAcc.Supporting_CS_Rep__c  != Null && objAcc.Supporting_CS_Rep__c != objUserSales.Id){
                            /*Code to Filter out Inactive Users*/   userAccUpdateList.add(objAcc.Supporting_CS_Rep__c);
                            objTeam = createAccountTeam(objAcc.Id, objAcc.Supporting_CS_Rep__c, STR_CLIENT_REP);
                            lstAccountTeam.add(objTeam);
                        }
                        
                        if(objAcc.Professional_Services_Rep__c  != Null && objAcc.Professional_Services_Rep__c != objUserSales.Id){
                            /*Code to Filter out Inactive Users*/   userAccUpdateList.add(objAcc.Professional_Services_Rep__c);
                            objTeam = createAccountTeam(objAcc.Id, objAcc.Professional_Services_Rep__c, STR_SERVICE_REP);
                            lstAccountTeam.add(objTeam);
                        }
                    }//If
                }// if
        }//for
        
        try{
            
            if(mapDelAccountTeam.size() > 0){
                for(AccountTeamMember lst : mapDelAccountTeam.values()){
                    lstDelAccountTeam.add(lst);
                }
                if(lstDelAccountTeam.size() > 0){
                    database.delete(lstDelAccountTeam);
                }
            }//if
            
            if(lstAccountTeam.size() > 0){
                           /*Code to Filter out Inactive Users*/
             Set<Id> inActiveUsers = new Set<Id>(New Map<Id, User>([Select Id from User where Id in :userAccUpdateList AND isActive=false]).keySet());
             
             Integer counter=0;
             while(counter<lstAccountTeam.size())
             {
                 if(inActiveUsers.contains(lstAccountTeam.get(counter).UserId))
                 {
                     lstAccountTeam.remove(counter);
                 }
                 else
                  {
                     counter++;
                 }
             }
             
             /*Code to Filter out Inactive Users end*/
                database.insert(lstAccountTeam);
            }//if   
            
            /*Code to Filter out Inactive Users*/
            for(AccountTeamMember acLst: lstAccountTeam)
            {
                System.debug(acLst);
            }
            /*Code to Filter out Inactive Users end*/
            List<AccountShare> lstShare = new List<AccountShare>();
            AccountShare objAccountShare;
            if(lstAccountTeam.size() > 0){
                for(AccountTeamMember objTeam : lstAccountTeam){
                        
                    if(!setOwerId.contains(objTeam.UserId)){
                        
                        objAccountShare = new AccountShare(UserOrGroupId = objTeam.UserId,
                                                            AccountId = objTeam.AccountId, 
                                                            AccountAccessLevel = 'Edit',
                                                            OpportunityAccessLevel= 'Read',
                                                            CaseAccessLevel = 'Read');
                        lstShare.add(objAccountShare);
                    }//if
                }//for
            }//if
            
            if(lstShare.size() > 0){
                database.insert(lstShare);
            }//if
        
        }catch(exception ex){
            System.debug(ex);
        }//catch
    }
    
    public static boolean hasAlreadyfired() {
         return flagvalue;
     }

    public static void setAlreadyfired(){
        flagvalue = true;
    }
    
    public AccountTeamMember createAccountTeam(Id accId,Id UserId, String TeamMemberRole){
            
            return (new AccountTeamMember(AccountId = accId,
                                          UserId = UserId,
                                          TeamMemberRole = TeamMemberRole
                                           ));
    }//createAccountTeam
}