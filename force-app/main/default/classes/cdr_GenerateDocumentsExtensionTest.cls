@isTest
public with sharing class cdr_GenerateDocumentsExtensionTest {
	
	public static Account accountInstance {get; set;}
	public static Opportunity opportunityInstance {get; set;}
    public static Quote quoteInstance {get; set;}

	@testSetup 
    public static void test_setup() {
         AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
    Insert avaCustomSetting;
        TestFactory.disableTriggers();
        accountInstance = TestFactory.createAccount();
        insert accountInstance;
        
        Product2 productInstance = TestFactory.createProduct();
        productInstance.ProductCode = 'PNLRetainer';
        insert productInstance;
        
        PricebookEntry priceBookEntryInstance = TestFactory.createPricebookEntry(test.getStandardPricebookId(), productInstance.Id);
        insert priceBookEntryInstance;
        
        Client__c clientInstance = TestFactory.createClient(accountInstance.Id);
        insert clientInstance;
        
        opportunityInstance = TestFactory.createOpportunity(accountInstance.Id, test.getStandardPricebookId());
        opportunityInstance.Amount = 500;       
        opportunityInstance.Client__c = clientInstance.Id;
        Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();
        opportunityInstance.RecordTypeId = rtMapByName.get('Enterprise').getRecordTypeId();
      //  opportunityInstance.Retainer__c = true;
        insert opportunityInstance;
        
        Quote quoteInstance = TestFactory.createQuote(opportunityInstance.Id, test.getStandardPricebookId());
        insert quoteInstance;
        
        Contract_Document__c contractDocumentInstance = TestFactory.createContractDoc(clientInstance.Id);
        contractDocumentInstance.Opportunity__c = opportunityInstance.Id;
        insert contractDocumentInstance;
    }

    public static void QueryStartingItems() {
        accountInstance = database.Query('SELECT '+BuildQuery('Account')+' WHERE name = \'test Account\'');
        opportunityInstance = database.Query('SELECT Id, Client__c, CurrencyIsoCode, Amount, ForecastCategoryName, StageName, Sales_Process_Stage__c, AccountId, Opportunity_Type__c, SyncedQuoteId, QSO_Override_Reason__c, Payment_Terms__c, Custom_Payment_Terms__c FROM Opportunity LIMIT 1');
        quoteInstance = database.Query('SELECT Id, Name, IsSyncing, AVA_SFQUOTES__Shipping_Last_Validated__c,AVA_SFQUOTES__Non_Taxable__c,AVA_SFQUOTES__Tax_Date__c,AVA_SFQUOTES__Tax_Now_Status__c,Product_Copy__c,Services__c,TotalPrice,ExpirationDate,License_Start_Date__c,Quote_Type__c,Current_License_End_Date__c,Deal_Desk_Status__c, Discount_Approval_Status__c,WaiveImplementationServices__c,ImplementationServicesRequired__c,IMP_Services__c,SME_Services__c,PS_Services__c,RecordType.DeveloperName,Exclude_0_Items_From_PDF__c, Multi_Year_Quote__c  FROM Quote');
    }
    
    public static string BuildQuery(String obj) {
        //Map<String, Schema.SObjectField> SobjtField = new Map<String, Schema.SObjectField>();
        //SobjtField = Schema.getGlobalDescribe().get(obj).getDescribe().fields.getMap();
        string qry = '';
        //for (Schema.SObjectField s : SobjtField.values()) {
        //    qry += s + ',';
        // }
        qry = 'Id';
        qry = qry+' from '+ obj;
        return qry;
    }

    @isTest
    public static void testMethodToAddPOAttachment() {
    	QueryStartingItems();
    	system.assertNotEquals(null, accountInstance);
    	system.assertNotEquals(null, opportunityInstance);
        
        PageReference pageRef = Page.POAttachement;
        pageRef.getParameters().put('Id',opportunityInstance.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdLead = new ApexPages.StandardController(opportunityInstance);
        cdr_GenerateDocumentsExtension cdrController = new cdr_GenerateDocumentsExtension(stdLead);
        cdrController.fileName='Unit_Test_Attachment.pdf';
        cdrController.fileBody=Blob.valueOf('Unit Test Attachment Body');
        cdrController.addPOAttachment();
        
        //cdrController.fileName='Unit_Test_Attachment.pdf';
        //cdrController.fileBody=Blob.valueOf('Unit Test Attachment Body');
        //cdrController.addPOAttachment();
        List<ContentDocumentLink> cdl = [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId  =: opportunityInstance.Id AND ContentDocument.Title = 'PO'];
      	System.assertEquals(1, cdl.size());
    }
    
    @isTest
    public static void testMethodToCheckQuoteAttachment() {
    	QueryStartingItems();
    	system.assertNotEquals(null, accountInstance);
    	system.assertNotEquals(null, opportunityInstance);
        
        PageReference pageRef = Page.QuoteAttachment;
        pageRef.getParameters().put('Id',opportunityInstance.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdLead = new ApexPages.StandardController(opportunityInstance);
        cdr_GenerateDocumentsExtension cdrController = new cdr_GenerateDocumentsExtension(stdLead);
        cdrController.fileName='Unit Test Attachment';
        cdrController.fileBody=Blob.valueOf('Unit Test Attachment Body');
        cdrController.addQuoteAttachment();
        
        //cdrController.fileName='Unit Test Attachment';
        //cdrController.fileBody=Blob.valueOf('Unit Test Attachment Body');
        //cdrController.approve();
        //cdrController.addQuoteAttachment();
        //List<Attachment> attachments = [SELECT ID, Name FROM Attachment WHERE parent.Id= :opportunityInstance.SyncedQuoteId];
      	//System.assertEquals(1, attachments.size());
        
        //cdrController.getCd();
        //cdrController.quoteOnly();
        //List<Apexpages.Message> msgs = ApexPages.getMessages();
        //boolean booleanValue = false;
        //for(Apexpages.Message msg:msgs){
            //if (msg.getDetail().contains('Changes saved')) booleanValue = true;
        //}
        //system.assert(booleanValue);
    }
    
    @isTest
    public static void testMethodToAddQSOAttachment() {
    	QueryStartingItems();
    	system.assertNotEquals(null, accountInstance);
    	system.assertNotEquals(null, opportunityInstance);
        
        PageReference pageRef = Page.QSOAttachment;
        pageRef.getParameters().put('Id',opportunityInstance.id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdLead = new ApexPages.StandardController(opportunityInstance);
        cdr_GenerateDocumentsExtension cdrController = new cdr_GenerateDocumentsExtension(stdLead);
        cdrController.fileName='Unit Test Attachment';
        cdrController.fileBody=Blob.valueOf('Unit Test Attachment Body');
        cdrController.addQSOAttachment();
        
        List<ContentDocumentLink> cdl = [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId  =: opportunityInstance.Id AND ContentDocument.Title = 'QSO'];
      	System.assertEquals(1, cdl.size());
        
        //cdrController.fileName='Unit Test Attachment';
        //cdrController.fileBody=Blob.valueOf('Unit Test Attachment Body');
        //cdrController.addQSOAttachment();
        //List<Attachment> attachments = [SELECT ID, Name FROM Attachment WHERE parent.Id= :opportunityInstance.Id];
      	//System.assertEquals(1, attachments.size());
    }

    @isTest
    public static void testMethodToRedirectAttachment() {
    	QueryStartingItems();
    	system.assertNotEquals(null, accountInstance);
    	system.assertNotEquals(null, opportunityInstance);
        PageReference poPageRef = Page.POAttachement;
        poPageRef.getParameters().put('Id',opportunityInstance.Id);
        Test.setCurrentPage(poPageRef);
        
        ApexPages.StandardController stdLead = new ApexPages.StandardController(opportunityInstance);
        cdr_GenerateDocumentsExtension cdrController = new cdr_GenerateDocumentsExtension(stdLead);
        
        System.assertEquals('/apex/quoteattachment?id='+opportunityInstance.Id, (cdrController.redirectQuoteAttachment()).getUrl());
        System.assertEquals('/apex/qsoattachment?id='+opportunityInstance.Id, (cdrController.redirectQSOAttachment()).getUrl());
        System.assertEquals('/apex/poattachement?id='+opportunityInstance.Id, (cdrController.redirectPOAttachment()).getUrl());
        System.assertEquals('/apex/cdr_generatedocs?id='+opportunityInstance.Id, (cdrController.redirectGenerateDocPage()).getUrl());
        System.assertEquals(
            '/apex/Q2C_RedirectOnboarding?returnToOpp=false&varOppID='+ApexPages.CurrentPage().getParameters().get('Id'), 
        	(cdrController.onboardingButton()).getUrl()
        );

        cdrController.cancelPay();
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean booleanValue = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('Changes saved')) booleanValue = true;
        }
        system.assert(booleanValue);

        cdrController.cancel();
        System.assertEquals(false, cdrController.isRendered);
        
        cdrController.showPanel();
        System.assertEquals(true, cdrController.isRendered);
    }

    @isTest
    public static void testMethodToUpdateQuoteRecords() {
    	QueryStartingItems();
    	system.assertNotEquals(null, accountInstance);
    	system.assertNotEquals(null, opportunityInstance);
        PageReference pageRef = Page.QuoteAttachment;
        pageRef.getParameters().put('Id',opportunityInstance.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdLead = new ApexPages.StandardController(opportunityInstance);
        cdr_GenerateDocumentsExtension cdrController = new cdr_GenerateDocumentsExtension(stdLead);
		
        quoteInstance.Name = 'Test';
        cdrController.lstQuote.clear();
        cdrController.lstQuote.add(quoteInstance);
        cdrController.updateQuotes();
        Quote QouteObject = new Quote();
        QouteObject = database.Query('SELECT '+BuildQuery('Quote')+' WHERE name = \'Test\'');
        System.assertNotEquals(null, QouteObject);
    }

    @isTest
    public static void testMethodToUpdateQSORecords() {
    	QueryStartingItems();
    	system.assertNotEquals(null, accountInstance);
    	system.assertNotEquals(null, opportunityInstance);
        PageReference pageRef = Page.QSOAttachment;
        pageRef.getParameters().put('Id',opportunityInstance.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdLead = new ApexPages.StandardController(opportunityInstance);
        cdr_GenerateDocumentsExtension cdrController = new cdr_GenerateDocumentsExtension(stdLead);
		
        cdrController.getCd();
		cdrController.updateQSO();
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean booleanValue = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('Changes saved')) booleanValue = true;
        }
        system.assert(booleanValue);
    }
    
    @isTest
    public static void testMethodToUpdateQSO2() {
    	QueryStartingItems();
    	system.assertNotEquals(null, accountInstance);
    	system.assertNotEquals(null, opportunityInstance);
        PageReference pageRef = Page.QSOAttachment;
        pageRef.getParameters().put('Id',opportunityInstance.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdLead = new ApexPages.StandardController(opportunityInstance);
        cdr_GenerateDocumentsExtension cdrController = new cdr_GenerateDocumentsExtension(stdLead);
        
        cdrController.getCd();
        cdrController.updateQSO2();        
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean booleanValue = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('Changes saved')) booleanValue = true;
        }
        system.assert(booleanValue);
    }

    @isTest
    public static void testMethodToCancelContractButton() {
    	QueryStartingItems();
    	system.assertNotEquals(null, accountInstance);
    	system.assertNotEquals(null, opportunityInstance);
        PageReference pageRef = Page.POAttachement;
        pageRef.getParameters().put('Id',opportunityInstance.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdLead = new ApexPages.StandardController(opportunityInstance);
        cdr_GenerateDocumentsExtension cdrController = new cdr_GenerateDocumentsExtension(stdLead);
       // cdrController.cancelContractButton();
       // System.assertNotEquals(null, cdrController.cd);
    }
    
    @isTest
    public static void testMethodToSendforCC() {
    	QueryStartingItems();
    	system.assertNotEquals(null, accountInstance);
    	system.assertNotEquals(null, opportunityInstance);
        PageReference pageRef = Page.POAttachement;
        pageRef.getParameters().put('Id',opportunityInstance.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdLead = new ApexPages.StandardController(opportunityInstance);
        cdr_GenerateDocumentsExtension cdrController = new cdr_GenerateDocumentsExtension(stdLead);
        cdrController.getCd();
		cdrController.sendforCC();
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean booleanValue = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('Changes saved')) booleanValue = true;
        }
        system.assert(booleanValue);
    }
    
    @isTest
    public static void testMethodToCheckSave() {
    	QueryStartingItems();
    	system.assertNotEquals(null, accountInstance);
    	system.assertNotEquals(null, opportunityInstance);
        
        PageReference pageRef = Page.QuoteAttachment;
        pageRef.getParameters().put('Id',opportunityInstance.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdLead = new ApexPages.StandardController(opportunityInstance);
        cdr_GenerateDocumentsExtension cdrController = new cdr_GenerateDocumentsExtension(stdLead);
        cdrController.save();
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean booleanValue = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('Changes saved')) booleanValue = true;
        }
        system.assert(booleanValue);
        system.assertEquals(false, cdrController.isRendered);
    }
    
    @isTest
    public static void testMethodToCheckQSOView() {
    	QueryStartingItems();
    	system.assertNotEquals(null, accountInstance);
    	system.assertNotEquals(null, opportunityInstance);
        
        PageReference pageRef = Page.QSOAttachment;
        pageRef.getParameters().put('Id',opportunityInstance.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdLead = new ApexPages.StandardController(opportunityInstance);
        cdr_GenerateDocumentsExtension cdrController = new cdr_GenerateDocumentsExtension(stdLead);
        cdrController.getCd();
        cdrController.qsoView();
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean booleanValue = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('Changes saved')) booleanValue = true;
        }
        system.assert(booleanValue);
    }
    
    @isTest
    public static void testMethodToCheckQSOAdobe() {
    	QueryStartingItems();
    	system.assertNotEquals(null, accountInstance);
    	system.assertNotEquals(null, opportunityInstance);
        
        PageReference pageRef = Page.QSOAttachment;
        pageRef.getParameters().put('Id',opportunityInstance.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdLead = new ApexPages.StandardController(opportunityInstance);
        cdr_GenerateDocumentsExtension cdrController = new cdr_GenerateDocumentsExtension(stdLead);
        cdrController.getCd();
		cdrController.qsoAdobe();
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean booleanValue = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('Changes saved')) booleanValue = true;
        }
        system.assert(booleanValue);
    }    

    @isTest
    public static void testMethodToCheckQSODownload() {
    	QueryStartingItems();
    	system.assertNotEquals(null, accountInstance);
    	system.assertNotEquals(null, opportunityInstance);
        
        PageReference pageRef = Page.QSOAttachment;
        pageRef.getParameters().put('Id',opportunityInstance.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdLead = new ApexPages.StandardController(opportunityInstance);
        cdr_GenerateDocumentsExtension cdrController = new cdr_GenerateDocumentsExtension(stdLead);
        cdrController.getCd();
		cdrController.qsoDownload();
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean booleanValue = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('Changes saved')) booleanValue = true;
        }
        system.assert(booleanValue);
    }
    
    @isTest
    public static void testMethodToCheckPaperQSO() {
    	QueryStartingItems();
    	system.assertNotEquals(null, accountInstance);
    	system.assertNotEquals(null, opportunityInstance);
        
        PageReference pageRef = Page.QSOAttachment;
        pageRef.getParameters().put('Id',opportunityInstance.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdLead = new ApexPages.StandardController(opportunityInstance);
        cdr_GenerateDocumentsExtension cdrController = new cdr_GenerateDocumentsExtension(stdLead);
        cdrController.getCd();
        cdrController.fileName='Unit Test Attachment';
        cdrController.fileBody=Blob.valueOf('Unit Test Attachment Body');
        cdrController.addQSOAttachment();
		cdrController.paperQSO();
        Id oppId = opportunityInstance.Id;
        opportunityInstance = database.Query('SELECT StageName, ForecastCategoryName from Opportunity where Id =: oppId');
        System.assertEquals('Invoice', opportunityInstance.StageName);
        System.assertEquals('Won', opportunityInstance.ForecastCategoryName);
    }
    
    @isTest
    public static void testMethodToCheckStaticCancelPay() {
    	QueryStartingItems();
    	system.assertNotEquals(null, accountInstance);
    	system.assertNotEquals(null, opportunityInstance);
        
        PageReference pageRef = Page.QSOAttachment;
        pageRef.getParameters().put('Id',opportunityInstance.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdLead = new ApexPages.StandardController(opportunityInstance);
        cdr_GenerateDocumentsExtension cdrController = new cdr_GenerateDocumentsExtension(stdLead);
        opportunityInstance.Name = 'Test';
        cdr_GenerateDocumentsExtension.staticCancelPay(new List<Opportunity> {opportunityInstance});
        opportunityInstance = database.Query('SELECT '+BuildQuery('Opportunity')+' WHERE name = \'Test\'');
        System.assertNotEquals(null, opportunityInstance);   
    }
}