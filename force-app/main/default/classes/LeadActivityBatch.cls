/*
Trigger/Batch Job Criteria:
An unconverted lead has no completed activity logged in the last 45 days AND no future activity scheduled in 30 days.
Action:
Change Lead Owner to the Queue "Nurture"
*/
global with sharing class LeadActivityBatch implements Database.Batchable<sObject>, Database.Stateful{
	
	
	global LeadActivityBatch(){}
	global Set<Id> setLead{get;set;}
	private static final string STR_GRPNAME = Label.LeadActivityBatch_GrpName;		// 'Nurture'
	
	global Database.QueryLocator start(Database.BatchableContext BC){
		return database.getQueryLocator([Select IsConverted,
											 OwnerId ,
											 (Select ActivityDate From ActivityHistories) ,
											 (Select ActivityDate From OpenActivities) 
									  From Lead 
									  Where IsConverted = false]);
	}
	
	global void execute(Database.BatchableContext BC, List<Lead> lstLead){
 	
 		setLead = new Set<Id>();
 		Date currentDate = system.today();
 		Integer flag = 0;
 		List<Lead> lstLeadUpdate = new List<Lead>();
 		
 		Group objGroup = [Select g.Name, g.Id, g.Email From Group g Where Name =: STR_GRPNAME];

 		for(Lead objLead : lstLead){   
	       
	        for(ActivityHistory tsk: objLead.ActivityHistories){
	        	if((currentDate.addDays(-45) <= tsk.ActivityDate) && (tsk.ActivityDate <= currentDate )){
 					flag = 1;
 				}
	        }
	        
	        for(OpenActivity objOpenActivity: objLead.OpenActivities){
	        	
	        	if((currentDate <= objOpenActivity.ActivityDate) && (objOpenActivity.ActivityDate <= currentDate.addDays(30))){
 					flag = 1; 
 				}
	        }
	        if(flag == 0){
		        objLead.OwnerId = objGroup.Id; 
		        lstLeadUpdate.add(objLead);
	        }
 		}
 		try{
	 		if(lstLeadUpdate.size() > 0){
	 			database.update(lstLeadUpdate);
	 		}
 		}catch(Exception ex){
 			system.debug(ex);
 		}
 	}
 	
	global void finish(Database.BatchableContext BC){}

}