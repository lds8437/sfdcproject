public class cdr_ContractDocumentUpdates {
    
 
    public static void updateMSA(List<Contract_Document__c> newList, map<Id,Contract_Document__c> OldMap){
        
   /*     List<Contract_Document__c> lstCd = new List<Contract_Document__c>();
        
        for(Contract_Document__c obj:newList){
            if(obj.MSA__c != OldMap.get(obj.Id).MSA__c)
            {               
                lstCd.add(obj);
            }           
        }
        
        if(lstCd!=Null && lstCd.size()>0){*/
            
            List<Contract_Document__c> cd = new List<Contract_Document__c>();
            
            for(Contract_Document__c c:newList){               
                Contract_Document__c cdNew = new Contract_Document__c();
                cdNew.Id = c.Id;
                if(c.MSA__c == TRUE){
                    cdNew.Language_Type__c = 'with MSA';
                }else if(c.MSA__c == FALSE){
                    cdNew.Language_Type__c = 'Standalone';
                }
                cd.add(cdNew); 
            }
            update cd;
        //}     
    }
    
    
/*    public static void updateClientRec(List<Contract_Document__c> newList, map<Id,Contract_Document__c> OldMap){
        
        List<Id> lstCd = new List<Id>();
        
        for(Contract_Document__c obj:newList){
            if(OldMap != Null &&(obj.Client__c != OldMap.get(obj.Id).Client__c))
            {               
                lstCd.add(obj.Client__c);
            }
            else if (oldMap == NULL){
                lstCd.add(obj.Client__c);
            }
        }
        
        if(lstCd!=Null && lstCd.size()>0){
            
            //updates
            Map<Id,Client__c> acc = new Map<Id,Client__c>([
                SELECT ID, BillToName__c, Billing_Street_Address__c, Billing_City__c, Billing_State_Province__c, 
                Billing_Zip_Postal_Code__c, BillingCountry2__c 
                FROM Client__c WHERE Id in:lstCd
            ]);
            
            Set<Id> accSet = acc.keySet();
            
            List<Client__c> cd = new List<Client__c>();
            
            for(Contract_Document__c c:newList){
                for(Id ac:accSet){
                    if(c.Client__c == ac){
                        Client__c cdNew = new Client__c();
                        cdNew.Id = ac;                       
                        cdNew.Billing_Street_Address__c = c.Billing_Street__c;
                        cdNew.Billing_City__c = c.Billing_City__c;
                        cdNew.Billing_State_Province__c = c.Billing_State__c;
                        cdNew.Billing_Zip_Postal_Code__c= c.Billing_Zip_Postal__c;
                        cdNew.BillingCountry2__c = c.Billing_Country__c;
                        cd.add(cdNew);
                    }
                }
            }
            update cd;
        }       
    }*/
     /*  public static void updatePriCon(List<Contract_Document__c> newList, map<Id,Contract_Document__c> OldMap){
        
        List<Id> lstCd = new List<Id>();
        
        for(Contract_Document__c obj:newList){
            if(OldMap != Null &&(obj.Primary_Contact__c != OldMap.get(obj.Id).Primary_Contact__c))
            {               
                lstCd.add(obj.Primary_Contact__c);
            }
            else if (oldMap == NULL){
                lstCd.add(obj.Primary_Contact__c);
            }
        }
        
        if(lstCd!=Null && lstCd.size()>0){
            
            //updates
            Map<Id,Contact> con = new Map<Id,Contact>([
                SELECT Id, Name, Phone, Email from Contact WHERE Id in:lstCd
            ]);
            
            Set<Id> conSet = con.keySet();
            
            List<Contract_Document__c> cd = new List<Contract_Document__c>();
            
            for(Contract_Document__c c:newList){
                for(Id cs:conSet){
                    if(c.Primary_Contact__c == cs){
                        Contract_Document__c cdNew = new Contract_Document__c();
                        cdNew.Id = c.Id;
                        cdNew.Primary_Contact_Email__c = con.get(cs).Email;
                        cdNew.Primary_Contact_Name__c = con.get(cs).Name;
                        cdNew.Primary_Contact_Phone__c = con.get(cs).Phone;
                        cd.add(cdNew);
                    }
                }
            }
            update cd;
        }     
    }*/
    
  /*  public static void updateAccount(List<Contract_Document__c> newList, map<Id,Contract_Document__c> OldMap){
        
        List<Id> lstCd = new List<Id>();
        
        for(Contract_Document__c obj:newList){
            if(OldMap != Null &&(obj.Account__c != OldMap.get(obj.Id).Account__c) && obj.Client__c == Null)
            {               
                lstCd.add(obj.Account__c);
            }
            else if (oldMap == NULL){
                lstCd.add(obj.Account__c);
            }
        }
        
        if(lstCd!=Null && lstCd.size()>0){
            
            //updates
            Map<Id,Account> acc = new Map<Id,Account>([
                SELECT ID, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry 
                FROM Account WHERE Id in:lstCd
            ]);
            
            Set<Id> accSet = acc.keySet();
            
            List<Contract_Document__c> cd = new List<Contract_Document__c>();
            
            for(Contract_Document__c c:newList){
                for(Id ac:accSet){
                    if(c.Account__c == ac){
                        Contract_Document__c cdNew = new Contract_Document__c();
                        cdNew.Id = c.Id;
                        cdNew.Bill_to_Name__c = acc.get(ac).Name;
                        cdNew.Billing_Street__c = acc.get(ac).BillingStreet;
                        cdNew.Billing_City__c = acc.get(ac).BillingCity;
                        cdNew.Billing_State__c = acc.get(ac).BillingState;
                        cdNew.Billing_Zip_Postal__c= acc.get(ac).BillingPostalCode;
                        cdNew.Billing_Country__c = acc.get(ac).BillingCountry;
                        cd.add(cdNew);
                    }
                }
            }
            update cd;
        }       
    }*/
    
  /*  public static void updateClient(List<Contract_Document__c> newList, map<Id,Contract_Document__c> OldMap){
        
        List<Id> lstCd = new List<Id>();
        
        for(Contract_Document__c obj:newList){
            if(OldMap != Null &&(obj.Client__c != OldMap.get(obj.Id).Client__c))
            {               
                lstCd.add(obj.Client__c);
            }
            else if (oldMap == NULL){
                lstCd.add(obj.Client__c);
            }
        }
        
        if(lstCd!=Null && lstCd.size()>0){
            
            //updates
            Map<Id,Client__c> acc = new Map<Id,Client__c>([
                SELECT ID, BillToName__c, Billing_Street_Address__c, Billing_City__c, Billing_State_Province__c, 
                Billing_Zip_Postal_Code__c, BillingCountry2__c 
                FROM Client__c WHERE Id in:lstCd
            ]);
            
            Set<Id> accSet = acc.keySet();
            
            List<Contract_Document__c> cd = new List<Contract_Document__c>();
            
            for(Contract_Document__c c:newList){
                for(Id ac:accSet){
                    if(c.Client__c == ac){
                        Contract_Document__c cdNew = new Contract_Document__c();
                        cdNew.Id = c.Id;
                        cdNew.Bill_to_Name__c = acc.get(ac).BillToName__c;
                        cdNew.Billing_Street__c = acc.get(ac).Billing_Street_Address__c;
                        cdNew.Billing_City__c = acc.get(ac).Billing_City__c;
                        cdNew.Billing_State__c = acc.get(ac).Billing_State_Province__c;
                        cdNew.Billing_Zip_Postal__c= acc.get(ac).Billing_Zip_Postal_Code__c;
                        cdNew.Billing_Country__c = acc.get(ac).BillingCountry2__c;
                        cd.add(cdNew);
                    }
                }
            }
            update cd;
        }       
    }*/
    
}