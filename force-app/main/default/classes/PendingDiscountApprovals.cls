public with sharing class PendingDiscountApprovals{ 
public List< ProcessInstance > Records {get; set;} 
public PendingDiscountApprovals(){ 
Records = 
[  
      SELECT x.CreatedBy.name , x.CreatedBy.Rep_Tier__c   ,x.CreatedBy.Global_Region__c,  x.Status, x.TargetObject.Name, TargetObjectId, TargetObject.Type,
 (SELECT y.Actor.Id , y.Actor.Name, y.Actor.Email, y.CreatedDate, y.ElapsedTimeInHours FROM WorkItems y )
                                    FROM ProcessInstance x
                                    WHERE TargetObject.Type = 'Quote' and Status='Pending'
and TargetObjectId in (select ID from quote where
 opportunity.Stagename<>'Lost'
and  opportunity.Stagename<>'Won'
and  opportunity.Stagename<>'Invoice')
  order by LastModifiedDate asc

    
 ]; 
} 




}