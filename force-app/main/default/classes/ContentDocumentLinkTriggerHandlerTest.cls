@IsTest
public class ContentDocumentLinkTriggerHandlerTest {
    testMethod static void ContractDocumentTriggerHandlerTest(){
        Account acct = new Account(Name='TEST');
        insert acct;
        
        Opportunity opp = new Opportunity(AccountId=acct.Id,
                                          Name='TEST',
                                          StageName = 'Pending',
                                          CloseDate = System.today(),
                                          Turn_Off_Initial_CX_EX_RC_Amounts__c = true);
        insert opp;
        
        List<Contract_Document__c> cd = [SELECT Id FROM Contract_Document__c WHERE Opportunity__c  =: opp.Id];
        
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion; 
 
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = cd[0].Id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.ShareType = 'V';
        cdl.Visibility = 'AllUsers';
        insert cdl;
        
    }
}