public with sharing class ClientAddressUpdate {
	
	/**
	 * This updates the client billing address from the contract document.
	 * @param contractDocNewMap 
	 * @param contractDocOldMap 
	 */
	public static void startAddressUpdateProcess(Map<Id,Contract_Document__c> contractDocNewMap, Map<Id,Contract_Document__c> contractDocOldMap){
		Map<Id,Contract_Document__c> contractDocWorkingMap = findWorkingmap(contractDocNewMap, contractDocOldMap);//get only contract documents whose address has changed
		if(contractDocWorkingMap.size() == 0){//stop processing if there are no contract documents with new objects.
			return;
		}
		Map<Id,Client__c> clientMap = queryClientsRelatedToContractDocuments(contractDocWorkingMap);
		updateClientRecords(contractDocWorkingMap, clientMap);
	}


	/**
	 * this will take the values from the contract object and assign them to the client obj.
	 * @param contractDocWorkingMap 
	 * @param clientMap             
	 */
	@testVisible
	private static void updateClientRecords(Map<Id,Contract_Document__c> contractDocWorkingMap, Map<Id,Client__c> clientMap){
		for(Contract_Document__c contractDocObj : contractDocWorkingMap.values()){
			clientMap.get(contractDocObj.Client__c).Billing_City__c = contractDocObj.Billing_City__c;
			clientMap.get(contractDocObj.Client__c).Billing_Country__c = contractDocObj.Billing_Country__c;
			clientMap.get(contractDocObj.Client__c).BillingCountry2__c = contractDocObj.Billing_Country__c;
			clientMap.get(contractDocObj.Client__c).Billing_State_Province__c = contractDocObj.Billing_State__c;
			clientMap.get(contractDocObj.Client__c).Billing_Street_Address__c = contractDocObj.Billing_Street__c;
			clientMap.get(contractDocObj.Client__c).Billing_Zip_Postal_Code__c = contractDocObj.Billing_Zip_Postal__c;
		}
		Try{
			update clientMap.values();
		}
		Catch(exception e){
			system.debug(logginglevel.error, '*****'+e);
		}
	}

	/**
	 * this method querries the Client records and returns them in a map
	 * @param  contractDocWorkingMap the contract documents whose client needs updating
	 * @return                       the client records
	 */
	@testVisible
	private static Map<Id,Client__c> queryClientsRelatedToContractDocuments(Map<Id,Contract_Document__c> contractDocWorkingMap){
		Set<Id> clientIdSet = new Set<Id>();
		for(Contract_Document__c contractDocObj : contractDocWorkingMap.values()){
			clientIdSet.add(contractDocObj.Client__c);
		}
		Map<Id,Client__c> returnMap = new Map<Id,Client__c>();
		returnMap.putall([SELECT Billing_City__c,BillingCountry2__c,Billing_Country__c,Billing_State_Province__c,Billing_Street_Address__c,Billing_Zip_Postal_Code__c FROM Client__c WHERE Id in :clientIdSet]);
		return returnMap;
	}


	/**
	 * this will check for only contract objects whose address has changed.
	 * @param  contractDocNewMap trigger.new
	 * @param  contractDocOldMap trigger.old
	 * @return                   the objects who are new or whose address has changed.
	 */
	@testVisible
	private static Map<Id,Contract_Document__c> findWorkingmap(Map<Id,Contract_Document__c> contractDocNewMap, Map<Id,Contract_Document__c> contractDocOldMap){
		Map<Id,Contract_Document__c> returnMap = new Map<Id,Contract_Document__c>();
		if(contractDocOldMap == null){
			for(Contract_Document__c contractDocObj : contractDocNewMap.values()){
				if(contractDocObj.Client__c != null){
					returnMap.put(contractDocObj.Id, contractDocObj);
				}
			}
			return returnMap;
		}
		for(Id objId : contractDocNewMap.keyset()){
			if(
				contractDocNewMap.get(objId).Billing_City__c != contractDocOldMap.get(objId).Billing_City__c
				|| contractDocNewMap.get(objId).Billing_Country__c != contractDocOldMap.get(objId).Billing_Country__c
				|| contractDocNewMap.get(objId).Billing_State__c != contractDocOldMap.get(objId).Billing_State__c
				|| contractDocNewMap.get(objId).Billing_Street__c != contractDocOldMap.get(objId).Billing_Street__c
				|| contractDocNewMap.get(objId).Billing_Zip_Postal__c != contractDocOldMap.get(objId).Billing_Zip_Postal__c
				|| contractDocNewMap.get(objId).Client__c != null
			){
				returnMap.put(objId, contractDocNewMap.get(objId));
			}
		}
		return returnMap;
	}


	public static void startUpdateShippingFromBilling(List<Client__c> clientList){
		for(Client__c clientObj : clientList){
			if(clientObj.Shipping_City__c == null && clientObj.Billing_City__c != null){
				clientObj.Shipping_City__c = clientObj.Billing_City__c;
			}
			if(clientObj.Shipping_Country__c == null && clientObj.Billing_Country__c != null){
				clientObj.Shipping_Country__c = clientObj.Billing_Country__c;
			}
			if(clientObj.ShippingCountry2__c == null && clientObj.BillingCountry2__c != null){
				clientObj.ShippingCountry2__c = clientObj.BillingCountry2__c;
			}
			if(clientObj.ShippingCountry2__c == null && clientObj.BillingCountry2__c != null){
				clientObj.ShippingCountry2__c = clientObj.BillingCountry2__c;
			}
			if(clientObj.Shipping_State__c == null && clientObj.Billing_State_Province__c != null){
				clientObj.Shipping_State__c = clientObj.Billing_State_Province__c;
			}
			if(clientObj.Shipping_Street__c == null && clientObj.Billing_Street_Address__c != null){
				clientObj.Shipping_Street__c = clientObj.Billing_Street_Address__c;
			}
			if(clientObj.Shipping_Zip_Postal_Code__c == null && clientObj.Billing_Zip_Postal_Code__c != null){
				clientObj.Shipping_Zip_Postal_Code__c = clientObj.Billing_Zip_Postal_Code__c;
			}
		}
	}



}