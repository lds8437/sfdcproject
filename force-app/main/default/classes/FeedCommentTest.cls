@isTest
public class FeedCommentTest {
    
    @isTest static void addCaseFeedComments() {
        //Create new cases to add comments to
        List<Case> cases = new List<Case>();
        Case newCase = new Case(
        	Subject = 'Test New Case',
            Description = 'This is a test case',
            Status = 'New'
        );
        cases.add(newCase);
        Case closedCase = new Case(
        	Subject = 'Test Closed Case',
            Description = 'This is a test case',
            Resolution_Notes__c = 'TEST',
            Status = 'Closed'
        );
        cases.add(closedCase);
        insert cases;
        //system.debug(cases);
        
        //Create new FeedItem records to comment on later
        List<FeedItem> feedItems = new List<FeedItem>();
        FeedItem fi1 = new FeedItem(
        	ParentId = newCase.Id,
            Body = 'Test Post'
        );
        feedItems.add(fi1);
        FeedItem fi2 = new FeedItem(
        	ParentId = closedCase.Id,
            Body = 'Test Post'
        );
        feedItems.add(fi2);
        insert feedItems;
        //system.debug(feedItems);
        
        //Create FeedComments to test trigger
        Test.startTest();
        List<FeedComment> feedComments = new List<FeedComment>();
        FeedComment fc1 = new FeedComment(
            FeedItemId = fi1.Id,
        	//ParentId = newCase.Id,
            CommentBody = 'Test Comment'
        );
        feedComments.add(fc1);
        FeedComment fc2 = new FeedComment(
        	FeedItemId = fi2.Id,
            //ParentId = closedCase.Id,
            CommentBody = 'Test Comment'
        );
        feedComments.add(fc2);
        insert feedComments;
        //system.debug(feedComments);
        Test.stopTest();
        
        //Assure that only one of the cases has been updated
        List<Case> inProgressCases = [SELECT Id FROM Case WHERE Status = 'In Progress'];
        System.assertEquals(1, inProgressCases.size());
    }

}