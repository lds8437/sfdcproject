@IsTest
public class cpq_ConfigureTabPageTestClass {

    static list<QuotelineItem> listQli {get;set;}

    @testsetup
    static void test_setup(){
        AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
    	insert avaCustomSetting;
		//Create Account
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        //Create Contact
        Contact con = new Contact();
        con.FirstName = 'Kruse';
        con.LastName = 'Collins';
        con.AccountId = acc.Id;
        con.LeadSource = 'Event';
        con.Contact_Role__c = 'VP';
        insert con;
        
        //Create Opportunity
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.CloseDate = date.today();
        opp.Accountid = acc.Id;
        opp.Contact2__c = con.Id;
        opp.Stagename = 'Discover and Assess';
        opp.Holdout_Opportunity__c = FALSE;
        opp.ForecastCategoryName = 'Commit';
        opp.Sales_Process_Stage__c = 'Discover and Assess';
        opp.Forecast_Percentage_NEW__c = 15.00;
        opp.Turn_Off_Initial_CX_EX_RC_Amounts__c = TRUE;
        Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();
        insert opp;
        
        //Create Quote 
        Quote qt = new Quote();
        qt.Name = 'Test Sync Quote';
        qt.OpportunityId = opp.Id;
        qt.License_Term_in_months__c = 12.00;
        Test.startTest();
        insert qt;
        Test.stopTest();
        
        //Create QService
        QService__c qserv = new QService__c();
        qserv.Name = 'Test Service';
        qserv.Service_Type__c = 'TAM';
        qserv.Quote__c = qt.Id;
        qserv.Opportunity__c = opp.Id;
        insert qserv;
        
        //Create new bundle
        Bundle__c b = new Bundle__c();
        b.Name = 'Test bundle';
        b.Experience__c = 'CX';
        b.Bundle__c = '1';
        b.Active__c = TRUE;
        b.Upgrade_Bundles__c = 'CX0; CX1; CX3; CX5; RC1; RC3; RC5; RC5d';
        insert b;
        
		Bundle__c b1 = new Bundle__c();
        b1.Name = 'RC1';
        b1.Experience__c = 'RC';
        b1.Bundle__c = '3';
        b1.Active__c = TRUE;
        insert b1;
        
        // Insert Product
        Product2 pr = new Product2();
        pr.Name='Moto - G1';
        pr.isActive=true;
        pr.Display_Order__c = 99;
        string prId = string.valueOf(pr.Id);
        insert pr;
        
        //Create new bundle product
        Product_Bundle_Junction__c bp = new Product_Bundle_Junction__c();
        bp.Name = 'Test Bundle Product';
        bp.Related_Bundle__c = b.Id;
        insert bp;
        
        //Create new pricing
        Pricing__c p = new Pricing__c();
        p.Pricing_Start_Date__c = date.parse('12/27/2000');
        p.Pricing_End_Date__c = date.parse('12/31/2040');
        insert p;
                
        //Create new service bundle
        Service_Bundle__c sb = new Service_Bundle__c();
        sb.Name = 'Test Service Bundle';
        sb.Bundle_Product_Prerequisite__c = bp.Id;
        insert sb;
        
        //Create new service bundle product
        Service_Bundle_Product_Junction__c sbpj = new Service_Bundle_Product_Junction__c();
        sbpj.Name = 'Test service bundle product junction';
        sbpj.Availability__c = 'Available';
        sbpj.Related_Service_Bundle__c = sb.Id;
        insert sbpj;
    }
    
    @IsTest static void testGetExperiences(){
        String exp = 'CX';
            
        Test.startTest();
        List<XM_Experience__mdt> exps = cpq_ConfigureTabPage.getExperiences(exp);
        Test.stopTest();
        System.debug(exps);
        system.assertNotEquals(0, exps.size());
            
    }
    @isTest static void testgetBundles() {
        Bundle__c b = [Select Id, Name, Experience__c, Bundle__c, Active__c From Bundle__c LIMIT 1];        
        List<Bundle__c> newQuery = cpq_ConfigureTabPage.getBundles(b.Experience__c, new list<String>{'CX'});        
        System.assertNotEquals(0, newQuery.size());
    } 
        
    @isTest static void testgetUpgradeBundles() {
        Bundle__c b = [Select Id, Name, Experience__c, Bundle__c, Active__c From Bundle__c LIMIT 1];        
        List<Bundle__c> getUpgradeBundleTestQuery = cpq_ConfigureTabPage.getUpgradeBundles(b.Id, new list<String>{'CX'}, b.Name);
    	System.assertNotEquals(0, getUpgradeBundleTestQuery.size());
    } 
    
    @isTest static void testgetSystemProducts(){
        
        Product2 pr = [Select Id, Name, isActive, Display_Order__c From Product2 limit 1];
        string prId = string.valueOf(pr.Id);
        
        List<Product2> newquerygetsystemproducts = cpq_ConfigureTabPage.getSystemProducts(new List<String>{prId});
    }
    
    @isTest static void testgetProducts() {
        Bundle__c b = [Select Id, Name, Experience__c, Bundle__c, Active__c From Bundle__c LIMIT 1];
                
        List<Product_Bundle_Junction__c> newquery = cpq_ConfigureTabPage.getProducts(b.Id);
    }
    
    @isTest static void testgetPricing() {
        Bundle__c b = [Select Id, Name, Experience__c, Bundle__c, Active__c From Bundle__c LIMIT 1];
                
        List<Pricing__c> newquery = cpq_ConfigureTabPage.getPricing(b.Id);
    }
    
    @isTest static void testgetServiceBundles() {
        Bundle__c b = [Select Id, Name, Experience__c, Bundle__c, Active__c From Bundle__c LIMIT 1];
                        
        List<Service_Bundle__c> newquery = cpq_ConfigureTabPage.getServiceBundles(b.Name);
    }
    
    @isTest static void testgetServiceProducts() {
        Bundle__c b = [Select Id, Name, Experience__c, Bundle__c, Active__c From Bundle__c LIMIT 1];
                        
        List<Service_Bundle_Product_Junction__c> newquery = cpq_ConfigureTabPage.getServiceProducts(b.Name);
    }
    
    @isTest static void testgetgetQuoteLineItems() {
        TestFactory.disableTriggers();

        //Create Account
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        //Create Contact
        Contact con = new Contact();
        con.FirstName = 'Kruse';
        con.LastName = 'Collins';
        con.AccountId = acc.Id;
        con.LeadSource = 'Event';
        con.Contact_Role__c = 'VP';
        insert con;
        
        Opportunity opp = TestFactory.createOpportunity(acc.Id, null);
        insert opp;
        Quote qt = TestFactory.createQuote(opp.Id, Test.getStandardPricebookId());
        insert qt;

        Product2 prod = new Product2(Name='Laptop', Family = 'Hardware', Display_Order__c=78, IsActive=true);
        insert prod;
        
        PricebookEntry standardPrice = new PricebookEntry(
           Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod.Id,
           UnitPrice = 10000, IsActive = true);
        insert standardPrice;

        listQLI = new list<QuotelineItem>();
        listQLI.add(TestFactory.createQuoteLineItem(qt.Id, standardPrice.Id));
        insert listQLI;

        List<QuoteLineItem> newquery = cpq_ConfigureTabPage.getQuoteLineItems(qt.Id);
    } 
        

    
    @IsTest static void testgetQservice(){                
        Quote qt = [SELECT Id, Name, OpportunityId, License_Term_in_months__c FROM Quote LIMIT 1];
        
        List<QService__c> newQuery = cpq_ConfigureTabPage.getQService(qt.Id);
    } 
    
    @IsTest static void testgetServiceComponents(){
		Quote qt = [SELECT Id, Name, OpportunityId, License_Term_in_months__c FROM Quote LIMIT 1];
        
        List<Service_Component__c> newquery = cpq_ConfigureTabPage.getServiceComponents(qt.Id);
    }  
}