public with sharing class OpportunityTriggerHandler {
	
	public static boolean firstRunOnBeforeInsert = true;
	public static boolean firstRunOnAfterInsert = true;
	public static boolean firstRunOnBeforeUpdate = true;
	public static boolean firstRunOnAfterUpdate = true;
	public static boolean firstRunOnBeforeDelete = true;
	public static boolean firstRunOnAfterDelete = true;
	public static boolean firstRunOnUndelete = true;
	
	
	public void OnBeforeInsert(Opportunity[] newObjects){
		if(firstRunOnBeforeInsert){
			firstRunOnBeforeInsert = false;
			OpportunityFieldUpdates.beforeInsertTriggerUpdates(newObjects, null);
           // System.debug('1.Number of Queries used in this apex code so far: ' + Limits.getQueries());
			TaxExempt.startFromOpportunity(newObjects);
          //  System.debug('2.Number of Queries used in this apex code so far: ' + Limits.getQueries());
            PanelDemographicsCountryScreen.executeScreen(newObjects);
         //   System.debug('3.Number of Queries used in this apex code so far: ' + Limits.getQueries());
		}
	}
	
	public void OnAfterInsert(Opportunity[] newObjects, map<id,Opportunity> MapNewMap){
		if(firstRunOnAfterInsert){
			firstRunOnAfterInsert = false;
			//cdr_CreateContractRecord.createCdr(newObjects);
        //    System.debug('4.Number of Queries used in this apex code so far: ' + Limits.getQueries());
            //PanelDemographicsCountryScreen.executeScreen(newObjects);
            PartnerShareOpportunityHelper.afterInsert(MapNewMap);
        //    System.debug('5.Number of Queries used in this apex code so far: ' + Limits.getQueries());
		}
	}
	
	public void OnBeforeUpdate(Opportunity[] oldObjects, Opportunity[] updatedObjects, map<id,Opportunity> MapNewMap, map<id,Opportunity> MapOldMap){
		if(firstRunOnBeforeUpdate){
			firstRunOnBeforeUpdate = false;
			OpportunityFieldUpdates.beforeUpdateTriggerUpdates(updatedObjects, MapOldMap);
         //   System.debug('6.Number of Queries used in this apex code so far: ' + Limits.getQueries());
            OpportunityFieldUpdates.reformatSAPInformation(updatedObjects);
          //  System.debug('7.Number of Queries used in this apex code so far: ' + Limits.getQueries());
			TaxExempt.startFromOpportunity(updatedObjects);
          //  System.debug('8a.Number of Queries used in this apex code so far: ' + Limits.getQueries());
            PanelsProjectManagerRecommendationClass.managerRecommendation(updatedObjects, MapOldMap);
            
          //  System.debug('8b.Number of Queries used in this apex code so far: ' + Limits.getQueries());
		}
	}
	
	public void OnAfterUpdate(Opportunity[] oldObjects, Opportunity[] updatedObjects, map<id,Opportunity> MapNewMap, map<id,Opportunity> MapOldMap){
		if(firstRunOnAfterUpdate){
			firstRunOnAfterUpdate = false;
			//Query opportunites to be used in all of the future methods.
			Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>([
	            SELECT RecordType.DeveloperName, AccountId, Client__c, SyncedQuote.Id, SyncedQuote.ExpirationDate, Qualification_Contact__c, OnboardingWizardUsed__c, 
	            	CurrencyISOCode, Owner.Id, StageName, Amount, OwnerId, Products__c, Qualification_Contact__r.Email, Qualification_Contact__r.Name, Count_Of_Surveys__c, 
		            Account_Name_Text__c, CloseDate, CreatedBy.Name, LeadSource, Owner.Name, Owner.Rep_Tier__c, Owner.Global_Region__c, Owner.Area__c, 
		            Owner.Region__c, Owner.Sales_Team__c, RecordType.Name, Pricebook2.Name, Territory2.Name, of_Products__c, of_Responses__c, of_Users__c, 
		            Account_Tier__c, Account_Type__c, Discount__c, Lead_Source_Detail__c, Lead_Type__c, Partner__r.Name, Product__c, 
		            CreatedBy.Rep_Tier__c, CreatedBy.Global_Region__c, CreatedBy.Area__c, CreatedBy.Region__c, CreatedBy.Sales_Team__c, Owner.Team_Lead__r.Email,
                Manager_Judgement__c
	            FROM Opportunity 
	            WHERE Id in :MapNewMap.keyset()
	        ]);
            System.debug('9.Number of Queries used in this apex code so far: ' + Limits.getQueries());
			wls_CreateSurveyRecord.createSurvey(updatedObjects, MapOldMap, oppMap);
            System.debug('10.Number of Queries used in this apex code so far: ' + Limits.getQueries());
			PartnerSurveyClass.getOpportunity(updatedObjects);
            System.debug('11.Number of Queries used in this apex code so far: ' + Limits.getQueries());
		//	cdr_CreateContractRecord.updateCdr(updatedObjects);
            System.debug('12.Number of Queries used in this apex code so far: ' + Limits.getQueries());
		//	SendemailController.sendInvoice(updatedObjects, MapOldMap);
		PanelDemographicsCountryScreen.executeScreen(updatedObjects);
            System.debug('13.Number of Queries used in this apex code so far: ' + Limits.getQueries());
            PartnerShareOpportunityHelper.afterUpdate(MapNewMap, MapOldMap);
            System.debug('14.Number of Queries used in this apex code so far: ' + Limits.getQueries());
		}
	}
	
	public void OnBeforeDelete(Opportunity[] ObjectsToDelete, map<id,Opportunity> MapNewMap, map<id,Opportunity> MapOldMap){
		if(firstRunOnBeforeDelete){
			firstRunOnBeforeDelete = false; 
	
		}
	}
	
	public void OnAfterDelete(Opportunity[] deletedObjects, map<id,Opportunity> MapOldMap){
		if(firstRunOnAfterDelete){
			firstRunOnAfterDelete = false;
	
		}
	}
	
	public void OnUndelete(Opportunity[] restoredObjects){
		if(firstRunOnUndelete){
			firstRunOnUndelete = false;
			
		}
	}
		
}