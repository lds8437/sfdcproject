@isTest public with sharing class PartnerShareOpportunityTests {
	@istest static void test1() {
		List<Account> testAccts = new List<Account>();
        Account acctObj1 = TestFactory.createAccount();
        Account acctObj2 = TestFactory.createAccount();
        Account acctObj3 = TestFactory.createAccount();
        Account acctObj4 = TestFactory.createAccount();
        testAccts.add(acctObj1);
        testAccts.add(acctObj2);
        testAccts.add(acctObj3);
        testAccts.add(acctObj4);
        insert testAccts;
        
        for (Account a : testAccts) {
            a.IsPartner = true;
        }
        update testAccts;

        List<Contact> testContacts = new List<Contact>();
		Contact cObj1 = new Contact(LastName = 'Test1', AccountId = acctObj1.Id);
        Contact cObj2 = new Contact(LastName = 'Test2', AccountId = acctObj2.Id);
        Contact cObj3 = new Contact(LastName = 'Test3', AccountId = acctObj3.Id);
        Contact cObj4 = new Contact(LastName = 'Test4', AccountId = acctObj4.Id);
		testContacts.add(cObj1);
        testContacts.add(cObj2);
        testContacts.add(cObj3);
        testContacts.add(cObj4);
        insert testContacts;

        List<Q_Partner__c> testPartners = new List<Q_Partner__c>();
        Q_Partner__c partnerObj1 = TestFactory.createPartner(acctObj1.Id);
        Q_Partner__c partnerObj2 = TestFactory.createPartner(acctObj2.Id);
        Q_Partner__c partnerObj3 = TestFactory.createPartner(acctObj3.Id);
        Q_Partner__c partnerObj4 = TestFactory.createPartner(acctObj4.Id);
        testPartners.add(partnerObj1);
        testPartners.add(partnerObj2);
        testPartners.add(partnerObj3);
        testPartners.add(partnerObj4);
        insert testPartners;

		Id sysAdminpId = [select id from profile where name='System Administrator'].id;

		User tempUser = [SELECT Id FROM User WHERE ProfileId =: sysAdminpId AND IsActive = TRUE LIMIT 1];

		Id p = [select id from profile where name='Partner Community User'].id;
	
        String orgId = UserInfo.getOrganizationId();
	    String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    	Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
	    String uniqueName = orgId + dateString + randomInt;
        
        List<User> testUsers = new List<User>();
		System.runAs(tempUser){
			User u1 = new User( email = uniqueName + '@test1' + orgId + '.org',
	                profileid = p,
	                Username = uniqueName + '@test1' + orgId + '.org',
	                Alias = 'GDS',
	                TimeZoneSidKey='America/New_York',
	                EmailEncodingKey='ISO-8859-1',
	                LocaleSidKey='en_US',
	                LanguageLocaleKey='en_US',
	                ContactId = cObj1.Id,
	                PortalRole = 'Manager',
	                FirstName = 'Test',
	                LastName = 'User1');
            
            User u2 = new User( email = uniqueName + '@test2' + orgId + '.org',
	                profileid = p,
	                Username = uniqueName + '@test2' + orgId + '.org',
	                Alias = 'GDS',
	                TimeZoneSidKey='America/New_York',
	                EmailEncodingKey='ISO-8859-1',
	                LocaleSidKey='en_US',
	                LanguageLocaleKey='en_US',
	                ContactId = cObj2.Id,
	                PortalRole = 'Manager',
	                FirstName = 'Test',
	                LastName = 'User2');
            
            User u3 = new User( email = uniqueName + '@test3' + orgId + '.org',
	                profileid = p,
	                Username = uniqueName + '@test3' + orgId + '.org',
	                Alias = 'GDS',
	                TimeZoneSidKey='America/New_York',
	                EmailEncodingKey='ISO-8859-1',
	                LocaleSidKey='en_US',
	                LanguageLocaleKey='en_US',
	                ContactId = cObj3.Id,
	                PortalRole = 'Manager',
	                FirstName = 'Test',
	                LastName = 'User3');
            
            User u4 = new User( email = uniqueName + '@test4' + orgId + '.org',
	                profileid = p,
	                Username = uniqueName + '@test4' + orgId + '.org',
	                Alias = 'GDS',
	                TimeZoneSidKey='America/New_York',
	                EmailEncodingKey='ISO-8859-1',
	                LocaleSidKey='en_US',
	                LanguageLocaleKey='en_US',
	                ContactId = cObj4.Id,
	                PortalRole = 'Manager',
	                FirstName = 'Test',
	                LastName = 'User4');
			
            testUsers.add(u1);
            testUsers.add(u2);
            testUsers.add(u3);
            testUsers.add(u4);
            insert testUsers;
		}

		Pricebook2 pb2Obj = TestFactory.createPriceBook();
        insert pb2Obj;

        Product2 prodObj = TestFactory.createProduct();
        insert prodObj;

        PricebookEntry standardPbe = TestFactory.createPricebookEntry(Test.getStandardPricebookId(), prodObj.Id);
        insert standardPbe;

        PricebookEntry customPbe = TestFactory.createPricebookEntry(pb2Obj.Id, prodObj.Id);
        insert customPbe;

		Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales Qualification').getRecordTypeId();

		Opportunity oppObj = TestFactory.createOpportunity(acctObj1.Id, pb2Obj.Id);
        oppObj.Partner__c = partnerObj1.Id;
        oppObj.QPN_Recommended_Partner__c = partnerObj3.Id;
		oppObj.RecordTypeId = recordTypeId;
		Test.startTest();
        insert oppObj;
        oppObj.Partner__c = partnerObj2.Id;
        oppObj.Partner__c = partnerObj4.Id;
        OpportunityTriggerHandler.firstRunOnAfterUpdate = true;
        update oppObj;
        Test.stopTest();

    }
}