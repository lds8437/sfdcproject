@IsTest
public class ConsolidatedScreeningTest {
    
    @IsTest
    public static void testApprovalCreation(){
        //Set up test
        List<Database.SaveResult> saveResults = new List<Database.SaveResult>();
        Account account = new Account(
            name = 'Caribe Sol'
        );
        saveResults.add(Database.insert(account));
        Client__c client = new Client__c(
            name = 'Caribe Sol',
            Account__c = account.Id
        );
		saveResults.add(Database.insert(client));
        Opportunity opportunity = new Opportunity(
            AccountId = account.Id,
            Name = 'Test',
            StageName = 'Pending',
            CloseDate = System.today(),
            Turn_Off_Initial_CX_EX_RC_Amounts__c = true
        );
		saveResults.add(Database.insert(opportunity));
        Test.setMock(HttpCalloutMock.class, new ConsolidatedScreeningMockCall());
        Test.startTest();
        ConsolidatedScreeningManager manager = new ConsolidatedScreeningManager(client.Id,account.Id,opportunity.Id);
        List<ProcessInstance> approvals = [SELECT Id FROM ProcessInstance WHERE TargetObjectId =: opportunity.Id];
        system.assert(approvals.size() > 0);
        Test.stopTest();
    }
    
    @IsTest
    public static void testListOfStrings(){
        //Set up test
        List<Object> results = new List<Object>();
        List<String> searchStrings = new List<String>{'Caribe Sol','Test Account'};
        Test.setMock(HttpCalloutMock.class, new ConsolidatedScreeningMockCall());
        Test.startTest();
        ConsolidatedScreeningManager manager = new ConsolidatedScreeningManager(searchStrings);
       	results = manager.getResults();
        system.assert(results != null);
        Test.stopTest();
    }
    
    
}