public with sharing class XM_Discount_Approvals {
	
	public Id quoteId {get;set;}
	public PageReference quoteUrl {get;set;}


	public XM_Discount_Approvals() {
		quoteId = ApexPages.CurrentPage().getParameters().get('id');
	}

	public PageReference getquoteUrl(){
		Pagereference MyPage = new Pagereference('/'+quoteId);
		MyPage.setRedirect(true);
		return MyPage;
	}

}