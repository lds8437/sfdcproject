public with sharing class RedirectionException extends Exception {

    /**
     * This custom exception supports a list of error messages
     * as opposed to a single message which standard exceptions
     * support.
     * 
     * This additional property could be updated to be a custom
     * class instead of a string.  This would allow additional
     * properties to be tracked and reported on when exceptions
     * occur. (line numbers, context, etc.)
     */
    private List<String> messages {get;set;}
    
    /**
     * Class Constructor
     * 
     * The only constructor supported is a list of messages.  If
     * the enhancement described above is implmented this constructor
     * would need to be updated to support the custom class with
     * additionl properties.
     */
    public RedirectionException(List<String> messages) {
        this.messages = messages;
    }
    
    /**
     * Getters
     */
    public List<String> getMessages() {
        return messages;
    }
}