global with sharing class xm_QuoteExtension {
    
    private final Opportunity opp;
    public static List<Quote> quoteList { get; set; }
    public static List<XM_Experience__mdt> xmexp { get; set; } 
    public static XM_Quoting__c xmq { get; set; } 
    public static List<Bundle__c> bundles {get; set;}
    public static List<Pricing__c> price { get; set; }
    public static List<Product_Bundle_Junction__c> products { get; set; }
    public static List<Client__c> clients { get; set; }     
    public static List<Client_License__c> clientBundles { get; set; }
    public static List<Client_License__c> clientLicense { get; set; }
    public static Account account { get; set; }
    public static List<PriceBookEntry> pbe { get; set; } 
    public static Bundle__c curBundle { get; set; }
    public static List<Bundle__c> upgradeBundles {get; set;}
    public static List<QuoteLineItem> qliList { get; set; }
    public static map<string,decimal> ct = new map<string,decimal>();
    //    public static map<Id,boolean> lockedquotes = new map<Id,boolean>();
    // public static List<QuoteLineItem> qliList2 { get; set; }
    public static string loc {get;set;} 
    
    //CONTROLLER
    public xm_QuoteExtension(ApexPages.StandardController sc) {
        Opportunity opp = (Opportunity)sc.getRecord();
        getLoc();
    }
    
    public static string getLoc(){
        loc = UserInfo.getLocale();
        return loc;
    }
    
    //GET CONVERSION RATES
    @RemoteAction    
    global static map<string,decimal> getct(){
        for(CurrencyType c : [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE ]){
            //  system.debug('c: '+c);
            ct.put(c.ISOCode,c.ConversionRate);
            // system.debug('currency: '+ct);
        }
        return ct;
    }
    
    //GET QUOTES
    @RemoteAction
    global static List<Quote> getQuoteList(string oppId){
        quoteList = [SELECT Id, Name, XM_Products__c, License_Start_Date__c, 
                     License_End_Date__c, Last_Configuration__c, //Related_Client__c,
                     TotalPrice, License_Term_in_months__c, ExpirationDate, Opportunity.IsClosed,
                     Quote_Type__c, IsSyncing, Unused_Credits__c, Freeze_Pricing__c, ChosenPriceTable__c, Locked__c, CurrencyIsoCode, Culture_Code__c 
                     FROM Quote WHERE OpportunityId =: oppId and RecordType.DeveloperName = 'XM' and XM_Products__c != null ORDER BY Name];
        return quoteList;
    }
    
    //DELETE QUOTE
    @RemoteAction    
    public static void deleteQuote(Quote q)
    {
        try {
            Database.delete(q);
        } catch(DmlException e) {
            system.debug(e.getmessage());
        }
        
    } 
    
     //GET EXPERIENCES FROM CUSTOM METADATA
    @RemoteAction
    global static List<XM_Experience__mdt> getXmexp(list<string> ex){
        xmexp = [SELECT MasterLabel, Code__c, Active__c, Core_Type__c, Core_Entry_Type__c, Dashboard_Users__c   
                 FROM XM_Experience__mdt WHERE Active__c = TRUE and Code__c in :ex];
        return xmexp;
    }
    
    //GET EXPERIENCES FROM CUSTOM METADATA
    @RemoteAction
    global static XM_Quoting__c getXmq(){
        Id u = UserInfo.getProfileId();
        xmq = XM_Quoting__c.getInstance(u);
        return xmq;
    }
    
    //GET BUNDLES
    @RemoteAction
    global static List<Bundle__c> getBundles(string b, boolean a, list<string> d){
        
        system.debug('d: ' + d);
        bundles = [SELECT Id, Name, NS_Item_ID__c, Bundle__c, Experience__c, 
                   Bundle_Full_Name__c
                   FROM Bundle__c WHERE Experience__c =: b and A_la_Carte_Bundle__c =: a and Active__c = true and Name not in :d ORDER BY Name];
        return bundles;
    }
    
    //GET RESPONSE AND USER TIERS
    @RemoteAction
    global static List<Pricing__c> getPrice(string exp, string bdl){
        
        price = [SELECT Experience__c, Bundle__c, Minimum__c, Maximum__c, Pricing_Type__c, Pricing_Key__c, List_Price__c, Bundle_Discount_Amount__c, 
                 Price_Multiplier__c, Max_Cap__c, Min_Cap__c, Quantity__c, Unlimited__c, CarahsoftID__c, FedRAMP_CarahsoftID__c, GSA__c, GSACarahsoft__c, FedRamp__c, FedRampCarahsoft__c
                 FROM Pricing__c 
                 WHERE Experience__c =: exp and Bundle__c =: bdl and Active_Pricing__c = TRUE Order By Maximum__c];
        system.debug('price: '+price);
        return price;
    }
    
    //GET BUNDLE PRODUCTS
    @RemoteAction
    global static List<Product_Bundle_Junction__c> getProducts(string exp, string bdl){
        
        products = [SELECT Id, Related_Product__c, Name, Related_Product__r.Name, Related_Product__r.ProductCode, PricingType__c, 
                    Related_Bundle__r.Bundle__c, Related_Bundle__r.Experience__c, Availability__c, Bundle_Key__c, User_Input_Required__c,
                    Quantity__c, Quantity_Multiplier__c, Related_Product__r.Revenue_Roll_up__c, Quantity_Multiplied_by_Core__c, 
                    Display_Label__c, Display_Variable__c, Upgrade_Availability__c, Related_Product__r.Revenue_Recognition_Rule__c,
                    Related_Product__r.Revenue_Type__c, Related_Product__r.Eligible_for_Proration__c, Related_Product__r.Upgrade_Comparison__c,
                    Related_Product__r.NS_Item_ID__c, Max_Cap__c, Min_Cap__c
                    FROM Product_Bundle_Junction__c 
                    WHERE Related_Bundle__r.Experience__c =: exp and Related_Bundle__r.Bundle__c = : bdl and Active__c = TRUE order by Related_Product__r.Name];          
        system.debug('prod'+products);
        return products;
        
    }
    
    //GET LINKED ACCOUNT
    @RemoteAction
    global static Account GetAccount(String acctId) {
        account = [SELECT 
                   Id, 
                   Name 
                   FROM Account WHERE Id = :acctId];
        return account;       
    }
    
    //GET CLIENT LIST
    @RemoteAction
    global static List<Client__c> GetClients(String acctId) {
        clients = [SELECT 
                   Id, 
                   Name,
                   Client_Start_Date__c, 
                   Soonest_License_End_Date__c,
                   Client_Status__c,
                   Billing_Country__c,
                   Total_Spend__c,
                   CurrencyIsoCode,
                   Culture_Code__c
                   FROM Client__c WHERE Account__c = :acctId];
        return clients;       
    }
    
    
    //GET PRICEBOOK IDS
    @RemoteAction
    global static List<PriceBookEntry> getPbe(List<String> p, string pb, string c){        
        pbe = [SELECT Id, Product2Id, UnitPrice
               FROM PriceBookEntry 
               WHERE Product2Id in: p and Pricebook2Id =: pb and CurrencyIsoCode = :c
               ORDER BY Id];
        //    system.debug('pbe: '+pbe);
        return pbe;
    }
    
    //CREATE QUOTE
    @RemoteAction    
    public static Database.SaveResult createQuote(Quote q, String d, String dt, String ldt)
    {
        Id recId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('XM').getRecordTypeId();
        q.Last_Configuration__c = datetime.valueOf(dt);
        q.License_Start_Date__c = date.valueOf(d);
        if(q.Quote_Type__c == 'Core Upgrade - Same Renewal Date' || q.Quote_Type__c == 'Non-Core Add-On'){
        q.Current_License_End_Date__c = date.valueOf(ldt);
        }
        
        q.RecordTypeId = recId;
        system.debug(q);
        return Database.insert(q);        
    } 
    
    //EDIT QUOTE
    @RemoteAction    
    public static Database.SaveResult editQuote(Quote q, string dt, string ldt)
    {
        q.License_Start_Date__c = date.valueOf(dt);
        if(q.Quote_Type__c == 'Core Upgrade - Same Renewal Date' || q.Quote_Type__c == 'Non-Core Add-On'){
            q.Current_License_End_Date__c = date.valueOf(ldt);
        }
        
        return Database.update(q);        
    } 
    
    //UPDATE QUOTE CONFIGURATION DATE
    @RemoteAction    
    public static Database.SaveResult updateQuoteConfig(Quote q, string dt)
    {
        q.Last_Configuration__c = datetime.valueOf(dt);
        return Database.update(q);        
    } 
    
    //CREATE QUOTE LINE ITEMS
    @RemoteAction
    public static void createQlis(List<QuoteLineItem> a) {
        try {
            Database.SaveResult[] sr = Database.insert(a);
        } catch(DmlException e) {
            system.debug(e.getmessage());
        }
        system.debug('qli: '+a);
    }  
    
    //DELETE QUOTE LINE ITEMS
    @RemoteAction
    public static void deleteQlis(List<QuoteLineItem> a) {
        try {
            Database.delete(a);
        } catch(DmlException e) {
            system.debug(e.getmessage());
        }
    }      
    
    //GET QUOTE LINE ITEMS
    @RemoteAction 
    global static List<QuoteLineItem> getQliList(string qid){
        qliList = [SELECT 
                   Id, 
                   Quantity, 
                   UnitPrice, 
                   PricebookEntryId, 
                   List_Price__c, 
                   ListPrice, 
                   Max_Bundle_Discount__c, 
                   NS_Item_ID__c, 
                   License_Change__c,
                   Related_License_Item__c, 
                   Related_License_Item__r.Active__c,
                   New_Business_Value__c, 
                   Renewal_Value__c,
                   Quota_Relief_Amount__c,
                   Quote.Related_Client__c,
                   Quote.CurrencyIsoCode,
                   Quote.Culture_Code__c,
                   Bundle_Lookup__c, 
                   Bundle_Lookup__r.Bundle__c, 
                   Bundle_Product__c, 
                   Bundle_Lookup__r.Experience__c,
                   Bundle_Product__r.Availability__c, 
                   Bundle_Product__r.Upgrade_Availability__c, 
                   Bundle_Product__r.PricingType__c, 
                   Bundle_Product__r.User_Input_Required__c, 
                   Bundle_Product__r.Bundle_Key__c, 
                   Bundle_Product__r.Display_Variable__c, 
                   Bundle_Product__r.Display_Label__c, 
                   Bundle_Product__r.Name, 
                   Product2Id, 
                   Product2.Name, 
                   Product2.ProductCode, 
                   Product2.Revenue_Roll_up__c,
                   Product2.Revenue_Recognition_Rule__c,
                   Product2.Revenue_Type__c,
                   Product2.Eligible_for_Proration__c,
                   Product2.Upgrade_Comparison__c
                   FROM QuoteLineItem
                   WHERE QuoteId =: qid and Bundle_Lookup__c != null  //Bundle_Lookup__r.Name LIKE :exp
                   ORDER BY Id];
        system.debug('qliList: '+qliList);
        return qliList;
    }
    
    //GET CLIENT LIST
    @RemoteAction
    global static List<Client_License__c> GetClientLicense(string client, string exp, string status, boolean active) {
        clientLicense = [SELECT
                         Id,
                         Name, 
                         Status__c,
                         Active__c,
                         License_Start_Date__c, 
                         License_End_Date__c,
                         Quantitiy__c, 
                         CurrencyIsoCode,
                         Culture_Code__c,
                         Invoice_Amount__c, 
                         Proration_Amount__c,
                         Bundle__c, 
                         Bundle__r.Name, 
                         Bundle__r.Experience__c,
                         Bundle__r.Bundle__c,
                         Bundle__r.Upgrade_Bundles__c,
                         Bundle__r.A_la_Carte_Bundle__c,
                         Bundle_Product__c,
                         Bundle_Product__r.Availability__c, 
                         Bundle_Product__r.Upgrade_Availability__c, 
                         Bundle_Product__r.PricingType__c, 
                         Bundle_Product__r.User_Input_Required__c, 
                         Bundle_Product__r.Bundle_Key__c, 
                         Bundle_Product__r.Display_Variable__c, 
                         Bundle_Product__r.Display_Label__c, 
                         Bundle_Product__r.Name, 
                         Product__r.Name,
                         Product__r.ProductCode, 
                         Product__r.Revenue_Roll_up__c,
                         Product__r.Revenue_Recognition_Rule__c,
                         Product__r.Revenue_Type__c,
                         Product__r.Upgrade_Comparison__c,
                         Product__r.Eligible_for_Proration__c,
                         Product__r.NS_Item_ID__c,
                         Core_Product__c, 
                         Client__c, 
                         Client__r.Name                          
                         FROM Client_License__c 
                         WHERE Client__c =: client and Bundle__r.Experience__c =: exp and Status__c = :status and Active__c =: active];
        system.debug('cl'+clientLicense);
        return clientLicense;       
    }
    
    //GET CLIENT LIST
    @RemoteAction
    global static List<Client_License__c> GetClientBundles(string c) {
        system.debug(c);
        clientBundles = [SELECT
                         Id,
                         Name, 
                         Status__c,
                         Active__c,
                        // format(License_Start_Date__c) startDate, 
                        // format(License_End_Date__c) endDate,
                          License_Start_Date__c, 
                          License_End_Date__c,
                         CurrencyIsoCode,
                         Culture_Code__c,
                         Quantitiy__c, 
                         Invoice_Amount__c, 
                         Bundle__c, 
                         Bundle__r.Name, 
                         Bundle__r.Experience__c,
                         Bundle__r.Bundle__c,
                         Bundle_Product__c,
                         Bundle__r.A_la_Carte_Bundle__c,
                         Bundle_Product__r.Availability__c, 
                         Bundle_Product__r.Upgrade_Availability__c, 
                         Bundle_Product__r.PricingType__c, 
                         Bundle_Product__r.User_Input_Required__c, 
                         Bundle_Product__r.Bundle_Key__c, 
                         Bundle_Product__r.Display_Variable__c, 
                         Bundle_Product__r.Display_Label__c, 
                         Bundle_Product__r.Name, 
                         Product__r.Name,
                         Product__r.ProductCode, 
                         Product__r.Revenue_Roll_up__c,
                         Product__r.Upgrade_Comparison__c,
                         Product__r.Eligible_for_Proration__c,
                         Core_Product__c, 
                         Client__c, 
                         Client__r.Name                          
                         FROM Client_License__c 
                         WHERE Client__c =: c and Bundle__c !=null and (Status__c = 'Active' or Status__c = 'Expired')
                         Order By License_End_Date__c];
        return clientBundles;       
    }
    
    //GET BUNDLES
    @RemoteAction
    global static List<Bundle__c> getUpgradeBundles(List<string> b){     
        bundles = [
            SELECT Id, Name, NS_Item_ID__c, Bundle__c, Experience__c 
            FROM Bundle__c 
            WHERE Name in: b 
            ORDER BY Name];
        //   system.debug(bundles);
        return bundles;
    }
    
    //GET CURRENT BUNDLES
    @RemoteAction
    global static Bundle__c getCurBundle(string b){
        curBundle = [SELECT Id, Name, Bundle__c, Experience__c, Upgrade_Bundles__c, NS_Item_ID__c, A_la_Carte_Bundle__c FROM Bundle__c WHERE Id =: b];
        return curBundle;
    }
    
    //EDIT OPPORTUNITY WITH SYNCED QUOTE
    
    @RemoteAction    
    public static Database.SaveResult editOpp(Opportunity o)
    {
        return Database.update(o);        
    } 
}