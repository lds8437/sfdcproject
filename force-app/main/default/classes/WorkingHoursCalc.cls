public class WorkingHoursCalc{
 public static double workingHoursCalc(Datetime start, Datetime endtime){
  if(start.hour()<7){
   start = Datetime.newInstance(start.year(), start.month(), start.day(), 7, 0, 0);
  }
  if(start.hour()>=18){
   start = Datetime.newInstance(start.addDays(1).year(), start.addDays(1).month(), start.addDays(1).day(), 7, 0, 0); 
  }
  while(Math.mod(Date.newInstance(1900,1,8).daysBetween(start.date()),7)>4){
   start = Datetime.newInstance(start.addDays(1).year(), start.addDays(1).month(), start.addDays(1).day(), 7, 0, 0);
  }
  
  if(endtime.hour()<7){
   endtime = Datetime.newInstance(endtime.year(), endtime.month(), endtime.day(), 7, 0, 0);
  }
  if(endtime.hour()>=18){
   endtime = Datetime.newInstance(endtime.addDays(1).year(), endtime.addDays(1).month(), endtime.addDays(1).day(), 7, 0, 0);   
  }
  while(Math.mod(Date.newInstance(1900,1,8).daysBetween(endtime.date()),7)>4){
   endtime = Datetime.newInstance(endtime.addDays(1).year(), endtime.addDays(1).month(), endtime.addDays(1).day(), 7, 0, 0);
  }
  
  Integer days = start.date().daysBetween(endtime.date());
  if(days >= 7){
   days -= days / 7 * 2;
  }
  if(Math.mod(Date.newInstance(1900,1,8).daysBetween(start.date()),7) > Math.mod(Date.newInstance(1900,1,8).daysBetween(endtime.date()),7)){
   days -= 2;
  }
  
  Double hour = (days * (18-7)) + endtime.hour() - start.hour();
  Double min = endtime.minute() - start.minute();
  Double sec = endtime.second() - start.second();
  
  if(sec<0){
   sec+=60;
   min--;
  }
  
  if(min<0){
   min+=60;
   hour--;
  }
  
  System.debug(hour + ':' + min + ':' + sec);
  
  return (hour+((min+(sec/60))/60));
 }
}