@isTest
private class ConsoleTasksExtTest {
	
	@isTest static void test_method_one() {
		Contact c = new Contact(firstname='Fname',lastname='Lname',email='test@test.com');
		insert c;
		PageReference Page = new PageReference('/apex/ConsoleEvents?id='+c.id);
		test.setcurrentpage(Page);
		ConsoleTasks2Ext cee = new ConsoleTasks2Ext();
		cee.s = 'test';
		cee.CreateTask();
		cee.MySaveMethod();
		cee.MyCancelMethod();
	}
	
}