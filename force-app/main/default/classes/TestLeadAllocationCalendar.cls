@isTest (SeeAllData = 'True')
private class TestLeadAllocationCalendar {

    static testMethod void LeadAllocationTest() {
    Profile P = new Profile();
    P = [Select ID from Profile where Name = 'System Administrator'];
    List<User> AdmnUser = new List<User>();
    AdmnUser = [Select Id, ProfileID, Alias, TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, 
        LanguageLocaleKey from User where ProfileID = :P.ID Limit 1];
    List<Lead> newLeads = new List<Lead>();
    
    User u = new User();
    User u2 = new USer();
    User u3 = new USer();
    User u4 = new USer();
    User u5 = new USer();
    User u6 = new USer();
    User u7 = new USer();
    
    
    For(User AU: AdmnUser){
    u.FirstName = 'Ron';
    u.LastName = 'Howard';
    u.ProfileID = P.ID;
    u.UserName = 'ron@howard.com';
    u.email = 'ron@howard.com';
    u.Alias = AU.Alias;
    u.TimeZoneSidKey = AU.TimeZoneSidKey;
    u.LocaleSidKey = AU.LocaleSidKey;
    u.EmailEncodingKey = AU.EmailEncodingKey;
    u.LanguageLocaleKey = AU.LanguageLocaleKey;
    u.Region__c = 'APAC';
    u.Tier_A__c = True;
    u.Tier_B__c = True;
    u.Tier_C__c = True;
    u.Tier_D__c = True;
    u.Tier_E__c = True;
    
    u2.FirstName = 'Ron';
    u2.LastName = 'Howard';
    u2.ProfileID = P.ID;
    u2.UserName = 'ron@howderd.com';
    u2.email = 'ron@hohward.com';
    u2.Alias = AU.Alias;
    u2.TimeZoneSidKey = AU.TimeZoneSidKey;
    u2.LocaleSidKey = AU.LocaleSidKey;
    u2.EmailEncodingKey = AU.EmailEncodingKey;
    u2.LanguageLocaleKey = AU.LanguageLocaleKey;
    u2.Region__c = 'Canada';
    u2.Tier_A__c = True;
    u2.Tier_B__c = True;
    u2.Tier_C__c = True;
    u2.Tier_D__c = True;
    u2.Tier_E__c = True;
    
    u3.FirstName = 'Ron';
    u3.LastName = 'Howard';
    u3.ProfileID = P.ID;
    u3.UserName = 'ron@howddard.com';
    u3.email = 'ron@howffdard.com';
    u3.Alias = AU.Alias;
    u3.TimeZoneSidKey = AU.TimeZoneSidKey;
    u3.LocaleSidKey = AU.LocaleSidKey;
    u3.EmailEncodingKey = AU.EmailEncodingKey;
    u3.LanguageLocaleKey = AU.LanguageLocaleKey;
    u3.Region__c = 'Central';
    u3.Tier_A__c = True;
    u3.Tier_B__c = True;
    u3.Tier_C__c = True;
    u3.Tier_D__c = True;
    u3.Tier_E__c = True;
    
    u4.FirstName = 'Ron';
    u4.LastName = 'Howard';
    u4.ProfileID = P.ID;
    u4.UserName = 'ron@howarrrd.com';
    u4.email = 'rorrn@howatrd.com';
    u4.Alias = AU.Alias;
    u4.TimeZoneSidKey = AU.TimeZoneSidKey;
    u4.LocaleSidKey = AU.LocaleSidKey;
    u4.EmailEncodingKey = AU.EmailEncodingKey;
    u4.LanguageLocaleKey = AU.LanguageLocaleKey;
    u4.Region__c = 'EMEA';
    u4.Tier_A__c = True;
    u4.Tier_B__c = True;
    u4.Tier_C__c = True;
    u4.Tier_D__c = True;
    u4.Tier_E__c = True;
    
    u5.FirstName = 'Ron';
    u5.LastName = 'Howard';
    u5.ProfileID = P.ID;
    u5.UserName = 'ron@howatttrd.com';
    u5.email = 'ron@howtttard.com';
    u5.Alias = AU.Alias;
    u5.TimeZoneSidKey = AU.TimeZoneSidKey;
    u5.LocaleSidKey = AU.LocaleSidKey;
    u5.EmailEncodingKey = AU.EmailEncodingKey;
    u5.LanguageLocaleKey = AU.LanguageLocaleKey;
    u5.Region__c = 'Great Lakes';
    u5.Tier_A__c = True;
    u5.Tier_B__c = True;
    u5.Tier_C__c = True;
    u5.Tier_D__c = True;
    u5.Tier_E__c = True;
    
    u6.FirstName = 'Ron';
    u6.LastName = 'Howard';
    u6.ProfileID = P.ID;
    u6.UserName = 'rogn@hogward.com';
    u6.email = 'rogn@howagrd.com';
    u6.Alias = AU.Alias;
    u6.TimeZoneSidKey = AU.TimeZoneSidKey;
    u6.LocaleSidKey = AU.LocaleSidKey;
    u6.EmailEncodingKey = AU.EmailEncodingKey;
    u6.LanguageLocaleKey = AU.LanguageLocaleKey;
    u6.Region__c = 'Northeast';
    u6.Tier_A__c = True;
    u6.Tier_B__c = True;
    u6.Tier_C__c = True;
    u6.Tier_D__c = True;
    u6.Tier_E__c = True;
    
    u7.FirstName = 'Ron';
    u7.LastName = 'Howard';
    u7.ProfileID = P.ID;
    u7.UserName = 'ronyuu@howuard.com';
    u7.email = 'ronuui@howard.com';
    u7.Alias = AU.Alias;
    u7.TimeZoneSidKey = AU.TimeZoneSidKey;
    u7.LocaleSidKey = AU.LocaleSidKey;
    u7.EmailEncodingKey = AU.EmailEncodingKey;
    u7.LanguageLocaleKey = AU.LanguageLocaleKey;
    u7.Region__c = 'Southeast';
    u7.Tier_A__c = True;
    u7.Tier_B__c = True;
    u7.Tier_C__c = True;
    u7.Tier_D__c = True;
    u7.Tier_E__c = True;
    
    
    }
    insert u;
    insert u2;
    insert u3;
    insert u4;
    insert u5;
    insert u6;
    insert u7;
    
    Lead L = new Lead();
    L.FirstName = 'Tom';
    L.LastName = 'Hardy';
    L.Company = 'X';
    L.Region__c = 'APAC';
    L.Tier__c = 'A';
    L.Industry = 'Market Research';
    L.AnnualRevenue = 31;
    newLeads.add(L);
    
    //insert L; 
        
        
    Lead L2 = new Lead();
    L2.FirstName = 'Luke';
    L2.LastName = 'Perry';
    L2.Company = 'L';
    L2.Region__c = 'APAC';
    L2.Tier__c = 'B';
    L2.Industry = 'Market Research';
    L2.AnnualRevenue = 29;
    newLeads.add(L2);
    //insert L2;


    Lead L3 = new Lead();
    L3.FirstName = 'Sam';
    L3.LastName = 'Wilson';
    L3.Company = 'Z';
    L3.Region__c = 'APAC';
    L3.Tier__c = 'C';
    L3.Industry = 'Market Research';
    L3.AnnualRevenue = 1;
    newLeads.add(L3);
    //insert L3;

    Lead L4 = new Lead();
    L4.FirstName = 'Stu';
    L4.LastName = 'Haley';
    L4.Company = 'R';
    L4.Region__c = 'APAC';
    L4.Tier__c = 'D';
    newLeads.add(L4);
    //insert L4;
  
    
    Lead L5 = new Lead();
    L5.FirstName = 'Fank';
    L5.LastName = 'Bates';
    L5.Company = 'P';
    L5.Region__c = 'APAC';
    L5.Tier__c = 'E';
    L5.Industry = 'Academia';
    newLeads.add(L5);
    
    Lead L6 = new Lead();
    L6.FirstName = 'Fank1';
    L6.LastName = 'Bates1';
    L6.Company = 'P1';
    L6.Region__c = 'Canada';
    L6.Tier__c = 'A';
    L6.Industry = 'Market Research';
    L6.AnnualRevenue = 31;
    newLeads.add(L6);
    
    Lead L7 = new Lead();
    L7.FirstName = 'Fank2';
    L7.LastName = 'Bates2';
    L7.Company = 'P2';
    L7.Region__c = 'Central';
    L7.Tier__c = 'A';
    L7.Industry = 'Market Research';
    L7.AnnualRevenue = 31;
    newLeads.add(L7);
    
    Lead L8 = new Lead();
    L8.FirstName = 'Fank3';
    L8.LastName = 'Bates3';
    L8.Company = 'P3';
    L8.Region__c = 'EMEA';
    L8.Tier__c = 'A';
    L8.Industry = 'Market Research';
    L8.AnnualRevenue = 31;
    newLeads.add(L8);
    
    Lead L9 = new Lead();
    L9.FirstName = 'Fank4';
    L9.LastName = 'Bates4';
    L9.Company = 'P4';
    L9.Region__c = 'Great Lakes';
    L9.Tier__c = 'A';
    L9.Industry = 'Market Research';
    L9.AnnualRevenue = 31;
    newLeads.add(L9);
    
    
    Lead L10 = new Lead();
    L10.FirstName = 'Fank12';
    L10.LastName = 'Bates5';
    L10.Company = 'P5';
    L10.Region__c = 'Northeast';
    L10.Tier__c = 'A';
    L10.Industry = 'Market Research';
    L10.AnnualRevenue = 31;
    newLeads.add(L10);
    
    Lead L11 = new Lead();
    L11.FirstName = 'Fank6';
    L11.LastName = 'Bates4';
    L11.Company = 'P4';
    L11.Region__c = 'Southeast';
    L11.Tier__c = 'A';
    L11.Industry = 'Market Research';
    L11.AnnualRevenue = 31;
    newLeads.add(L11);
    
    Lead L12 = new Lead();
    L12.FirstName = 'Fank7';
    L12.LastName = 'Bates4';
    L12.Company = 'P4';
    L12.Region__c = 'West';
    L12.Tier__c = 'A';
    L12.Industry = 'Market Research';
    L12.AnnualRevenue = 31;
    newLeads.add(L12);
    
    Lead L13 = new Lead();
    L13.FirstName = 'Fank8';
    L13.LastName = 'Bates4';
    L13.Company = 'P4';
    L13.Region__c = 'Canada';
    L13.Tier__c = 'B';
    L13.Industry = 'Market Research';
    L13.AnnualRevenue = 29;
    newLeads.add(L13);
    
    Lead L14 = new Lead();
    L14.FirstName = 'Fank9';
    L14.LastName = 'Bates4';
    L14.Company = 'P4';
    L14.Region__c = 'Central';
    L14.Tier__c = 'B';
    L14.Industry = 'Market Research';
    L14.AnnualRevenue = 29;
    newLeads.add(L14);
    
    Lead L15 = new Lead();
    L15.FirstName = 'Fank10';
    L15.LastName = 'Bates4';
    L15.Company = 'P4';
    L15.Region__c = 'EMEA';
    L15.Tier__c = 'B';
    L15.Industry = 'Market Research';
    L15.AnnualRevenue = 29;
    newLeads.add(L15);
    
    Lead L16 = new Lead();
    L16.FirstName = 'Fank13';
    L16.LastName = 'Bates4';
    L16.Company = 'P4';
    L16.Region__c = 'Great Lakes';
    L16.Tier__c = 'B';
    L16.Industry = 'Market Research';
    L16.AnnualRevenue = 29;
    newLeads.add(L16);
    
    Lead L17 = new Lead();
    L17.FirstName = 'Fank14';
    L17.LastName = 'Bates4';
    L17.Company = 'P4';
    L17.Region__c = 'Northeast';
    L17.Tier__c = 'B';
    L17.Industry = 'Market Research';
    L17.AnnualRevenue = 29;
    newLeads.add(L17);
    
    Lead L18 = new Lead();
    L18.FirstName = 'Fank15';
    L18.LastName = 'Bates4';
    L18.Company = 'P4';
    L18.Region__c = 'Southeast';
    L18.Tier__c = 'B';
    L18.Industry = 'Market Research';
    L18.AnnualRevenue = 29;
    newLeads.add(L18);
    
    Lead L19 = new Lead();
    L19.FirstName = 'Fank16';
    L19.LastName = 'Bates4';
    L19.Company = 'P4';
    L19.Region__c = 'West';
    L19.Tier__c = 'B';
    L19.Industry = 'Market Research';
    L19.AnnualRevenue = 29;
    newLeads.add(L19);
    
    Lead L20 = new Lead();
    L20.FirstName = 'Fank17';
    L20.LastName = 'Bates4';
    L20.Company = 'P4';
    L20.Region__c = 'Canada';
    L20.Tier__c = 'C';
    L20.Industry = 'Market Research';
    L20.AnnualRevenue = 1;
    newLeads.add(L20);
    
    Lead L21 = new Lead();
    L21.FirstName = 'Fank18';
    L21.LastName = 'Bates4';
    L21.Company = 'P4';
    L21.Region__c = 'Central';
    L21.Tier__c = 'C';
    L21.Industry = 'Market Research';
    L21.AnnualRevenue = 1;
    newLeads.add(L21);
    
    Lead L22 = new Lead();
    L22.FirstName = 'Fank19';
    L22.LastName = 'Bates4';
    L22.Company = 'P4';
    L22.Region__c = 'EMEA';
    L22.Tier__c = 'C';
    L22.Industry = 'Market Research';
    L22.AnnualRevenue = 1;
    newLeads.add(L22);
    
    Lead L23 = new Lead();
    L23.FirstName = 'Fank20';
    L23.LastName = 'Bates4';
    L23.Company = 'P4';
    L23.Region__c = 'Great Lakes';
    L23.Tier__c = 'C';
    L23.Industry = 'Market Research';
    L23.AnnualRevenue = 1;
    newLeads.add(L23);
    
    Lead L24 = new Lead();
    L24.FirstName = 'Fank21';
    L24.LastName = 'Bates4';
    L24.Company = 'P4';
    L24.Region__c = 'Northeast';
    L24.Tier__c = 'C';
    L24.Industry = 'Market Research';
    L24.AnnualRevenue = 1;
    newLeads.add(L24);
    
    Lead L25 = new Lead();
    L25.FirstName = 'Fank22';
    L25.LastName = 'Bates41';
    L25.Company = 'P4';
    L25.Region__c = 'Southeast';
    L25.Tier__c = 'C';
    L25.Industry = 'Market Research';
    L25.AnnualRevenue = 1;
    newLeads.add(L25);
    
    
    insert newLeads;
    
    for(Lead F: NewLeads){
        ApexPages.StandardController X = new ApexPages.StandardController(F);    
        LeadAllocationCalendar LLAC = new LeadAllocationCalendar(X);
        LLAC.UserAllocation();
    }
    
    /*
    
    ApexPages.StandardController X = new ApexPages.StandardController(L);    
    LeadAllocationCalendar LAC = new LeadAllocationCalendar(X);
    LAC.UserAllocation();
    System.debug('This is LAC  ' + LAC);
    
    ApexPages.StandardController X2 = new ApexPages.StandardController(L2);    
    LeadAllocationCalendar LAC2 = new LeadAllocationCalendar(X2);
    LAC2.UserAllocation();
    
    ApexPages.StandardController X3 = new ApexPages.StandardController(L3);    
    LeadAllocationCalendar LAC3 = new LeadAllocationCalendar(X3);
    LAC3.UserAllocation();
    
    ApexPages.StandardController X4 = new ApexPages.StandardController(L4);    
    LeadAllocationCalendar LAC4 = new LeadAllocationCalendar(X4);
    LAC4.UserAllocation();
  
    ApexPages.StandardController X5 = new ApexPages.StandardController(L5);    
    LeadAllocationCalendar LAC5 = new LeadAllocationCalendar(X5);
    LAC5.UserAllocation();



    Test.StartTest();
    
    u.Region__c = 'Canada';
    L.Region__c = 'Canada';
    update u;
    update L;
    
    ApexPages.StandardController Y = new ApexPages.StandardController(L);    
    LeadAllocationCalendar YAC = new LeadAllocationCalendar(Y);
    YAC.UserAllocation();

    u.Region__c = 'Central';
    L.Region__c = 'Central';
    update u;
    update L;
    
    ApexPages.StandardController Y2 = new ApexPages.StandardController(L);    
    LeadAllocationCalendar YAC2 = new LeadAllocationCalendar(Y2);
    YAC2.UserAllocation();

    u.Region__c = 'EMEA';
    L.Region__c = 'EMEA';
    update u;
    update L;
    
    ApexPages.StandardController Y3 = new ApexPages.StandardController(L);    
    LeadAllocationCalendar YAC3 = new LeadAllocationCalendar(Y3);
    YAC3.UserAllocation();

    u.Region__c = 'Great Lakes';
    L.Region__c = 'Great Lakes';
    update u;
    update L;
    
    ApexPages.StandardController Y4 = new ApexPages.StandardController(L);    
    LeadAllocationCalendar YAC4 = new LeadAllocationCalendar(Y4);
    YAC4.UserAllocation();
   
    u.Region__c = 'Northeast';
    L.Region__c = 'Northeast';
    update u;
    update L;
    
    ApexPages.StandardController Y5 = new ApexPages.StandardController(L);    
    LeadAllocationCalendar YAC5 = new LeadAllocationCalendar(Y5);
    YAC5.UserAllocation();

    u.Region__c = 'Southeast';
    L.Region__c = 'Southeast';
    update u;
    update L;
    
    ApexPages.StandardController Y6 = new ApexPages.StandardController(L);    
    LeadAllocationCalendar YAC6 = new LeadAllocationCalendar(Y6);
    YAC6.UserAllocation();
    
    u.Region__c = 'West';
    L2.Region__c = 'West';
    update u;
    update L2;
    
    ApexPages.StandardController Y7 = new ApexPages.StandardController(L2);    
    LeadAllocationCalendar YAC7 = new LeadAllocationCalendar(Y7);
    YAC7.UserAllocation();
    
    u.Region__c = 'Canada';
    L2.Region__c = 'Canada';
    update u;
    update L2;
    
    ApexPages.StandardController A = new ApexPages.StandardController(L2);    
    LeadAllocationCalendar AAC = new LeadAllocationCalendar(A);
    AAC.UserAllocation();

    u.Region__c = 'Central';
    L2.Region__c = 'Central';
    update u;
    update L2;
    
    ApexPages.StandardController A2 = new ApexPages.StandardController(L2);    
    LeadAllocationCalendar AAC2 = new LeadAllocationCalendar(A2);
    AAC2.UserAllocation();

    u.Region__c = 'EMEA';
    L2.Region__c = 'EMEA';
    update u;
    update L2;
    
    ApexPages.StandardController A3 = new ApexPages.StandardController(L2);    
    LeadAllocationCalendar AAC3 = new LeadAllocationCalendar(A3);
    AAC3.UserAllocation();

    u.Region__c = 'Great Lakes';
    L2.Region__c = 'Great Lakes';
    update u;
    update L2;
    
    ApexPages.StandardController A4 = new ApexPages.StandardController(L2);    
    LeadAllocationCalendar AAC4 = new LeadAllocationCalendar(A4);
    AAC4.UserAllocation();
   
    u.Region__c = 'Northeast';
    L2.Region__c = 'Northeast';
    update u;
    update L2;
    
    ApexPages.StandardController A5 = new ApexPages.StandardController(L2);    
    LeadAllocationCalendar AAC5 = new LeadAllocationCalendar(A5);
    AAC5.UserAllocation();

    u.Region__c = 'Southeast';
    L2.Region__c = 'Southeast';
    update u;
    update L2;
    
    ApexPages.StandardController A6 = new ApexPages.StandardController(L2);    
    LeadAllocationCalendar AAC6 = new LeadAllocationCalendar(A6);
    AAC6.UserAllocation();

     Test.StopTest();
    */

    /*
     Test.StartTest();
     
     u.Region__c = 'West';
    L3.Region__c = 'West';
    update u;
    update L3;
    
    ApexPages.StandardController B7 = new ApexPages.StandardController(L3);    
    LeadAllocationCalendar BAC7 = new LeadAllocationCalendar(B7);
    BAC7.UserAllocation();
    
    u.Region__c = 'Canada';
    L3.Region__c = 'Canada';
    update u;
    update L3;
    
    ApexPages.StandardController B = new ApexPages.StandardController(L3);    
    LeadAllocationCalendar BAC = new LeadAllocationCalendar(B);
    BAC.UserAllocation();

    u.Region__c = 'Central';
    L3.Region__c = 'Central';
    update u;
    update L3;
    
    ApexPages.StandardController B2 = new ApexPages.StandardController(L3);    
    LeadAllocationCalendar BAC2 = new LeadAllocationCalendar(B2);
    BAC2.UserAllocation();

    u.Region__c = 'EMEA';
    L3.Region__c = 'EMEA';
    update u;
    update L3;
    
    ApexPages.StandardController B3 = new ApexPages.StandardController(L3);    
    LeadAllocationCalendar BAC3 = new LeadAllocationCalendar(B3);
    BAC3.UserAllocation();

    u.Region__c = 'Great Lakes';
    L3.Region__c = 'Great Lakes';
    update u;
    update L3;
    
    ApexPages.StandardController B4 = new ApexPages.StandardController(L3);    
    LeadAllocationCalendar BAC4 = new LeadAllocationCalendar(B4);
    BAC4.UserAllocation();
   
    u.Region__c = 'Northeast';
    L3.Region__c = 'Northeast';
    update u;
    update L3;
    
    ApexPages.StandardController B5 = new ApexPages.StandardController(L3);    
    LeadAllocationCalendar BAC5 = new LeadAllocationCalendar(B5);
    BAC5.UserAllocation();

    u.Region__c = 'Southeast';
    L3.Region__c = 'Southeast';
    update u;
    update L3;
    
    ApexPages.StandardController B6 = new ApexPages.StandardController(L3);    
    LeadAllocationCalendar BAC6 = new LeadAllocationCalendar(B6);
    BAC6.UserAllocation();
     
     
     Test.StopTest();
     
     Test.StartTest();
     
     u.Region__c = 'West';
    L4.Region__c = 'West';
    update u;
    update L4;
    
    ApexPages.StandardController C7 = new ApexPages.StandardController(L4);    
    LeadAllocationCalendar CAC7 = new LeadAllocationCalendar(C7);
    CAC7.UserAllocation();
    
    u.Region__c = 'Canada';
    L4.Region__c = 'Canada';
    update u;
    update L4;
    
    ApexPages.StandardController C = new ApexPages.StandardController(L4);    
    LeadAllocationCalendar CAC = new LeadAllocationCalendar(C);
    CAC.UserAllocation();

    u.Region__c = 'Central';
    L4.Region__c = 'Central';
    update u;
    update L4;
    
    ApexPages.StandardController C2 = new ApexPages.StandardController(L4);    
    LeadAllocationCalendar CAC2 = new LeadAllocationCalendar(C2);
    CAC2.UserAllocation();

    u.Region__c = 'EMEA';
    L4.Region__c = 'EMEA';
    update u;
    update L4;
    
    ApexPages.StandardController C3 = new ApexPages.StandardController(L4);    
    LeadAllocationCalendar CAC3 = new LeadAllocationCalendar(C3);
    CAC3.UserAllocation();

    u.Region__c = 'Great Lakes';
    L4.Region__c = 'Great Lakes';
    update u;
    update L4;
    
    ApexPages.StandardController C4 = new ApexPages.StandardController(L4);    
    LeadAllocationCalendar CAC4 = new LeadAllocationCalendar(C4);
    CAC4.UserAllocation();
   
    u.Region__c = 'Northeast';
    L4.Region__c = 'Northeast';
    update u;
    update L4;
    
    ApexPages.StandardController C5 = new ApexPages.StandardController(L4);    
    LeadAllocationCalendar CAC5 = new LeadAllocationCalendar(C5);
    CAC5.UserAllocation();

    u.Region__c = 'Southeast';
    L4.Region__c = 'Southeast';
    update u;
    update L4;
    
    ApexPages.StandardController C6 = new ApexPages.StandardController(L4);    
    LeadAllocationCalendar CAC6 = new LeadAllocationCalendar(C6);
    CAC6.UserAllocation();
     
     Test.StopTest();
    */
    }
    
}