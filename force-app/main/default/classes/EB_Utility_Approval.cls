/**
 * Provides methods for working with Approvals
 */
public with sharing class EB_Utility_Approval {

/*	public static Approval.ProcessResult submitForApproval(Id targetObjectId, string comment)
    {
        // Create an approval request
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments(comment);
        req1.setObjectId(targetObjectId);

        // Submit the approval request
        Approval.ProcessResult result = Approval.process(req1);
        return result;
    }*/

public static boolean Reject(Id targetObjectId, string comment){
    	return SetApproval(getWorkItemId(targetObjectId), 'Reject', comment);
    }
    public static boolean Approve(Id targetObjectId, string comment){
    	return SetApproval(getWorkItemId(targetObjectId), 'Approve', comment);
    }

    

    private static boolean SetApproval(ID approvalID, String status, String comment) {
        try {
            // Instantiate the new ProcessWorkitemRequest object and populate it
            Approval.ProcessWorkitemRequest req2 =
                new Approval.ProcessWorkitemRequest();
            req2.setComments(comment);
            req2.setAction(status);
            req2.setWorkitemId(approvalID);

            // Submit the request for approval
            Approval.ProcessResult result2 =  Approval.process(req2);
            return true;
        } catch (Exception e) {
            System.debug(e.getMessage());
            return false;
        }
    }

    private static Id getWorkItemId(Id targetObjectId) {
        Id retVal = null;

        for (ProcessInstanceWorkitem workItem  : [Select p.Id from ProcessInstanceWorkitem p
                where p.ProcessInstance.TargetObjectId = : targetObjectId Limit 1]) {
            retVal  =  workItem.Id;
        }
        return retVal;
    }
}