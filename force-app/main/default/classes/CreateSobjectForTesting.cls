@isTest
public with sharing class CreateSobjectForTesting {
	public CreateSobjectForTesting() {}

	public sObject CreateObject(string sObjectName){
		sObject sObj = Schema.getGlobalDescribe().get(sObjectName).newSObject() ;
		Map<String, Schema.SObjectField> SobjtField = Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap();
		for (string s : SobjtField.keySet()) {
			Schema.DescribeFieldResult field = SobjtField.get(s).getDescribe();
			if(field.isAccessible() == true && field.isCreateable() == true){
				if(field.getSoapType() == Schema.SoapType.String){
					if(field.getType() == Schema.DisplayType.Picklist || field.getType() == Schema.DisplayType.multipicklist){
						sObj.put(s,SobjtField.get(s).getDescribe().getPickListValues().get(0).getValue());
					} else if(field.getType() == Schema.DisplayType.Email){
						sObj.put(s,'test@eidebailly.com');
					}
					 else {
					 	if(field.getLength() < 4){
					 		sObj.put(s,'a');
					 	}
						else{
							sObj.put(s,'abcd');
						}
					}
				} else if(field.getSoapType() == Schema.SoapType.Boolean){
					sObj.put(s,True);
				} else if(field.getSoapType() == Schema.SoapType.Date){
					sObj.put(s,Date.today());
				} else if(field.getSoapType() == Schema.SoapType.DateTime){
					sObj.put(s,DateTime.now());
				} else if(field.getSoapType() == Schema.SoapType.Double){
					sObj.put(s,1.2);
				} else if(field.getSoapType() == Schema.SoapType.Integer){
					sObj.put(s,3);
				}
			}
		}
		return sObj;
	}
}