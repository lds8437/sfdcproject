/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class QualtricsMavenlinkCustomJobTest {
	
	public static Account sAcc = null; public static Product2 sProd = null; public static Product2 sProd2 = null; public static PriceBook2 sCustPB = null;public static Service_Bundle__c cxSB = null;public static QuoteLineItem sQLI = null;
    public static PriceBookEntry sStdPBE = null; public static PriceBookEntry sStdPBE2 = null; public static PriceBookEntry sCustPBE = null; public static PriceBookEntry sCustPBE2 = null; public static Opportunity sOpp = null;
    public static QService__c sService = null; public static mavenlink__Mavenlink_Project__c sProject = null; public static mavenlink__Mavenlink_Task__c sTask = null;public static Quote sQuote = null; public static OpportunityLineItem sOLI = null;
    
    @isTest public static void seedConfig(){
        mavenlink__PackageConfiguration__c config = new mavenlink__PackageConfiguration__c(
            name = 'default_config',
            mavenlink__Opportunity_Stage_Values__c = 'test Stage'
        );
        insert config;
    }

    @isTest public static Account seedAccount(){
        Account seedAcc = new Account(
                name='Test Account',
                BillingCity='Boston');
        insert seedAcc;
    return seedAcc;        
    }
    
    @isTest public static Product2 seedProduct1(){
        Product2 seedProduct = new Product2(name='Product 1',productCode='XX1234', isActive=true,Display_Order__c=1);
        insert seedProduct;
        return seedProduct;
    }

    @isTest public static Product2 seedProduct2(){
        Product2 seedProduct = new Product2(name='Product 2',productCode='XX9876', isActive=true,Display_Order__c=1);
        insert seedProduct;
        return seedProduct;
    }

    @isTest public static PriceBook2 seedPriceBook2(){
        PriceBook2 customPriceBook = new PriceBook2(Name='Custom Pricebook',IsActive=true);
        insert customPriceBook;
        return customPriceBook;
    }
    
    @isTest public static PriceBookEntry seedStdPriceBookEntry1(){
        PricebookEntry pbe = null;
        try{
            pbe = new PriceBookEntry(
                Product2Id=sProd.id,
                Pricebook2Id=Test.getStandardPricebookId(),
                UnitPrice = 300000.00,
                UseStandardPrice = false,
                IsActive=true
            );
            insert pbe;
        }catch(Exception e){}
        return pbe;
    }   
    
    @isTest public static PriceBookEntry seedStdPriceBookEntry2(){
        PricebookEntry pbe = null;
        try{
            pbe = new PriceBookEntry(
                Product2Id=sProd2.id,
                Pricebook2Id=Test.getStandardPricebookId(),
                UnitPrice = 400000.00,
                UseStandardPrice = false,
                IsActive=true
            );
            insert pbe;
        }catch(Exception e){}
        return pbe;
    }   
    
    @isTest public static PriceBookEntry seedPriceBookEntry1(){
        PricebookEntry pbe = null;
        try{
            pbe = new PriceBookEntry(
                Product2Id=sProd.id,
                Pricebook2Id=sCustPB.id,
                UnitPrice = 300000.00,
                UseStandardPrice = false,
                IsActive=true
            );
            insert pbe;
        }catch(Exception e){}
        return pbe;
    }   
    
    @isTest public static PriceBookEntry seedPriceBookEntry2(){
        PricebookEntry pbe = null;
        try{
            pbe = new PriceBookEntry(
                Product2Id=sProd2.id,
                Pricebook2Id=sCustPB.id,
                UnitPrice = 400000.00,
                UseStandardPrice = false,
                IsActive=true
            );
            insert pbe;
        }catch(Exception e){}
        return pbe;
    }   

    @isTest public static Opportunity seedOpportunity(){
        Opportunity seedOpp = null;
        try{
            seedOpp = new Opportunity(
                name = 'Test Opportunity',
                AccountId = sAcc.id,
                stageName = 'Commit',
                Amount = 700000.00,
                closeDate = Date.today(),
                Forecast_Percentage_NEW__c = 60.0,
                pricebook2Id=sCustPB.id,
                Change_Order_Source_Opportunity__c=null,
                Turn_Off_Initial_CX_EX_RC_Amounts__c = true
            );
            insert seedOpp;
        }catch(Exception e){System.debug(e.getMessage());}
    return seedOpp;        
    }
    
    @isTest public static QService__c seedQService(){
        QService__c seedService = null;
        try{
            seedService = new QService__c(
                name = 'Test Service',
                //Quote__c = sQuote.id,
                Opportunity__c = sOpp.id,
                //Quote_Line_Item__c = sQLI.id,
                Status__c = 'SOW Requested',//'SOW Requested','SOW Created','SOW out for Signature','SOW Signed','SOW in QSO' - true; 'Estimate Requested'-false
                //Package__c=cxSB.id,
                Service_Type__c = 'Advanced Analytics (Conjoint)',
                Duration__c = 1.0
            );
            insert seedService;
        }catch(Exception e){}
    return seedService;        
    }
    
    @isTest public static mavenlink__Mavenlink_Project__c seedOppProject(){
        mavenlink__Mavenlink_Project__c oppProject = null;
        try{
            oppProject = new mavenlink__Mavenlink_Project__c(
                Name = 'Test Opp Project',
                mavenlink__Project_Title__c = 'Test Opp Project',
                mavenlink__Account__c = sAcc.id,
                mavenlink__Opportunity__c = sOpp.Id,
                //mavenlink__Related_OLI__c = sOLI.Id,
                mavenlink__Automatically_Created__c = true,
                mavenlink__Project_Type__c = 'Execution',
                mavenlink__Object_Type__c = 'Opportunity',
                mavenlink__Mavenlink_Id__c = '20014595');
            insert oppProject;
        }catch(Exception e){}
    return oppProject;        
    }
    
    @isTest public static mavenlink__Mavenlink_Task__c seedOLITask(){
        mavenlink__Mavenlink_Task__c oliTask = null;
        try{
            oliTask = new mavenlink__Mavenlink_Task__c(
                Name = 'Main Deliverable',
                mavenlink__Task_Mavenlink_Id__c = '317620595',
                mavenlink__Mavenlink_Project__c = sProject.id,
                mavenlink__Task_Hours__c = 100,
                mavenlink__Task_Budget_Amount__c = 10000,
                mavenlink__Task_Budget_Used_Amount__c = 0,
                mavenlink__Task_Type_Selection__c = 'DELIVERABLE',
                mavenlink__Task_Status__c = 'not started',
                mavenlink__Task_Complete__c = 0,
                mavenlink__Task_Bill_Type__c = 'Fixed Fee',
                mavenlink__Task_Weight__c = 1.0,
                mavenlink__Task_Target_Date__c = date.newinstance(2018,12,1),
                mavenlink__Task_Parent_Project__c = '20014595',
                mavenlink__Task_Parent_Project_Name__c = 'Test Opp Project',
                mavenlink__Task_Tags__c = 'Engineer,Consultant', 
                Service__c = sService.id, 
                //mavenlink__Related_OLI__c = String.valueOf(sOLI.id), 
                Service_Name__c='Test Service', 
                Service_Package__c='CX1');
            insert oliTask;
        }catch(Exception e){}
    return oliTask;        
    }
    
    @isTest public static void testQualtricsMavenlinkWSAllocBatch(){
    		TestFactory.disableTriggers();
        sAcc = seedAccount();
        sCustPB = seedPriceBook2();
        sOpp = seedOpportunity();
    		sProd = seedProduct1();
        sProd2 = seedProduct2();
        sStdPBE = seedStdPriceBookEntry1();
        sStdPBE2 = seedStdPriceBookEntry2();
        sCustPBE = seedPriceBookEntry1();
        sCustPBE2 = seedPriceBookEntry2();
        sService = seedQService();
        sProject = seedOppProject();
        sTask = seedOLITask();
        
        System.debug('sOpp.Id='+sOpp.Id);
        System.debug('sProject.mavenlink__Opportunity__c='+sProject.mavenlink__Opportunity__c);
        System.assertEquals(sOpp.Id,sProject.mavenlink__Opportunity__c,'Project Opportunity Correct');
        
        Set<String> totalProcessedWorkspaceIds = new Set<String>();
        Integer totalProjects = 1;
        Integer page = 1;
        Set<String> workspaceIds = new Set<String>();
        
        
        String testWSA = '{'+
    							'"count": 1,'+
    							'"results": ['+
    								'{"key": "workspace_allocations","id": "16783325"}'+
    							'],'+
    							'"workspace_allocations": {'+
    								'"16783325": {'+
    									'"start_date": "2018-06-26",'+
    									'"workspace_resource_id": "119281205",'+
    									'"created_at": "2018-07-24T13:19:05-07:00",'+
            							'"updated_at": "2018-08-10T18:50:25-07:00",'+
    									'"id": "16783325"'+
								'}'+
							'},'+
							'"workspace_resources": {'+
								'"119281205": {'+
									'"workspace_id": 20014595,'+
									'"role_name": "PS.Something"'+
								'}'+
							'}'+
						'}';
        Map<String,Object> r = (Map<String,Object>) JSON.deserializeUntyped(testWSA);
        Map<String,Object> workspaceAllocations = (Map<String,Object>)r.get('workspace_allocations');
        Map<String,Object> workspaceResources = (Map<String,Object>)r.get('workspace_resources');
        Map<String,String> resourceWorkspaceMap = new Map<String,String>();
        Map<String,String> allocationResourceMap = new Map<String,String>();
        
        System.debug('workspaceAllocations='+workspaceAllocations.size());
        System.debug('workspaceResources='+workspaceResources.size());
        
        for (String key:workspaceResources.keySet()){
            String workspaceId = String.valueOf((((Map<String,Object>)workspaceResources.get(key)).get('workspace_id')));
            workspaceIds.add(workspaceId);
            resourceWorkspaceMap.put(key,workspaceId);
	    }
        for (String key:workspaceAllocations.keySet()){
            String workspaceResourceId = String.valueOf((((Map<String,Object>)workspaceAllocations.get(key)).get('workspace_resource_id')));
            allocationResourceMap.put(key,workspaceResourceId);
        }
    
        
        QualtricsMavenlinkWSAllocBatch obj = new QualtricsMavenlinkWSAllocBatch(totalProcessedWorkspaceIds,totalProjects,page,workspaceIds,
                    workspaceAllocations,workspaceResources,resourceWorkspaceMap,allocationResourceMap);
        DataBase.executeBatch(obj);
    }
	
	
    @isTest public static void testQualtricsMavenlinkWSAMasterBatch() {
    	
    	Test.startTest();
        QualtricsMavenlinkWSAMasterBatch obj = new QualtricsMavenlinkWSAMasterBatch('2018-08-10T13-47-00');
        DataBase.executeBatch(obj);
    Test.stopTest();
        
    }
    
   
    @isTest public static void testQualtricsMavenlinkWSAllocSchedulable(){
    		//seedTestSchedule();
    		
    		
    		//QualtricsMavenlinkSchedulable qsch = new QualtricsMavenlinkSchedulable(sOpp.Id);
    		
		DateTime currentDateTime = DateTime.now();
			DateTime futureDateTime = currentDateTime.addSeconds(QualtricsMavenlinkSchedulable.OPPORTUNITY_UPDATE_DELAY);
			String cron = futureDateTime.second()+' '+futureDateTime.minute()+' '+futureDateTime.hour()+' '+futureDateTime.day()+' '+futureDateTime.month()+' ? '+futureDateTime.year();

			/*System.schedule('QualtricsMavenlinkSchedulable '+sOpp.Id+' '+DateTime.now().format('yyyy.MM.dd.HH.mm.ss.SSS'),
				cron,new QualtricsMavenlinkSchedulable(sOpp.Id));*/
		Test.startTest();
			System.schedule('Test',
					cron,new QualtricsMavenlinkWSAllocSchedulable());
		Test.stopTest();
    }
    
    @isTest public static void testQualtricsMavenlinkCustomWSAllocScheduler(){
    		Test.startTest();
    		QualtricsMavenlinkCustomWSAllocScheduler.executeSync();
    		DateTime currentDateTime = DateTime.now();
    		Date startingDay = Date.today();
    		String cron = QualtricsMavenlinkCustomWSAllocScheduler.getCronExpression(String.valueOf(currentDateTime.hour()),String.valueOf(currentDateTime.minute()+1),'monthly',startingDay,'');
    		 cron = QualtricsMavenlinkCustomWSAllocScheduler.getCronExpression(String.valueOf(currentDateTime.hour()),String.valueOf(currentDateTime.minute()+1),'weekly',startingDay,'');
    		 cron = QualtricsMavenlinkCustomWSAllocScheduler.getCronExpression(String.valueOf(currentDateTime.hour()),String.valueOf(currentDateTime.minute()+1),'daily',startingDay,'');
    		 cron = QualtricsMavenlinkCustomWSAllocScheduler.getCronExpression(String.valueOf(currentDateTime.hour()),String.valueOf(currentDateTime.minute()+1),'twiceDaily',startingDay,'');
    		 cron = QualtricsMavenlinkCustomWSAllocScheduler.getCronExpression(String.valueOf(currentDateTime.hour()),String.valueOf(currentDateTime.minute()+1),'threeDaily',startingDay,'');
    		Test.stopTest();
    }
}