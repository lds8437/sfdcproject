@IsTest
public class SyncQuoteExtensionTests {

    testMethod static void testExtension() {
        
        // Prepare for tests
        Account testAccount = new Account();
        testAccount.Name='Test Account';
        insert testAccount;
        
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name='Test Opportunity';
        testOpportunity.AccountId=testAccount.Id;
        testOpportunity.StageName='Pending';
        testOpportunity.CloseDate=System.today();
        testOpportunity.Turn_Off_Initial_CX_EX_RC_Amounts__c=true;
        testOpportunity.Prior_Owner_Id__c = UserInfo.getUserId();
        insert testOpportunity;
        
        Quote testQuote = new Quote();
        testQuote.Name = 'Test Quote';
        testQuote.OpportunityId = testOpportunity.Id;
        insert testQuote;
        
		Test.setCurrentPage(new PageReference('/apex/SyncQuote'));
        ApexPages.StandardController controller;
        SyncQuoteExtension extension;
        
        Test.startTest();
        
        // Failing tests
        controller = new ApexPages.StandardController(testOpportunity);
        
        ApexPages.currentPage().getParameters().put('action', null);
        extension = new SyncQuoteExtension(controller);
        extension.execute();
        
        ApexPages.currentPage().getParameters().put('action', 'sync');
        extension = new SyncQuoteExtension(controller);
        extension.execute();
        
        ApexPages.currentPage().getParameters().put('action', 'unsync');
        extension = new SyncQuoteExtension(controller);
        extension.execute();
        
        // Passing tests
        controller = new ApexPages.StandardController(testQuote);

        ApexPages.currentPage().getParameters().put('action', null);
		extension = new SyncQuoteExtension(controller);
        extension.execute();
        
        ApexPages.currentPage().getParameters().put('action', 'sync');
		extension = new SyncQuoteExtension(controller);
        extension.execute();
        
        ApexPages.currentPage().getParameters().put('action', 'unsync');
		extension = new SyncQuoteExtension(controller);
        extension.execute();
        
        Test.stopTest();
    }
}