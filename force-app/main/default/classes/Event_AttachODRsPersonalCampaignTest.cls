@isTest
private class Event_AttachODRsPersonalCampaignTest {

    static testMethod void Event_AttachODRsPersonalCampaignTest1() {
        Test.startTest();
        String X = Label.Profile_Name;
        
        //Id profileId = [Select Name, Id From Profile where Name =: Label.Profile_Name].Id;
        Id profileId = [Select Name, Id From Profile where Name Like '%OptDev%' Limit 1].Id;
        system.debug(LoggingLevel.DEBUG,'profileId= '+ ProfileId );
        
        String ran = String.valueOf(Math.abs(Crypto.getRandomInteger())).left(3);
        
        //create User of Q-OptDev Rep Profile
        User user = new User();
        user.FirstName = 'TestFirstName';
        user.LastName = 'TestLastName';
        user.Username = 'test1' + ran + '@test' + ran + '.com';
        user.Email = 'test' + ran + '@test' + ran + '.com';
        user.CommunityNickname = 'TestNickName' + ran;
        user.alias = 'test' + ran;
        user.emailencodingkey='UTF-8';
        user.languagelocalekey='en_US';
        user.localesidkey='en_US';
        user.ProfileId = profileId;
        user.timezonesidkey='America/Los_Angeles';
        insert user;
        
        system.debug(LoggingLevel.DEBUG,'User Created' );
        
        system.runAs(user)
        {
            //create Campaign
            Campaign campaign = new Campaign();
            campaign.Name = 'Test campaign';
            insert campaign;
            system.debug(LoggingLevel.DEBUG,'Campaign Created' );
            
            //create Contact
            Contact contact = new Contact();
            contact.LastName = 'Test contact';
             
            contact.MailingState='Utah';
            contact.MailingStreet='123 Main Street';
            contact.MailingCountry='USA';
            contact.MailingPostalCode='84602';
            insert contact;
            system.debug(LoggingLevel.DEBUG,'Contact Created' );
            
            Lead L = new Lead();
            L.FirstName = 'Tommy';
            L.LastName = 'Guido';
            L.Company = 'Godfather';
            insert L;
            system.debug(LoggingLevel.DEBUG,'Lead Created' );
            
            //create Task
            
            Event E = new Event();
            E.Subject = 'Test';
            //E.ActivityDate = Date.Today();
            E.ActivityDateTime = DateTime.Now();
            E.DurationInMinutes = 100;
            E.WhoID = Contact.ID;
            insert E;
            system.debug(LoggingLevel.DEBUG,'Event 1 Created' );
            
            DateTime DT = DateTime.Now();
            Event E2 = new Event();
            E2.Subject = 'Test';
            //E.ActivityDate = Date.Today();
            E2.ActivityDateTime = DT.addhours(5);
            E2.DurationInMinutes = 100;
            E2.WhoID = L.ID;
            insert E2;
            system.debug(LoggingLevel.DEBUG,'Event 2 Created' );
        }
        Test.stopTest();
    }
}