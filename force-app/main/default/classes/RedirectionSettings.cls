public with sharing class RedirectionSettings {

    /**
     * This class takes a request and returns the
     * configuration that matches the details of
     * the request.
     */
    private RedirectionRequest request;
    private Redirection_Setting__mdt settings;
    
    /**
     * Class Constructor
     * 
     * Only the request is required to instantiate this class.
     */
    public RedirectionSettings(RedirectionRequest request) {
        this.request = request;
        init();
    }
    
    /**
     * During initialization only the settings variable is
     * populated.
     */
    private void init() {
        initSettings();
    }
    
    /**
     * If the proper permission exist and an exact match
     * of the request is found in the configurations the
     * settings variable is populated.
     */
    private void initSettings() {
       
        // Exit immediately if the current user does not have
        // the proper permissions.
        if (!Schema.SObjectType.Redirection_Setting__mdt.isQueryable()
	            || !Schema.SObjectType.Redirection_Setting__mdt.fields.Base_URL__c.isAccessible()
	            || !Schema.SObjectType.Redirection_Setting__mdt.fields.Debug_Mode__c.isAccessible()
	            || !Schema.SObjectType.Redirection_Setting__mdt.fields.Redirect_Entire_Window__c.isAccessible()
	            || !Schema.SObjectType.Redirection_Setting__mdt.fields.SObject_Name__c.isAccessible()
	            || !Schema.SObjectType.Redirection_Setting__mdt.fields.Action__c.isAccessible()
           		|| !Schema.SObjectType.Redirection_Field__mdt.isQueryable()
	            || !Schema.SObjectType.Redirection_Field__mdt.fields.Field_Name__c.isAccessible()
	            || !Schema.SObjectType.Redirection_Field__mdt.fields.Property_Name__c.isAccessible()
	            || !Schema.SObjectType.Redirection_Field__mdt.fields.Type__c.isAccessible()
	            || !Schema.SObjectType.Redirection_Field__mdt.fields.Comparison_Operator__c.isAccessible()
	            || !Schema.SObjectType.Redirection_Field__mdt.fields.Field_Value__c.isAccessible()
	            || !Schema.SObjectType.Redirection_Field__mdt.fields.Error_Message__c.isAccessible()
	            || !Schema.SObjectType.Redirection_Field__mdt.fields.Value_Prefix__c.isAccessible()
	            || !Schema.SObjectType.Redirection_Field__mdt.fields.Value_Suffix__c.isAccessible()) {
            throw new InvalidPermissionsException(InvalidPermissionsException.REDIRECTION_ACCESS_MESSAGE);
        }
        
        // Query for the configured settings and all related
        // fields that match the request provided.
        List<Redirection_Setting__mdt> records =
            [SELECT Base_URL__c, Debug_Mode__c, Redirect_Entire_Window__c,
             	(SELECT Field_Name__c, Property_Name__c, Type__c,
                 	Comparison_Operator__c, Field_Value__c,
					Error_Message__c, Value_Prefix__c, Value_Suffix__c
                 FROM Redirection_Fields__r)
             FROM Redirection_Setting__mdt
             WHERE SObject_Name__c = : request.getRedirectionManager().getSObjectName()
             AND Action__c = : request.getRedirectionManager().getAction()
             LIMIT 1];
        
        // If a match is found, assign it to the settings variable.
        if (records.size() == 1) {
            settings = records[0];
        }
    }
    
    /**
     * This is considered valid if the settings variable is
     * populated.
     */
    public Boolean isValid() {
        return (settings != null);
    }
    
    /**
     * This is considered in debug mode if this class is valid
     * and either the redirection manager is in debug mode or
     * the setting record is configured to be in debug mode.
     */
    public Boolean isDebugMode() {
        
        // Exit immediatly if this is invalid.
        if (!isValid()) {
            return false;
        }
        return request.getRedirectionManager().getForcedDebugMode() || settings.Debug_Mode__c;
    }
    
    /**
     * Returns the configuration of the setting related
     * to redirecting the entire window.
     */
    public Boolean getRedirectEntireWindow() {

        // Exit immediatly if this is invalid.
        if (!isValid()) {
            return false;
        }
        return settings.Redirect_Entire_Window__c;
    }
    
    /**
     * Returns the configuration of the setting related
     * to the base URL.
     */
    public String getBaseUrl() {
        
        // Exit immediatly if this is invalid.
        if (!isValid()) {
            return null;
        }
        return settings.Base_URL__c;
    }
    
    /**
     * Return output parameters with static values.  These
     * parameters are not related to any SObject or User record.
     */
    public List<Redirection_Field__mdt> getStaticOutputParameters() {
        return getRedirectionFields(RedirectionConstants.FIELD_TYPE_STATIC_OUTPUT_PARAMETER);
    }
    
    /**
     * Return URL nodes related to the SObject record.
     */
    public List<Redirection_Field__mdt> getUrlNodesFromRecord() {
        return getRedirectionFields(RedirectionConstants.FIELD_TYPE_URL_NODE_FROM_RECORD);
    }
    
    /**
     * Return URL nodes related to the user record.
     */
    public List<Redirection_Field__mdt> getUrlNodesFromUser() {
        return getRedirectionFields(RedirectionConstants.FIELD_TYPE_URL_NODE_FROM_USER);
    }
    
    /**
     * Return output parameters related to the SObject record.
     */
    public List<Redirection_Field__mdt> getOutputParametersFromRecord() {
        return getRedirectionFields(RedirectionConstants.FIELD_TYPE_OUTPUT_PARAMETER_FROM_RECORD);
    }
    
    /**
     * Return output parameters related to the user record.
     */
    public List<Redirection_Field__mdt> getOutputParametersFromUser() {
        return getRedirectionFields(RedirectionConstants.FIELD_TYPE_OUTPUT_PARAMETER_FROM_USER);
    }
    
    /**
     * Return field validations related to the SObject record.
     */
    public List<Redirection_Field__mdt> getFieldValidationsFromRecord() {
        return getRedirectionFields(RedirectionConstants.FIELD_TYPE_FIELD_VALIDATION_FROM_RECORD);
    }
    
    /**
     * Return field validations related to the user record.
     */
    public List<Redirection_Field__mdt> getFieldValidationsFromUser() {
        return getRedirectionFields(RedirectionConstants.FIELD_TYPE_FIELD_VALIDATION_FROM_USER);
    }
    
    /**
     * Loops through redirection fields filtering only the
     * ones that match the field type provided.
     */
    public List<Redirection_Field__mdt> getRedirectionFields(String fieldType) {
        List<Redirection_Field__mdt> records = new List<Redirection_Field__mdt>();
        
        // Exit immediately if this is not valid.
        if (!isValid()) {
            return records;
        }
        
        for (Redirection_Field__mdt redirectionField : settings.Redirection_Fields__r) {
            if (redirectionField.Type__c == fieldType) {
		        records.add(redirectionField);
            }
        }
        return records;
    }
}