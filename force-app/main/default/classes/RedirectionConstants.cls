public with sharing class RedirectionConstants {

    /**
     * Querystring parameter constants.
     */
    public final static String QUERYSTRING_PARAMETER_SOBJECT = 'sobject';
    public final static String QUERYSTRING_PARAMETER_ID = 'id';
    public final static String QUERYSTRING_PARAMETER_ACTION = 'action';

    /**
     * Field type constants.
     */
    public final static String FIELD_TYPE_STATIC_OUTPUT_PARAMETER = 'Static Output Parameter';
    public final static String FIELD_TYPE_URL_NODE_FROM_RECORD = 'URL Node from Record';
    public final static String FIELD_TYPE_URL_NODE_FROM_USER = 'URL Node from User';
    public final static String FIELD_TYPE_OUTPUT_PARAMETER_FROM_RECORD = 'Output Parameter from Record';
    public final static String FIELD_TYPE_OUTPUT_PARAMETER_FROM_USER = 'Output Parameter from User';
    public final static String FIELD_TYPE_FIELD_VALIDATION_FROM_RECORD = 'Field Validation from Record';
    public final static String FIELD_TYPE_FIELD_VALIDATION_FROM_USER = 'Field Validation from User';
    
    /**
     * Comparison operator constants.
     */
    public final static String COMPARISON_NULL = 'null';
    public final static String COMPARISON_NOT_NULL = 'not null';
    public final static String COMPARISON_EQUAL_TO = 'equal to';
    public final static String COMPARISON_NOT_EQUAL_TO = 'not equal to';
    
    /**
     * Redirection mode constants.
     */
    public final static String REDIRECTION_MODE_PAGEREFERENCE = 'PageReference';
    public final static String REDIRECTION_MODE_JAVASCRIPT = 'JavaScript';
    public final static String REDIRECTION_MODE_NONE = 'None';
    
    /**
     * Error message constants.
     */
    public final static String ERROR_RECORD_NOT_FOUND = 'Record not found.';
    public final static String ERROR_USER_NOT_FOUND = 'User not found.';
    
    /**
     * Data type constants.
     */
    public final static String DATA_TYPE_BOOLEAN = 'Boolean';
    public final static String DATA_TYPE_DECIMAL = 'Decimal';
    public final static String DATA_TYPE_OBJECT = 'Object';
    
    /**
     * Validation results constants.
     */
    public final static String VALIDATION_PASS = 'PASS';
    public final static String VALIDATION_FAIL = 'FAIL';
    
    /**
     * Debug log prefix constants.
     */
    public final static String LOG_PREFIX_SOQL = 'SOQL: ';
    public final static String LOG_PREFIX_RECORD = 'RECORD: ';
    
    /**
     * Encoding type constants.
     */
    public final static String ENCODING_UTF_8 = 'UTF-8';
    
    /**
     * SOQL statement constants.
     */
    public final static String SOQL_SELECT = 'SELECT';
    public final static String SOQL_FROM = 'FROM';
    public final static String SOQL_WHERE = 'WHERE';
    public final static String SOQL_LIMIT = 'LIMIT';
    
    /**
     * SObject type constants.
     */
    public final static String OBJECT_USER = 'User';
    
    /**
     * Field name constants.
     */
    public final static String FIELD_ID = 'Id';
}