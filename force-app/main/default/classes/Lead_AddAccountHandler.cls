/*     
 *    Controller for Lead account field updation , if account website domain matches to 
 *    Lead's email domain then it will update the lead Account lookup field to matching acount Id. 
 */

public with sharing class Lead_AddAccountHandler {

    /*     Method will serach for matching account website domain with lead's email domain,
     *    and Will update lead's Account__c field to matching account ID.
    */
/*

    @future
    public static void onInsertLead(Set<Id> setLeadId){

     REMOVED PER WILL BEHUNIN 3/9

        String strEmail; 
        String sql;
        Set<String> setEmail = new Set<String>();
        Set<Id> leadOwnerId = new Set<Id>();
        Set<String> setEmailNew = new Set<String>();
        Map<String ,Id> mapDomain = new Map<String, Id>(); 
        map<string,List<Account>> mapDomainToAccount = new map<string,List<Account>>();   
        List<Account> lstAccountNew = new List<Account>();
        map<string,List<Lead>> mapDomainLead = new map<string,List<Lead>>();
        List<Lead> lstLead = new List<Lead>();
        
        if(setLeadId != Null){
        	lstLead = [Select Id, Email, Matching_Account__c, Account__c From Lead Where Id IN : setLeadId];
        }
        
        if(lstLead != Null && lstLead.size() > 0){
	        for (Lead objLead : lstLead) {
	            if((objLead.Email) != Null){
	                if (!(mapDomainLead.isEmpty()) && mapDomainLead.containsKey((objLead.Email).substringBetween('@', '.'))) {
	                    List<Lead> lstLeadNew = mapDomainLead.get((objLead.Email).substringBetween('@', '.'));
	                    lstLeadNew.add(objLead);
	                    mapDomainLead.put((objLead.Email).substringBetween('@', '.'), lstLeadNew);
	                } else {
	                    mapDomainLead.put((objLead.Email).substringBetween('@', '.'), new List<Lead> { objLead });
	                }//else
	            }//if
	        }//for
        }
        
        if(!mapDomainLead.isEmpty()){
	        for (String str : mapDomainLead.keyset()) {
	             str = '%' +str + '%';
	             setEmailNew.add(str);
	        }//for
        }
        
        // Search matching account wwebsite domain
        if (setEmailNew != Null && setEmailNew.size() > 0 ) {
             lstAccountNew = [Select Id
                                  , Website 
                                  , ParentId
                                  , Ultimate_Parent_in_Hierarchy_ID__c
                               From Account 
                              Where Website <> NULL 
                              AND Website like : setEmailNew];
        }//if
        
        //Begin Change for new enhancement : 8/1/2014
        Map<id, List<id>> mapUltimateParentChildAccount = new Map<id, List<id>>();
        
        String buildWebsite = '';
        String domainFromWebsite = '';
        String[] domainFromWebsiteSplit; 
        String stringWWW = 'www';

        if(lstAccountNew != Null){
	        
	        for (Account objAccount : lstAccountNew) {
	            if (!(objAccount.Website.contains('http'))) {
	                buildWebsite = 'https://'+ objAccount.Website;
	            } else {
	                buildWebsite = objAccount.Website;
	            }
	            if (new System.Url(buildWebsite).getHost() != '') {
	                domainFromWebsiteSplit = new System.Url(buildWebsite).getHost().replace('.',':').split(':');
	                
	                for(String str: domainFromWebsiteSplit) {
	                    if(mapDomainLead.keyset().contains(str)) {
	                        domainFromWebsite = str;
	                    }//if
	                }//for
	            }//if
	            
	            if (!(mapDomainToAccount.isEmpty()) && mapDomainToAccount.containsKey(domainFromWebsite)) {
	                List<Account> lstAccount = mapDomainToAccount.get(domainFromWebsite);
	                lstAccount.add(objAccount);
	                mapDomainToAccount.put(domainFromWebsite, lstAccount);
	            }// if
	            else {
	                mapDomainToAccount.put(domainFromWebsite, new List<Account> { objAccount });
	            }// else
	        }//for
        }//if
        List<Lead> lstUpdateLead = new List<Lead>();
        map <id, id> mapAccountUltimateParent = new map<id, id>();
        if(!(mapDomainLead.isEmpty())) { 
	        for(string domain : mapDomainLead.keyset()) {
	            if (mapDomainToAccount.containsKey(domain)) {
	                
	                List<Account> lstAccountNew1 = mapDomainToAccount.get(domain);
	                
	                for (Lead objLead : mapDomainLead.get(domain)) {
	                    
	                    if (lstAccountNew1.size() == 1) {
	                        objLead.Account__c = mapDomainToAccount.get(domain)[0].id;
	                        objLead.Matching_Account__c = true;
	                    	lstUpdateLead.add(objLead);
	                    }//if
	                    else if (lstAccountNew1 != Null && lstAccountNew1.size() > 0 ) {
	                    	
	                    	for (Account objAccount : lstAccountNew1) {
	                            mapAccountUltimateParent.put(objAccount.Ultimate_Parent_in_Hierarchy_ID__c, objAccount.id);
	                        }//for
	                        
	                        if (mapAccountUltimateParent.size() == 1) {
	                            
	                            for(Id objParentID : mapAccountUltimateParent.keySet()) {
	                            	objLead.Account__c = objParentID;
	                            	objLead.Matching_Account__c = true;
	                            	lstUpdateLead.add(objLead);
	                            }//for
	                        }//if
	                        
	                        mapAccountUltimateParent.clear();
	                    }// else if	
	                }//for
	            }//if
	        }//for
	        if(lstUpdateLead != Null && lstUpdateLead.size() > 0){
	        	
	        	Database.DMLOptions dmo = new Database.DMLOptions();
				dmo.assignmentRuleHeader.useDefaultRule = true;
				Database.update(lstUpdateLead, dmo);
	        	//database.update(lstUpdateLead);
	        }
        }//if

        
    }//onInsertLead  */
}