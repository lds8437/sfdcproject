public class ConsolidatedScreeningRequest {
    
	private ConsolidatedScreeningManager manager; 
    private ConsolidatedScreeningParameter parameter;
    private ConsolidatedScreeningResponse response;
    
    private List<Object> results;
    private String fuzzyName;
	
    //Constructor for use with onboarding attachments extenison
    public ConsolidatedScreeningRequest(ConsolidatedScreeningManager manager){
        this.manager = manager;
        parameter = new ConsolidatedScreeningParameter(this,manager);
        request(parameter.createSearchStringList(manager.getSearchClient(),manager.getSearchAccount()));
    }
    
    //Generic constructor that takes a list of strings directly 
    public ConsolidatedScreeningRequest(List<String> searchNames){
        parameter = new ConsolidatedScreeningParameter(this,manager);
        request(parameter.encodeSearchStringList(searchNames));
    }
    
    //Build and send API call to trade.gov 
    public void request(List<String> searchNames){
        response = new ConsolidatedScreeningResponse(this,manager);
        system.debug(searchNames);
        for(String searchName : searchNames){
            setSearchType(searchName);
            HttpRequest request = new HttpRequest();
            request.setMethod('GET');
            request.setEndpoint('https://api.trade.gov/consolidated_screening_list/search?api_key=9HXpXhd75xZoZu0E1KVgbsEo&name='+searchName+'&fuzzy_name='+fuzzyName);
            Http http = new Http();
            system.debug(request.getEndpoint());
            try{
                HTTPResponse apiResponse = http.send(request);
                system.debug('STATUS:' + apiResponse.getStatus());
                system.debug('RESPONSE'+apiResponse.getBody());
                if(apiResponse.getStatusCode() == 200){
                    system.debug(apiResponse.getStatus());
                    response.parseResponse(apiResponse);
                }
            }
            catch(system.CalloutException e){
                system.debug(e);
            } 
            //system.debug(response.getResults());
        }
        results = response.getResults();
    }
    
    //Determine if exact or fuzzy search should be used
    Private void setSearchType(String searchName){
        if(searchName.length() > 8){
            fuzzyName = 'true';
        }
        else{
            fuzzyName = 'false';
        }
    }
    
    public List<Object> getResults(){
        return results;
    }
}