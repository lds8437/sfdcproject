@IsTest
public with sharing class RedirectionTestUtilities {

    public static void createSObjects(List<SObject> sObjs) {
        List<Database.SaveResult> saveResults = Database.insert(sObjs, false);
        for (Database.SaveResult saveResult : saveResults) {
            System.debug(saveResult);
        }
    }
    
    public static Account createAccount() {
        return createAccounts(1)[0];
    }
    
    public static List<Account> createAccounts(Integer qty) {
        List<Account> records = new List<Account>();
        Account record;
        for (Integer i = 0; i < qty; i++) {
			record = new Account();
            record.Name = 'Test';
            records.add(record);
        }
        createSObjects(records);
        return records;
    }
    
    public static Opportunity createOpportunity() {
        return createOpportunities(1)[0];
    }
    
    public static List<Opportunity> createOpportunities(Integer qty) {
        List<Opportunity> records = new List<Opportunity>();
        Account parentRecord = createAccount();
        Opportunity record;
        for (Integer i = 0; i < qty; i++) {
			record = new Opportunity();
            record.AccountId = parentRecord.Id;
            record.Name = 'Test';
            record.StageName = 'Pending';
            record.CloseDate = System.today();
            record.Turn_Off_Initial_CX_EX_RC_Amounts__c = true;
            records.add(record);
        }
        createSObjects(records);
        return records;
    }
    
    public static Contact createContact() {
        return createContacts(1)[0];
    }
    
    public static List<Contact> createContacts(Integer qty) {
        List<Contact> records = new List<Contact>();
        Account parentRecord = createAccount();
        Contact record;
        for (Integer i = 0; i < qty; i++) {
			record = new Contact();
            record.AccountId = parentRecord.Id;
            record.FirstName = 'Test';
            record.LastName = 'Test';
            records.add(record);
        }
        createSObjects(records);
        return records;
    }
    
    public static Redirection_Field__mdt getRedirectionField() {
        return getRedirectionFields(1)[0];
    }
    
    public static Redirection_Field__mdt getRedirectionField(String sObjectName, String fieldType) {
        return getRedirectionFields(sObjectName, fieldType, 1)[0];
    }
    
    public static List<Redirection_Field__mdt> getRedirectionFields(Integer qty) {
        return [SELECT Id, Field_Name__c, Property_Name__c, Type__c,
                 	Comparison_Operator__c, Field_Value__c,
					Error_Message__c, Value_Prefix__c, Value_Suffix__c
                FROM Redirection_Field__mdt
                LIMIT : qty];
    }
    
    public static List<Redirection_Field__mdt> getRedirectionFields(String sObjectName, String fieldType, Integer qty) {
        return [SELECT Id, Field_Name__c, Property_Name__c, Type__c,
                 	Comparison_Operator__c, Field_Value__c,
					Error_Message__c, Value_Prefix__c, Value_Suffix__c,
                	Redirection_Setting__r.Action__c
                FROM Redirection_Field__mdt
                WHERE Redirection_Setting__r.SObject_Name__c = : sObjectName
                AND Type__c = : fieldType
                LIMIT : qty];
    }

    // Aggregate results are not supported for custom metadata.
    // Efficiency gain possible if this changes in the future.
    public static List<String> getActions(String sObjectName) {
        Set<String> actions = new Set<String>();
        for (Redirection_Field__mdt field:
	            [SELECT Redirection_Setting__r.Action__c
				 FROM Redirection_Field__mdt
				 WHERE Redirection_Setting__r.SObject_Name__c = : sObjectName]) {
			actions.add(field.Redirection_Setting__r.Action__c);
		}
        return new List<String>(actions);
    }
}