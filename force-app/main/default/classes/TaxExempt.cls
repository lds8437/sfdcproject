public with sharing class TaxExempt {
	
	public static void startFromQuote(List<Quote> quoteList){
		Map<Id,Opportunity> opportunityMap = queryOpportunities(quoteList);
		for(Quote quoteObj : quoteList){
			if(
				quoteObj.OpportunityId != null
				&& opportunityMap.containsKey(quoteObj.OpportunityId)
				&& opportunityMap.get(quoteObj.OpportunityId).Client__c != null
				&& opportunityMap.get(quoteObj.OpportunityId).Client__r.Tax_Exempt__c == true
			){
				quoteObj.AVA_SFQUOTES__Non_Taxable__c = true;
			}
		}
	}

	@testVisible
	private static Map<Id,Opportunity> queryOpportunities(List<Quote> quoteList){
		Set<Id> opportunityIdSet = new Set<Id>();
		for(Quote quoteObj : quoteList){
			opportunityIdSet.add(quoteObj.OpportunityId);
		}
		Map<Id,Opportunity> opportunityMap = new Map<Id,Opportunity>();
		opportunityMap.putall([SELECT Client__c,Client__r.Tax_Exempt__c FROM Opportunity WHERE Id IN : opportunityIdSet]);
		return opportunityMap;
	}

	public static void startFromOpportunity(List<Opportunity> opportunityList){
		Map<Id,Client__c> clientMap = queryClient(opportunityList);
		for(Opportunity opportunityObj : opportunityList){
			if(
				opportunityObj.Client__c != null
				&& clientMap != null
				&& clientMap.containsKey(opportunityObj.Client__c)
				&& clientMap.get(opportunityObj.Client__c).Tax_Exempt__c == true
			){
				opportunityObj.AVA_SFCORE__NonTaxable__c = true;
			}
		}
	}

	@testVisible
	private static Map<Id,Client__c> queryClient(List<Opportunity> opportunityList){
		Set<Id> clientIdSet = new Set<Id>();
		for(Opportunity opportunityObj : opportunityList){
			if(opportunityObj.Client__c != null){
				clientIdSet.add(opportunityObj.Client__c);
			}
		}
		if(clientIdSet == null || clientIdSet.size() == 0){
			return null;
		}
		Map<Id,Client__c> clientMap = new Map<Id,Client__c>();
		clientMap.putall([SELECT Tax_Exempt__c FROM Client__c WHERE Id IN : clientIdSet]);
		return clientMap;
	}

}