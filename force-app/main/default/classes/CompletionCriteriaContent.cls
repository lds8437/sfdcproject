public with sharing class CompletionCriteriaContent {

	private String text {get;set;}
	private List<DynamicAction> buttons {get;set;}
	private List<DynamicAction> hyperlinks {get;set;}
    
    public CompletionCriteriaContent(String text) {
        this.text = text;
        this.buttons = new List<DynamicAction>();
        this.hyperlinks = new List<DynamicAction>();
    }
    
    @AuraEnabled
    public String getText() {
        return text;
    }

    @AuraEnabled
    public List<DynamicAction> getButtons() {
        buttons.sort();
        return buttons;
    }
    
    public void addButton(DynamicAction button) {
        this.buttons.add(button);
    }

    @AuraEnabled
    public List<DynamicAction> getHyperlinks() {
        hyperlinks.sort();
        return hyperlinks;
    }
    
    public void addHyperlink(DynamicAction hyperlink) {
        this.hyperlinks.add(hyperlink);
    }
}