global class ScheduledEmployeeTableUpdate implements Schedulable {
    global void execute(SchedulableContext sc){
        BatchableEmployeeTableUpdate b = new BatchableEmployeeTableUpdate();
        database.executeBatch(b);
    }
}