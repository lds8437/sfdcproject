@isTest()
public class clientLicenseChartTest {
 static Account a;
    static Opportunity o;
    static Quote q;
    static Bundle__c b0;
    static Bundle__c b1;
    static Client__c c0;
    static Client__c c1;
    static Client__c c2;
    static Client_License__c cl0;
    static Client_License__c cl1;
    static Client_License__c cl2;
    static Client_License__c cl3;
    static Client_License__c cl4;
    static Client_License__c cl5;
    
   
    
    @IsTest static void testGetClientLicenses(){
        a = new Account(Name = 'Test Account',
                        BillingStreet = '123 Billing Street',
                        BillingCity = 'Billville',
                        BillingState = 'BL',
                        BillingPostalCode = '12345');
        insert a;
        RecordType xmtype = [SELECT ID FROM RecordType WHERE sObjectType = 'Opportunity' AND Name = 'XM'];
        o = new Opportunity(Name = 'Test Opportunity',
                            AccountId = a.Id,
                            StageName = 'Discover and Assess',
                            CloseDate = system.today(),
                            RecordTypeId = xmtype.Id,
                            Turn_Off_Initial_CX_EX_RC_Amounts__c = TRUE);
        insert o;
        q = new Quote(Name = 'Test Quote',
                      OpportunityId = o.Id);
        insert q;
        b0 = new Bundle__c(Name = 'Test Bundle 0',
                           Experience__c = 'CX',
                           Bundle__c = '0');
        insert b0;
        b1 = new Bundle__c(Name = 'Test Bundle 1',
                           Experience__c = 'CX',
                           Bundle__c = '1');
        insert b1;
        c0 = new Client__c(Name = 'Test Client 0',
                           Account__c = a.Id);
        insert c0;
        c1 = new Client__c(Name = 'Test Client 1',
                           Account__c = a.Id,
                           CurrencyIsoCode = 'USD');
        insert c1;
        c2 = new Client__c(Name = 'Test Client 2',
                           Account__c = a.Id);
        insert c2;
        cl0 = new Client_License__c(Name = 'Test Client License 0',
                                    Client__c = c0.Id,
                                    License_End_Date__c = system.today() + 10,
                                    Bundle__c = b0.Id,
                                    Status__c = 'Active',
                                    CurrencyIsoCode = 'USD',
                                    Invoice_Amount__c = 100);
        insert cl0;
        cl1 = new Client_License__c(Name = 'Test Client License 1',
                                    Client__c = c0.Id,
                                    License_End_Date__c = system.today() - 10,
                                    Bundle__c = b0.Id,
                                    Status__c = 'Expired');
        insert cl1;
        cl2 = new Client_License__c(Name = 'Test Client License 2',
                                    Client__c = c1.Id,
                                    License_End_Date__c = system.today() + 10,
                                    Bundle__c = b0.Id,
                                    Status__c = 'Active');
        insert cl2;
        cl3 = new Client_License__c(Name = 'Test Client License 3',
                                    Client__c = c0.Id,
                                    License_End_Date__c = system.today() + 10,
                                    Bundle__c = b1.Id,
                                    Status__c = 'Active');
        insert cl3;
        cl4 = new Client_License__c(Name = 'Test Client License 4',
                                    Client__c = c0.Id,
                                    License_End_Date__c = system.today() + 20,
                                    Bundle__c = b0.Id,
                                    Status__c = 'Active');
        insert cl4;
        cl5 = new Client_License__c(Name = 'Test Client License 5',
                                    Client__c = c0.Id,
                                    License_End_Date__c = system.today() + 10,
                                    Bundle__c = b0.Id,
                                    Status__c = 'Active',
                                    CurrencyIsoCode = 'EUR',
                                    Invoice_Amount__c = 100);
        insert cl5;
        String cl = clientLicenseChart.getClientLicensesJSON(a.Id);
    }
}