public class ConsolidatedScreeningManager {
    
    private ConsolidatedScreeningRequest request;
    private ConsolidatedScreeningResponse response;
    private ConsolidatedScreeningApproval approval;
    
    private Id searchAccount;
    private Id searchClient;
    private Id opportunityId;
    private List<Object> results;
    
    //Constructor for use with the onboarding attachments extension
    public ConsolidatedScreeningManager(Id targetClient, Id targetAccount, Id opportunityId){
        this.searchClient = targetClient;
        this.searchAccount = targetAccount;
        this.opportunityId = opportunityId;
        execute();
    }
    
    //Generic constructor used for just passing a list of strings directly
    //Executes search directly
    public ConsolidatedScreeningManager(List<String> searchNames){
        if(searchNames.size()>0){
           request = new ConsolidatedScreeningRequest(searchNames); 
           results = request.getResults();
           system.debug(results);
           system.debug(results.size());
        }
    }
    
    //Execute search
    private void execute(){
        request = new ConsolidatedScreeningRequest(this);     
        results = request.getResults();
        createApproval();
    }
    
    //Take search results and trigger approval process if applicable
    private void createApproval(){
        if(results.size() > 0){
            approval = new ConsolidatedScreeningApproval(this);  
        }
    }
    
    //Check for null Ids incase client is not set on opp
    public Boolean validationCheck(Id checkId){
        if(checkId == null){
            return false;
        }
        else{
            return true;
        }
    }
    
    public Id getSearchAccount(){
        return searchAccount;
    }
    
    public Id getSearchClient(){
        return searchClient;
    } 
    
    public Id getOpportunityId(){
        return opportunityId;
    }
    
    public List<Object> getResults(){
        return results;
    }
    
    public void setResults(string results){
        results = results;
    }
    
}