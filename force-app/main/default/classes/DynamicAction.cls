public with sharing class DynamicAction implements Comparable {

    /**
     * Private variables tightly coupled with the fields
     * on the dynamic action custom metadata object.
     */
	private String label;
	private String userInterface;
	private String destinationType;
	private String goToDestination;
	private Boolean openInNewWindow;
	private String modalTitle;
    private String recordIdOutputParameterName;
    private String userIdOutputParameterName;
    private String tooltip;
    private String controllingCheckboxFieldName;
    
    /**
     * Private variables tightly coupled with the fields
     * on the dynamic action location custom metadata object.
     */
    private Decimal displayOrder;
    
    /**
     * Calculated private variables.
     */
    private Boolean display = true;

    /**
     * Class Constructor
     * 
     * The custom metadata record is used to populate
     * a majority of the properties within this class.
     * The SObject provided is used to determine if
     * the checkbox should be displayed or not.
     */
    public DynamicAction(Dynamic_Action_Location__mdt record, SObject sObj) {
		init(record, sObj);
    }
    
    /**
     * All variables are populated during initialization.
     */
    private void init(Dynamic_Action_Location__mdt record, SObject sObj) {
        
        // Dynamic Action Properties
        this.label = record.Dynamic_Action__r.Label__c;
        this.userInterface = record.Dynamic_Action__r.User_Interface__c;
        this.destinationType = record.Dynamic_Action__r.Destination_Type__c;
        this.goToDestination = record.Dynamic_Action__r.Go_To_Destination__c;
        this.openInNewWindow = record.Dynamic_Action__r.Open_in_New_Window__c;
        this.modalTitle = record.Dynamic_Action__r.Modal_Title__c;
        this.recordIdOutputParameterName  = record.Dynamic_Action__r.Record_Id_Output_Parameter_Name__c;
        this.userIdOutputParameterName  = record.Dynamic_Action__r.User_Id_Output_Parameter_Name__c;
        this.tooltip  = record.Dynamic_Action__r.Tooltip__c;
        this.controllingCheckboxFieldName  = record.Dynamic_Action__r.Controlling_Checkbox_Field_Name__c;

        // Dynamic Action Location Properties
        this.displayOrder = record.Display_Order__c;
        
        // Determine if the criteria is met to be displayed
        this.setDisplay(sObj);
    }
    
    /**
     * Required because the current class implements the
     * comparable interface.  This allows instances of
     * this class to be ordered using the sort command.
     */
    public Integer compareTo(Object otherRecord) {
        
        DynamicAction otherDynamicAction = (DynamicAction)otherRecord;
		Decimal thisDisplayOrder = this.getDisplayOrder();
        Decimal otherDisplayOrder = otherDynamicAction.getDisplayOrder();
        
        if (thisDisplayOrder < otherDisplayOrder) {
            return -1;
        } else if (otherDisplayOrder < thisDisplayOrder) {
            return 1;
        }
        return 0;
    }
    
    /**
     * Determines if this dynamic action should be displayed
     * or not.  The displayed property originally was used to
     * hide dynamic actions.  Today it is used to disable
     * dynamic actions instead.
     */
    private void setDisplay(SObject sObj) {
        this.display = RecordUtilities.getBooleanFieldValue(sObj, getControllingCheckboxFieldName(), true);
    }
    
    /**
     * Properties exposed to lightning components.
     */
    @AuraEnabled
    public Boolean getDisplay() {
        return this.display;
    }
    
    @AuraEnabled
    public String getLabel() {
        return this.label;
    }
    
    @AuraEnabled
    public String getUserInterface() {
        return userInterface;
    }
    
    @AuraEnabled
    public String getDestinationType() {
        return this.destinationType;
    }
    
    @AuraEnabled
    public String getGoToDestination() {
        return this.goToDestination;
    }
    
    @AuraEnabled
    public Boolean getOpenInNewWindow() {
        return this.openInNewWindow;
    }
    
    @AuraEnabled
    public Decimal getDisplayOrder() {
        
        // If the display order is not populated we
        // return a 0.  Numeric values are required
        // to properly sort instances of this class.
        return this.displayOrder == null ? 0 : this.displayOrder;
    }
    
    @AuraEnabled
    public String getModalTitle() {
        return this.modalTitle;
    }
    
    @AuraEnabled
    public String getRecordIdOutputParameterName() {
        return this.recordIdOutputParameterName;
    }
    
    @AuraEnabled
    public String getUserIdOutputParameterName() {
        return this.userIdOutputParameterName;
    }
    
    @AuraEnabled
    public String getTooltip() {
        
        // If the tooltip is not populated we return
        // an empty string.  Text values are required
        // to properly support the UI tooltips.
        return this.tooltip == null ? '' : this.tooltip;
    }
    
    @AuraEnabled
    public String getControllingCheckboxFieldName() {
        return this.controllingCheckboxFieldName;
    }
}