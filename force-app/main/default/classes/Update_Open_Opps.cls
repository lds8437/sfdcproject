public class Update_Open_Opps {   
    @InvocableMethod
    public static void updateopenopportunitycount(List<Id> primaryId){        
        List<Opportunity> oppLists = [select Id,Qualification_Contact__c from Opportunity where(isClosed != true AND Qualification_Contact__c =:primaryId)];
        Integer openOpps = oppLists.size();
        Contact toUpdate = new Contact(Id = primaryId[0],
                                       Number_of_Open_Opps__c = openOpps);
        update toUpdate;
    }
}