public with sharing class RedirectionRequest {

    /**
     * The parent object of every redirection request is
     * a redirection manager.  This variable is used to
     * allow public properties and methods exposed from
     * the redirection maanger to be available within
     * this redirection request.
     */
    private RedirectionManager manager;
    
    /**
     * A request has 3 main properties that dictate the
     * success and status of the request.
     * 
     * Settings - Configuration (custom metadata records)
     *     for the action requested.
     * Record - The SObject record the action is being
     *     performed against.
     * User - The user executing the request.
     */
    private RedirectionSettings settings;
    private RedirectionSObject record;
    private RedirectionUser user;
    
    /**
     * Logs (debug logs) and error messages are collections
     * used to provide feedback to the end user.  Debug logs
     * are only used when the request is being processed in
     * debug mode.  Error messages are populated as custom
     * validation rules fail.
     */
    private List<String> logs;
    private List<String> errorMessages;

    /**
     * Class Constructor
     * 
     * A reference to the parent object, Redirection Manager,
     * is passed while instantiating the request.
     */
    public RedirectionRequest(RedirectionManager manager) {
        this.manager = manager;
        init();
    }
    
    /**
     * The 3 main properties needed to complete the request
     * are populated during the initialization phase of the
     * request creation.
     */
    private void init() {
		initLogs();
        settings = new RedirectionSettings(this);
        record = new RedirectionSObject(this, settings);
        user = new RedirectionUser(this, settings);
    }
    
    /**
     * The debug mode state of this request is just passed
     * through from the settings class.  This keeps the method
     * as simple as possible here, and allows the management
     * of the debug mode state to be completed in a single
     * location (in the settings class).
     */
    public Boolean isDebugMode() {
        return settings.isDebugMode();
    }
    
    /**
     * For a request to be valid ALL components must be valid.
     * If any portion of this request is invalid, the request
     * itself is invalid.
     */
    public Boolean isValid() {
        return settings.isValid() && record.isValid() && user.isValid() && errorMessages == null;
    }
    
    /**
     * Error Messages
     * 
     * Error messages are a collection of string values helping
     * the end user resolve any custom validation rule failures.
     * The messages are defined in the custom metadata and exposed
     * via the RedirectionSettings class.
     */
    public List<String> getErrorMessages() {
        return errorMessages;
    }
    
    /**
     * Adding an error message simply accepts a string value and
     * adds it to the list of error messages.
     * 
     * The list of error messages is not instantiated until the
     * first error message is added.
     */
    public void addErrorMessage(String message) {
        if (errorMessages == null) {
            errorMessages = new List<String>();
        }
        errorMessages.add(message);
    }
    
    /**
     * Convenience Method
     * 
     * To simplify the addition of error messages, this method
     * allows you to pass the custom metadata record itself.  The
     * error message related to the custom metadata is extracted
     * and passed to the addErrorMessage method that only accepts
     * primitive values.
     */
    public void addErrorMessage(Redirection_Field__mdt field) {
        addErrorMessage(field.Error_Message__c);
    }
    
    /**
     * Logs (Debug)
     * 
     * Debug logs are only used when the redirection manager is
     * in debug mode.  This is defined in the custom metadata
     * records and exposed via the RedirectionSettings class.
     */
    private void initLogs() {
        logs = new List<String>();
    }
    
    /**
     * Getter
     */
    public List<String> getLogs() {
        return logs;
    }
    
    /**
     * Adding an debug log simply accepts a string value and
     * adds it to the list of debug logs.
     * 
     * Before any addition is processed we verify we are in
     * debug mode.  If we are not in debug mode the log is
     * discarded.
     */
    public void addLog(String log) {
        if (isDebugMode()) {
	        logs.add(log);
        }
    }
    
    /**
     * Convenience Method
     * 
     * To simplify the addition of debug logs, this method
     * allows you to pass the custom metadata record itself.  The
     * debug log related values to the custom metadata is extracted
     * and passed to the addLog method that only accepts primitive
     * values.
     */
    public void addLog(Redirection_Field__mdt field, Boolean pass) {
        if (isDebugMode()) {
            String fieldName = field.Field_Name__c;
            String comparisonOperator = field.Comparison_Operator__c;
            String fieldValue = field.Field_Value__c;
            String status = pass ? RedirectionConstants.VALIDATION_PASS : RedirectionConstants.VALIDATION_FAIL;
            addLog(status + ' ' + fieldName + ' ' + comparisonOperator + ' ' + fieldValue);
        }
    }
    
    /**
     * The redirection URL is composed of URL nodes and
     * querystring parameters.  The base URL,  URL nodes
     * and keys are specific to the settings configured
     * in the custom metadata records.  The values are
     * specific to the SObject and user records.
     */
    public String getRedirectionUrl() {
        
        //Quit immediately if the request is not valid.
        if (!isValid()) {
            return null;
        }
        
        // The base URL is static and defined in the custom
        // metadata record.
        String redirectionUrl = settings.getBaseUrl();
        
        // Append URL nodes and querystring parameters
        // to the base URL.
        redirectionUrl = appendUrlNodes(redirectionUrl);
		redirectionUrl = appendQuerystringParameters(redirectionUrl);
        
        // Return the final redirection URL.
        return redirectionUrl;
    }
    
    private String appendUrlNodes(String redirectionUrl) {
        
        // All record-based and user-based URL nodes are added
        // to a single collection to be processed.
        List<String> listOfUrlNodes = new List<String>();
        listOfUrlNodes.addAll(record.getUrlNodes());
        listOfUrlNodes.addAll(user.getUrlNodes());

        // Append a forward slash if necessary.  Necessary is
        // defined as a scenario where URL nodes must be added
        // and the base URL does not end with a forward slash.
        if (listOfUrlNodes.size() > 0 && !redirectionUrl.endsWith('/')) {
            redirectionUrl += '/';
        }

		// Add all URL nodes to the redirection URL.
        for (Integer i = 0; i < listOfUrlNodes.size(); i++) {
            redirectionUrl += listOfUrlNodes[i];
            
            // If this is not the last URL node to be added
            // we neeed to add a forward slash to separate the
            // nodes.
            if (i < listOfUrlNodes.size() - 1) {
                redirectionurl += '/';
            }
        }
        
        return redirectionUrl;
    }
    
    private String appendQuerystringParameters(String redirectionUrl) {
        
        // All querystring parameters are added to a single
        // collection to be processed.  Querystring parameters
        // could be static, record-based or user-based.
		Map<String, String> mapOfQuerystringParameters = new Map<String, String>();
        mapOfQuerystringParameters.putAll(getStaticOutputQuerystringParamters());
        mapOfQuerystringParameters.putAll(record.getOutputQuerystringParamters());
        mapOfQuerystringParameters.putAll(user.getOutputQuerystringParamters());
        
        // Only continue if querytring parameters exist.
        if (mapOfQuerystringParameters.size() > 0) {
            
            // If the current redirection URL does not contain
            // a question mark we must add it.  Otherwise we
            // use an ampersand before adding querystring
            // parameters.
            if (!redirectionUrl.contains('?')) {
                redirectionUrl += '?';
            } else {
                redirectionUrl += '&';
            }
            
            // Add each querystring parameter and its value
            // to the redirection URL.
            for (String key : mapOfQuerystringParameters.keySet()) {
                redirectionUrl += key + '=' + mapOfQuerystringParameters.get(key) + '&';
            }
        }
        
        return redirectionUrl;
    }
    
    /**
     * Getters
     */
    public Map<String, String> getStaticOutputQuerystringParamters() {
        return RedirectionUtilities.getOutputQuerystringParamters(settings.getStaticOutputParameters(), null);
    }
    
    public RedirectionManager getRedirectionManager() {
        return this.manager;
    }
    
    public RedirectionSettings getRedirectionSettings() {
        return this.settings;
    }
}