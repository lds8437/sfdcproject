public class ContentDocumentLinkTriggerHandler {
    
    public void OnAfterInsert(ContentDocumentLink[] newObjects, map<id,ContentDocumentLink> MapNewMap){
        List<Id> cdIds = new List<Id>();
        for(ContentDocumentLink cdl : newObjects){
            String linkedEntity = cdl.LinkedEntityId;
            if(linkedEntity.startsWith('a4V')){
                cdIds.add(linkedEntity); 
            }
            if(cdIds.size() > 0){
                if(System.isFuture() || System.isBatch()){
                } else {
                    attachContractToOpp(cdIds);
                }
            }
        }    
    }
    
    public static void attachContractToOpp(List<Id> cdIds){
        
        List<ContentDocumentLink> cdLinks = new List<ContentDocumentLink>();
        List<Contract_Document__c> cds = [SELECT Id, Opportunity__c FROM Contract_Document__c WHERE Id in :cdIds];
        set<Id> oppDocs = new Set<Id>();
        
        Id cdId = cds[0].Id;
        Id oppId = cds[0].Opportunity__c;
        
        List<ContentDocumentLink> contentDocumentIds = database.query('SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntity.Id =: cdId');
        List<ContentDocumentLink> contentDocumentIdsOpp = database.query('SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntity.Id =: oppId');
        
        if(contentDocumentIdsOpp.size() > 0){
            for(ContentDocumentLink cd : contentDocumentIdsOpp){
                oppDocs.add(cd.ContentDocumentId);
            }
        }
        
        for(ContentDocumentLink cdl : contentDocumentIds){
            if(!oppDocs.contains(cdl.ContentDocumentId)){
                ContentDocumentLink link = new ContentDocumentLink();
                link.LinkedEntityId = cds[0].Opportunity__c;
                link.ContentDocumentId = cdl.ContentDocumentId;
                link.ShareType = 'V';
                link.Visibility = 'AllUsers';
                cdLinks.add(link);                
            }
        }
        if(cdLinks.size() > 0){
            insert cdLinks;  
        }
    }
    

}