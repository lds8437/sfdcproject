@IsTest
public with sharing class OpportunityAccessTests {

    testMethod static void testRevertOppHandover() {
        
        // Prepare for tests
        Account testAccount = new Account();
        testAccount.Name='Test Account';
        insert testAccount;
        
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name='Test Opportunity';
        testOpportunity.AccountId=testAccount.Id;
        testOpportunity.StageName='Pending';
        testOpportunity.CloseDate=System.today();
        testOpportunity.Turn_Off_Initial_CX_EX_RC_Amounts__c=true;
        testOpportunity.Prior_Owner_Id__c = UserInfo.getUserId();
        insert testOpportunity;
        
        OpportunityTeamMember opportunityTeamMember = new OpportunityTeamMember(
        	OpportunityId = testOpportunity.Id,
            UserId = UserInfo.getUserId()
        );
        insert opportunityTeamMember;
        List<Id> opportunityTeamMembers = new List<Id>();    
        opportunityTeamMembers.add(opportunityTeamMember.Id);
        
        Test.startTest();
        
        // Successful test
        testOpportunity = OpportunityAccess.revertOppHandover(testOpportunity.Id);
        System.assertEquals('012500000009noC', testOpportunity.RecordTypeId);
        System.assertEquals('Rejected', testOpportunity.Approval_Status__c);
        System.assertEquals('Rejected', testOpportunity.Sales_Process_Stage__c);
        
        // Failed test
        try {
	        testOpportunity = OpportunityAccess.revertOppHandover(null);
        } catch (DmlException e) {
            System.assertEquals(e.getMessage(), 'Record not found');
        }
		OpportunityAccess.enforceDistinctMemberships(opportunityTeamMembers);
        Test.stopTest();
        
    }
}