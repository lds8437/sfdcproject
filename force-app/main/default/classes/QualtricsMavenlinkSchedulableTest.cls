/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class QualtricsMavenlinkSchedulableTest {
	public static Account sAcc = null; public static Product2 sProd = null; public static Product2 sProd2 = null; public static PriceBook2 sCustPB = null;
    public static PriceBookEntry sStdPBE = null; public static PriceBookEntry sStdPBE2 = null; public static PriceBookEntry sCustPBE = null; public static PriceBookEntry sCustPBE2 = null; public static Opportunity sOpp = null;
        
    @isTest public static void seedConfig(){
        mavenlink__PackageConfiguration__c config = new mavenlink__PackageConfiguration__c(
            name = 'default_config',
            mavenlink__Opportunity_Stage_Values__c = 'test Stage'
        );
        insert config;
    }

    @isTest public static Account seedAccount(){
        Account seedAcc = new Account(
                name='Test Account',
                BillingCity='Boston');
        insert seedAcc;
    return seedAcc;        
    }
    
    @isTest public static Product2 seedProduct1(){
        Product2 seedProduct = new Product2(name='Product 1',productCode='XX1234', isActive=true,Display_Order__c=1);
        insert seedProduct;
        return seedProduct;
    }

    @isTest public static Product2 seedProduct2(){
        Product2 seedProduct = new Product2(name='Product 2',productCode='XX9876', isActive=true,Display_Order__c=1);
        insert seedProduct;
        return seedProduct;
    }

    @isTest public static PriceBook2 seedPriceBook2(){
        PriceBook2 customPriceBook = new PriceBook2(Name='Custom Pricebook',IsActive=true);
        insert customPriceBook;
        return customPriceBook;
    }
    
    @isTest public static PriceBookEntry seedStdPriceBookEntry1(){
        PricebookEntry pbe = null;
        try{
            pbe = new PriceBookEntry(
                Product2Id=sProd.id,
                Pricebook2Id=Test.getStandardPricebookId(),
                UnitPrice = 300000.00,
                UseStandardPrice = false,
                IsActive=true
            );
            insert pbe;
        }catch(Exception e){}
        return pbe;
    }   
    
    @isTest public static PriceBookEntry seedStdPriceBookEntry2(){
        PricebookEntry pbe = null;
        try{
            pbe = new PriceBookEntry(
                Product2Id=sProd2.id,
                Pricebook2Id=Test.getStandardPricebookId(),
                UnitPrice = 400000.00,
                UseStandardPrice = false,
                IsActive=true
            );
            insert pbe;
        }catch(Exception e){}
        return pbe;
    }   
    
    @isTest public static PriceBookEntry seedPriceBookEntry1(){
        PricebookEntry pbe = null;
        try{
            pbe = new PriceBookEntry(
                Product2Id=sProd.id,
                Pricebook2Id=sCustPB.id,
                UnitPrice = 300000.00,
                UseStandardPrice = false,
                IsActive=true
            );
            insert pbe;
        }catch(Exception e){}
        return pbe;
    }   
    
    @isTest public static PriceBookEntry seedPriceBookEntry2(){
        PricebookEntry pbe = null;
        try{
            pbe = new PriceBookEntry(
                Product2Id=sProd2.id,
                Pricebook2Id=sCustPB.id,
                UnitPrice = 400000.00,
                UseStandardPrice = false,
                IsActive=true
            );
            insert pbe;
        }catch(Exception e){}
        return pbe;
    }   

    @isTest public static Opportunity seedOpportunity(){
        Opportunity seedOpp = null;
        try{
            seedOpp = new Opportunity(
                name = 'Test Opportunity',
                AccountId = sAcc.id,
                stageName = 'Commit',
                Amount = 700000.00,
                closeDate = Date.today(),
                Forecast_Percentage_NEW__c = 60.0,
                pricebook2Id=sCustPB.id,
                Change_Order_Source_Opportunity__c=null
            );
            insert seedOpp;
        }catch(Exception e){}
    return seedOpp;        
    }
    
    @isTest public static void seedTestSchedule(){
      TestFactory.disableTriggers();
        sAcc = seedAccount();
        sCustPB = seedPriceBook2();
        sOpp = seedOpportunity();
    }
    
    @isTest public static void executeTestSchedule(){
    		seedTestSchedule();
    		Test.startTest();
    		
    		//QualtricsMavenlinkSchedulable qsch = new QualtricsMavenlinkSchedulable(sOpp.Id);
    		
		DateTime currentDateTime = DateTime.now();
			DateTime futureDateTime = currentDateTime.addSeconds(QualtricsMavenlinkSchedulable.OPPORTUNITY_UPDATE_DELAY);
			String cron = futureDateTime.second()+' '+futureDateTime.minute()+' '+futureDateTime.hour()+' '+futureDateTime.day()+' '+futureDateTime.month()+' ? '+futureDateTime.year();

			System.schedule('QualtricsMavenlinkSchedulable '+sOpp.Id+' '+DateTime.now().format('yyyy.MM.dd.HH.mm.ss.SSS'),
				cron,new QualtricsMavenlinkSchedulable(sOpp.Id));
    }
}