global class QualtricsMavenlinkWSAllocBatch implements Database.Batchable<Integer>, Database.AllowsCallouts, Database.Stateful {
    private static string c = 'QualtricsMavenlinkWSAllocBatch';
    private Set<String> totalProcessedWorkspaceIds {get;set;}
    private Map<String,Object> workspaceAllocations {get;set;}
    private Map<String,Object> workspaceResources {get;set;}
    private Set<String> workspaceIds {get;set;}
    private Map<String,String> resourceWorkspaceMap {get;set;}
    private Map<String,String> allocationResourceMap {get;set;}
    private Integer totalProjects{get;set;}
    private Integer page{get;set;}
    private Boolean processMore{get;set;}
    private Id projectId {get;set;}
    private mavenlink__PackageConfiguration__c config {get;set;}
    
    private static Integer totalEstimatedAPICallOuts = 12; //(100/12)
    private static Integer batchSize = Integer.valueOf(Math.floor(100/totalEstimatedAPICallOuts)); //(100/12)
    
    private static String sOppIDGlobal = null;
    private static String needReviewCF = 'Needs Review (Automation)';
	private static String needReviewDetailCF = 'Review Details (Automation)';
	private static String reservationDtCF = 'Reservation Date';
	private static String reservationExpDtCF = 'Reservation Expiration Date';
	private static String plannedStartDtCF = 'Start Date (Planned )';
	private static String plannedEndDtCF = 'End Date (Planned )';
    
    
    public QualtricsMavenlinkWSAllocBatch(Set<String> totalProcessedWorkspaceIds,Integer totalProjects,Integer page,
                Set<String> workspaceIds,Map<String,Object> workspaceAllocations,Map<String,Object> workspaceResources,
                Map<String,String> resourceWorkspaceMap,Map<String,String> allocationResourceMap){
        this.totalProcessedWorkspaceIds = totalProcessedWorkspaceIds;
        this.totalProjects = totalProjects;
        this.page = page;
        //this.config = ConfigurationService.getConfig();
        this.workspaceIds = workspaceIds;
        this.workspaceAllocations = workspaceAllocations;
        this.workspaceResources = workspaceResources;
        this.resourceWorkspaceMap = resourceWorkspaceMap;
        this.allocationResourceMap = allocationResourceMap;
    }

    global List<Integer> start(Database.BatchableContext context) {
        String m = 'start';
        try{
        mavenlink.MavenlinkLogExternal.Save();
        return new Integer[]{page};
        } catch(Exception e){mavenlink.MavenlinkLogExternal.Write(c,m,'Error = ('+e.getStackTraceString()+') '+e.getMessage()); mavenlink.MavenlinkLogExternal.Save(); return null;}
    }

    global void execute(Database.BatchableContext context, List<Integer> pages){
        String m = 'execute';
        try{
        
        mavenlink.MavenlinkLogExternal.Save();
        } catch(Exception e){mavenlink.MavenlinkLogExternal.Write(c,m,'Error = ('+e.getStackTraceString()+') '+e.getMessage()); mavenlink.MavenlinkLogExternal.Save();}
    }

    global void finish(Database.BatchableContext ctx) {
        String m = 'finish';
        try{
    		
    		mavenlink.MavenlinkLogExternal.Write(c,m,'Start QualtricsMavenlinkWSAllocBatch');
        
        Set<String> batchProcessedWorkspaceIds = new Set<String>();
        //Set<String> totalPendingWorkspaces = new Set<String>(workspaceIds);
        
        Set<String> totalBatchWorkspaces = new Set<String>();
        
        //get batch of batchSize elemnts from workspaceIds
        
        List<mavenlink__Mavenlink_Project__c> projects = new List<mavenlink__Mavenlink_Project__c>();
        projects = [select Id, mavenlink__Mavenlink_Id__c
                        from mavenlink__Mavenlink_Project__c
                        where mavenlink__Mavenlink_Id__c not in :totalProcessedWorkspaceIds 
                        and mavenlink__Mavenlink_Id__c in :workspaceIds LIMIT :batchSize]; //Smaller limit due to more API calls

        Boolean projectsProcessed = false;
        if (projects.size()>0) {
                projectsProcessed = true;
                mavenlink.MavenlinkLogExternal.Write(c,m,'projectProcessed = '+projectsProcessed);   
                for(mavenlink__Mavenlink_Project__c p:projects){
                    totalBatchWorkspaces.add(p.mavenlink__Mavenlink_Id__c);
                }
        }   
        
            
        if (projectsProcessed){
            batchProcessedWorkspaceIds = processProjects(new List<String>(totalBatchWorkspaces));
            mavenlink.MavenlinkLogExternal.Write(c,m,'batchProcessedWorkspaceIds = '+batchProcessedWorkspaceIds.size());
                if(batchProcessedWorkspaceIds.size()>0)
                    totalProcessedWorkspaceIds.addAll(batchProcessedWorkspaceIds);
        }       
        
        
        
        mavenlink.MavenlinkLogExternal.Write(c,m,'Workspace Allocations Batch '+page+'('+totalProcessedWorkspaceIds.size()+'/'+totalProjects+')');       
        //The conditions below prevents runaway process
        //Total projects processed for all iterations less than total,
        //Projects were found for processing this iteration,
        //Actual projects processed > 0 
        processMore = false;
        if (totalProcessedWorkspaceIds.size()<totalProjects && projectsProcessed && batchProcessedWorkspaceIds.size()>0){
        //if (totalPendingWorkspaces.size() > 0){
            processMore = true;
        }
        mavenlink.MavenlinkLogExternal.Write(c,m,'End QualtricsMavenlinkWorkspaceAllocationsBatch');
    		
        if (processMore != null && processMore){
            if(!Test.isRunningTest()) {
                Database.executeBatch(new QualtricsMavenlinkWSAllocBatch(totalProcessedWorkspaceIds,totalProjects,(page+1),workspaceIds,
                    workspaceAllocations,workspaceResources,resourceWorkspaceMap,allocationResourceMap), 1);
            }
        }
        mavenlink.MavenlinkLogExternal.Save();
        } catch(Exception e){mavenlink.MavenlinkLogExternal.Write(c,m,'Error = ('+e.getStackTraceString()+') '+e.getMessage()); mavenlink.MavenlinkLogExternal.Save();}
    }
    
    private Set<String> processProjects(List<String> workspaceIds){
        String m='processProjects';
        Set<String> retVals = new Set<String>();
        
        Integer counter = 0;
        Set<QService__c> tServices = new Set<QService__c>();
        try{
            while(Limits.getLimitCallouts()>totalEstimatedAPICallOuts && counter<workspaceIds.size()){
                
                try{
                		mavenlink.MavenlinkLogExternal.Write(c,m,'Processing Reservation Dates for project '+workspaceIds.get(counter));
                    tServices.addAll(setReservationFields(workspaceIds.get(counter)));
                    
                retVals.add(workspaceIds.get(counter));
                counter++;
                    
                }catch(Exception e){
                    if(!e.getMessage().contains('LimitException')){
                        retVals.add(workspaceIds.get(counter)); //Anything other than limitsException is skipped as it is a bad project
                        counter++;
                    }
                }
            }
        }catch(Exception e){
        		mavenlink.MavenlinkLogExternal.Write(c,m,'Error = ('+e.getStackTraceString()+') '+e.getMessage());
        }finally{
        		try{
            		List<QService__c> updServices = new List<QService__c>(tServices);
            		mavenlink.MavenlinkLogExternal.Write(c,m,'Saving to SF '+updServices.size());
            		update updServices;
        		}catch(Exception e){
        			mavenlink.MavenlinkLogExternal.Write(c,m,'Error = ('+e.getStackTraceString()+') '+e.getMessage());
        		}
        		mavenlink.MavenlinkLogExternal.Save();
        }
        //Loop through projectsProcessed
        //Process as many as possible based on governor limits (use Limts class)
        //Updated processedCount to the value representing the projects that were processed
        return retVals;
    }
    
    
    //Custom Exception class
	class QualtricsMLSetResDateException extends Exception {
	}
    
    
    public static List<QService__c> setReservationFields(String mlWorkspaceId){
    	 	String m = 'setReservationFields';
    	 	
    	 	Set<String> returnVal = new Set<String>();
    		mavenlink__Mavenlink_Project__c MLProj = [select Id, mavenlink__Mavenlink_Id__c, mavenlink__Opportunity__c,mavenlink__Archived__c,mavenlink__Mavenlink_Url__c from mavenlink__Mavenlink_Project__c where mavenlink__Mavenlink_Id__c=:mlWorkspaceId];
    		
    		sOppIDGlobal = MLProj.mavenlink__Opportunity__c;
    		Opportunity opp = [select Id, StageName, CloseDate from Opportunity where Id=:sOppIDGlobal];
    		
    		mavenlink.MavenlinkLogExternal.Write(c,m,'('+String.valueOf(sOppIDGlobal)+')'+'Processing Reservation Dates');
    		
    		List<QService__c> Services = new List<QService__c>();
    		
    		try{
    		//Do nothing if project is archvied
    		if(MLProj.mavenlink__Archived__c==true) 
    			return Services;
    		
    		//15-Aug-2018 No More check for Opp Stage Name as this will be controlled by planned start date check
    		//if(opp.StageName!='Won' && opp.StageName!='Invoice'){
	    		List<mavenlink__Mavenlink_Task__c> MLDeliverables = null;
	    		
	    		try{
	            	  MLDeliverables = [select Id, mavenlink__Task_Mavenlink_Id__c, Service__c, Service_Name__c, Service__r.Duration__c,Service__r.Reservation_Date__c, Service__r.Reservation_Expiration_Date__c
	               from mavenlink__Mavenlink_Task__c where mavenlink__Mavenlink_Project__c=:MLProj.Id];
	          	}
	            		catch(Exception e){if(e.getTypeName()=='System.QueryException') MLDeliverables=new List<mavenlink__Mavenlink_Task__c>();
	          	}
	        
	        mavenlink.MavenlinkLogExternal.Write(c,m,'('+String.valueOf(sOppIDGlobal)+')'+'# of deliverables='+MLDeliverables.size());
	    
	    		List<Id> serviceIds = new List<Id>();
		    for(mavenlink__Mavenlink_Task__c task:MLDeliverables){
		      serviceIds.add(task.Service__c);
		    }
		    
		    
		    
		    QualtricsBuildDeliverablesQ.setServiceFields(opp,MLProj,MLDeliverables,Services);
		    
		    
	        mavenlink.MavenlinkLogExternal.Write(c,m,'('+String.valueOf(opp.Id)+')'+'Updating Services in SF='+Services.size());
	        //update Services;
	        
	        //return Services;
	        
        //	}//StageName check
        //	else{
        //		mavenlink.MavenlinkLogExternal.Write(c,m,'('+String.valueOf(sOppIDGlobal)+')'+'Reservation Dates not calculated as Stage Won/Invoice');
        //	}
        	
        	}catch(Exception e){
        		mavenlink.MavenlinkLogExternal.Write(c,m,'Exception '+e.getMessage()+' at line '+e.getLineNumber());
        		throw new QualtricsMLSetResDateException(e.getMessage());
        		//return false;
        }finally{
        		//mavenlink.MavenlinkLogExternal.Save();
        		return Services;
    		}
        	return null;
    }

}