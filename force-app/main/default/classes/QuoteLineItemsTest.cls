@isTest
public class QuoteLineItemsTest {
    static Account acct {get;set;}
    static Opportunity opp {get;set;}
    static Quote quote {get;set;}
    
 
    
    @testsetup
    static void test_setup(){
        TestFactory.disableTriggers();
        acct = TestFactory.createAccount();
        insert acct;
        
        opp = TestFactory.createOpportunity(acct.Id, null);
        insert opp;
        
        quote = TestFactory.createQuote(opp.Id, Test.getStandardPricebookId());
        insert quote; 
    }
    
    static void QueryStartingItems(){
        acct = database.Query('select '+BuildQuery('Account')+' where name = \'test Account\'');
        opp = [SELECT Id from Opportunity where AccountId =: acct.Id LIMIT 1];
        quote = [SELECT Id from Quote where OpportunityId =: opp.Id LIMIT 1];
    }
    
    static string BuildQuery(String obj){
        Map<String, Schema.SObjectField> SobjtField = Schema.getGlobalDescribe().get(obj).getDescribe().fields.getMap();
        string qry = '';
        for (Schema.SObjectField s : SobjtField.values()) {
            qry += s + ',';
        }
        qry = qry.removeEnd(',');
        qry = qry+' from '+ obj;
        return qry;
    }
    
    @isTest static void test_queryOli() {
        QueryStartingItems();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(quote);
        PageReference pageRef = Page.QuoteLineItems;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',quote.Id);
        QuoteLineItems controller = new QuoteLineItems(sc);
        
        Test.startTest();
        controller.queryOli();
        Test.stopTest();
        system.assertNotEquals(controller.oliList,null);
    }
}