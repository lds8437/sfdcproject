public class ContactAccess {
    
    @AuraEnabled
    public static EmailHunterResponse getEmailFromEmailHunter(Id id) {
        Contact record = getContact(id);
        EmailHunterResponse emailHunterResponse = EmailHunter.callAPI(record);
        if (emailHunterResponse != null) {
	        emailHunterResponse.setRecord(record);
        }
		return emailHunterResponse;
    }
    
    @AuraEnabled
    public static void updateEmailAddress(Id id, String email) {
        
        if (!Schema.SObjectType.Contact.isUpdateable()
	            || !Schema.SObjectType.Contact.fields.Email.isUpdateable()) {
            throw new InvalidPermissionsException(InvalidPermissionsException.CONTACT_ACCESS_MESSAGE);
        }
        
        Contact record = new Contact(Id=id);
        record.Email = email;
        update record;
    }

    private static Contact getContact(Id id) {
        
        if (!Schema.SObjectType.Contact.isAccessible()
	            || !Schema.SObjectType.Contact.fields.Id.isAccessible()
	            || !Schema.SObjectType.Contact.fields.Email.isAccessible()
	            || !Schema.SObjectType.Contact.fields.Domain__c.isAccessible()
	            || !Schema.SObjectType.Contact.fields.FirstName.isAccessible()
	            || !Schema.SObjectType.Contact.fields.LastName.isAccessible()
            	|| !Schema.SObjectType.Account.isAccessible()
	            || !Schema.SObjectType.Account.fields.Name.isAccessible()) {
            throw new InvalidPermissionsException(InvalidPermissionsException.CONTACT_ACCESS_MESSAGE);
        }
        
        List<Contact> records =
            [SELECT Id, Email, Domain__c, FirstName, LastName, Account.Name
             FROM Contact
             WHERE Id = : id
             LIMIT 1];
        if (records.size() == 1) {
            return records[0];
        }
        throw new QueryException('Contact not found');
    }
}