@isTest()
public class odo_OpportunityControllerTest {

   
     @IsTest static void testGetClient(){
        //Create Account
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        //Create Client
        Client__c c = new Client__c();
        c.Name = 'Test Client';
        c.Account__c = acc.Id;
        c.Client_Status__c = 'Active';
        insert c;
        
     
        Test.startTest();
        Client__c c1 = odo_OpportunityController.getClient(c.Id);
        //  Client__c c2 = odo_OpportunityController.getClient('123455');
        Test.stopTest();
    }
    
}