public class cpq_Rules {
    @auraEnabled
    public static Service_Bundle_Product_Junction__c[] getServiceBundleProducts(){
        return [SELECT ID, Name, Related_Service_Bundle__r.Bundle__c, Related_Service_Bundle__c FROM Service_Bundle_Product_Junction__c];        
    }
    @auraEnabled
    public static Product_Bundle_Junction__c[] getBundleProducts(){
        return [SELECT ID, Name, Related_Bundle__r.Name, Related_Bundle__c, Related_Product__r.Name FROM Product_Bundle_Junction__c];        
    }
    @auraEnabled
    public static Map<String, String> getBundleNamesById(){
        Map<String, String> bundlemap = new Map<String, String>();
        Bundle__c[] bundles = [SELECT ID, Name FROM Bundle__c];
        for(Bundle__c b : bundles){
            bundlemap.put(String.valueOf(b.Id), b.Name);
        }
        return bundlemap;
    }
}