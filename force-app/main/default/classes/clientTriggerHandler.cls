public with sharing class clientTriggerHandler {
	public static boolean firstRunOnBeforeInsert = true;
	public static boolean firstRunOnAfterInsert = true;
	public static boolean firstRunOnBeforeUpdate = true;
	public static boolean firstRunOnAfterUpdate = true;
	public static boolean firstRunOnBeforeDelete = true;
	public static boolean firstRunOnAfterDelete = true;
	public static boolean firstRunOnUndelete = true;
	
	
	public void OnBeforeInsert(List<Client__c> newObjects){
		if(firstRunOnBeforeInsert){
			firstRunOnBeforeInsert = false;
			ClientAddressUpdate.startUpdateShippingFromBilling(newObjects);
		}
	}
	
	public void OnAfterInsert(List<Client__c> newObjects, map<Id,Client__c> MapNewMap){
		if(firstRunOnAfterInsert){
			firstRunOnAfterInsert = false;
	
		}
	}
	
	public void OnBeforeUpdate(List<Client__c> oldObjects, List<Client__c> updatedObjects, map<Id,Client__c> MapNewMap, map<Id,Client__c> MapOldMap){
		if(firstRunOnBeforeUpdate){
			firstRunOnBeforeUpdate = false;
			ClientAddressUpdate.startUpdateShippingFromBilling(updatedObjects);
		}
	}
	
	public void OnAfterUpdate(List<Client__c> oldObjects, List<Client__c> updatedObjects, map<Id,Client__c> MapNewMap, map<Id,Client__c> MapOldMap){
		if(firstRunOnAfterUpdate){
			firstRunOnAfterUpdate = false;
			cdr_OnboardingUpdates.updateClient(updatedObjects, MapOldMap);
		}
	}
	
	public void OnBeforeDelete(List<Client__c> oldObjects,map<Id,Client__c> MapOldMap){
		if(firstRunOnBeforeDelete){
			firstRunOnBeforeDelete = false;
	
		}
	}
	
	public void OnAfterDelete(List<Client__c> oldObjects, map<Id,Client__c> MapOldMap){
		if(firstRunOnAfterDelete){
			firstRunOnAfterDelete = false;
	
		}
	}
	
	public void OnUndelete(List<Client__c> restoredObjects, map<Id,Client__c> MapNewMap){
		if(firstRunOnUndelete){
			firstRunOnUndelete = false;
			
		}
	}
	
}