public with sharing class QuoteToOpportunityRollup {
	
	public static Void taxRollup(List<Quote> quoteList){
		List<Quote> syncedQuoteList = findSyncedQuotes(quoteList);
		if(syncedQuoteList.size() == 0){
			return;
		}
		Map<Id,Opportunity> opportunityMap = findOpportunitiesFromSyncedQuotes(syncedQuoteList);
		updateOpportunitiesFromQuotes(syncedQuoteList, opportunityMap);
	}

	@testVisible
	private static List<Quote> findSyncedQuotes(List<Quote> quoteList){
		List<Quote> syncedQuoteList = new List<Quote>();
		for(Quote quoteObj : quoteList){
			if(quoteObj.IsSyncing){
				syncedQuoteList.add(quoteObj);
			}
		}
		return syncedQuoteList;
	}

	@testVisible
	private static Map<Id,Opportunity> findOpportunitiesFromSyncedQuotes(List<Quote> syncedQuoteList){
		Set<Id> opportunityIdSet = new Set<Id>();
		for(Quote quoteObj : syncedQuoteList){
			opportunityIdSet.add(quoteObj.OpportunityId);
		}
		Map<Id,Opportunity> opportunityMap = new Map<Id,Opportunity>();
		opportunityMap.putall([SELECT Tax_From_Quote__c FROM Opportunity WHERE Id in :opportunityIdSet]);
		return opportunityMap;
	}

	@testVisible
	private static void updateOpportunitiesFromQuotes(List<Quote> syncedQuoteList, Map<Id,Opportunity> opportunityMap){
		for(Quote quoteObj : syncedQuoteList){
			if(opportunityMap.get(quoteObj.OpportunityId) != null){
				opportunityMap.get(quoteObj.OpportunityId).Tax_From_Quote__c = quoteObj.Tax;
			}			
		}
		Try{
			update opportunityMap.values();
		}
		Catch(exception e){
			system.debug(logginglevel.error, '*****'+e);
		}
	}

}