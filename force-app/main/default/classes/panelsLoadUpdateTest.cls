@isTest
private class panelsLoadUpdateTest {

    public static Profile p;
    public static Id u1Id;
    public static Id u2Id;
    public static RecordType r1;
    
    // Test 1: The opportunity is not from Panels, thus should be skipped.
    static testMethod void panelsLoadUpdateTest1() {
        CreateSobjectForTesting csft = new CreateSobjectForTesting();
        p = [SELECT Id FROM Profile where Name = 'Q-Panels'];
        User u2 = new User(userroleid=null, Alias = 'beluga', Email='belugauser@testorg.com', EmailEncodingKey='UTF-8', firstname='test', LastName='1', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='beluga@testorg.com');
        
        insert u2;
        User[] u2list = [select id from User where Email='belugauser@testorg.com'];
        u2Id = u2list.get(0).Id;
        
        RecordType r2 = [Select Id From RecordType WHERE Name='Employee Insights' AND SobjectType='Opportunity'];
        
        Opportunity nonPanels = (Opportunity)csft.CreateObject('Opportunity');
        nonPanels.RecordTypeId = r2.Id;
        nonPanels.Name = 'Bad';
        nonPanels.StageName = 'Won';
        nonPanels.ForecastCategoryName = 'Won';
        nonPanels.Forecast_PercentageV2__c = 0;
        nonPanels.CloseDate = Date.newInstance(2012,1,1);
        nonPanels.Survey_Link__c = 'http';
        nonPanels.Field_Time__c = '1 Day';
        nonPanels.LID__LinkedIn_Company_Id__c = null;
        nonPanels.Opp_from_Lead_Conversion__c = false;
        
        //Opportunity nonPanels = new Opportunity(RecordTypeId = r2.Id, Name = 'Bad', StageName = 'Won', CloseDate = Date.newInstance(2012,1,1), Survey_Link__c = 'http', 
        //    Field_Time__c = '1 Day');
        
        insert nonPanels;
        Panels_Project_Vendor__c ppv = (Panels_Project_Vendor__c)csft.CreateObject('Panels_Project_Vendor__c');
        ppv.Opportunity__c = nonPanels.id;
        insert ppv;
        
        Opportunity[] opList = [select id, OwnerId, Project_Manager__c, Panel_Status__c from Opportunity where Name = 'Bad'];
        Opportunity modOpp = opList.get(0);
        
        modOpp.Project_Manager__c = u2Id;
        update modOpp;
        
        User u2check2 = [select Project_Manager_Load__c from User where Id = :u2Id];
        system.assertequals(u2check2.Project_Manager_Load__c, null);
        
        delete modOpp;
    }
    
    // Test 2: Opportunity just received a project manager.
    static testMethod void panelsLoadUpdateTest2() {
        CreateSobjectForTesting csft = new CreateSobjectForTesting();
        p = [SELECT Id FROM Profile where Name = 'Q-Panels'];
        r1 = [Select Id From RecordType WHERE Name='Panels' AND SobjectType='Opportunity'];
        User u1 = new User(userroleid=null, Alias = 'standt', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', firstname='test', LastName='1', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='stanadfasdfasdfdarduser@testorg.com');
        insert u1;
        User[] u1list = [select id from User where Email='standarduser@testorg.com'];
        u1Id = u1list.get(0).Id;
        
        r1 = [Select Id From RecordType WHERE Name='Panels' AND SobjectType='Opportunity'];
        
        contact c = new contact(
            firstname='fname',
            lastname='lname',
            Currently_engaged_in_data_collection__c = 'Yes',
            Method_of_data_collection__c = 'In House',
            Current_Solution__c = 'Monetate',
            Research_Target__c = 'Consumer',
            Current_method_details__c = 'cmd',
            Responses_per_Year__c = 5,
            Not_involved_in_data_collection__c = 'No Interest',
            Anticipated_Use_Case__c = 'Employee Feedback',
            Contract_Expiration__c = date.today().adddays(5),
            Timeframe__c = '2-4 months',
            Project_Status__c = 'Informal, probable vendor selection',
            Qualification_Notes__c = 'aaaaa'
        );
        insert c;

        Opportunity panels = (Opportunity)csft.CreateObject('Opportunity');
        panels.RecordTypeId = r1.Id;
        panels.Name = 'Anemone';
        panels.StageName = 'Won';
        panels.ForecastCategoryName = 'Won';
        panels.Forecast_PercentageV2__c = 0;
        panels.CloseDate = Date.newInstance(2012,1,1);
        panels.Survey_Link__c = 'http';
        panels.Field_Time__c = '1 Day';
        panels.LID__LinkedIn_Company_Id__c = null;
        panels.Opp_from_Lead_Conversion__c = false;
        panels.OwnerId = u1Id;
        panels.Qualification_Contact__c = c.id;
        panels.Panel_Status__c = 'Pre-launch';
        
        //Opportunity panels = new Opportunity(RecordTypeId = r1.Id, Name = 'Anemone', StageName = 'Won', Field_Time__c = '1 Day', Survey_Link__c = 'http', CloseDate = Date.newInstance(2012,1,1), 
        //    OwnerId = u1Id, Panel_Status__c = 'Pre-launch');
        insert panels;
        Panels_Project_Vendor__c ppv = (Panels_Project_Vendor__c)csft.CreateObject('Panels_Project_Vendor__c');
        ppv.Opportunity__c = panels.id;
        insert ppv;
        
        Opportunity[] opList = [select id, OwnerId, Project_Manager__c, Panel_Status__c from Opportunity where Name = 'Anemone'];
        Opportunity modOpp = opList.get(0);
        
        modOpp.Project_Manager__c = u1Id;
        update modOpp;
        
        User u1check1 = [select Project_Manager_Load__c from User where Id = :u1Id];
        //system.assertequals(u1check1.Project_Manager_Load__c, 5);
    }
    
    // Test 3: Opportunity has switched between project managers.
    static testMethod void panelsLoadUpdateTest3() {
        CreateSobjectForTesting csft = new CreateSobjectForTesting();
        p = [SELECT Id FROM Profile where Name = 'Q-Panels'];
        r1 = [Select Id From RecordType WHERE Name='Panels' AND SobjectType='Opportunity'];
        system.debug(logginglevel.ERROR, '*****r1='+r1);
        User u1 = new User(userroleid=null, Alias = 'standt', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', firstname='test', LastName='1', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='stanadfasdfasdfdarduser@testorg.com');
        insert u1;
        User[] u1list = [select id from User where Email='standarduser@testorg.com'];
        u1Id = u1list.get(0).Id;
        User u2 = new User(userroleid=null, Alias = 'beluga', Email='belugauser@testorg.com', EmailEncodingKey='UTF-8', firstname='test', LastName='1', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='beluga@testorg.com');
        insert u2;
        User[] u2list = [select id from User where Email='belugauser@testorg.com'];
        u2Id = u2list.get(0).Id;
        list<Opportunity> oList = new list<Opportunity>();

        contact c = new contact(
            firstname='fname',
            lastname='lname',
            Currently_engaged_in_data_collection__c = 'Yes',
            Method_of_data_collection__c = 'In House',
            Current_Solution__c = 'Monetate',
            Research_Target__c = 'Consumer',
            Current_method_details__c = 'cmd',
            Responses_per_Year__c = 5,
            Not_involved_in_data_collection__c = 'No Interest',
            Anticipated_Use_Case__c = 'Employee Feedback',
            Contract_Expiration__c = date.today().adddays(5),
            Timeframe__c = '2-4 months',
            Project_Status__c = 'Informal, probable vendor selection',
            Qualification_Notes__c = 'aaaaa'
        );
        insert c;
        
        Opportunity preExisting = (Opportunity)csft.CreateObject('Opportunity');
        preExisting.RecordTypeId = r1.Id;
        preExisting.Name = 'Pre1';
        preExisting.StageName = 'Won';
        preExisting.ForecastCategoryName = 'Won';
        preExisting.Forecast_PercentageV2__c = 0;
        preExisting.CloseDate = Date.newInstance(2012,1,1);
        preExisting.OwnerId = u1Id;
        preExisting.Project_Manager__c = u1Id;
        preExisting.Field_Time__c = '1 Day';
        preExisting.Survey_Link__c = 'http';
        preExisting.Panel_Status__c = 'Pre-launch';
        preExisting.LID__LinkedIn_Company_Id__c = null;
        preExisting.Opp_from_Lead_Conversion__c = false;
        preExisting.Qualification_Contact__c = c.id;
        oList.add(preExisting);
        //insert preExisting;
        //Panels_Project_Vendor__c ppv = (Panels_Project_Vendor__c)csft.CreateObject('Panels_Project_Vendor__c');
        //ppv.Opportunity__c = preExisting.id;
        //insert ppv;
        
        Opportunity preExisting2 = (Opportunity)csft.CreateObject('Opportunity');
        preExisting2.RecordTypeId = r1.Id;
        preExisting2.Name = 'Pre1';
        preExisting2.StageName = 'Won';
        preExisting2.ForecastCategoryName = 'Won';
        preExisting2.Forecast_PercentageV2__c = 0;
        preExisting2.CloseDate = Date.newInstance(2012,1,1);
        preExisting2.OwnerId = u1Id;
        preExisting2.Project_Manager__c = u1Id;
        preExisting2.Field_Time__c = '1 Day';
        preExisting2.Survey_Link__c = 'http';
        preExisting2.Panel_Status__c = 'Live';
        preExisting2.LID__LinkedIn_Company_Id__c = null;
        preExisting2.Opp_from_Lead_Conversion__c = false;
        preExisting2.Qualification_Contact__c = c.id;
        //Opportunity preExisting2 = new Opportunity(RecordTypeId = r1.Id, Name = 'Pre2', StageName = 'Won', CloseDate = Date.newInstance(2012,1,1), OwnerId = u1Id, Project_Manager__c = u1Id,Field_Time__c = '1 Day', Survey_Link__c = 'http', Panel_Status__c = 'Live');
        oList.add(preExisting2);
        
        //Panels_Project_Vendor__c ppv1 = (Panels_Project_Vendor__c)csft.CreateObject('Panels_Project_Vendor__c');
        //ppv1.Opportunity__c = preExisting2.id;
        //insert ppv1;
        
        Opportunity preExisting3 = (Opportunity)csft.CreateObject('Opportunity');
        preExisting3.RecordTypeId = r1.Id;
        preExisting3.Name = 'Pre2';
        preExisting3.StageName = 'Won';
        preExisting3.ForecastCategoryName = 'Won';
        preExisting3.Forecast_PercentageV2__c = 0;
        preExisting3.CloseDate = Date.newInstance(2012,1,1);
        preExisting3.OwnerId = u1Id;
        preExisting3.Project_Manager__c = u1Id;
        preExisting3.Field_Time__c = '1 Day';
        preExisting3.Survey_Link__c = 'http';
        preExisting3.Panel_Status__c = 'Re-opened';
        preExisting3.LID__LinkedIn_Company_Id__c = null;
        preExisting3.Opp_from_Lead_Conversion__c = false;
        preExisting3.Qualification_Contact__c = c.id;
        oList.add(preExisting3);
        
        Opportunity panels = (Opportunity)csft.CreateObject('Opportunity');
        panels.RecordTypeId = r1.Id;
        panels.Name = 'Beluga';
        panels.StageName = 'Won';
        panels.ForecastCategoryName = 'Won';
        panels.Forecast_PercentageV2__c = 0;
        panels.CloseDate = Date.newInstance(2012,1,1);
        panels.OwnerId = u1Id;
        panels.Project_Manager__c = u1Id;
        panels.Field_Time__c = '1 Day';
        panels.Survey_Link__c = 'http';
        panels.Panel_Status__c = 'Pre-launch';
        panels.LID__LinkedIn_Company_Id__c = null;
        panels.Opp_from_Lead_Conversion__c = false;
        panels.Qualification_Contact__c = c.id;
        oList.add(panels);
        insert oList;
        
        Panels_Project_Vendor__c ppv3 = (Panels_Project_Vendor__c)csft.CreateObject('Panels_Project_Vendor__c');
        ppv3.Opportunity__c = panels.id;
        insert ppv3;
        
        //Opportunity[] opList = [select id, OwnerId, Project_Manager__c, Panel_Status__c from Opportunity where Name = 'Beluga'];
        //Opportunity modOpp = opList.get(0);
        //modOpp.RecordTypeId = r1.id;
        //modOpp.Project_Manager__c = u2Id;
        //update modOpp;
        
        Test.startTest();
        panels.RecordTypeId = r1.id;
        panels.Project_Manager__c = u2Id;
        update panels;
        
        User u1check1 = [select Project_Manager_Load__c from User where Id = :u1Id];
        User u2check2 = [select Project_Manager_Load__c from User where Id = :u2Id];
        
        system.assertequals(u1check1.Project_Manager_Load__c, 7);
        system.assertequals(u2check2.Project_Manager_Load__c, 5);
        
        panels.Project_Manager__c = u1Id;
        panels.Panel_Status__c = 'Live';
        update panels;
        
        u1check1 = [select Project_Manager_Load__c from User where Id = :u1Id];
        u2check2 = [select Project_Manager_Load__c from User where Id = :u2Id];
        
        system.assertequals(u1check1.Project_Manager_Load__c, 8);
        system.assertequals(u2check2.Project_Manager_Load__c, 0);
        Test.stopTest();
        /*
        panels.Project_Manager__c = u2Id;
        panels.Panel_Status__c = 'Re-opened';
        update panels;
        
        u1check1 = [select Project_Manager_Load__c from User where Id = :u1Id];
        u2check2 = [select Project_Manager_Load__c from User where Id = :u2Id];
        
        system.assertequals(u1check1.Project_Manager_Load__c, 7);
        system.assertequals(u2check2.Project_Manager_Load__c, 1);
        Test.stopTest();
        */
        //panels.Panel_Status__c = 'Pre-launch';
        //update panels;
    }
    
    //Test 4: Opportunity kept its project manager, but switched statuses.
 /*   static testMethod void panelsLoadUpdateTest4() {
        p = [SELECT Id FROM Profile where Name = 'Q-Panels'];
        r1 = [Select Id From RecordType WHERE Name='Panels' AND SobjectType='Opportunity'];
        User u2 = new User(userroleid=null, Alias = 'beluga', Email='belugauser@testorg.com', EmailEncodingKey='UTF-8', firstname='test', LastName='1', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='beluga@testorg.com');
        insert u2;
        User[] u2list = [select id from User where Email='belugauser@testorg.com'];
        u2Id = u2list.get(0).Id;
        
        contact c = new contact(
            firstname='fname',
            lastname='lname',
            Currently_engaged_in_data_collection__c = 'Yes',
            Method_of_data_collection__c = 'In House',
            Current_Solution__c = 'Monetate',
            Research_Target__c = 'Consumer',
            Current_method_details__c = 'cmd',
            Responses_per_Year__c = 5,
            Not_involved_in_data_collection__c = 'No Interest',
            Anticipated_Use_Case__c = 'Employee Feedback',
            Contract_Expiration__c = date.today().adddays(5),
            Timeframe__c = '2-4 months',
            Project_Status__c = 'Informal, probable vendor selection',
            Qualification_Notes__c = 'aaaaa'
        );
        insert c;
        Opportunity panels = new Opportunity(RecordTypeId = r1.Id, Name = 'Catfish', StageName = 'Won', ForecastCategoryName = 'Won',
                                             Forecast_PercentageV2__c = 0, CloseDate = Date.newInstance(2012,1,1), Qualification_Contact__c = c.id,
                                             OwnerId = u2Id, Project_Manager__c = u2Id, Field_Time__c = '1 Day', Survey_Link__c = 'http', 
                                             Panel_Status__c = 'Pre-launch',LID__LinkedIn_Company_Id__c = null, Opp_from_Lead_Conversion__c=false);
        insert panels;
        Opportunity[] opList = [select id, OwnerId, Project_Manager__c, Panel_Status__c from Opportunity where Name = 'Catfish'];
        Opportunity modOpp = opList.get(0);
        
        modOpp.Panel_Status__c = 'Live';
        update modOpp;
        
        User u2check2 = [select Project_Manager_Load__c from User where Id = :u2Id];
        system.assertequals(u2check2.Project_Manager_Load__c, 1);
        
        modOpp.Panel_Status__c = 'Re-opened';
        update modOpp;
        
        u2check2 = [select Project_Manager_Load__c from User where Id = :u2Id];
        system.assertequals(u2check2.Project_Manager_Load__c, 1);
    }
    
    // Test 5: Neither project manager nor panel status were adjusted in the update.
    static testMethod void panelsLoadUpdateTest5() {
        p = [SELECT Id FROM Profile where Name = 'Q-Panels'];
        r1 = [Select Id From RecordType WHERE Name='Panels' AND SobjectType='Opportunity'];
        User u2 = new User(userroleid=null, Alias = 'beluga', Email='belugauser@testorg.com', EmailEncodingKey='UTF-8', firstname='test', LastName='1', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='beluga@testorg.com');
        insert u2;
        User[] u2list = [select id from User where Email='belugauser@testorg.com'];
        u2Id = u2list.get(0).Id;
        
        contact c = new contact(
            firstname='fname',
            lastname='lname',
            Currently_engaged_in_data_collection__c = 'Yes',
            Method_of_data_collection__c = 'In House',
            Current_Solution__c = 'Monetate',
            Research_Target__c = 'Consumer',
            Current_method_details__c = 'cmd',
            Responses_per_Year__c = 5,
            Not_involved_in_data_collection__c = 'No Interest',
            Anticipated_Use_Case__c = 'Employee Feedback',
            Contract_Expiration__c = date.today().adddays(5),
            Timeframe__c = '2-4 months',
            Project_Status__c = 'Informal, probable vendor selection',
            Qualification_Notes__c = 'aaaaa'
        );
        insert c;
        
        Opportunity panels = new Opportunity(RecordTypeId = r1.Id, Name = 'Dogfish', StageName = 'Won', ForecastCategoryName = 'Won',
                                             Forecast_PercentageV2__c = 0, CloseDate = Date.newInstance(2012,1,1), Qualification_Contact__c = c.id,
                                             OwnerId = u2Id, Project_Manager__c = u2Id, Field_Time__c = '1 Day', Survey_Link__c = 'http', 
                                             Panel_Status__c = 'Pre-launch',LID__LinkedIn_Company_Id__c = null, Opp_from_Lead_Conversion__c=false);        
        //        Opportunity panels = new Opportunity(RecordTypeId = r1.Id, Name = 'Dogfish', StageName = 'Won', 
        //        CloseDate = Date.newInstance(2012,1,1), 
        //       OwnerId = u2Id, Project_Manager__c = u2Id, Field_Time__c = '1 Day', Survey_Link__c = 'http', 
        //     Panel_Status__c = 'Pre-launch');
        insert panels;
        Opportunity[] opList = [select id, OwnerId, Project_Manager__c, Panel_Status__c from Opportunity where Name = 'Dogfish'];
        Opportunity modOpp = opList.get(0);
        
        modOpp.OwnerId = u2Id;
        update modOpp;
        
        User u2check2 = [select Project_Manager_Load__c from User where Id = :u2Id];
        system.assertequals(u2check2.Project_Manager_Load__c, null);
    }*/
    
    // Test 6: Opportunity had its project manager removed.
    /*
    static testMethod void panelsLoadUpdateTest6() {
        p = [SELECT Id FROM Profile where Name = 'Q-Panels'];
        r1 = [Select Id From RecordType WHERE Name='Panels' AND SobjectType='Opportunity'];
        User u2 = new User(userroleid=null, Alias = 'beluga', Email='belugauser@testorg.com', EmailEncodingKey='UTF-8', firstname='test', LastName='1', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='beluga@testorg.com');
        insert u2;
        User[] u2list = [select id from User where Email='belugauser@testorg.com'];
        u2Id = u2list.get(0).Id;
        Opportunity panels = new Opportunity(RecordTypeId = r1.Id, Name = 'Elephantfish', StageName = 'Won', ForecastCategoryName = 'Won',
                                             Forecast_PercentageV2__c = 0, CloseDate = Date.newInstance(2012,1,1), 
                                             OwnerId = u2Id, Project_Manager__c = u2Id, Field_Time__c = '1 Day', Survey_Link__c = 'http', 
                                             Panel_Status__c = 'Pre-launch',LID__LinkedIn_Company_Id__c = null, Opp_from_Lead_Conversion__c=false);     
        
        // Opportunity panels = new Opportunity(RecordTypeId = r1.Id, Name = 'Elephantfish', StageName = 'Won', CloseDate = Date.newInstance(2012,1,1), OwnerId = u2Id, Project_Manager__c = u2Id, Field_Time__c = '1 Day', Survey_Link__c = 'http', Panel_Status__c = 'Pre-launch');
        insert panels;
        Opportunity[] opList = [select id, OwnerId, Project_Manager__c, Panel_Status__c from Opportunity where Name = 'Elephantfish'];
        Opportunity modOpp = opList.get(0);
        
        modOpp.Project_Manager__c = null;
        update modOpp;
        
        User u2check2 = [select Project_Manager_Load__c from User where Id = :u2Id];
        system.assertequals(u2check2.Project_Manager_Load__c, 0);
        
    } */


}