public with sharing class CompletionCriteria implements Comparable {

    /**
     * Private variables tightly coupled with the fields
     * on the completion criteria custom metadata object.
     */
	private Id id {get;set;}
	private String label {get;set;}
	private String resultsFieldName {get;set;}
    private Decimal displayOrder {get;set;}
    
    /**
     * Calculated private variables.
     */
	private Boolean isComplete {get;set;}
    
    /**
     * Class Constructor
     * 
     * The custom metadata record is used to populate
     * a majority of the properties within this class.
     * The SObject provided is used to determine if
     * the checkbox should be checked or not.
     */
    public CompletionCriteria(Completion_Criteria__mdt record, SObject sObj) {
        init(record, sObj);
    }
    
    /**
     * All variables are populated during initialization.
     */
    private void init(Completion_Criteria__mdt record, SObject sObj) {
        
        this.id = record.Id;
        this.label = record.Label__c;
        this.resultsFieldName = record.Results_Field_Name__c;
        this.displayOrder = record.Display_Order__c;
        this.isComplete = RecordUtilities.getBooleanFieldValue(sObj, record.Results_Field_Name__c);
    }
    
    /**
     * Required because the current class implements the
     * comparable interface.  This allows instances of
     * this class to be ordered using the sort command.
     */
    public Integer compareTo(Object otherRecord) {
        
        CompletionCriteria otherCompletionCriteria = (CompletionCriteria)otherRecord;
		Decimal thisDisplayOrder = this.getDisplayOrder();
        Decimal otherDisplayOrder = otherCompletionCriteria.getDisplayOrder();
        
        if (thisDisplayOrder < otherDisplayOrder) {
            return -1;
        } else if (otherDisplayOrder < thisDisplayOrder) {
            return 1;
        }
        return 0;
    }
    
    /**
     * Properties exposed to lightning components.
     */
    @AuraEnabled
    public Id getId() {
        return this.id;
    }
    
    @AuraEnabled
    public String getLabel() {
        return this.label;
    }
    
    @AuraEnabled
    public String getResultsFieldName() {
        return this.resultsFieldName;
    }
    
    @AuraEnabled
    public Boolean getIsComplete() {
        return this.isComplete;
    }
    
    @AuraEnabled
    public Decimal getDisplayOrder() {
        
        // If the display order is not populated we
        // return a 0.  Numeric values are required
        // to properly sort instances of this class.
        return this.displayOrder == null ? 0 : this.displayOrder;
    }
}