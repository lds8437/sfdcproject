@isTest
public with sharing class LeadAcceptHandlerTest {
	
	static testMethod void leadHandler_Test() {

		lead l = new lead(firstname='Test ', lastname='Lead', Product__c='Research Suite;360', company='Test Company', Status='Qualified');
        insert l;
		
		test.startTest();	// test start
		
		LeadAcceptHandler.updateLead(l.id);
		
		list<Lead> leadrecd= new list<Lead>();
		leadrecd=[Select id,Status,OwnerId from Lead where id=:l.id];
		system.debug('*****'+leadrecd[0].Status);
		
		system.assertequals(leadrecd[0].Status, 'Working');
        system.assertequals(leadrecd[0].OwnerId,UserInfo.getUserId());
        
        test.stopTest();	// test stop
	}//leadHandler_Test
}