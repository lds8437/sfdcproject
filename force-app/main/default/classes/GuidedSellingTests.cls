@IsTest
public with sharing class GuidedSellingTests {
    
    testMethod static void testCompletionCriteria() {

        Opportunity sObj = GuidedSellingTestUtilities.createOpportunity();
        List<Completion_Criteria__mdt> completionCriteriaRecords = GuidedSellingTestUtilities.getAllCompletionCriteriaRecords();
        List<CompletionCriteria> listOfCompletionCriteria = new List<CompletionCriteria>();
        CompletionCriteria completionCriteria;
        
        Test.startTest();
        
        for (Completion_Criteria__mdt record : completionCriteriaRecords) {
            completionCriteria = new CompletionCriteria(record, sObj);
            System.assertEquals(record.Id, completionCriteria.getId());
            System.assertEquals(record.Label__c, completionCriteria.getLabel());
            System.assertEquals(record.Results_Field_Name__c, completionCriteria.getResultsFieldName());
            System.assertEquals(RecordUtilities.getBooleanFieldValue(sObj, record.Results_Field_Name__c), completionCriteria.getIsComplete());
            if (record.Display_Order__c == null) {
	            System.assertEquals(0, completionCriteria.getDisplayOrder());
            } else {
	            System.assertEquals(record.Display_Order__c, completionCriteria.getDisplayOrder());
            }
            listOfCompletionCriteria.add(completionCriteria);
        }
        listOfCompletionCriteria.sort();
        
        Test.stopTest();
    }
    
    testMethod static void testDynamicAction() {

        Opportunity sObj = GuidedSellingTestUtilities.createOpportunity();
        List<Dynamic_Action_Location__mdt> dynamicActionLocations = GuidedSellingTestUtilities.getAllGlobalDynamicActionLocations();
        List<DynamicAction> dynamicActions = new List<DynamicAction>();
        DynamicAction dynamicAction;
        Test.startTest();
        
        for (Dynamic_Action_Location__mdt dynamicActionLocation : dynamicActionLocations) {
            dynamicAction = new DynamicAction(dynamicActionLocation, sObj);
            dynamicActions.add(dynamicAction);
            System.assertEquals(dynamicActionLocation.Dynamic_Action__r.Label__c, dynamicAction.getLabel());
            System.assertEquals(dynamicActionLocation.Dynamic_Action__r.User_Interface__c, dynamicAction.getUserInterface());
            System.assertEquals(dynamicActionLocation.Dynamic_Action__r.Destination_Type__c, dynamicAction.getDestinationType());
            System.assertEquals(dynamicActionLocation.Dynamic_Action__r.Go_to_Destination__c, dynamicAction.getGoToDestination());
            System.assertEquals(dynamicActionLocation.Dynamic_Action__r.Open_in_New_Window__c, dynamicAction.getOpenInNewWindow());
            System.assertEquals(dynamicActionLocation.Dynamic_Action__r.Modal_Title__c, dynamicAction.getModalTitle());
            System.assertEquals(dynamicActionLocation.Dynamic_Action__r.Record_Id_Output_Parameter_Name__c, dynamicAction.getRecordIdOutputParameterName());
            System.assertEquals(dynamicActionLocation.Dynamic_Action__r.User_Id_Output_Parameter_Name__c, dynamicAction.getUserIdOutputParameterName());
            System.assertEquals(RecordUtilities.getBooleanFieldValue(sObj, dynamicAction.getControllingCheckboxFieldName(), true), dynamicAction.getDisplay());
            if (dynamicActionLocation.Display_Order__c == null) {
	            System.assertEquals(0, dynamicAction.getDisplayOrder());
            } else {
                System.assertEquals(dynamicActionLocation.Display_Order__c, dynamicAction.getDisplayOrder());
            }
        }
        dynamicActions.sort();
        CompletionCriteriaContent completionCriteriaContent = new completionCriteriaContent('TEST');
		List<dynamicAction> buttons = completionCriteriaContent.getButtons();
        system.debug(buttons);
        List<dynamicAction> hyperlinks = completionCriteriaContent.getHyperlinks();
        system.debug(hyperlinks);
        string text = completionCriteriaContent.getText();
        system.debug(text);
        Test.stopTest();
    }

    testMethod static void testStageProgress() {

        Guided_Sales__c sObj = GuidedSellingTestUtilities.createGuidedSalesRecord();
        List<Stage_Details__mdt> allStageDetails = GuidedSellingTestUtilities.getAllStageDetails('Opportunity');

        StageProgress stageProgress;
        Decimal completionPercentage;
        
        Test.startTest();
        
        for (Stage_Details__mdt stageDetails : allStageDetails) {
            
            // Not populated
            stageProgress = new StageProgress(stageDetails, sObj);
            System.assertEquals(stageDetails.Field_Value__c, stageProgress.getFieldValue());
            System.assertEquals(stageDetails.Display_Order__c, stageProgress.getDisplayOrder());
            completionPercentage = stageProgress.getCompletionPercentage();
            
            // Populate
			for (Completion_Criteria__mdt completionCriteria : stageDetails.Completion_Criteria__r) {
                try {
	                sObj.put(completionCriteria.Results_Field_Name__c, true);
                } catch (Exception e) {
                    System.debug('Invalid field specified: ' + e);
                }
            }
            
            // Populated
            stageProgress = new StageProgress(stageDetails, sObj);
            System.assertEquals(stageDetails.Field_Value__c, stageProgress.getFieldValue());
            System.assertEquals(stageDetails.Display_Order__c, stageProgress.getDisplayOrder());
            completionPercentage = stageProgress.getCompletionPercentage();
        }
        
        Test.stopTest();
    }
    
    testMethod static void testStageDetailsController() {

        Opportunity sObj = GuidedSellingTestUtilities.createOpportunity();
        final String SCOPE = 'Research';
        final String FIELD_NAME = 'Guided_Sales_Path__c';
        final String FIELD_VALUE = 'Discovery';
        
        final String UPDATE_FIELD_NAME = 'Name';
        final String UPDATE_FIELD_VALUE = 'Test';
        
        List<CompletionCriteria> completionCriteria;
        List<DynamicAction> globalDynamicActions;
		List<StageProgress> stageProgress;
        List<OpportunityTeamMember> opportunityTeamMembers;
        
        Test.startTest();

        // Fail
		completionCriteria = StageDetailsController.getCompletionCriteriaRecords(null, sObj.Id, FIELD_NAME, FIELD_VALUE);
        System.assertEquals(0, completionCriteria.size());
        
        globalDynamicActions = StageDetailsController.getGlobalDynamicActions(null, sObj.Id, FIELD_NAME, FIELD_VALUE);
        System.assertEquals(0, globalDynamicActions.size());
        
		stageProgress = StageDetailsController.getStageProgress(SCOPE, null);
        System.assertEquals(0, stageProgress.size());
        
        StageDetailsController.updateFieldValue(null, UPDATE_FIELD_NAME, UPDATE_FIELD_VALUE);
        
        // Succeed
		completionCriteria = StageDetailsController.getCompletionCriteriaRecords(SCOPE, sObj.Id, FIELD_NAME, FIELD_VALUE);
        globalDynamicActions = StageDetailsController.getGlobalDynamicActions(SCOPE, sObj.Id, FIELD_NAME, FIELD_VALUE);
		stageProgress = StageDetailsController.getStageProgress(SCOPE, sObj.Id);

        StageDetailsController.updateFieldValue(sObj.Id, UPDATE_FIELD_NAME, UPDATE_FIELD_VALUE);
        
        Test.stopTest();
    }
}