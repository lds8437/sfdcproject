public class PanelDemographicsCountryScreen {
    
    private List<Opportunity> screeningOpps;
    
    public PanelDemographicsCountryScreen(List<Opportunity> opportunities){
        this.screeningOpps = opportunities;
        
    }
    
    public PanelDemographicsCountryScreen(){
        
    }
    
    public static void executeScreen(List<Opportunity> screeningOpps){
        list<Opportunity> oppsToUpdate = new List<Opportunity>();
        for(Opportunity opportunity : screeningOpps){
            if(checkCountries(opportunity)&& opportunity.Consolidated_Screening_Cleared__c == null){
				//opportunity.Consolidated_Screening_Hit__c = true;
                createApproval(opportunity);
            }
        }
    }
    
    private static Boolean checkCountries(Opportunity opportunity){
        if(opportunity.Countries__c != null && (opportunity.Countries__c.contains('Iran') || 
           opportunity.Countries__c.contains('Cuba') || opportunity.Countries__c.contains('North Korea') || 
           opportunity.Countries__c.contains('Syria') || opportunity.Countries__c.contains('Crimea'))){
           return true;
        }
        else if(opportunity.Panel_Demographics__c != null && (opportunity.Panel_Demographics__c.contains('Iran') || 
               opportunity.Panel_Demographics__c.contains('Cuba') || opportunity.Panel_Demographics__c.contains('North Korea') || 
               opportunity.Panel_Demographics__c.contains('Syria') || opportunity.Panel_Demographics__c.contains('Crimea'))){
               return true;
        }
        else{
            return false;
        }
    }
    
    private static String createComments(Opportunity opportunity){
        String comments = 'Research Services Country Flagged\n'+'Countries: '+ opportunity.Countries__c + 
            '\n' + 'Panel Demographics: ' + opportunity.Panel_Demographics__c;
        return comments;
    }
    
    private static void createApproval(Opportunity opportunity){
        try {
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments(createComments(opportunity));
            req1.setObjectId(opportunity.Id);
            req1.setSubmitterId(UserInfo.getUserId());
            req1.setProcessDefinitionNameOrId('Consolidated_Screening_Approval');
            req1.setSkipEntryCriteria(false);
            Approval.ProcessResult result = Approval.process(req1);
        }
        catch(System.DmlException excp) {
            system.debug(excp);
        }
    }
    
}