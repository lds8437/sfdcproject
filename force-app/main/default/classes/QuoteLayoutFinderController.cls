public class QuoteLayoutFinderController {

	private final String PARAMETER_ID = 'Id';
    
    private final String ERROR_URL = '/' + getRecordId();
    private final String CLASSIC_SUPPORT_URL = '/apex/QuoteLayout?Id=' + getRecordId();
    private final String CLASSIC_SALES_URL = '/apex/QuoteLayout?Id=' + getRecordId();
    private final String LIGHTNING_SUPPORT_URL = '/' + getRecordId();
    private final String LIGHTNING_SALES_URL = '/' + getRecordId();
    
    private final Set<String> SUPPORT_PROFILE_NAMES = new Set<String>{
        'System Admin', 'Sales Ops Admin', 'Finance', 'Deal Desk'
    };
    
    private Profile profile;
    
    public QuoteLayoutFinderController() {
        init();
    }
    
    private void init() {
        initProfile();
    }
    
    private void initProfile() {
        List<Profile> profiles =
            [SELECT Id, Name
             FROM Profile
             WHERE Id = : UserInfo.getProfileId()
             LIMIT 1];
        if (profiles.size() == 1) {
            profile = profiles[0];
        }
    }
    
    private String getRecordId() {
        return ApexPages.currentPage().getParameters().get(PARAMETER_ID);
    }
    
    public String getClassicURL() {
        if (profile == null) {
            return ERROR_URL;
        } else if (SUPPORT_PROFILE_NAMES.contains(profile.Name)) {
            return CLASSIC_SUPPORT_URL;
        } else {
            return CLASSIC_SALES_URL;
        }
    }
    
    public String getLightningURL() {
        if (profile == null) {
            return ERROR_URL;
        } else if (SUPPORT_PROFILE_NAMES.contains(profile.Name)) {
            return LIGHTNING_SUPPORT_URL;
        } else {
            return LIGHTNING_SALES_URL;
        }
    }
}