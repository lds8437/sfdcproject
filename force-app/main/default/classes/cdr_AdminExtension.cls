public class cdr_AdminExtension {
 /*private ApexPages.StandardController std;
    public Contract_Document__c cd;
    public List<Attachment> att;
     public String fileName {get;set;}
    public Blob fileBody {get;set;}
     public String var {get;set;}
    public String var2 {get;set;}
    public List<echosign_dev1__SIGN_Agreement__c> agreement;
    
    
    public cdr_AdminExtension(ApexPages.StandardController stdCtrl)
    {
        std=stdCtrl;
    }
    
    public Opportunity getOpportunity()
    {
        return(Opportunity) std.getRecord();
    }
    
    private boolean updateCd()
    {
        boolean result=true;
        if(null!=cd){
            Contract_Document__c updCd=new Contract_Document__c();
            
            try{
                update cd;
            }
            catch (Exception e)
            {
                string msg=e.getMessage();
                integer pos;
                if(-1==(pos-msg.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')))
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
                }
                result=false;
            }
        }
         return result;
         }
    
    public PageReference save(){
        Boolean result=true;
        PageReference pr=Page.cdr_AdminPage;
        if(null!=getOpportunity().id)
        {
            result=updateCd();
        }
        else
        {
            pr.SetRedirect(true);
        }
        if(result)
        {
            std.save();
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Changes saved'));
        }
        pr.getParameters().put('id', getOpportunity().id);
        
        return pr;
    }
    
    public Contract_Document__c getCd()
    {
        if((null!=getOpportunity().id) && (cd == null))
        {
            cd = [SELECT ID, Name, Custom_Contract_Status__c, Status__c
                  FROM Contract_Document__c 
                  WHERE Opportunity__c =:getOpportunity().Id LIMIT 1];          
        }
        return cd;
    }
    
    public List<Attachment> getAtt(){
        if((null!=getOpportunity().id) && (null!=getCd().Id) ){
            att = [SELECT Id, Name FROM Attachment WHERE ParentId =:getCd().Id];
            
        }
        return att;
    }
   
    // create an actual Attachment record with the contract document as parent
    private Database.SaveResult saveStandardAttachment(Id parentId) {
        Database.SaveResult result;
        
        Attachment attachment = new Attachment();
        attachment.body = this.fileBody;
        attachment.name = this.filename;
        attachment.parentId = parentId;
        result = Database.insert(attachment);
        // reset the file for the view state
        fileBody = Blob.valueOf(' ');
        return result;
    }
    
    public PageReference processUpload() {
        try {           
            
            Database.SaveResult attachmentResult = saveStandardAttachment(cd.Id);
            
            if (attachmentResult == null || !attachmentResult.isSuccess()) {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                                                           'Could not save attachment.'));            
                return null;
            } 
            
        } catch (Exception e) {
            ApexPages.AddMessages(e);
            return null;
        }
        
        PageReference page = ApexPages.currentPage();
        page.setRedirect(true);
        return page;
    }
    
     public pageReference deleteAttachment(){
        
        try{
            Attachment att=new Attachment(id=var);
            delete att;
        }
        catch(Exception e){
            ApexPages.AddMessages(e);
        }
        
        PageReference page = ApexPages.currentPage();
        page.setRedirect(true);
        return page;
    }
    
    public List<echosign_dev1__SIGN_Agreement__c> getAgreement()
    {
        if((null!=getOpportunity().id) && (agreement == null))
        {
            agreement = [SELECT ID, Name, echosign_dev1__Status__c
                  FROM echosign_dev1__SIGN_Agreement__c 
                  WHERE Contract_Document__c =:getCd().Id];          
        }
        return agreement;
    }   
    
    public pageReference deleteAgreement(){
        
        try{
            echosign_dev1__SIGN_Agreement__c agreement=new echosign_dev1__SIGN_Agreement__c(id=var2);
            delete agreement;
        }
        catch(Exception e){
            ApexPages.AddMessages(e);
        }
        
        PageReference page = ApexPages.currentPage();
        page.setRedirect(true);
        return page;
    }*/
    
}