public class QualtricsMavenlinkSchedulable implements System.Schedulable {
	public static Integer OPPORTUNITY_UPDATE_DELAY = 10;

	id opportunityId;
	
	public QualtricsMavenlinkSchedulable(id opportunityId){
		this.opportunityId = opportunityId;
	}

    public void execute(SchedulableContext context){
		try{
			mavenlink.MavenlinkExternal.checkOpportunity(opportunityId);
		} catch(Exception e){system.debug('Error in QualtricsMavenlinkSchedulable ('+String.valueOf(opportunityId)+') = ('+e.getStackTraceString()+') '+e.getMessage());}
	}
}