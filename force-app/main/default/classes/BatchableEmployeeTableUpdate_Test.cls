@isTest
public class BatchableEmployeeTableUpdate_Test {
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
    @testSetup
    static void setup(){
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        List<Employee__c> employees = new List<Employee__c>();
        List<User> users = new List<User>();
        
        //insert 10 employees
        for(Integer i=0; i<10; i++){
            employees.add(new Employee__c(Name='TestEmployee'+i,EmployeeIndentificationNumber__c=12345+i,EmployeeCode__c='Temployee'+i));
        }
        insert employees;
        
        for(Integer i=0; i<10; i++){
            users.add(new User(LastName='TestUser'+i,Email='Test555456854@test.com.qualtrics'+i,Alias='Tcode',
                               Username='Test555456854@test.com.qualtrics'+i,CommunityNickname='test12'+i,LocaleSidKey='en_US',
                               TimeZoneSidKey='GMT',ProfileId=profileId.Id,LanguageLocaleKey='en_US',EmailEncodingKey='UTF-8',
                               EIN__c=12345+i,IsActive=False));
        }
        User thisUser = [SELECT Id from User WHERE Id =:UserInfo.getUserId()];
        System.runAs(thisUser){
            insert users;
        }        
    }
    
    static testmethod void testbatchupdate(){
        Test.startTest();
        BatchableEmployeeTableUpdate b = new BatchableEmployeeTableUpdate();
        Id batchId = Database.executeBatch(b);
        Test.stopTest();
    }
    
    static testmethod void testscheduleupdate(){
        Test.startTest();
        // Schedule the test job
        String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP, 
            new ScheduledEmployeeTableUpdate());
        
        Test.stopTest();
    }
}