public class GuidedSellingConstants {

    public final static String SOBJECT_GUIDED_SALES = 'Guided_Sales__c';
    public final static String RECORD_ID_FIELD_NAME = 'Opportunity__c';
    
    public final static String FIELD_ID = 'Id';
    public final static String FIELD_OPPORTUNITY = 'Opportunity__c';
    public final static String FIELD_RESULTS_FIELD_NAME = 'Results_Field_Name__c';
    public final static String FIELD_CONTROLLING_CHECKBOX_FIELD_NAME = 'Controlling_Checkbox_Field_Name__c';
    
    public final static String USER_INTERFACE_BUTTON = 'Button';
    public final static String USER_INTERFACE_HYPERLINK = 'Hyperlink';
    
    public final static String SOQL_SELECT = 'SELECT';
    public final static String SOQL_FROM = 'FROM';
    public final static String SOQL_WHERE = 'WHERE';
    public final static String SOQL_LIMIT = 'LIMIT';
}