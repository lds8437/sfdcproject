global class BatchableEmployeeTableUpdate implements Database.Batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        // collect the batches of records or objects to be passed to execute
        return Database.getQueryLocator(
            'SELECT ID, EmployeeIndentificationNumber__c, UserName__c FROM Employee__c WHERE Username__c = NULL AND EmployeeIndentificationNumber__c != Null'
        );
    }
    
    global void execute(Database.BatchableContext bc, List<Employee__c> scope){
        // process each batch of records     
        if (!scope.isEmpty()) q2c_UpdateEmployeeSFDCId_Class.updateSFDCId(scope);                          
    }    
    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, 
            JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob
            WHERE Id = :bc.getJobId()];
        System.debug('Job Id: ' + bc.getJobId());
        //String[] sendingTo = new String[]{'sfdcadmins@qualtrics.com'};
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setToAddresses(new String[]{'sfdcadmins@qualtrics.com'});
        email.setSubject('Batch Update: ' + job.Status);
        email.setPlainTextBody('The Employee table record updates finished processing.\nId: ' + job.Id
        + '\nNumberOfErrors: ' + job.NumberOfErrors + '\nJobItemsProcessed: ' + job.JobItemsProcessed + '\nTotalJobItems: ' + job.TotalJobItems);
        
        try{
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }catch(exception e) {System.debug(LoggingLevel.Error, e.getMessage());}
    }        
}