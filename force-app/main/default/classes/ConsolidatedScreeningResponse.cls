public class ConsolidatedScreeningResponse {
    
    private ConsolidatedScreeningManager manager;
    private ConsolidatedScreeningRequest request;
    
    private HTTPResponse apiResponse;
    private List<Object> results;
    
    public ConsolidatedScreeningResponse(ConsolidatedScreeningRequest request, ConsolidatedScreeningManager manger){
        this.manager = manager;
        this.request = request;
        this.results = new List<Object>();
    }
    
    //Parse JSON returned from the API request
    public void parseResponse(HTTPResponse response){
        JSONParser parser = JSON.createParser(response.getBody());
        while (parser.nextToken() != null){
            if(parser.getCurrentToken() == JSONToken.FIELD_NAME && parser.getText() == 'results' && parser.nextToken() == JSONToken.START_ARRAY){
                while(parser.nextToken() != null){
                    if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                        Result result = (Result)parser.readValueAs(Result.class);
                        parser.skipChildren();
                        if(validResponse(result)){
                               results.add(result);
                           }
                    }
                }
            }  
        }
    }
    
    private Boolean validResponse(Result result){
        Boolean isValid;
        if(result != null && result.type != 'Vessel' && result.source != 'ITAR Debarred (DTC) - State Department' 
           && result.type != 'Individual' && result.type != 'Aircraft' && result.score >= 80){
               isValid = true;
           }
        else{
            isValid = false;
        }
        return isValid;
    }
    
    //New object created as an Inner class to store parsed JSON data
    private class Result{
        public String name;
        public String source;
        public String type;
        public String id;
        public Double score;
    }
    
    public List<Object> getResults(){
        return results;
    }
}