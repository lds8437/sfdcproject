/***********
Description: This class will be used to schedule 'CountNumberOfBids_db' batch class
***********/

public class Schedule_CountNumberOfBids_db implements Schedulable {

    //Description: Execute method for scheduling.

   public void execute(SchedulableContext sc) {
      database.executebatch(new CountNumberOfBids_db(),1);
   }
}