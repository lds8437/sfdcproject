public class EmailHunterResponse {

	@AuraEnabled public ResponseData data {get;set;}
	@AuraEnabled public ResponseMeta meta {get;set;}
    
    // Custom properties used to facilitate UI decisions
    @AuraEnabled public Contact record {get;set;}
    @AuraEnabled public Boolean newEmailFound {get;set;}
    
    public EmailHunterResponse() {
        this.data = new ResponseData();
        this.meta = new ResponseMeta();
    }
    
    public class ResponseData {
        
        @AuraEnabled public String first_name {get;set;}
        @AuraEnabled public String last_name {get;set;}
        @AuraEnabled public String email {get;set;}
        @AuraEnabled public Integer score {get;set;}
        @AuraEnabled public String domain {get;set;}
        @AuraEnabled public String position {get;set;}
        @AuraEnabled public String twitter {get;set;}
        @AuraEnabled public String linkedin_url {get;set;}
        @AuraEnabled public String phone_number {get;set;}
        @AuraEnabled public String company {get;set;}
        @AuraEnabled public List<ResponseDataSource> sources {get;set;}
        
        public ResponseData() {
            this.first_name = null;
            this.last_name = null;
            this.email = null;
            this.score = 0;
            this.domain = null;
            this.position = null;
            this.twitter = null;
            this.linkedin_url = null;
            this.phone_number = null;
            this.company = null;
            this.sources = new List<ResponseDataSource>();
        }
    }
    
    public class ResponseDataSource {
        
        @AuraEnabled public String domain {get;set;}
        @AuraEnabled public String uri {get;set;}
        @AuraEnabled public String extracted_on {get;set;}
        @AuraEnabled public String last_seen_on {get;set;}
        @AuraEnabled public Boolean still_on_page {get;set;} 
        
        public ResponseDataSource() {
            this.domain = null;
            this.uri = null;
            this.extracted_on = null;
            this.last_seen_on = null;
            this.still_on_page = null;
        }
    }
    
    public class ResponseMeta {
        
        @AuraEnabled public ResponseMetaParams params {get;set;}
        
        public ResponseMeta() {
            this.params = new ResponseMetaParams();
        }
    }
    
    public class ResponseMetaParams {
        
        @AuraEnabled public String first_name {get;set;}
        @AuraEnabled public String last_name {get;set;}
        @AuraEnabled public String full_name {get;set;}
        @AuraEnabled public String domain {get;set;}
        @AuraEnabled public String company {get;set;}
        
        public ResponseMetaParams() {
            this.first_name = null;
            this.last_name = null;
            this.full_name = null;
            this.domain = null;
            this.company = null;
        }
    }
    
    public void setRecord(Contact record) {
        this.record = record;
        this.newEmailFound = (this.data.email != this.record.Email);
    }
}