global class QualtricsMavenlinkPostArchiveBatch implements Database.Batchable<String>, Database.AllowsCallouts, Database.Stateful{
	private static string c = 'QualtricsMavenlinkPostArchiveBatch';
    
    private String toStatus {get;set;}
    private Id projId {get;set;}
    private Id oppId {get;set;}
    private Id jobId {get;set;}
    
    private static String sOppIDGlobal = null;
    
    private static Integer thisThread = Math.mod(Math.round(Math.random()*1000),150);
    
    public static final String automationUser='00550000006r7sKAAQ';
    
    private static Map<String, Integer> MLStatuses = new Map<String, Integer>{'GreyPipeLine' => 138,'LightGreenOKToStart'=>210,
												'RedCancelled'=>510, 'GreyInactive'=>125};
    
    public QualtricsMavenlinkPostArchiveBatch(Id oppId, Id projId, String toStatus, Id jobId){
		this.toStatus = toStatus;
		this.projId = projId;
		this.oppId = oppId;
		this.jobId = jobId;
		sOppIDGlobal=String.valueof(oppId);
	}
	
	global List<String> start(Database.BatchableContext context) {
        String m = 'start';
        try{
        		mavenlink.MavenlinkLogExternal.Save();
        return new String[]{toStatus};
        } catch(Exception e){mavenlink.MavenlinkLogExternal.Write(c,m,'Error = ('+e.getStackTraceString()+') '+e.getMessage()); mavenlink.MavenlinkLogExternal.Save(); return null;}
    }
	
	global void execute(Database.BatchableContext context, List<String> strings){
    	String m = 'execute';
    	try{
    		mavenlink__Mavenlink_Project__c MLProj = [select mavenlink__Mavenlink_Id__c from mavenlink__Mavenlink_Project__c where id=:projId];
    			if(ArchiveProject(oppId))
            		updateStatusAndArchive(MLProj.mavenlink__Mavenlink_Id__c,toStatus);
    		   mavenlink.MavenlinkLogExternal.Save();
        } catch(Exception e){mavenlink.MavenlinkLogExternal.Write(c,m,'Error = ('+e.getStackTraceString()+') '+e.getMessage()); mavenlink.MavenlinkLogExternal.Save();}
    }
    
     global void finish(Database.BatchableContext ctx) {
        String m = 'finish';
        
        
     }
     
     private static Boolean ArchiveProject(Id oppId)
    {
            String m = 'ArchiveProject';
            
            mavenlink__Mavenlink_Project__c MLProj=null;
	    		
            mavenlink.MavenlinkLogExternal.Write(c,m,'('+String.valueOf(oppId)+')'+'{'+thisThread+'}'+'Reached Archive project for project');
            
            Opportunity opp = null;
            try{
            //28-Aug-2018[Subbu] Moved Opp conditions to SOQL and removed Forcast % criteria for SOW4 Release1
            //28-Aug-2018 Changes Service Status condition to like %SOW% for SOW4 Release 1  
            		opp = [select Id, LastModifiedBy.Id, Service_Edited__c from Opportunity where Id=:oppId and SyncedQuoteId!=null 
            						 and CloseDate!=null and StageName not in ('Lost','Cancelled')];
            		
            	}catch(exception e){
            		if(e.getTypeName()=='System.QueryException') {
            			mavenlink.MavenlinkLogExternal.Write(c,m,'('+String.valueOf(oppId)+')'+'{'+thisThread+'}'+ 'Oppportunity Failed initial Check. To be archived');
            			//updateStatus(MLProj.mavenlink__Mavenlink_Id__c);
            			return true;
            		}
            }
            if(String.valueOf(opp.LastModifiedBy.Id)==automationUser) {
        			mavenlink.MavenlinkLogExternal.Write(c,m,'('+String.valueOf(oppId)+')'+'{'+thisThread+'}'+ 'Skipping for automation user '+automationUser);
        			return false;
      		}
            
            mavenlink.MavenlinkLogExternal.Write(c,m,'('+String.valueOf(oppId)+')'+'{'+thisThread+'}'+'Initial test Pass');

            //This is required to avoid archiving in the middle of a sync
            //The flag should be set at the end of the Quote sync job in SF, and at that point
            //is where qualified services should be checked
            if (opp.Service_Edited__c==false){mavenlink.MavenlinkLogExternal.Write(c,m,'('+String.valueOf(oppId)+')'+'{'+thisThread+'}'+'Service edited flag not set'); return false;}
            
			// 25-May-2018: Change to exclude OLIs with certain words in the name                
          	Integer qualifiedServices = [select count() from OpportunityLineItem where OpportunityId=:opp.Id and Service__r.Synced_Service__c=true and Service__r.Status__c like '%SOW%' and (NOT name LIKE '%Markup%') and (NOT name LIKE '%Maintenance%')];
          	if (qualifiedServices>0) return false; // Service found with Status SOW - Dont Archive
          
           	mavenlink.MavenlinkLogExternal.Write(c,m,'('+String.valueOf(oppId)+')'+'{'+thisThread+'}'+'No Qualified Services found-Project to Archive');
          	//updateStatus(MLProj.mavenlink__Mavenlink_Id__c);
          	return true; //no Services found with Status SOW
    }
	
	
	
	private static void updateStatusAndArchive(String wsId,String statusName){
        String m = 'updateStatus';
        
        String testWS = '{'+
    							'"count": 0,'+
    							'"results": ['+
        							'{'+
            							'"key": "workspaces",'+
            							'"id": "22185425"'+
        							'}'+
    							'],'+
    							'"workspaces": {'+
        							'"22185425": {'+
        								'"title": "Qualtrics Account - Quoted Opp",'+
        								'"status": {'+
                							'"color": "red",'+
                							'"key": 510,'+
                							'"message": "Cancelled"'+
            							'}'+
        							'}'+
    							'}'+
    						'}';	
    		
        Map<String,Object> apiJSON = null;
        Boolean archived = false;
        String wsJSON = null;
        try{
	        	if (!Test.isRunningTest()){
		    		apiJSON = mavenlink.MavenlinkExternalAPI.callAPI('/workspaces/'+wsId+'.json?include_archived=false','GET',null);
		    }else{
		    		apiJSON = (Map<String,Object>) JSON.deserializeUntyped(testWS);
		    }
		    
		    mavenlink.MavenlinkLogExternal.Write(c,m,'('+sOppIDGlobal+')'+'{'+thisThread+'}'+ 'apiJSON='+apiJSON);
		        
		    Integer wsCount=apiJSON.get('count')==null?0:(Integer)apiJSON.get('count');
	        	
	        if(wsCount!=1){
		        mavenlink.MavenlinkLogExternal.Write(c,m,'('+sOppIDGlobal+')'+'{'+thisThread+'}'+ 'Project was found archived');
	            archived=true;
	            wsJSON = '{"workspace":{"archived":false}}';
	            apiJSON = mavenlink.MavenlinkExternalAPI.callAPI('/workspaces/'+wsId+'.json','PUT',wsJSON);
	            mavenlink.MavenlinkLogExternal.Write(c,m,'('+sOppIDGlobal+')'+'{'+thisThread+'}'+ 'Project Unarchived');
	            
	            String wsStatusChange = '{"workspace_id":"'+wsId+'","workspace_status_change":{"to_status_key":'+MLStatuses.get(statusName)+'}}';
		        	if (!Test.isRunningTest()){
		        		apiJSON = mavenlink.MavenlinkExternalAPI.callAPI('/workspace_status_changes.json','POST',wsStatusChange);
		        	}
	        }else mavenlink.MavenlinkLogExternal.Write(c,m,'('+sOppIDGlobal+')'+'{'+thisThread+'}'+ 'Project was found not archived. Performing no action');
	    
        }catch(Exception e){
            mavenlink.MavenlinkLogExternal.Write(c,m,'Error = ('+e.getStackTraceString()+') '+e.getMessage());
        }finally{
            if(archived){
                wsJSON = '{"workspace":{"archived":true}}';
                apiJSON = mavenlink.MavenlinkExternalAPI.callAPI('/workspaces/'+wsId+'.json','PUT',wsJSON);
                mavenlink.MavenlinkLogExternal.Write(c,m,'('+sOppIDGlobal+')'+'{'+thisThread+'}'+ 'Project Archived');
            }
        }
    }
}