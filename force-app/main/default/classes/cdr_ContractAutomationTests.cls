@isTest
public class cdr_ContractAutomationTests {
    static testMethod void NewContractTest(){
         AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
    Insert avaCustomSetting;
        List<APXTConga4__Conga_Template__c> conga = new List<APXTConga4__Conga_Template__c>();
        
        APXTConga4__Conga_Template__c conga1 = new APXTConga4__Conga_Template__c ();
        conga1.APXTConga4__Description__c = '1';
        conga1.APXTConga4__Template_Group__c = 'Quote';
        conga1.APXTConga4__Name__c = 'Quote';
        conga.add(conga1);
        
        APXTConga4__Conga_Template__c conga2 = new APXTConga4__Conga_Template__c ();
        conga2.APXTConga4__Description__c = '1';
        conga2.APXTConga4__Template_Group__c = 'QSO Required';
        conga2.APXTConga4__Name__c = 'QSO Required';
        conga.add(conga2);
        
        APXTConga4__Conga_Template__c conga3 = new APXTConga4__Conga_Template__c ();
        conga3.APXTConga4__Description__c = '1';
        conga3.APXTConga4__Template_Group__c = 'QSO';
        conga3.APXTConga4__Name__c = 'QSO with MSA';
        conga.add(conga3);
        
        APXTConga4__Conga_Template__c conga4 = new APXTConga4__Conga_Template__c ();
        conga4.APXTConga4__Description__c = '1';
        conga4.APXTConga4__Template_Group__c = 'QSO';
        conga4.APXTConga4__Name__c = 'QSO Standalone';
        conga.add(conga4);
        
        APXTConga4__Conga_Template__c conga5 = new APXTConga4__Conga_Template__c ();
        conga5.APXTConga4__Description__c = '1';
        conga5.APXTConga4__Template_Group__c = 'Implementation';
        conga5.APXTConga4__Name__c = 'MRCXSTD';
        conga.add(conga5);
        
        APXTConga4__Conga_Template__c conga6 = new APXTConga4__Conga_Template__c ();
        conga6.APXTConga4__Description__c = '1';
        conga6.APXTConga4__Template_Group__c = 'Implementation';
        conga6.APXTConga4__Name__c = 'MRCXPLU';
        conga.add(conga6);
       
       APXTConga4__Conga_Template__c conga7 = new APXTConga4__Conga_Template__c ();
        conga7.APXTConga4__Description__c = '1';
        conga7.APXTConga4__Template_Group__c = 'Quote';
        conga7.APXTConga4__Name__c = 'Quote - Itemized';
        conga.add(conga7);
        
        insert conga;
       system.debug('conga: '+conga);
        
        List<Account> accList = new List<Account>();
        
        Account acc = new Account();
        acc.Name = 'TEST ACCOUNT';
        acc.Tier__c ='A';
        accList.add(acc);
        
        Account acc2 = new Account();
        acc2.Name = 'TEST ACCOUNT2';
        accList.add(acc2);
        
        insert accList;
        
        List<Contact> conList = new List<Contact>();
        
        Contact con = new Contact();
        con.LastName = 'TEST CONTACT';
        con.AccountId = acc.Id;
        conList.add(con);
        
        Contact con3 = new Contact();
        con3.LastName = 'TEST CONTACT';
        con3.AccountId = acc2.Id;
        conList.add(con3);
        
        Contact con2 = new Contact();
        con2.LastName = 'TEST CONTACT2';
        con2.AccountId = acc2.Id;
        conList.add(con2);
        
        Insert conList;
        
        List<Client__c> cliList = new List<Client__c>();
        
        Client__c cli = New Client__c();
        cli.Name = 'TEST CLIENT';
        cli.Billing_City__c = 'Provo';
        cli.Account__c = acc.id;
        cliList.add(cli);  
        
        Client__c cli2 = New Client__c();
        cli2.Name = 'TEST CLIENT2';
        cli2.Account__c = acc2.id;
        cliList.add(cli2); 
        
        Client__c cli3 = New Client__c();
        cli3.Name = 'TEST CLIENT3';
        cli3.Account__c = acc2.id;
        cliList.add(cli3); 
        
        insert cliList;
        
        List<Opportunity> oppList = new List<Opportunity>();
        
        Opportunity opp = (Opportunity)EB_Utility_CreateObjectForTesting.CreateObject('Opportunity');
        Opp.LID__LinkedIn_Company_Id__c = '5';
        opp.Forecast_PercentageV2__c = 0;
        opp.Opp_from_Lead_Conversion__c = false;      
        opp.AccountId = acc.Id;
        opp.Qualification_Contact__c = con.id;
        opp.Client_Use_Case__c = 'Market Research';       
        opp.Key_Risk_Category__c = 'Pricing';
        opp.CurrencyIsoCode = 'USD';
        oppList.add(opp);
        
        Opportunity opp2 = (Opportunity)EB_Utility_CreateObjectForTesting.CreateObject('Opportunity');
        Opp2.LID__LinkedIn_Company_Id__c = '5';
        opp2.Forecast_PercentageV2__c = 0;
        opp2.Opp_from_Lead_Conversion__c = false;      
        opp2.AccountId = acc.Id;
        opp2.Client__c = cli.Id;
        opp2.Qualification_Contact__c = con.id;
        opp2.Key_Risk_Category__c = 'Pricing';
        oppList.add(opp2);
         
        insert oppList;
       
       Contract_Document__c cd = New Contract_Document__c();
       cd.Opportunity__c = opp.id;
       cd.Account__c = acc2.id;
       insert cd;
        system.debug('cd1: '+cd);
        
        Quote quo = new Quote();
        quo.Name = 'TEST QUOTE';
        quo.OpportunityId = opp.Id;
        quo.Quote_Type__c = 'New License';
        quo.RS_User_Type__c = 'Corporate';
        quo.License_Start_Date__c = Date.today();
        quo.License_Term_in_months__c = 12;
        insert quo;   
        
     /*   QuoteLineItem qli = [SELECT Id from QuoteLineItem WHERE Id='0QL5000000310iH' OR Id='0QL4C000000IKvI' LIMIT 1];
        qli.Quantity = 25000;
        update qli;*/
        
        opp.SyncedQuoteId = quo.id;
        opp.Qualification_Contact__c = con3.Id;
        opp.AccountId = acc2.Id;
        update opp;
        
        opp2.AccountId = acc2.Id;
        opp2.Client__c = cli3.Id;
        update opp2;
       
       
        
    //    Contract_Document__c cd = [SELECT ID from Contract_Document__c WHERE Opportunity__c =:opp.id];
       
        cd.MSA__c = TRUE;
       cd.Language_Type__c = 'with MSA';
        cd.Client__c = cli.id;
        update cd;
		system.debug('cd2: '+cd);        
        cli.Billing_City__c = 'OREM';
        update cli;
        
        Service__c ser = new Service__c();
        ser.Name = 'Test Implementation';
        ser.Opportunity__c = opp.id;
        ser.Quote__c = quo.id;
        ser.RecordTypeId = '01250000000Hu2S';
        insert ser;
        
        ser.EstimateNotesForRep__c = 'MRCXSTD';
        update ser;
        system.debug('cd3: '+cd);
        ser.EstimateNotesForRep__c = 'MRCXPLU';
        update ser;
       system.debug('cd4: '+cd);
    cd.Language_Type__c = 'Standalone';
       cd.Quote_Type__c = 'Quote - Itemized';
       update cd;
       system.debug('cd5: '+cd);
   /*      Attachment a = new Attachment();
       a.ParentId = cd.Id;
       a.Body = blob.valueOf('Unit Test Attachment Body');
       a.Name = 'QSO.exec';
       insert a;
       
       Attachment a2 = new Attachment();
       a2.ParentId = cd.Id;
       a2.Body = blob.valueOf('Unit Test Attachment Body');
       a2.Name = 'QSO.final';
       insert a2;
       
     ApexPages.StandardController sc = new ApexPages.StandardController(opp2);
        cdr_GenerateDocumentsExtension cdrCont  = new cdr_GenerateDocumentsExtension(sc);
        
        PageReference pageRef = Page.cdr_GenerateDocs;
        pageRef.getParameters().put('id', String.valueOf(opp.Id));
        pageRef.getParameters().put('Synced_Quote__c', String.valueOf(quo.Id));
        Test.setCurrentPage(pageRef);      */  
        
        
    }
    
  /*  @isTest (seeAllData=true)
    
    static void pageTest(){
        
        Opportunity opp = [SELECT Id from Opportunity WHERE ID = '0065000000ejCfz' LIMIT 1];
        system.debug('opp: '+opp);
        Quote quo = [SELECT ID from Quote WHERE OpportunityId =:opp.id LIMIT 1];
        system.debug('quo: '+quo);
        ApexPages.StandardController sc = new ApexPages.StandardController(opp2);
        cdr_GenerateDocumentsExtension cdrCont  = new cdr_GenerateDocumentsExtension(sc);
        
        PageReference pageRef = Page.cdr_GenerateDocs;
        pageRef.getParameters().put('id', String.valueOf(opp.Id));
        pageRef.getParameters().put('Synced_Quote__c', String.valueOf(quo.Id));
        Test.setCurrentPage(pageRef);        
        
        cdrCont.getOpportunity();
        cdrCont.getCd();   
       // cdrCont.getQli();
      //  cdrCont.getService();
        cdrCont.Save();
        cdrCont.showPanel();
        cdrCont.qsoAdobe();
        cdrCont.qsoDownload();
        cdrCont.qsoView();
        cdrCont.quoteOnly();        
    }*/
    
 /*    @isTest (seeAllData=true)
    static void AdminpageTest(){
        
        Opportunity opp = [SELECT Id from Opportunity WHERE ID = '0065000000ejCfz' LIMIT 1];
        Quote quo = [SELECT ID from Quote WHERE OpportunityId =:opp.id LIMIT 1];
        
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        cdr_AdminExtension cdrCont  = new cdr_AdminExtension(sc);
        
        PageReference pageRef = Page.cdr_AdminPage;
        pageRef.getParameters().put('id', String.valueOf(opp.Id));
        pageRef.getParameters().put('Synced_Quote__c', String.valueOf(quo.Id));
        Test.setCurrentPage(pageRef);        
        
        cdrCont.getOpportunity();
        cdrCont.getCd();  
        cdrCont.Save();
        cdrCont.getAtt();
        cdrCont.deleteAttachment();
        cdrCont.processUpload();
    }*/
  /* @isTest static void qliTest(){
        QuoteLineItem qli = [SELECT Id, Quote_Display_Name0_del__c FROM QuoteLineItem WHERE Id='0QL5000000310iH' OR Id='0QL4C000000IKvI' LIMIT 1];
        
        qli.Included_Themes__c = 2;
        update qli;
        
    }*/
    

   
}