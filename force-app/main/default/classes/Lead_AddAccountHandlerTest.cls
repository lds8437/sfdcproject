/*    
*    Test class for Lead_AddAccountHandler.
*/

@isTest
private class Lead_AddAccountHandlerTest {

  

   /*    When lead gets created then trigger will serach for matching account domain name with lead's email domain, 
   *    And will update lead's Account lookup field with matching account Id.
   */

   /* REMOVED PER WILL BEHUNIN 3/9 */
   

   private Static Account CreateAccount (String strAccountName, String strWebsiteUrl, Id Parent) {
        Account objAccount = new Account();
        objAccount.Name = strAccountName;
        objAccount.Website = strWebsiteUrl;
        objAccount.ParentId = Parent;
        return objAccount;
   }
   
   private Static Lead CreateLead (String strCompany, String strLastName, String strStatus, String strEmailAddress) {
        Lead objLead = new Lead();
        objLead.Company = strCompany;
        objLead.LastName = strLastName;
        objLead.Status = strStatus;
        objLead.Email = strEmailAddress;
        return objLead;
   }
   
   //When the records are inserted through UI //For single Account to Single Lead 
   static testMethod void test_LeadAccount_SingleAccountSingleLead () {
        //Created the Account and Lead records.
        Account objAccount = CreateAccount('test Account', 'www.gmail.com', null);
        objAccount.BillingState = 'Utah';
        objAccount.BillingCountry = 'USA';
        objAccount.BillingPostalCode = '60645';
        database.insert(objAccount);
        
        Lead objLead = CreateLead('test Company', 'test Lead', 'test', 'test@gmail.com');

        test.startTest();        // test start
            database.insert(objLead);
            Lead objLeadUpdate = [Select ID, Account__c From Lead where Id =: objLead.Id];
            //System.assertEquals(objLeadUpdate.Account__c, objAccount.Id);    // System assert                
        test.stopTest();
   }
   
   static testMethod void test_LeadAccont_SingleLeadMultipleAccount () {
        //Create Account records.
        Account objAccountFirst = CreateAccount('test Account', 'www.gmail.com',null);
        objAccountFirst.BillingState = 'Utah';
        objAccountFirst.BillingCountry = 'USA';
        objAccountFirst.BillingPostalCode = '60645';
        database.insert(objAccountFirst);
        
        Account objAccountFirstChild = CreateAccount('test AccountFirst', 'www.gmail.com',objAccountFirst.id);
        objAccountFirstChild.BillingState = 'Utah';
        objAccountFirstChild.BillingCountry = 'USA';
        objAccountFirstChild.BillingPostalCode = '60645';
        database.insert(objAccountFirstChild);
        Account objAccountSecondChild = CreateAccount('test AccountSecond', 'www.gmail.com',objAccountFirst.id);
        objAccountSecondChild.BillingState = 'Utah';
        objAccountSecondChild.BillingCountry = 'USA';
        objAccountSecondChild.BillingPostalCode = '60645';
        database.insert(objAccountSecondChild);
        
        //Create Lead record. 
        Lead objLead = CreateLead('test Company', 'test Lead', 'test', 'test@gmail.com');
        
        test.startTest();        // test start
            database.insert(objLead);
            Lead objLeadUpdate = [Select ID, Account__c From Lead where Id =: objLead.Id];
            
            //System.assertEquals(objLeadUpdate.Account__c, objAccountFirst.Id);    // System assert                
        test.stopTest();
   }

   
}