@isTest
public class OppHelper_Test {
@isTest static void OppHelperTestMethod()
    {
        PageReference pageRef = Page.OppHelper;
        Test.setCurrentPage(pageRef);
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Opportunity opp = (Opportunity)EB_Utility_CreateObjectForTesting.CreateObject('Opportunity');
        Opp.LID__LinkedIn_Company_Id__c = '5';
        opp.Forecast_PercentageV2__c = 0;
        opp.Opp_from_Lead_Conversion__c = true;
        opp.Qualification_Contact__c = NULL;
        insert opp;
        
        Contact con = New Contact();
        con.AccountId = acc.Id;
        con.FirstName= 'Test First';
        con.LastName = 'Test Last';
        insert con;
        
              
        System.currentPagereference().getParameters().put('id',opp.id);
        ApexPages.StandardController stdOpp = new ApexPages.StandardController(opp);
        OppHelper cont = new OppHelper(stdOpp);              
        
        cont.rollupOppContacts();
           
    }
    
   
    
}