/**
 * Run As System
 * The logic within this class runs as system, as opposed to
 * the logged in user.  Security and sharing rules do not apply
 * to the logic within this class.
 * 
 * For security purposes, this class should only be used when
 * absolutely necessary.
 */
public without sharing class RunAsSystem {

    @AuraEnabled
    public static void joinOpportunityTeam(Id opportunityId, Id userId, String accessLevel) {
        OpportunityTeamMember teamMember = new OpportunityTeamMember();
        teamMember.OpportunityId = opportunityId;
        teamMember.UserId = userId;
        teamMember.OpportunityAccessLevel = accessLevel;
        Database.SaveResult result = Database.insert(teamMember, false);
		System.debug(result);
    }
    
    public static void deleteOpportunityTeamMembers(List<OpportunityTeamMember> opportunityTeamMembersToBeDeleted) {
        System.debug('-- deleting opportunity team members --');
        for (OpportunityTeamMember opportunityTeamMemberToBeDeleted : opportunityTeamMembersToBeDeleted) {
            System.debug(opportunityTeamMemberToBeDeleted);
        }
		Database.delete(opportunityTeamMembersToBeDeleted, false);
    }
}