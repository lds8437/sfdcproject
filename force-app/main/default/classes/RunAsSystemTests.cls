@IsTest
public class RunAsSystemTests {

    testMethod static void testJoinOpportunityTeam() {
        
        // Prepare for tests
        final String ACCESS_LEVEL = 'Read';
        
        Account testAccount = new Account();
        testAccount.Name='Test Account';
        insert testAccount;
        
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name='Test Opportunity';
        testOpportunity.AccountId=testAccount.Id;
        testOpportunity.StageName='Pending';
        testOpportunity.CloseDate=System.today();
        testOpportunity.Turn_Off_Initial_CX_EX_RC_Amounts__c=true;
        testOpportunity.Prior_Owner_Id__c = UserInfo.getUserId();
        insert testOpportunity;
        
        Test.startTest();
        
        RunAsSystem.joinOpportunityTeam(testOpportunity.Id, UserInfo.getUserId(), ACCESS_LEVEL);
        
        // Verify the current user is part of the opportunity team.
        Set<Id> opportunityMemberUserIds = new Set<Id>();
        List<OpportunityTeamMember> opportunityTeamMembers = new List<OpportunityTeamMember>();
		for (OpportunityTeamMember opportunityTeamMember : 
				[SELECT Id, UserId
	             FROM OpportunityTeamMember
    	         WHERE OpportunityId = : testOpportunity.Id
                 LIMIT 100]) {
			opportunityMemberUserIds.add(opportunityTeamMember.UserId);
            opportunityTeamMembers.add(opportunityTeamMember);
		}
        System.assert(opportunityMemberUserIds.contains(UserInfo.getUserId()));
        RunAsSystem.deleteOpportunityTeamMembers(opportunityTeamMembers);
        
        Test.stopTest();
    }
}