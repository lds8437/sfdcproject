@isTest
private class EB_Utility_ApprovalTest {
	/*@isTest static void test_SubmitForApproval() {
		Account a = new Account();
		a.Name = 'Test';
		insert a;
		Test.StartTest();
		try {
			EB_Utility_Approval.submitForApproval(a.id, 'Test Approval');
		} catch (Exception e) {
			System.debug(e.getMessage());
		}
		Test.StopTest();
	}*/

	@isTest static void test_Approve() {
		Account a = new Account();
		a.Name = 'Test';
		insert a;
		Test.StartTest();
		try {
			EB_Utility_Approval.Approve(a.id, 'Test Approval');
		} catch (Exception e) {
			System.debug(e.getMessage());
		}
		Test.StopTest();
	}

	@isTest static void test_Reject() {
		Account a = new Account();
		a.Name = 'Test';
		insert a;
		Test.StartTest();
		try {
			EB_Utility_Approval.Reject(a.id, 'Test Approval');
		} catch (Exception e) {
			System.debug(e.getMessage());
		}
		Test.StopTest();
	}
}