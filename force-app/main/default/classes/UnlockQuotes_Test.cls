@isTest
public class UnlockQuotes_Test {
@isTest
    static void test_unlock(){
        
        Opportunity opp = (Opportunity) EB_Utility_CreateObjectForTesting.CreateObject('Opportunity');
        Opp.LID__LinkedIn_Company_Id__c = '0';
        Opp.Forecast_PercentageV2__c = 0;
        Opp.Opp_from_Lead_Conversion__c = false;
         insert opp;
        
        List<Quote> qs = new List <Quote>();
        
        Quote q = new Quote();
        q.name = 'Test Quote';
        q.OpportunityId = opp.id;
        qs.add(q);
        
        Quote q2 = new Quote();
        q2.Name = 'Test Quote 2';
        q2.OpportunityId = opp.id;
        qs.add(q2);
        
        insert qs;
         
        Approval.LockResult[] lrList = Approval.Lock(qs,false);
        
        Test.startTest();
        UnlockQuotes.RecallApproval(qs);
        // Iterate through each returned result
         /*   for(Approval.LockResult lr : qs) {
                if (lr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully unlocked quote with ID: ' + lr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : lr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Quote fields that affected this error: ' + err.getFields());
                        
                    }
                    
                }
            }*/
      
        Test.stopTest();
      
   // Verify the result 
   //System.assert (UnlockQuotes.RecallApproval(qs).);  
    }
    
    @isTest
    static void test_unlockfail(){
        
        Opportunity opp = (Opportunity) EB_Utility_CreateObjectForTesting.CreateObject('Opportunity');
        Opp.LID__LinkedIn_Company_Id__c = '0';
        Opp.Forecast_PercentageV2__c = 0;
        Opp.Opp_from_Lead_Conversion__c = false;
         insert opp;
        
        List<Quote> qs = new List <Quote>();
        
        Quote q = new Quote();
        q.name = 'Test Quote';
        q.OpportunityId = opp.id;
        qs.add(q);
        
        Quote q2 = new Quote();
        q2.Name = 'Test Quote 2';
        q2.OpportunityId = opp.id;
        qs.add(q2);
        
        insert qs;
         
       // Approval.LockResult[] lrList = Approval.Lock(qs,false);
        
        Test.startTest();
        UnlockQuotes.RecallApproval(qs);
        // Iterate through each returned result
         /*   for(Approval.LockResult lr : qs) {
                if (lr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully unlocked quote with ID: ' + lr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : lr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Quote fields that affected this error: ' + err.getFields());
                        
                    }
                    
                }
            }*/
      
        Test.stopTest();
      
   // Verify the result 
   //System.assert (UnlockQuotes.RecallApproval(qs).);  
    }
}