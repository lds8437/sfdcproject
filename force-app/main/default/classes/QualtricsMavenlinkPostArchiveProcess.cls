public class QualtricsMavenlinkPostArchiveProcess implements System.Schedulable, Database.AllowsCallouts{
    
    private static String c = 'QualtricsMavenlinkPostArchiveProcess';
    private static Integer thisThread = Math.mod(Math.round(Math.random()*1000),150);
    
    public static Integer PROJECT_UPDATE_DELAY = 20;
    public static String jobNameFormat = 'QualtricsMavenlinkPostArchiveProcess';
    private static String sOppIdGlobal = null;
    Id projId;
    Id oppId;
    String jobName;
    
    class QualtricsMavenlinkPostArchiveException extends Exception {
        public QualtricsMavenlinkPostArchiveException(String c, String m, Exception e){
                mavenlink.MavenlinkLogExternal.Write(c,m,'{'+thisThread+'}'+'Exception: '+e.getMessage()+' at line '+e.getLineNumber());
        }
    
    }
    
    public QualtricsMavenlinkPostArchiveProcess(Id projId, Id oppId, String jobName){
        this.projId = projId;
        this.oppId = oppId;
        this.jobName = jobName;
        sOppIdGlobal = String.valueOf(this.oppId);
    }
    
    public void execute(SchedulableContext context){
        String m='execute';
        
        Boolean proceed = true;
        try{
            try{
            		mavenlink.MavenlinkLogExternal.Write(c,m,'Start '+projId);
	            List<CronJobDetail> CJD =[select Id, Name from CronJobDetail Where Name like :(QualtricsMavenlinkPostArchiveProcess.jobNameFormat+ projId+'%') and Name!=:jobName];
	            mavenlink.MavenlinkLogExternal.Write(c,m,'CJD.size() '+CJD.size());
	            List<Id> CJDIds = new List<Id>();
	            for(CronJobDetail j:CJD){
	            		CJDIds.add(j.Id);
	            }
	            
	            List<CronTrigger> CTOther = [select Id, NextFireTime, CronJobDetailId from CronTrigger Where  CronJobDetailId in :CJDIds and NextFireTime > :system.now() limit 1];
	            mavenlink.MavenlinkLogExternal.Write(c,m,'CTOther.size() '+CTOther.size());
	            if(CTOther.size()>0) {
	            		proceed = false;mavenlink.MavenlinkLogExternal.Write(c,m,'Not running further Found a future job running for this project '+projId);
	            }
            }catch(System.QueryException sqe){
            		proceed=true;
            }
            //updateStatus(MLProj.mavenlink__Mavenlink_Id__c);
            if(proceed){
            		mavenlink__Mavenlink_Project__c MLProj = [select mavenlink__Mavenlink_Id__c from mavenlink__Mavenlink_Project__c where id=:projId];
            		Database.executeBatch( new QualtricsMavenlinkPostArchiveBatch(oppId, projId, 'GreyInactive',context.getTriggerId()));
            }
        }catch(Exception e){mavenlink.MavenlinkLogExternal.Write(c,m,'{'+thisThread+'}'+'Exception: '+e.getMessage()+' at line '+e.getLineNumber());
        }finally{
            mavenlink.MavenlinkLogExternal.Save();
        }
    }

    /*private static void updateStatus(String wsId){
        String m = 'updateStatus';
        
        String testWS = '{'+
    							'"count": 1,'+
    							'"results": ['+
        							'{'+
            							'"key": "workspaces",'+
            							'"id": "22185425"'+
        							'}'+
    							'],'+
    							'"workspaces": {'+
        							'"22185425": {'+
        								'"title": "Qualtrics Account - Quoted Opp",'+
        								'"status": {'+
                							'"color": "red",'+
                							'"key": 510,'+
                							'"message": "Cancelled"'+
            							'}'+
        							'}'+
    							'}'+
    						'}';	
        Map<String,Object> apiJSON = null;
        Boolean archived = false;
        String wsJSON = null;
        try{
        		if (!Test.isRunningTest()){
	        		apiJSON = mavenlink.MavenlinkExternalAPI.callAPI('/workspaces/'+wsId+'.json?include_archived=false','GET',null);
        		}else{
        			apiJSON = (Map<String,Object>) JSON.deserializeUntyped(testWS);
        		}
	        
	        mavenlink.MavenlinkLogExternal.Write(c,m,'('+sOppIdGlobal+')'+'{'+thisThread+'}'+ 'apiJSON='+apiJSON);
	        
	        Integer wsCount=apiJSON.get('count')==null?0:(Integer)apiJSON.get('count');
	        
	        if(wsCount!=1){
	            mavenlink.MavenlinkLogExternal.Write(c,m,'('+sOppIdGlobal+')'+'{'+thisThread+'}'+ 'Project was found archived');
	            archived=true;
	            wsJSON = '{"workspace":{"archived":false}}';
	            apiJSON = mavenlink.MavenlinkExternalAPI.callAPI('/workspaces/'+wsId+'.json','PUT',wsJSON);
	            mavenlink.MavenlinkLogExternal.Write(c,m,'('+sOppIdGlobal+')'+'{'+thisThread+'}'+ 'Project Unarchived');
	            
	            String wsStatusChange = '{"workspace_id":"'+wsId+'","workspace_status_change":{"to_status_key":125}}';
        			if (!Test.isRunningTest()){
        				apiJSON = mavenlink.MavenlinkExternalAPI.callAPI('/workspace_status_changes.json','POST',wsStatusChange);
        			}
			}else mavenlink.MavenlinkLogExternal.Write(c,m,'('+sOppIdGlobal+')'+'{'+thisThread+'}'+ 'Project was found not archived. Performing no action');
	    
        }catch(Exception e){
            throw new QualtricsMavenlinkPostArchiveException(c,m,e);
        }finally{
            if(archived){
                wsJSON = '{"workspace":{"archived":true}}';
                apiJSON = mavenlink.MavenlinkExternalAPI.callAPI('/workspaces/'+wsId+'.json','PUT',wsJSON);
                mavenlink.MavenlinkLogExternal.Write(c,m,'('+sOppIdGlobal+')'+'{'+thisThread+'}'+ 'Project Unarchived');
            }
        }
        
        
    }*/
    
}