@isTest
private class PanelsProjectManagerRecommendationTests {
    
    public static Opportunity panels {get; set;}
    
    @testSetup 
    public static void test_setup() {
        AVA_SFCORE__AvaTaxSettings__c avaCustomSetting = new AVA_SFCORE__AvaTaxSettings__c(Name='AvaTax', AVA_SFCORE__Enable_AvaTax_Tax_Calculation__c=true);
        Insert avaCustomSetting;
        CreateSobjectForTesting csft = new CreateSobjectForTesting();
        
        Profile p = [SELECT Id FROM Profile where Name = 'Q-Panels'];
        
        RecordType r1 = [Select Id From RecordType WHERE Name='Panels' AND SobjectType='Opportunity'];
        RecordType ar2 = [Select Id From RecordType WHERE Name ='Sales' AND SobjectType = 'Account'];
        RecordType cr2 = [Select Id From RecordType WHERE Name = 'Sales' AND SobjectType = 'Contact'];
        
        User u1 = new User(
            userroleid=null, 
            Alias = 'standt', 
            Email='standarduser@qualtrics.com', 
            EmailEncodingKey='UTF-8', 
            firstname='test', 
            LastName='1', 
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', 
            ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', 
            UserName='stanadfasdfasdfdarduser@testorg.com', 
            Project_Manager_Load__c = 12, 
            Receive_Panels_Projects__c = true,
            EIN__c = 9999,
            EmployeeID__c = 'TestU',
            FTE__c = 1,
            Sales_Level__c = 'Rep',
            Rep_Tier__c = 'AE1',
            Area__c = 'Americas AEs',
            Region__c = 'APAC',
            Sales_Team__c = 'APAC Inbound OpDev'
        );
        insert u1;
        User[] u1list = [select id from User where Email='standarduser@qualtrics.com'];
        Id u1Id = u1list.get(0).Id;
        
        User u2 = new User(
            userroleid=null, 
            Alias = 'beluga', 
            Email='belugauser@qualtrics.com', 
            EmailEncodingKey='UTF-8', 
            firstname='test', 
            LastName='1', 
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', 
            ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', 
            UserName='beluga@testorg.com', 
            Project_Manager_Load__c = 0, 
            Receive_Panels_Projects__c = true,
            EIN__c = 9999,
            EmployeeID__c = 'TestU',
            FTE__c = 1,
            Sales_Level__c = 'Rep',
            Rep_Tier__c = 'AE1',
            Area__c = 'Americas AEs',
            Region__c = 'APAC',
            Sales_Team__c = 'APAC Inbound OpDev'
        );
        insert u2;
        User[] u2list = [select id from User where Email='belugauser@qualtrics.com'];
        Id u2Id = u2list.get(0).Id;
        
        Account a = (Account)csft.CreateObject('Account');
        a.RecordTypeID = ar2.Id;
        a.OwnerId = u1Id;
        a.Name = 'Test Account';
        a.Type = 'Customer';
        a.LID__LinkedIn_Company_Id__c = null;
        a.Account_from_Lead_Conversion__c = false;
        a.Engineering_Slack_Channel__c = '#eng';
     
        insert a;
        
        // Updated by Arthur Lockrem
        Account[] acctlist = [select id from Account where Name = 'Test Account' limit 1];
       
        Contact c = (Contact)csft.CreateObject('Contact');
        c.RecordTypeID = cr2.Id;
        c.AccountId = a.id;
        c.LastName = 'LastName';
        c.Contact_Role__c = 'VP';
        c.Status__c = 'Prospect';
        c.LeadSource = 'Event';
        c.LID__LinkedIn_Company_Id__c = null;
        c.ZI_Confirmed_Correct__c = TRUE;
        c.ZI_Confirmed_Incorrect__c = FALSE;
        insert c;
        
         panels = new Opportunity();//(Opportunity)csft.CreateObject('Opportunity');
        panels.AccountId = a.id;
        panels.RecordTypeId = r1.Id;
        panels.Name = 'Beluga';
        panels.StageName = 'Commit';
        panels.ForecastCategoryName = 'Commit';
        panels.Forecast_PercentageV2__c = 0;
        panels.CloseDate = Date.today()+30;
        panels.Project_Start_Date__c = Date.today()+30;
        panels.OwnerId = u1Id;
        panels.Panel_Status__c = 'Pre-launch';
        panels.Field_Time__c = 'q Day';
        panels.Survey_Link__c = 'http';
        panels.Type = 'New Deal';
        panels.Lead_Type__c = 'Corporate';
        panels.LeadSource = 'Event';
        panels.Lead_Source_Detail__c = 'Cold Call or Email';
        panels.Panel_Type__c = 'B2B';
        panels.Panel_Demographics__c = 'Human';
        panels.N__c = 1;
        panels.LOI__c = 3;
        panels.Incidence_Rate__c = 10;
        panels.Price_per_response__c = 1;
        panels.Qualification_Contact__c = c.id;
        panels.LID__LinkedIn_Company_Id__c = null;
        panels.Opp_from_Lead_Conversion__c = false;
        panels.Ready_for_PM__c = false;
        panels.Estimated_Project_End_Date__c = date.today()+60;        
        insert panels;
       
        Panels_Project_Vendor__c ppv = (Panels_Project_Vendor__c)csft.CreateObject('Panels_Project_Vendor__c');
        ppv.Opportunity__c = panels.id;
        ppv.Price_per_Response__c = 1;
        ppv.Number_of_Responses__c = 1000;
        ppv.Total_Vendor_Quoted_Cost__c = 1000;
        ppv.Selected_for_Use__c = true;
        insert ppv;
        
        
        
    }
    
    static testMethod void RecommendationTest2() {
        Opportunity opp = [SELECT Id, Ready_for_PM__c FROM Opportunity WHERE Name =: 'Beluga'];
        
        opp.Ready_for_PM__c = true;
        update opp;
       
    }
}